<?php

/* * *
 * Creates pages for each website.
 * - register HTML pages and their associated CSS and JS files here.
 * - each website should use its own function, e.g.
 *   getDesktopPage() for desktop site.
 * * */

class PageMaker {

    static function getDesktopPage($name) {
        // Set the layout of the pages
        $page = new Page($name);
        $page->setLayout(array(
            'head' => WEBROOT . '/html/templates/HTMLhead.php',
            'header' => WEBROOT . '/html/templates/header.php',
            'content' => WEBROOT . '/html/' . $name . '.php',
            'footer' => WEBROOT . '/html/templates/footer.php',
        ));

        // Set page data
        switch ($name) {
            case '401':
                $page->setTitle('401 Unauthorized');
                $page->setCss(array('common/error.css'));
                $page->setJs(array('common/error.js'));
                break;
            case '404':
                $page->setTitle('404 Page Not Found');
                $page->setCss(array('common/error.css'));
                $page->setJs(array('common/error.js'));
                break;
            case '500':
                $page->setTitle('500 Internal Server Error');
                $page->setCss(array('common/error.css'));
                $page->setJs(array('common/error.js'));
                break;
            case 'home':
                $page->setTitle('Home | WL Builder');
                $page->setMenuTitle('Home');
                $page->setCss(array('home.css'));
                $page->setJs(array('home.js'));
                break;
            case 'project':
                $page->setTitle('Projects | WL Builder');
                $page->setMenuTitle('Project');
                $page->setCss(array('project.css'));
                $page->setJs(array('project.js'));
                break;
            case 'project-map':
                $page->setTitle('Project Overview | WL Builder');
                $page->setMenuTitle('Projects Map');
                $page->setCss(array('project-map.css'));
                $page->setJs(array('project-map.js'));
                break;
            case 'profile':
                $page->setTitle('Profile | WL Builder');
                $page->setMenuTitle('Profile');
                $page->setCss(array('profile.css'));
                $page->setJs(array('profile.js'));
                break;
            case 'capabilities':
                $page->setTitle('Capabilities | WL Builder');
                $page->setMenuTitle('Capabilities');
                $page->setCss(array('capabilities.css'));
                $page->setJs(array('capabilities.js'));
                break;
            case 'contact':
                $page->setTitle('Contact | WL Builder');
                $page->setMenuTitle('Contact');
                $page->setCss('contact.css');
                $page->setJs(array('contact.js'));
                break;
            default:
                throw new Exception('Error in PageMaker::getDesktopPage(): page requested is not defined: ' . $name);
                break;
        } //switch
       
        return $page;
    }

// getDesktopPage()

    static function getStaffPage($name) {

        $page = new Page($name);
        $page->setLayout(array(
            'head' => WEBROOT . '/html/templates/staff-HTMLhead.php',
            'header' => WEBROOT . '/html/templates/staff-header.php',
            'content' => WEBROOT . '/html/' . $name . '.php',
            'footer' => WEBROOT . '/html/templates/staff-footer.php',
        ));
        $page->setAccessLevel(GENERAL_STAFF_ACCESS_LEVEL);

        switch ($name) {
            case 'staff-login':
                $page->setTitle('Login | WL Builder');
                $page->setCss(array('staff-login.css'));
                $page->setJs(array('staff-login.js'));
                $page->setAccessLevel(DEFAULT_ACCESS_LEVEL);
                break;

            case 'staff-dashboard':
                $page->setTitle('Dashboard');
                $page->setCss(array('staff-dashboard.css'));
                $page->setJs('staff-dashboard.js');
                break;

            case 'staff-account-settings':
                $page->setTitle('Account Settings');
                $page->setCss('staff-account-settings.css');
                $page->setJs(array('staff-account-settings.js'));
                break;

            case 'staff-testimonial':
                $page->setTitle('Testimonial');
                $page->setCss('staff-testimonial.css');
                $page->setJs(array('staff-testimonial.js'));
                break;

            case 'staff-capabilities':
                $page->setTitle('Capabilities');
                $page->setCss(array('staff-capabilities.css'));
                $page->setJs('staff-capabilities.js');
                break;

            case 'staff-project':
                $page->setTitle('Project');
                $page->setCss(array('staff-project.css'));
                $page->setJs('staff-project.js');
                break;

            case 'staff-project-basic-info':
                $page->setTitle('Project Basic Info');
                $page->setCss(array('staff-project-basic-info.css', 'staff-project.css'));
                $page->setJs(array('common/staff-project-sub-menu.js', 'staff-project-basic-info.js'));
                break;

            case 'staff-project-feature':
                $page->setTitle('Project Feature');
                $page->setCss(array('staff-project-feature.css', 'staff-project.css','bootstrap-googleapis-font.css','bootstrap-googleapis-icon.css'));
                $page->setJs(array('common/staff-project-sub-menu.js', 'staff-project-feature.js'));
                break;


            case 'staff-project-blue-print':
                $page->setTitle('Project Blue Prints');
                $page->setCss(array('staff-project-blue-print.css', 'staff-project.css'));
                $page->setJs(array('common/staff-project-sub-menu.js', 'staff-project-blue-print.js'));
                break;

            case 'staff-project-image':
                $page->setTitle('Project Image');
                $page->setCss(array('staff-project-image.css', 'staff-project.css'));
                $page->setJs(array('common/staff-project-sub-menu.js', 'staff-project-image.js'));
                break;

            case 'staff-fabric-color':
                $page->setTitle('Feature');
                $page->setCss(array('staff-fabric-color.css', 'staff-fabric-properties.css'));
                $page->setJs(array('common/staff-fabric-properties-sub-menu.js', 'staff-fabric-color.js'));
                break;



            //    case 'staff-fabric-properties':
            // 	$page->setTitle('Fabric');
            // 	$page->setCss(array('staff-fabric-properties.css'));
            // 	$page->setJs('staff-fabric-properties.js');
            // 	break;
            // case 'staff-fabric-care':
            // 	$page->setTitle('Fabric Care');
            // 	$page->setCss(array('staff-fabric-care.css', 'staff-fabric-properties.css'));
            // 	$page->setJs(array('common/staff-fabric-properties-sub-menu.js', 'staff-fabric-care.js'));
            // 	break;
            // case 'staff-fabric-color':
            // 	$page->setTitle('Fabric Color');
            // 	$page->setCss(array('staff-fabric-color.css', 'staff-fabric-properties.css'));
            // 	$page->setJs(array('common/staff-fabric-properties-sub-menu.js', 'staff-fabric-color.js'));
            // 	break;






            case 'staff-image-library':
                $page->setTitle('Image Library');
                $page->setCss(array('staff-image-library.css'));
                $page->setJs(array('vendor/hammer-2.0.2.min.js', 'staff-image-library.js'));
                break;

            case 'staff-themes':
                $page->setTitle('Change theme');
                $page->setCss(array('staff-themes.css'));
                $page->setJs('staff-themes.js');
                break;

            case 'staff-image-library':
                $page->setTitle('Image Library');
                $page->setCss(array('staff-image-library.css'));
                $page->setJs(array('vendor/hammer-2.0.2.min.js', 'staff-image-library.js'));
                break;

            case 'staff-customer':
                $page->setTitle('Customer Account Information');
                $page->setCss(array('staff-customer.css'));
                $page->setJs('staff-customer.js');
                break;




            case 'staff-staff':
                $page->setTitle('Staff');
                $page->setCss(array('staff-staff.css'));
                $page->setJs('staff-staff.js');
                break;
	    

            default:
                throw new Exception('Error in PageMaker::getStaffPage(): page requested is not defined: ' . $name);
                break;
        } //switch

        return $page;
    }

//getStaffPage()
}

// class PageMaker
