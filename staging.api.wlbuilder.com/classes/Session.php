<?php
/***
 * Helps manage sessions
 * - any default session values should be set in start()
 ***/

class Session {
	//Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Session class: method '.$name.'() does not exist');
	} //call()
	
	/***
	 * Starts or resumes session, and inits default session variables.
	 * Must be called prior to accessing $_SESSION array
	 ***/
	static function start() {
		// Init new session if necessary
		ini_set('session.gc_maxlifetime', SESSION_DURATION); // minimally make sessions last the entire session duration
		ini_set('session.cookie_lifetime', 0); // don't store cookies on user's computer after browser closed
		ini_set('session.cookie_httponly', 1); // use HTTP only cookies
		session_name(SESSION_NAME);
		session_start();
		
		// Expire Session if necessary
		$time = time(); // don't need fine-grain level of microtime
		if (isset($_SESSION['LAST_ACTIVITY']) && ($time - $_SESSION['LAST_ACTIVITY'] > SESSION_DURATION)) {
			// Restart a new session
			self::stop();
			session_start();
			session_regenerate_id();
		}
		$_SESSION['LAST_ACTIVITY'] = $time;
		
		// Set deafult session user
		if (!isset($_SESSION['user'])) {
			$_SESSION['user'] = new Customer( array(
				'id' => 0,
				'firstName' => 'Guest',
				'lastName' => 'User',
				'email' => 'guest@estebartin.com',
				'accessLevel' => 0
			));
		}
	} //start()
	
	/***
	 * Ends session, destroying all $_SESSION variables
	 ***/ 
	static function stop() {
		// Destroy session, along with any variables used in this session
		session_destroy();
		$_SESSION = array();
	
		// Reset session cookie
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 86400,
			$params['path'], $params['domain'],
			$params['secure'], $params['httponly']
		);
	} //stop()
	
	/***
	 * Checks if a session has started
	 ***/
	static function hasStarted() {
		return (session_id() != '');
	} //hasStarted()
	
	/***
	 * Regenerates a session ID. Useful for preventing session fixation
	 ***/
	static function regenerateID() {
		session_regenerate_id();
	} //regenerateID()
	
	/***
	 * Checks request's CSRF token against server session's CSRF token
	 * @return: TRUE on match, FALSE otherwise
	 ***/
	static function checkCSRFToken() {
		// TODO: remove once dev ends
		return TRUE;
		
		if (!isset($_SERVER['HTTP_CSRF_TOKEN'])) {
			return FALSE;
		}
		return ($_SESSION['CSRF_TOKEN'] == $_SERVER['HTTP_CSRF_TOKEN']);

		/*
		$request_headers = apache_request_headers(); //may return as either underscore or dash
		if ($_SESSION['CSRF_TOKEN'] != $request_headers['CSRF_TOKEN'] &&
			$_SESSION['CSRF_TOKEN'] != $request_headers['CSRF-TOKEN']) {
			return FALSE;
		}
		return TRUE;
		*/
	} //checkCSRFToken()
} // class Session
