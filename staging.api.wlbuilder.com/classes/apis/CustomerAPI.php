<?php
class CustomerAPI {
	/***
	 * Attempts to set the customer as the session user.
	 * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
	 * @input: $_POST['email', 'password', 'persistent']
	 * @output: Customer {id, firstName, lastName, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime...}
	 *          'persistentLoginCookie' (optional)
	 ***/
	
	function login($response) {
		// Data checks
		if (!isValidEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		if (strlen($_POST['password']) < MIN_PASSWORD_LENGTH) {
			$response->addData('error', 'Wrong password.');
			return 400;
		}
		
		$time = getTimeInMs();

		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Find this user in db
			$user = $db->readSingle('Customer', array(
				array('email', '=', $_POST['email'])
			), TRUE);
			if (!isset($user)) {
				$response->addData('error', 'Invalid email address.');
				return 401;
			} // No such user
			
			// Check if account is banned
			if ($user->isBanned()) {
				$response->addData('error', 'Account banned.');
				return 403;
			}
			
			// Check if account is email verified
			if (!$user->isEmailVerified()) {
				$response->addData('error', 'Account not verified.');
				return 403;
			}
	
			// Check password
			if (!$user->checkPassword($_POST['password'])) {
				$response->addData('error', 'Invalid password.');
				return 401;
			} // Wrong password
			
			// Generate persistent cookie if requested
			if ($_POST['persistent']) {
				$cookie = $user->generatePersistentLoginCookie();
				$response->addData('persistentLoginCookie', $cookie);
			}
			
			// Update user
			$user->update(array(
				'active' => TRUE,
				'lastUpdateTime' => $time,
				'lastActivityTime' => $time // can't use the class static method because not session user
			));
			if (!$db->update($user)) {
				Log::error(__METHOD__.'() - DB Unable to update login status for ' . $user . '. Persistent: '.$_POST['persistent'].'. Cookie: '.$cookie);
				$response->addData('error', 'Unable to store login status.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $user->getID(),
				'userType' => 'Customer',
				'action' => 'Logged In',
				'createdTime' => $time,
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' login"');
			}
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Set session user
		$_SESSION['user'] = $user;
		Session::regenerateID(); //remember to regenerate ID to prevent session fixation

		// Yay!
		$response->addData('customer', $user->toOutputArray());
		return 200;
	} //login()
	
	/***
	 * Attempts to set the customer as the session user using a persistent cookie.
	 * A new persistent cookie will be issued.
	 * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
	 * @input: $_POST['email', 'persistentLoginCookie']
	 * @output: Customer {id, firstName, lastName, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime...}
	 *          'persistentLoginCookie'
	 ***/
	function persistentLogin($response) {
		// Data checks
		if (!isValidEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		
		$time = getTimeInMs();

		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Find this user in db
			$user = $db->readSingle('Customer', array(
				array('email', '=', $_POST['email'])
			), TRUE);
			if (!isset($user)) {
				$response->addData('error', 'Invalid email address.');
				return 401;
			} // No such user
			
			// Check if account is banned
			if ($user->isBanned()) {
				$response->addData('error', 'Account banned.');
				return 403;
			}
			
			// Check if account is email verified
			if (!$user->isEmailVerified()) {
				$response->addData('error', 'Account not verified.');
				return 403;
			}
			
			// Check and renew cookie (which will check cookie validity)
			$new_cookie = $user->renewPersistentLoginCookie($_POST['persistentLoginCookie']);
			if (!$new_cookie) {
				$response->addData('error', 'Invalid cookie.');
				return 401;
			}
			$response->addData('persistentLoginCookie', $new_cookie);
			
			// Update user
			$user->update(array(
				'active' => TRUE,
				'lastUpdateTime' => $time,
				'lastActivityTime' => $time
			));
			if (!$db->update($user)) {
				Log::error(__METHOD__.'() - DB Unable to update login status for ' . $user . '. Persistent: '.$_POST['persistent'].'. Cookie: '.$cookie);
				$response->addData('error', 'Unable to process persistent login.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $user->getID(),
				'userType' => 'Customer',
				'action' => 'Logged In',
				'createdTime' => $time,
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $_SESSION['user'] . ' login (persistent)"');
			}
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Set session user
		$_SESSION['user'] = $user;
		Session::regenerateID(); //remember to regenerate ID to prevent session fixation

		// Yay!
		$response->addData('customer', $user->toOutputArray());
		return 200;
	} //persistentLogin()

	/***
	 * Logout a customer
	 * Any existing Session ID is cleared and a new session will be started
	 * @input: none
	 * @output: none
	 ***/
	function logout($response) {
		// Ensure action is legal - check that the user is indeed a logged in customer
		if (get_class($_SESSION['user']) != 'Customer' || $_SESSION['user']->getAccessLevel() == 0) {
			$response->addData('error', 'No session customer');
			return 400;
		}
		
		$time = getTimeInMs();
		$userID = $_SESSION['user']->getID();

		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Find this user in db
			$user = $db->readSingle('Customer', array(
				array('id', '=', $userID)
			), TRUE);
			if (!isset($user)) {
				Log::fatal(__METHOD__.'() - '.$_SESSION['user']. ' not found in DB.');
				$response->addData('error', 'User not found in DB.');
				return 500;
			} // No such user
			
			// Update active status - may fail in the extremely unlikely event that two requests come at the exact same time.
			$updated = $user->update(array(
				'active' => FALSE,
				'lastUpdateTime' => $time,
				'lastActivityTime' => $time
			));
			if ($updated && !$db->update($user)) {
				Log::error(__METHOD__.'() - DB Unable to logout ' . $user);
				$response->addData('error', 'Unable to logout user.');
				return 500;
			}
		
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $_SESSION['user']->getID(),
				'userType' => 'Customer',
				'action' => 'Logged Out',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $userID . ' logout"');
			}
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed

		// Restart this session with new session ID
		Session::stop();
		Session::start();
		Session::regenerateID();

		// YAY!
		return 200;
	} //logout()

	/***
	 * Sign up as a new customer.
	 * - On success, a verification code email will be sent to user.
	 * @input: $_POST['email, referrerID]
	 * @output: none
	 */
	function register($response) {
		// Data checks
		if (!isValidEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		
		$time = getTimeInMs();
		
		// Generate email verification code
		$code = Encryptor::generateRandomUpperCaseAlphanumericCode(EMAIL_VERIFICATION_CODE_LENGTH);
		$hash = Encryptor::getHash($code, $time);
		
		// Create user		
		$user = new Customer(array(
			'passwordHash' => NULL,
			'email' => $_POST['email'],
			'emailVerified' => FALSE,
			'emailVerificationHash' => $hash,
			'referrerID' => $_POST['referrerID'],
			'createdTime' => $time,
			'lastUpdateTime' => $time,
			'lastActivityTime' => $time // can't use the class static method because not session user
		));
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Check that this email is not in use
			$existing_user = $db->readSingle('Customer', array(
				array('email', '=', $_POST['email'])
			), TRUE);
			if (isset($existing_user) && $existing_user->isEmailVerified()) {
				$response->addData('error', 'This email is already in use.');
				return 400;
			}
			
			// Create user
			if (!isset($existing_user)) {
				if (!$db->create($user)) {
					Log::error(__METHOD__.'() - DB unable to create user.');
					$response->addData('error', 'DB unable to create user.');
					return 500;
				}
				
				// Create and store this activity
				$activity = new Activity(array(
					'userID' => $user->getID(),
					'userType' => 'Customer',
					'action' => 'Registered',
					'createdTime' => $time
				));
				if (!$db->create($activity)) {
					Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' register"');
				}
			} // Customer does not exist
			else {
				$existing_user->update(array(
					'passwordHash' => NULL,
					'emailVerificationHash' => $hash,
					'lastUpdateTime' => $time,
					'lastActivityTime' => $time // can't use the class static method because not session user
				));
				if (!$db->update($existing_user)) {
					Log::error(__METHOD__.'() - DB unable to update user.');
					$response->addData('error', 'DB unable to update user.');
					return 500;
				}
				
				// Create and store this activity
				$activity = new Activity(array(
					'userID' => $existing_user->getID(),
					'userType' => 'Customer',
					'action' => 'Registered (retry)',
					'createdTime' => $time
				));
				if (!$db->create($activity)) {
					Log::error(__METHOD__.'() - DB Unable to create activity "' . $existing_user . ' register (retry)"');
				}
			} // Customer already exists (but not verified)
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Send email
		$mail = new Mail();
		$mail->setSubject('Este Bartin New Account Sign-Up Request');
		$mail->setTo($_POST['email']);
		$mail->setFrom(NO_REPLY_EMAIL);
		$mail->setReplyTo(WEBMASTER_EMAIL);
		$mail->addHTMLMail(APPROOT.'/mails/customer-sign-up.html');
		$mail->addTextMail(APPROOT.'/mails/customer-sign-up.txt');
		$mail->setVariable(array (
			'name' => $user->getName(),
			'email' => $_POST['email'],
			'code' => $code,
		));
		if (!$mail->send()) {
			Log::error(__METHOD__.'() - unable to send mail to '.$user.' ('.$_POST['email'].')');
			$response->addData('error', 'Acccount successfully created but unable to send out new sign up mail!');
			return 500;
		}

		// Yay!
		return 200;
	} //register()
	
	/***
	 * Sends a new email verification code to customer.
	 * - must be an account with unverified email
	 * @input: $_POST['email']
	 * @output: none
	 */
	function resendEmailVerificationCode($response) {
		// Data checks
		if (!isValidEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		
		$time = getTimeInMs();
		
		// Generate email verification code
		$code = Encryptor::generateRandomUpperCaseAlphanumericCode(EMAIL_VERIFICATION_CODE_LENGTH);
		$hash = Encryptor::getHash($code, $time);
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Check that this email exists
			$user = $db->readSingle('Customer', array(
				array('email', '=', $_POST['email'])
			), FALSE);
			if (!isset($user)) {
				$response->addData('error', 'Email not found.');
				return 404;
			}
			if ($user->isEmailVerified()) {
				$response->addData('error', 'Email already verified.');
				return 400;
			}
			
			// Update user
			$updated = $user->update(array(
				'emailVerified' => FALSE,
				'emailVerificationHash' => $hash,
				'lastUpdateTime' => $time,
				'lastActivityTime' => $time // can't use the class static method because not session user
			));
			if (!$updated) {
				Log::error(__METHOD__.'() - Unable to update emailVerificationHash for '.$user);
				$response->addData('error', 'Unable to update user.');
				return 500;
			}
			if (!$db->update($user)) {
				Log::error(__METHOD__.'() - DB unable to update '.$user);
				$response->addData('error', 'DB unable to update user.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $user->getID(),
				'userType' => 'Customer',
				'action' => 'Resend Verification Code',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' Resend Verification Code"');
			}
		
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Send email
		$mail = new Mail();
		$mail->setSubject('New Verification Code Request');
		$mail->setTo($_POST['email']);
		$mail->setFrom(NO_REPLY_EMAIL);
		$mail->setReplyTo(WEBMASTER_EMAIL);
		$mail->addHTMLMail(APPROOT.'/mails/customer-new-verification-code.html');
		$mail->addTextMail(APPROOT.'/mails/customer-new-verification-code.txt');
		$mail->setVariable(array (
			'name' => $user->getName(),
			'email' => $_POST['email'],
			'code' => $code,
		));
		if (!$mail->send()) {
			Log::error(__METHOD__.'() - unable to send mail to '.$user.' ('.$_POST['email'].')');
			$response->addData('error', 'Acccount successfully created but unable to send out new sign up mail!');
			return 500;
		}

		// Yay!
		return 200;
	} //resendVerificationCode()
	
	/***
	 * Verify email with a 6-char code.
	 * - On success, customer's password will become NULL and he would be able to continue on with set password (either during sign up / forget password).
	 * - On success, the referrer will have his referralCount increased (and potentially trigger credit rewards).
	 * @input: $_POST['email', 'code']
	 * @output: none
	 */
	function verifyEmail($response) {
		// Data checks
		if (!isValidEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		if (strlen($_POST['code']) != EMAIL_VERIFICATION_CODE_LENGTH) {
			$response->addData('error', 'Invalid code.');
			return 400;
		}
		
		$time = getTimeInMs();
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Read this user
			$user = $db->readSingle('Customer', array(
				array('email', '=', $_POST['email'])
			), TRUE);
			if (!isset($user)) {
				$response->addData('error', 'User not found.');
				return 400;
			}
			
			// Check if account is banned
			if ($user->isBanned()) {
				$response->addData('error', 'Account banned.');
				return 403;
			}
			
			// Check email verification code
			if (!$user->checkEmailVerificationCode($_POST['code'])) {
				$response->addData('error', 'Wrong/expired code.');
				return 401;
			}
			
			// Update user
			$updated = $user->update(array(
				'emailVerified' => TRUE,
				'emailVerificationHash' => NULL,
				'passwordHash' => NULL,
				'lastUpdateTime' => $time,
				'lastActivityTime' => $time // can't use the class static method because not session user
			));
			if (!$updated) {
				Log::error(__METHOD__.'() - Unable to update emailVerified to TRUE for '.$user);
				$response->addData('error', 'Unable to update user.');
				return 500;
			}
			if (!$db->update($user)) {
				Log::error(__METHOD__.'() - DB unable to update '.$user);
				$response->addData('error', 'DB unable to update user.');
				return 500;
			}
			
			// Check if this account has a referrer we need to add credits to
			$referrerID = $user->getReferrerID();
			if (isset($referrerID)) {
				InternalAPI::incrementReferralCount($time, $referrerID);
			} //has referrer
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $user->getID(),
				'userType' => 'Customer',
				'action' => 'Verified Email',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' verified email"');
			}
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Yay!
		return 200;
	} //verifyEmail()
	
	/***
	 * Sets password of an account. Can only be performed when password is NULL (after verifyEmail)
	 * @input: $_POST['email', 'password']
	 * @output: none
	 */
	function setPassword($response) {
		// Data checks
		if (!isValidEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email ' . $_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		if (strlen($_POST['password']) < MIN_PASSWORD_LENGTH) {
			$response->addData('error', 'Password must be at least '.MIN_PASSWORD_LENGTH.' characters.');
			return 400;
		}
		
		$hash = Encryptor::getHash($_POST['password']);
		$time = getTimeInMs();
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Read this user
			$user = $db->readSingle('Customer', array(
				array('email', '=', $_POST['email'])
			), TRUE);
			if (!isset($user)) {
				$response->addData('error', 'User not found.');
				return 400;
			}
			
			// Check if account is banned
			if ($user->isBanned()) {
				$response->addData('error', 'Account banned.');
				return 403;
			}
			
			// Check if account is email verified
			if (!$user->isEmailVerified()) {
				$response->addData('error', 'Account not verified.');
				return 403;
			}
			
			// Check that user has never set password before
			$existingPasswordHash = $user->getPasswordHash();
			if (isset($existingPasswordHash)) {
				$response->addData('error', 'Password has already been set.');
				return 403;
			}
			
			// Update user's passwordHash
			$updated = $user->update(array(
				'passwordHash' => $hash,
				'lastUpdateTime' => $time,
				'lastActivityTime' => $time, // can't use the class static method because not session user
			));
			if (!$updated) {
				Log::error(__METHOD__.'() - Unable to update passwordHash for '.$user);
				$response->addData('error', 'Unable to update user.');
				return 500;
			}
			if (!$db->update($user)) {
				Log::error(__METHOD__.'() - DB unable to update '.$user);
				$response->addData('error', 'DB unable to update user.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $user->getID(),
				'userType' => 'Customer',
				'action' => 'Set Password',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' set password"');
			}
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Yay!
		return 200;
	} //setPassword()
	
	/***
	 * Customer forgot password. Sends a verification code to his registered email.
	 * @input: $_POST['email']
	 * @output: none
	 ***/
	function forgetPassword($response) {
		// Data checks
		if (!isValidEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email.');
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		
		// Generate new email verification code
		$code = Encryptor::generateRandomUpperCaseAlphanumericCode(EMAIL_VERIFICATION_CODE_LENGTH);
		$time = getTimeInMs();
		$hash = Encryptor::getHash($code, $time);
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Find this user in DB
			$user = $db->readSingle('Customer', array(
				array('email', '=', $_POST['email'])
			), TRUE);
			if (!isset($user)) {
				$response->addData('error', 'No such customer.');
				return 404;
			}
			
			// Check if account is banned
			if ($user->isBanned()) {
				$response->addData('error', 'Account banned.');
				return 403;
			}
			
			// Update user with NULL password
			$updated = $user->update(array(
				'emailVerified' => FALSE,
				'emailVerificationHash' => $hash,
				'lastUpdateTime' => $time,
				'lastActivityTime' => $time, // can't use the class static method because not session user
			));
			if (!$updated) {
				Log::error(__METHOD__.'() - unable to update '.$user.' with new email verification hash.');
				$response->addData('error', 'Unable to remove old password.');
				return 500;
			}
			if (!$db->update($user)) {
				Log::error(__METHOD__.'() - DB unable to update '.$user.' with new email verification hash.');
				$response->addData('error', 'DB Unable to update user.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $user->getID(),
				'userType' => 'Customer',
				'action' => 'Forget Password',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' Forget password"');
			}
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Send email
		$mail = new Mail();
		$mail->setSubject('Este Bartin Password Reset Request');
		$mail->setTo($_POST['email']);
		$mail->setFrom(NO_REPLY_EMAIL);
		$mail->setReplyTo(WEBMASTER_EMAIL);
		$mail->addHTMLMail(APPROOT.'/mails/customer-forget-password.html');
		$mail->addTextMail(APPROOT.'/mails/customer-forget-password.txt');
		$mail->setVariable(array (
			'name' => $user->getName(),
			'email' => $_POST['email'],
			'code' => $code,
		));
		if (!$mail->send()) {
			Log::error(__METHOD__.'() - unable to send mail to '.$user.' ('.$_POST['email'].')');
			$response->addData('error', 'Unable to send out new verification code mail!');
			return 500;
		}
		
		// YAY!
		return 200;
	} //forgetPassword()
	
	/***
	 * Updates profile info of the session customer
	 * - must be logged in
	 * @input: $_POST['firstName, lastName, profession, company, residenceCountryID... , homeCountryID... , officeCountryID... ]
	 * @output: Customer {id, firstName, lastName, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime...}
	 */
	function editProfile($response) {
		// Ensure action is legal - check that the user is indeed a customer
		if (get_class($_SESSION['user']) != 'Customer' || $_SESSION['user']->getAccessLevel() == 0) {
			$response->addData('error', 'No session customer');
			return 400;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}
		
		$time = getTimeInMs();
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Find this user in DB
			$user = $db->readSingle('Customer', array(
				array('id', '=', $_SESSION['user']->getID())
			), TRUE);
			if (!isset($user)) {
				$response->addData('error', 'No such customer.');
				return 404;
			}
			
			// Update user
			try {
				$updated = $user->update(array(
					'firstName' => $_POST['firstName'],
					'lastName' => $_POST['lastName'],
					'profession' => $_POST['profession'],
					'company' => $_POST['company'],
					'residenceCountryID' => $_POST['residenceCountryID'],
					'residenceStateID' => $_POST['residenceStateID'],
					'residencePhone' => $_POST['residencePhone'],
					'homeCountryID' => $_POST['homeCountryID'],
					'homeStateID' => $_POST['homeStateID'],
					'homePostalCode' => $_POST['homePostalCode'],
					'homeAddress' => $_POST['homeAddress'],
					'homePhone' => $_POST['homePhone'],
					'officeCountryID' => $_POST['officeCountryID'],
					'officeStateID' => $_POST['officeStateID'],
					'officePostalCode' => $_POST['officePostalCode'],
					'officeAddress' => $_POST['officeAddress'],
					'officePhone' => $_POST['officePhone'],
					'birthTime' => $_POST['birthTime'],
					'lastUpdateTime' => $time,
					'lastActivityTime' => $time, // use this instead of the class static method to save 1 x UPDATE query
				));
			}
			catch (Exception $e) {
				$response->addData('error', $e->getMessage());
				return 400;
			}
			if (!$updated) {
				Log::error(__METHOD__.'() - unable to update '.$user.' profile.');
				$response->addData('error', 'Unable to update profile.');
				return 500;
			}
			if (!$db->update($user)) {
				Log::error(__METHOD__.'() - DB unable to update '.$user.' profile.');
				$response->addData('error', 'DB Unable to update profile.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $user->getID(),
				'userType' => 'Customer',
				'action' => 'Edit Profile',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' Edit Profile"');
			}
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Update session user
		$_SESSION['user'] = $user;
		
		// YAY!
		$response->addData('customer', $user->toOutputArray());
		return 200;
	} //editProfile()
	
	/***
	 * Changes password of session customer
	 * - must be logged in
	 * @input: $_POST['oldPassword, newPassword']
	 * @output: Customer {id, firstName, lastName, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime...}
	 */
	function changePassword($response) {
		// Ensure action is legal - check that the user is indeed a customer
		if (get_class($_SESSION['user']) != 'Customer' || $_SESSION['user']->getAccessLevel() == 0) {
			$response->addData('error', 'No session customer');
			return 400;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}
		
		// Data checks
		if (strlen($_POST['oldPassword']) < MIN_PASSWORD_LENGTH) {
			$response->addData('error', 'Wrong password.');
			return 401;
		}
		if (strlen($_POST['newPassword']) < MIN_PASSWORD_LENGTH) {
			$response->addData('error', 'Password must be at least '.MIN_PASSWORD_LENGTH.' characters.');
			return 400;
		}
		
		// Check password
		if (!$_SESSION['user']->checkPassword($_POST['oldPassword'])) {
			$response->addData('error', 'Wrong password.');
			return 401;
		}
		
		$hash = Encryptor::getHash($_POST['newPassword']);
		$time = getTimeInMs();
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Find this user in DB
			$user = $db->readSingle('Customer', array(
				array('id', '=', $_SESSION['user']->getID())
			), TRUE);
			if (!isset($user)) {
				$response->addData('error', 'No such customer.');
				return 404;
			}
			
			// Update user
			try {
				$updated = $user->update(array(
					'passwordHash' => $hash,
					'lastUpdateTime' => $time,
					'lastActivityTime' => $time, // use this instead of the class static method to save 1 x UPDATE query
				));
			}
			catch (Exception $e) {
				$response->addData('error', $e->getMessage());
				return 400;
			}
			if (!$updated) {
				Log::error(__METHOD__.'() - unable to update '.$user.' passwordHash.');
				$response->addData('error', 'Unable to update new password.');
				return 500;
			}
			
			// Update him in DB
			if (!$db->update($user)) {
				Log::error(__METHOD__.'() - DB unable to update '.$user.' passwordHash.');
				$response->addData('error', 'DB Unable to update new password.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $user->getID(),
				'userType' => 'Customer',
				'action' => 'Change Password',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $user . ' change password"');
			}
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// Update session user
		$_SESSION['user'] = $user;
		
		// YAY!
		$response->addData('customer', $user->toOutputArray());
		return 200;
	} //changePassword()
	
	/***
	 * Checks if this customer's email is in the Customer mail list
	 * - Must be logged in
	 * @input: none
	 * @output: subscribed (bool)
	 ***/
	function getMailListSubscriptionStatus($response) {
		// Ensure action is legal - check that the user is indeed a customer
		if (get_class($_SESSION['user']) != 'Customer' || $_SESSION['user']->getAccessLevel() == 0) {
			$response->addData('error', 'No session customer');
			return 400;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}
		
		$email = $_SESSION['user']->getEmail();
		
		// Check if this mail list entry exists
		$db = Database::getConnection();
		$entry = $db->readSingle('MailListEntry', array(
			array('email','=',$email),
			array('list','=','Customer')
		), FALSE);
		$status = isset($entry);
			
		$response->addData('subscribed', $status);
		return 200;
	} //getMailListSubscriptionStatus()
	
	/***
	 * Adds this customer's email to Customer mail list.
	 * - Must be logged in
	 * @input: none
	 * @output: none
	 ***/
	function subscribeMailList($response) {
		// Ensure action is legal - check that the user is indeed a customer
		if (get_class($_SESSION['user']) != 'Customer' || $_SESSION['user']->getAccessLevel() == 0) {
			$response->addData('error', 'No session customer');
			return 400;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}
		
		$time = getTimeInMs();
		
		// Create MailListEntry - can be done outside transaction as we don't need persistence (yet)
		$email = $_SESSION['user']->getEmail();
		$entry = new MailListEntry(array(
			'email'=>$email,
			'list'=>'Customer',
			'createdTime'=>$time
		));
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Check if this mail list entry already exists
			$existing_entry = $db->readSingle('MailListEntry', array(
				array('email','=',$email),
				array('list','=','Customer')
			), FALSE);
			if (isset($existing_entry)) {
				$response->addData('info', 'Already in the list');
				return 202;
			}
			
			// Create new mail list entry
			if (!$db->create($entry)) {
				Log::error(__METHOD__.'() - unable to create MailListEntry: ['.$email.', Customer].');
				$response->addData('error', 'Unable to create new mail list entry.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $_SESSION['user']->getID(),
				'userType' => 'Customer',
				'action' => 'Subscribe Mail List',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $_SESSION['user'] . ' Subscribe Mail List"');
			}
			CustomerAPI::storeLastActivityTime($time, 'Subscribe Mail List');
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// YAY
		return 200;
	} //subscribeMailList()
	
	/***
	 * Remove this customer from Customer mail list.
	 * - Must be logged in
	 * @input: none
	 * @output: none
	 ***/
	function unsubscribeMailList($response) {
		// Ensure action is legal - check that the user is indeed a customer
		if (get_class($_SESSION['user']) != 'Customer' || $_SESSION['user']->getAccessLevel() == 0) {
			$response->addData('error', 'No session customer');
			return 400;
		}
		
		// Check CSRF
		if (!Session::checkCSRFToken()) {
			Log::warning(__METHOD__.'() - CSRF Detected on '.$_SESSION['user']);
			$response->setData(array('error' => 'CSRF detected'));
			return 401;
		}
		
		$time = getTimeInMs();
		
		// Cache variables
		$email = $_SESSION['user']->getEmail();
		$userID = $_SESSION['user']->getID();
		
		// Transaction for ACID-ity
		$db = Database::getConnection();
		$committed = FALSE;
		while (!$committed) {
			// Begin DB transaction
			if (!$db->beginTransaction()) {
				Log::fatal(__METHOD__.'() - DB unable to begin transaction');
				$response->addData('error', 'DB unable to begin transaction.');
				return 500;
			}
			
			// Check if this mail list entry already exists
			$existing_entry = $db->readSingle('MailListEntry', array(
				array('email', '=', $email),
				array('list','=','Customer')
			), FALSE);
			if (!isset($existing_entry)) {
				$response->addData('info', 'Already not in the list');
				return 202;
			}
			
			// Delete it
			if (!$db->delete($existing_entry)) {
				Log::error(__METHOD__.'() - unable to delete MailListEntry: ['.$email.', Customer].');
				$response->addData('error', 'Unable to delete mail list entry.');
				return 500;
			}
			
			// Create and store this activity
			$activity = new Activity(array(
				'userID' => $userID,
				'userType' => 'Customer',
				'action' => 'Unsubscribe Mail List',
				'createdTime' => $time
			));
			if (!$db->create($activity)) {
				Log::error(__METHOD__.'() - DB Unable to create activity "' . $_SESSION['user'] . ' Unsubscribe Mail List"');
			}
			CustomerAPI::storeLastActivityTime($time, 'Unsubscribe Mail List');
			
			// Commit transaction
			$committed = $db->commit();
			if (!$committed) {
				Log::warning(__METHOD__.'() - DB unable to commit transaction');
				if (!$db->rollback()) {
					Log::fatal(__METHOD__.'() - DB unable to rollback transaction');
					$response->addData('error', 'DB unable to commit transaction');
					return 500;
				}
				
				$time = getTimeInMs();
			}
		} // while not committed
		
		// YAY
		return 200;
	} //unsubscribeMailList()
	
	
	/***
	 * Stores last activity time onto this session customer in DB.
	 * - Note: should be done in the context of a transaction.
	 * - Note: should be done only if API function doesn't store customer.
	 * - Note: must have a session customer
	 * @param: time (int), activity name (string)
	 * @return: none
	 ***/
	private static function storeLastActivityTime($time, $activity_name = '') {
		$db = Database::getConnection();
		
		$query = 'UPDATE WB_Customer SET lastActivityTime = '.$time.' WHERE id = '.$_SESSION['user']->getID().' LIMIT 1';
		if (!$db->query($query)) {
			Log::error(__METHOD__.'() - unable to update last activity time of ' . $_SESSION['user'] . ' to ' . $time . ' (Activity: ' . $activity_name . ')');
		}
	} //storeLastActivityTime()
} //class CustomerAPI
