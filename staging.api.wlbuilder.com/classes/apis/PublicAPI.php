<?php
class PublicAPI {
	
	/***
	 * Gets the current session user
	 * @input: none
	 * @output: User {id, name, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime}
	 ***/
	function getSessionUser($response) {
		$response->addData('user', $_SESSION['user']->toOutputArray());
		return 200;
	} //getSessionUser()
        
        /***
	 * Gets all partners in the system that have been updated since a certain time.
	 * @input: $_GET['fromTime']
	 * @output: Array of partners('id', 'email', 'name', 'url', 'description','websitURL', 'createdTime', 'lastUpdateTime','displayPriority')
	 ***/
	function getUpdatedPartners($response) {
		// Param check
		if (!isUnsignedInt($_GET['fromTime'])) {
			$response->addData('error', 'illegal value for fromTime: '.$_GET['fromTime']);
			return 400;
		}
		
		// Read partners from DB
		$db = Database::getConnection();
                
		$partners = $db->readMultiple('Partner', array(
			array('lastUpdateTime', '>=', $_GET['fromTime'])
		), 0, 0, NULL, 'DESC', FALSE);
                
		//print_r($partners);die;
		if (!isset($partners)) {
			$response->addData('WLpartners', array());
			return 202;
		}
                
		// Package into array data
		$data = array();
                $i = 0;
		foreach ($partners as $partner) {
                    $data[] = $partner->toOutputArray();
                    $imageFile = WEBROOT . '/img/partners/' . $partner->getID() . '.png';
                    
                    if(file_exists($imageFile)){
                        $data[$i]['hasImage'] = 1;
                    }else{
                        $data[$i]['hasImage'] = 0;
                    }
                    $i++;
    		}
		
		// YAY
		$response->addData('WLpartners', $data);
		return 200;
	} //getUpdatedCountries()
        
        /***
	 * Gets a list of Testimonials. The highest priority ones will be returned.
	 * @input: $_GET['count']
	 * @output: array of Testimonials
	 ***/
	function getTestimonials($response) {
		// Param check
                if (!isUnsignedInt($_GET['count'])) {
			$response->addData('error', 'illegal value for count: '.$_GET['count']);
			return 400;
		}
		
		// Read testimonial from DB
		$db = Database::getConnection();
		$testimonials = $db->readMultiple('Testimonial', array(
		), 0, $_GET['count'], 'displayPriority', 'DESC', FALSE);
		
		// Handle empty cases
		if (!is_array($testimonials)) {
			$response->addData('WLtestimonials', array());
			return 202;
		}
		
		// Make each testimonial into array
		$data = array();
                $i = 0;
		foreach ($testimonials as $testimonial) {
                    $data[] = $testimonial->toOutputArray();
                    $imageFile = WEBROOT . '/img/testimonials/' . $testimonial->getID() . '.png';
                    if(file_exists($imageFile)){
                        $data[$i]['hasImage'] = 1;
                    }else{
                        $data[$i]['hasImage'] = 0;
                    }
                    $i++;
		}

		// YAY
		$response->addData('WLtestimonials', $data);
		return 200;
	} //getTestimonials()
        
        /***
	 * Gets a list of Projects. The highest priority ones will be returned.
	 * @input: $_GET['fromTime']
	 * @output: array of Project basic info
	 ***/
	function getUpdatedProjects($response) {
		// Param check
		if (!isUnsignedInt($_GET['fromTime'])) {
			$response->addData('error', 'illegal value for fromTime: '.$_GET['fromTime']);
			return 400;
		}
		
		// Read testimonials from DB
		$db = Database::getConnection();
                
                $projects = $db->readMultiple('Project', array(
			array('lastUpdateTime', '>=', $_GET['fromTime']),
                        array('displayPriority', '>', 0)
		), 0, 0, NULL, 'Desc', FALSE);
                
		// Handle empty cases
		if (!is_array($projects)) {
			$response->addData('WLprojects', array());
			return 202;
		}
		
		// Make each testimonial into array
		$data = array();
		foreach ($projects as $project) {
			$data[] = $project->toOutputArray();
		}

		// YAY
		$response->addData('WLprojects', $data);
		return 200;
	} //getProject()
        
        
        /***
	 * Gets a list of Project Images. The highest priority ones will be returned.
	 * @input: $_GET['fromTime']
	 * @output: array of Project images
	 ***/
	function getUpdatedProjectImages($response) {
		// Param check
		if (!isUnsignedInt($_GET['fromTime'])) {
			$response->addData('error', 'illegal value for fromTime: '.$_GET['fromTime']);
			return 400;
		}
		
		// Read testimonials from DB
		$db = Database::getConnection();
                $projectImages = $db->readMultiple('ProjectImage', array(
			array('lastUpdateTime', '>=', $_GET['fromTime'])
		), 0, 0, 'displayPriority', 'DESC', FALSE);
		
		
		// Handle empty cases
		if (!is_array($projectImages)) {
			$response->addData('WLprojectImages', array());
			return 202;
		}
		
		// Make each testimonial into array
		$data = array();
		foreach ($projectImages as $projectImg) {
			$data[] = $projectImg->toOutputArray();
		}
                
		// YAY
		$response->addData('WLprojectImages', $data);
		return 200;
	} //getProjectImages()
        
        /***
	 * Gets a list of Project Blue prints. The highest priority ones will be returned.
	 * @input: $_GET['fromTime']
	 * @output: array of Project blue print
	 ***/
	function getUpdatedProjectBluePrints($response) {
		// Read testimonials from DB
		$db = Database::getConnection();
		$projectBluePrints = $db->readMultiple('ProjectBluePrint', array(
			array('projectID', '>', 0)
		), 0, 0, NULL, 'ASC', FALSE);
		
		// Handle empty cases
		if (!is_array($projectBluePrints)) {
			$response->addData('WLprojectBluePrints', array());
			return 202;
		}
                
		
		// Make each testimonial into array
		$data = array();
		foreach ($projectBluePrints as $bluePrint) {
			$data[] = $bluePrint->toOutputArray();
                }
                
                
		// YAY
		$response->addData('WLprojectBluePrints', $data);
                
		return 200;
	} //getProjectImages()
	
	/***
	 * Gets all countries in the system that have been updated since a certain time.
	 * @input: $_GET['fromTime']
	 * @output: Array of countries('id', 'code', 'name', 'enabled', 'createdTime', 'lastUpdateTime')
	 ***/
	function getUpdatedCountries($response) {
		// Param check
		if (!isUnsignedInt($_GET['fromTime'])) {
			$response->addData('error', 'illegal value for fromTime: '.$_GET['fromTime']);
			return 400;
		}
		
		// Read countries from DB
		$db = Database::getConnection();
		$countries = $db->readMultiple('Country', array(
			array('lastUpdateTime', '>=', $_GET['fromTime'])
		), 0, 0, NULL, 'ASC', FALSE);
		
		if (!isset($countries)) {
			$response->addData('countries', array());
			return 202;
		}
		
		// Package into array data
		$data = array();
		foreach ($countries as $country) {
			$data[] = $country->toOutputArray();
		}
		
		// YAY
		$response->addData('countries', $data);
		return 200;
	} //getUpdatedCountries()
        
       
	
	/***
	 * Gets all countries in the system that have been updated since a certain time.
	 * @input: $_GET['fromTime']
	 * @output: Array of states('id', 'code', 'name', 'enabled', 'createdTime', 'lastUpdateTime')
	 ***/
	function getUpdatedStates($response) {
		// Param check
		if (!isUnsignedInt($_GET['fromTime'])) {
			$response->addData('error', 'illegal value for fromTime: '.$_GET['fromTime']);
			return 400;
		}
		
		// Read states from DB
		$db = Database::getConnection();
		$states = $db->readMultiple('State', array(
			array('lastUpdateTime', '>=', $_GET['fromTime'])
		), 0, 0, NULL, 'ASC', FALSE);
		
		if (!isset($states)) {
			$response->addData('states', array());
			return 202;
		}
		
		// Package into array data
		$data = array();
		foreach ($states as $state) {
			$data[] = $state->toOutputArray();
		}
		
		// YAY
		$response->addData('states', $data);
		return 200;
	} //getUpdatedStates()

	
	/***
	 * Sends email to website contact person
	 * @input: $_POST['name', 'email', 'type', 'message']
	 * @output: message
	 ***/
	function sendContactMessage($response) {
		// Data check: name
		if (!isset($_POST['name']) || strlen(trim($_POST['name'])) < MIN_NAME_LENGTH) {
			$response->addData('error', 'Name must be at least '.MIN_NAME_LENGTH.' characters.');
			return 400;
		}
		
		// Data check: email
		if (!isValidEmail($_POST['email'])) {
			$response->addData('error', 'Invalid email: '.$_POST['email']);
			return 400;
		}
		else {
			$_POST['email'] = strtolower($_POST['email']);
		}
		
		// Data check: type
		if (!isset($_POST['type']) || ($_POST['type'] != 'Make Appointment' && $_POST['type'] != 'Feedback' && $_POST['type'] != 'Order Enquiry')) {
			$response->addData('error', 'Unknown type: '.$_POST['type']);
			return 400;
		}
		
		// Data check: message
		if (!isset($_POST['message']) || strlen(trim($_POST['message'])) == 0) {
			$response->addData('error', 'Message cannot be empty.');
			return 400;
		}
		
		// Create email
		$mail = new Mail();
		$mail->setSubject('Message From Website Contact Form');
		$mail->setTo(CONTACT_EMAIL);
		$mail->setFrom($_POST['email']);
		$mail->setReplyTo($_POST['email']);
		$mail->addHTMLMail(APPROOT.'/mails/contact-message.html');
		$mail->addTextMail(APPROOT.'/mails/contact-message.txt');
		$mail->setVariable(array (
			'name' => htmlspecialchars($_POST['name']),
			'email' => $_POST['email'],
			'type' => htmlspecialchars($_POST['type']),
			'message' => nl2br(htmlspecialchars($_POST['message']))
		));
		
		// Send email
		if (!$mail->send()) {
			Log::error('CustomerAPI::sendContactMessage() - unable to send out email to '.$_POST['email']);
			$response->addData('error', 'Unable to send out new contact message email!');
			return 500;
		}
		
		// YAY
		return 200;
	} //sendContactMessage()

} //class PublicAPI
