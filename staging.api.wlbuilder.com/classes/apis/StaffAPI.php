<?php

class StaffAPI {

    /**
     * Attempts to set the staff as the session user.
     * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
     * @input: $_POST['email', 'password', 'persistent']
     * @output: Staff {id, name, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime}
     *          'persistentLoginCookie' (optional)
     */
    function login($response) {


        // Data checks
        if (!isValidEmail($_POST['email'])) {
            $response->addData('error', 'Invalid email ' . $_POST['email']);
            return 400;
        } else {
            $_POST['email'] = strtolower($_POST['email']);
        }
        if (strlen($_POST['password']) < MIN_PASSWORD_LENGTH) {
            $response->addData('error', 'Wrong password.');
            return 400;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in db
            $user = $db->readSingle('Staff', array(
                array('email', '=', $_POST['email'])
                    ), TRUE);

            if (!isset($user)) {
                $response->addData('error', 'Invalid email address.');
                return 401;
            } // No such user
            // Check if account is banned
            if ($user->isBanned()) {
                $response->addData('error', 'Account banned.');
                return 403;
            }

            // Check password
            if (!$user->checkPassword($_POST['password'])) {
                $response->addData('error', 'Invalid password.');
                return 401;
            } // Wrong password
            // Generate persistent cookie if requested
            if ($_POST['persistent']) {
                $cookie = $user->generatePersistentLoginCookie();
                $response->addData('persistentLoginCookie', $cookie);
            }

            //Update user
            $user->update(array(
                'active' => TRUE,
                'lastUpdateTime' => $time,
                'lastActivityTime' => $time
            ));


            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB Unable to update login status for ' . $user . '. Persistent: ' . $_POST['persistent'] . '. Cookie: ' . $cookie);
                $response->addData('error', 'Unable to store login status.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $user->getID(),
                'userType' => 'Staff',
                'action' => 'Logged In',
                'createdTime' => $time,
            ));

            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' login"');
            } else {
                Log::error(__METHOD__ . '() - DB created to create activity "' . $user . ' login"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // Set session user
        $_SESSION['user'] = $user;

        Session::regenerateID(); //remember to regenerate ID to prevent session fixation
        // Yay!
        $response->addData('staff', $user->toOutputArray());
        return 200;
    }

//login()

    /**
     * Attempts to set the staff as the session user using a persistent cookie.
     * A new persistent cookie will be issued.
     * The Session ID will be passed to the browser in the background, and will be used to identify this session for as long as the browser is open.
     * @input: $_POST['email', 'cookie']
     * @output: Staff {id, name, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime}
     *          'persistentLoginCookie'
     */
    function persistentLogin($response) {
        // Data checks
        if (!isValidEmail($_POST['email'])) {
            $response->addData('error', 'Invalid email ' . $_POST['email']);
            return 400;
        } else {
            $_POST['email'] = strtolower($_POST['email']);
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in db
            $user = $db->readSingle('Staff', array(
                array('email', '=', $_POST['email'])
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'Invalid email address.');
                return 401;
            } // No such user
            // Check if account is banned
            if ($user->isBanned()) {
                $response->addData('error', 'Account banned.');
                return 403;
            }

            // Check and renew cookie (which will check cookie validity)
            $new_cookie = $user->renewPersistentLoginCookie($_POST['persistentLoginCookie']);
            if (!$new_cookie) {
                $response->addData('error', 'Invalid cookie.');
                return 401;
            }
            $response->addData('persistentLoginCookie', $new_cookie);

            // Update user
            $user->update(array(
                'active' => TRUE,
                'lastUpdateTime' => $time,
                'lastActivityTime' => $time
            ));
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB Unable to update login status for ' . $user . '. Persistent: ' . $_POST['persistent'] . '. Cookie: ' . $cookie);
                $response->addData('error', 'Unable to process persistent login.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $user->getID(),
                'userType' => 'Staff',
                'action' => 'Logged In (P)',
                'createdTime' => $time
            ));



            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' login (persistent)"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // Set session user
        $_SESSION['user'] = $user;


        Session::regenerateID(); //remember to regenerate ID to prevent session fixation
        // Yay!
        $response->addData('staff', $user->toOutputArray());
        return 200;
    }

//persistentLogin()

    /**
     * Logout a staff
     * Any existing Session ID is cleared and a new session will be started
     * @input: none
     * @output: message
     */
    function logout($response) {
        // Ensure action is legal - check that the user is indeed a logged in staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Logged Out',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user']->getID() . ' logout"');
            }
            StaffAPI::storeLastActivityTime($time, 'logout');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // Restart this session with new session ID
        Session::stop();
        Session::start();
        Session::regenerateID();

        // YAY!
        return 200;
    }

//logout()

    /**
     * Edits the session's staff profile.
     * - nust be logged in
     * @input: $_POST['firstName, lastName, email']
     * @output: Staff {id, firstName, lastName, email, createdTime, lastUpdateTime, lastActivtyTime, accessLevel, banned , language}
     */
    function editProfile($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Cache variables
        $userID = $_SESSION['user']->getID();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in db
            $user = $db->readSingle('Staff', array(
                array('id', '=', $userID)
                    ), TRUE);

            if (!isset($user)) {
                $response->addData('error', 'Unable to find user in DB.');
                return 500;
            } // No such user
            // Update staff (may throw exceptions if input data invalid)
            try {
                $updated = $user->update(array(
                    'firstName' => $_POST['firstName'],
                    'phoneNumber' => $_POST['phoneNumber'],
                    'email' => $_POST['email'],
                    'lastUpdateTime' => $time,
                    'lastActivityTime' => $time, // use this instead of the class static method to save 1 x UPDATE query
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }

            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' profile.');
                $response->addData('error', 'Unable to update new profile.');
                return 500;
            }
            // Store session staff to DB

            if (!$db->update($user)) {
//                Log::error(__METHOD__ . '() - unable to update ' . $user . ' in DB');
//                $response->addData('error', 'DB Unable to update new profile');
//                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $userID,
                'userType' => 'Staff',
                'action' => 'Edited Profile',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' edited profile');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // Set session user
        $_SESSION['user'] = $user;

        // YAY!				
        $response->addData('staff', $user->toOutputArray());
        return 200;
    }

//editProfile()

    /**
     * Changes password of session staff
     * - must be logged in
     * @input: $_POST['oldPassword, newPassword']
     * @output: Staff {id, firstName, lastName, email, userType, accessLevel, banned, language, createdTime, lastUpdateTime, lastActivityTime...}
     */
    function changePassword($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Data checks
        if (strlen($_POST['oldPassword']) < MIN_PASSWORD_LENGTH) {
            $response->addData('error', 'Wrong password.');
            return 401;
        }
        if (strlen($_POST['newPassword']) < MIN_PASSWORD_LENGTH) {
            $response->addData('error', 'Password must be at least ' . MIN_PASSWORD_LENGTH . ' characters.');
            return 400;
        }

        // Check password
        if (!$_SESSION['user']->checkPassword($_POST['oldPassword'])) {
            $response->addData('error', 'Wrong password.');
            return 401;
        }

        $time = getTimeInMs();

        // Cache variables
        $hash = Encryptor::getHash($_POST['newPassword']);
        $userID = $_SESSION['user']->getID();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            if (!isset($_POST['newPassword']) || strlen($_POST['newPassword']) < MIN_PASSWORD_LENGTH) {
                $response->addData('error', 'Password NOT changed: your password must be at least ' . MIN_PASSWORD_LENGTH . ' characters long.');
                return 400;
            }

            // Find this user in DB
            $user = $db->readSingle('Staff', array(
                array('id', '=', $userID)
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'No such staff.');
                return 404;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'passwordHash' => $hash,
                    'lastUpdateTime' => $time,
                    'lastActivityTime' => $time, // use this instead of the class static method to save 1 x UPDATE query
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' profile.');
                $response->addData('error', 'Unable to update new profile.');
                return 500;
            }

            // Update him in DB
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' profile.');
                $response->addData('error', 'DB Unable to update new profile.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $userID,
                'userType' => 'Staff',
                'action' => 'Change Password',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $user . ' change password"');
            }

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // Update session user
        $_SESSION['user'] = $user;

        return 200;
    }

//changePassword()

    /**
     * Gets a list of activiteies by Staff users within a certain time frame.
     * - nust be logged in
     * - Note: if 'toTime' is not specified, curent time will be used.
     * @input: $_GET['fromTime', 'toTime']
     * @output: array of Activities {id, createdTime, userID, userType, action, modelID, modelType}
     */
    function getStaffActivities($response) {
        // print_r($_GET);
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Data check
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'Illegal fromTime');
            return 400;
        }
        if (!isUnsignedInt($_GET['toTime'])) {
            $_GET['toTime'] = getTimeInMs();
        }


        // Init return data
        $data = array();


        //Read activities from DB
        $db = Database::getConnection();
        $activities = $db->readMultiple('Activity', array(
            array('userType', '=', 'Staff'),
            array('createdTime', '>=', $_GET['fromTime']),
            array('createdTime', '<', $_GET['toTime'])
                ), 0, 0, 'createdTime', 'ASC', FALSE);



        // Handle empty cases
        if (!is_array($activities)) {
            $response->addData('activities', array());
            return 202;
        }

        // Make each activity into array
        foreach ($activities as $activity) {
            $data[] = $activity->toOutputArray();
        }

        // YAY
        $response->addData('activities', $data);
        return 200;
    }

//getStaffActivities()

    /*     * ***********
     * Customers *
     * *********** */

    /**
     * Adds a customer to the system. All customers added are automatically verified.
     * @input: $_POST['email, password, firstName, lastName, profession]
     * @output: Customer
     */
    function addCustomer($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Data checks
        if (strlen($_POST['password']) < MIN_PASSWORD_LENGTH) {
            $response->addData('error', 'Password must be at least ' . MIN_PASSWORD_LENGTH . ' characters.');
            return 400;
        }

        $time = getTimeInMs();

        // Generate hashes
        $emailVerificationCode = Encryptor::generateRandomUpperCaseAlphanumericCode(EMAIL_VERIFICATION_CODE_LENGTH);
        $emailVerificationHash = Encryptor::getHash($emailVerificationCode, $time);
        $passwordHash = Encryptor::getHash($_POST['password']);

        // Create user - can be done outside transaction as we don't need persistence (yet)
        try {
            $user = new Customer(array(
                'firstName' => $_POST['firstName'],
                'lastName' => $_POST['lastName'],
                'profession' => $_POST['profession'],
                'email' => $_POST['email'],
                'passwordHash' => $passwordHash,
                'emailVerified' => TRUE,
                'emailVerificationHash' => $emailVerificationHash,
                'createdTime' => $time,
                'lastUpdateTime' => $time,
                'lastActivityTime' => $time
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Check that this email is not in use
            $existing_user = $db->readSingle('Customer', array(
                array('email', '=', $_POST['email'])
                    ), TRUE);
            if (isset($existing_user)) {
                if ($existing_user->isEmailVerified()) {
                    $response->addData('error', 'This email is already in use.');
                    return 400;
                }

                // Delete the unverified user from DB
                if (!$db->delete($existing_user)) {
                    Log::error(__METHOD__ . '() - DB unable to delete unverified ' . $existing_user);
                    $response->addData('error', 'DB unable to delete unverified customer with same email ' . $_POST['email']);
                    return 500;
                }
            }

            // Create user in DB
            if (!$db->create($user)) {
                Log::error(__METHOD__ . '() - DB unable to create customer ' . $_POST['email']);
                $response->addData('error', 'DB unable to create customer.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Added Customer',
                'modelID' => $user->getID(),
                'modelType' => 'Customer',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' added customer"');
            }
            StaffAPI::storeLastActivityTime($time, 'added customer');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('customer', $user->toOutputArray());
        return 200;
    }

//addCustomer()

    /**
     * Gets a list of Customers that have been updated since a certain time
     * - Note: must have a session staff
     * @input : $_GET['fromTime']
     * @output: array of Customers
     */
    function getUpdatedCustomers($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Read customers from DB
        $db = Database::getConnection();
        $customers = $db->readMultiple('Customer', array(
            array('lastUpdateTime', '>=', $_GET['fromTime'])
                ), 0, 0, 'lastUpdateTime', 'ASC', FALSE);

        // Handle empty cases
        if (!is_array($customers)) {
            $response->addData('customers', array());
            return 202;
        }

        // Make each customer into array
        $data = array();
        foreach ($customers as $user) {
            $data[] = $user->toOutputArray();
        }

        // YAY
        $response->addData('customers', $data);
        return 200;
    }

//getUpdatedCustomers()

    /**
     * Updates a customer's profile.
     * - Also used to save home and office addresses to this customer's profile.
     * @input: $_POST[id, email, firstName, lastName, profession, company, residenceCountryID... , homeCountryID... , officeCountryID...]
     * @output: Customer
     */
    function editCustomerProfile($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('Customer', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'No such customer.');
                return 404;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'email' => $_POST['email'],
                    'firstName' => $_POST['firstName'],
                    'lastName' => $_POST['lastName'],
                    'profession' => $_POST['profession'],
                    'company' => $_POST['company'],
                    'residenceCountryID' => $_POST['residenceCountryID'],
                    'residenceStateID' => $_POST['residenceStateID'],
                    'residencePhone' => $_POST['residencePhone'],
                    'homeCountryID' => $_POST['homeCountryID'],
                    'homeStateID' => $_POST['homeStateID'],
                    'homePostalCode' => $_POST['homePostalCode'],
                    'homeAddress' => $_POST['homeAddress'],
                    'homePhone' => $_POST['homePhone'],
                    'officeCountryID' => $_POST['officeCountryID'],
                    'officeStateID' => $_POST['officeStateID'],
                    'officePostalCode' => $_POST['officePostalCode'],
                    'officeAddress' => $_POST['officeAddress'],
                    'officePhone' => $_POST['officePhone'],
                    'birthTime' => $_POST['birthTime'],
                    'lastUpdateTime' => $time,
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' profile.');
                $response->addData('error', 'Unable to update customer.');
                return 500;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' profile.');
                $response->addData('error', 'DB Unable to update customer.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Customer Profile',
                'modelID' => $user->getID(),
                'modelType' => 'Customer',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' Edited Customer"');
            }
            StaffAPI::storeLastActivityTime($time, 'edited customer');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('customer', $user->toOutputArray());
        return 200;
    }

//editCustomerProfile()

    /**
     * Adds an email to a mail list.
     * - Must be logged in
     * @input: $_POST['email, list']
     * @output: none
     */
    function addMailListEntry($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Create MailListEntry - can be done outside transaction as we don't need persistence (yet)
        $entry = new MailListEntry(array(
            'email' => $_POST['email'],
            'list' => $_POST['list'],
            'createdTime' => $time
        ));

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Check if this mail list entry already exists
            $existing_entry = $db->readSingle('MailListEntry', array(
                array('email', '=', $_POST['email']),
                array('list', '=', $_POST['list'])
                    ), FALSE);
            if (isset($existing_entry)) {
                $response->addData('info', 'Already in the list');
                return 202;
            }

            // Create new mail list entry
            if (!$db->create($entry)) {
                Log::error(__METHOD__ . '() - unable to create MailListEntry: [' . $_POST['email'] . ', ' . $_POST['list'] . '].');
                $response->addData('error', 'Unable to create new mail list entry.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Added Mail List Entry',
                'modelID' => $entry->getID(),
                'modelType' => 'MailListEntry',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' Added Mail List Entry"');
            }
            StaffAPI::storeLastActivityTime($time, 'Added Mail List Entry');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('mailListEntry', $entry->toOutputArray());
        return 200;
    }

//addMailListEntry()

    /**
     * Checks if a particular email exists in a particular list
     * - Must be logged in
     * @input: $_POST['email', 'list']
     * @output: subscribed (bool)
     */
    function getMailListSubscriptionStatus($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Check if this mail list entry exists
        $db = Database::getConnection();
        $entry = $db->readSingle('MailListEntry', array(
            array('email', '=', $_POST['email']),
            array('list', '=', $_POST['list'])
                ), FALSE);
        $status = isset($entry);

        $response->addData('subscribed', $status);
        return 200;
    }

//getMailListSubscriptionStatus()

    /**
     * Removes a particular mail list entry
     * - Must be logged in
     * @input: $_POST['email, list']
     * @output: none
     */
    function removeMailListEntry($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Check if this mail list entry already exists
            $existing_entry = $db->readSingle('MailListEntry', array(
                array('email', '=', $_POST['email']),
                array('list', '=', $_POST['list']),
                    ), FALSE);
            if (!isset($existing_entry)) {
                $response->addData('error', 'No such entry');
                return 202;
            }

            // Create new mail list entry
            if (!$db->delete($existing_entry)) {
                Log::error(__METHOD__ . '() - unable to delete ' . $existing_entry);
                $response->addData('error', 'Unable to delete ' . $existing_entry);
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Removed Mail List Entry',
                'modelID' => $existing_entry->getID(),
                'modelType' => 'MailListEntry',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' Removed Mail List Entry"');
            }
            StaffAPI::storeLastActivityTime($time, 'Removed Mail List Entry');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        return 200;
    }

//removeMailListEntry()

    /**
     * Updates a customer's credits.
     * @input: $_POST[id, credits]
     * @output: Customer
     */
    function editCustomerCredits($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Data checks
        if (!isUnsignedInt($_POST['credits'])) {
            $response->addData('error', 'Error value for credits: ' . $_POST['credits']);
            return 400;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('Customer', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'No such customer.');
                return 404;
            }

            // Create CreditActivity
            $credits_to_add = $_POST['credits'] - $user->getCredits();
            $creditActivity = new CreditActivity(array(
                'customerID' => $_POST['id'],
                'amount' => $credits_to_add,
                'description' => 'Updated by Staff',
                'createdTime' => $time
            ));
            if (!$db->create($creditActivity)) {
                Log::error(__METHOD__ . '() - DB unable to create credit activity for Customer ' . $_POST['id']);
                return FALSE;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'credits' => $_POST['credits'],
                    'lastUpdateTime' => $time,
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' profile.');
                $response->addData('error', 'Unable to update customer.');
                return 500;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' profile.');
                $response->addData('error', 'DB Unable to update customer.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Customer Credits',
                'modelID' => $user->getID(),
                'modelType' => 'Customer',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' Edited Customer Credits"');
            }
            StaffAPI::storeLastActivityTime($time, 'edited customer credits');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('customer', $user->toOutputArray());
        return 200;
    }

//editCustomerCredits()

    /**
     * Updates a customer's referrerID.
     * - If an old referrer exists, his referralCount will be decreased.
     * - The new referrer's referralCount will be increased.
     * @input: $_POST[id, referrerID]
     * @output: Customer
     */
    function editCustomerReferrerID($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Cehck pararm
        if (!isset($_POST['referrerID'])) {
            $response->addData('error', 'referrerID not set.');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('Customer', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'No such customer.');
                return 404;
            }

            // Read old (bad) referrer
            $badReferrerID = $user->getReferrerID();
            if (isset($badReferrerID)) {
                if ($badReferrerID == $_POST['referrerID']) {
                    $response->addData('customer', $user->toOutputArray());
                    return 202;
                } // nothing real to do

                if (!InternalAPI::decrementReferralCount($time, $badReferrerID)) {
                    Log::error(__METHOD__ . '() - Unable to decrement referral count for Customer ' . $badReferrerID);
                    $response->addData('error', 'DB Unable to decrement referral count for old referring Customer.');
                    return 500;
                }
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'referrerID' => $_POST['referrerID'],
                    'lastUpdateTime' => $time,
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' referrerID.');
                $response->addData('error', 'Unable to update customer.');
                return 500;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' referrerID.');
                $response->addData('error', 'DB Unable to update customer.');
                return 500;
            }

            // Update referrer's referral count
            if (!InternalAPI::incrementReferralCount($time, $_POST['referrerID'])) {
                Log::error(__METHOD__ . '() - Unable to increment referral count for Customer ' . $_POST['referrerID']);
                $response->addData('error', 'DB Unable to increment referral count for Customer.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Customer ReferrerID',
                'modelID' => $user->getID(),
                'modelType' => 'Customer',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' Edited Customer ReferrerID"');
            }
            StaffAPI::storeLastActivityTime($time, 'edited customer ReferrerID');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('customer', $user->toOutputArray());
        return 200;
    }

//editCustomerReferrerID()

    /**
     * Bans a customer.
     * @input: $_POST[id]
     * @output: Customer
     */
    function banCustomer($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('Customer', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'No such customer.');
                return 404;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'banned' => TRUE,
                    'lastUpdateTime' => $time,
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' to banned.');
                $response->addData('error', 'Unable to update customer to banned.');
                return 500;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' to banned.');
                $response->addData('error', 'DB Unable to update customer to banned.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Banned Customer',
                'modelID' => $user->getID(),
                'modelType' => 'Customer',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' Banned Customer"');
            }
            StaffAPI::storeLastActivityTime($time, 'banned customer');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('customer', $user->toOutputArray());
        return 200;
    }

//banCustomer()

    /**
     * Unbans a customer.
     * @input: $_POST[id]
     * @output: Customer
     */
    function unbanCustomer($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('Customer', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'No such customer.');
                return 404;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'banned' => FALSE,
                    'lastUpdateTime' => $time,
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user . ' to unbanned.');
                $response->addData('error', 'Unable to update customer to unbanned.');
                return 500;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user . ' to unbanned.');
                $response->addData('error', 'DB Unable to update customer to unbanned.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Unbanned Customer',
                'modelID' => $user->getID(),
                'modelType' => 'Customer',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' Unbanned Customer"');
            }
            StaffAPI::storeLastActivityTime($time, 'unbanned customer');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('customer', $user->toOutputArray());
        return 200;
    }

//unbanCustomer()

    /*     * ***********
     *   Staffs  *
     * *********** */

    /**
     * Adds staff to system
     * - must be logged in as Admin
     * @input: $_POST['email, password, firstName, lastName, accessLevel']
     * @output: Staff
     */
    function addStaff($response) {
        // Ensure action is legal - check that the user is indeed a staff with admin rights
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }
        if ($_SESSION['user']->getAccessLevel() < ADMIN_ACCESS_LEVEL) {
            $response->addData('error', 'Insufficient privilege');
            return 403;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Generate hash ahead of transaction
        $passwordHash = Encryptor::getHash($_POST['password']);

        // Create user - can be done outside transaction as we don't need persistence (yet)
        try {
            $user = new Staff(array(
                'firstName' => $_POST['firstName'],
                'lastName' => $_POST['lastName'],
                'email' => $_POST['email'],
                'passwordHash' => $passwordHash,
                'accessLevel' => $_POST['accessLevel'],
                'createdTime' => $time,
                'lastUpdateTime' => $time,
                'lastActivityTime' => $time
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Check that this email is not in use
            $existing_user = $db->readSingle('Staff', array(
                array('email', '=', $_POST['email'])
                    ), TRUE);
            if (isset($existing_user)) {
                $response->addData('error', 'This email is already in use.');
                return 400;
            }

            // Create user in DB
            if (!$db->create($user)) {
                Log::error(__METHOD__ . '() - DB unable to create staff ' . $_POST['email']);
                $response->addData('error', 'DB unable to create staff.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Added Staff',
                'modelID' => $user->getID(),
                'modelType' => 'Staff',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' added staff"');
            }
            StaffAPI::storeLastActivityTime($time, 'added staff');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('staff', $user->toOutputArray());
        return 200;
    }

//addStaff()

    /**
     * Gets a list of Staffs that have been updated since a certain time
     * - Note: must have a session staff
     * @input : $_GET['fromTime']
     * @output: array of staffs {id, name, email, createdTime, lastUpdateTime, lastActivtyTime, accessLevel, banned , language}
     */
    function getUpdatedStaffs($response) {
        // Ensure action is legal - check that the user is indeed a staff
        // if (get_class($_SESSION['user']) != 'Staff') {
        // 	$response->addData('error', 'No session staff');
        // 	return 400;
        // }
        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        //Read staffs from DB
        $db = Database::getConnection();

        $staffs = $db->readMultiple('Staff', array(
            array('lastUpdateTime', '>=', $_GET['fromTime'])
                ), 0, 0, 'lastUpdateTime', 'ASC', FALSE);

        // Handle empty cases
        if (!is_array($staffs)) {
            /* $response->addData('staffs', array()); */
            return 202;
        }

        // Make each staff into array
        $data = array();
        foreach ($staffs as $user) {
            $data[] = $user->toOutputArray();
        }

        // YAY
        $response->addData('staffs', $data);
        return 200;
    }

//getUpdatedStaffs()

    /**
     * Edits a staff user
     * - must be logged in as Admin
     * @input: $_POST['id, email, password, firstName, lastName, accessLevel']
     * @output: Staff
     */
    function editStaff($response) {
        // Ensure action is legal - check that the user is indeed a staff with admin rights
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }
        if ($_SESSION['user']->getAccessLevel() < ADMIN_ACCESS_LEVEL) {
            $response->addData('error', 'Insufficient privilege');
            return 403;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        $time = getTimeInMs();

        // Generate hash ahead of transaction
        $passwordHash = Encryptor::getHash($_POST['password']);

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find this user in DB
            $user = $db->readSingle('Staff', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($user)) {
                $response->addData('error', 'No such staff.');
                return 404;
            }

            // Update user
            try {
                $updated = $user->update(array(
                    'email' => $_POST['email'],
                    'passwordHash' => $passwordHash,
                    'accessLevel' => $_POST['accessLevel'],
                    'firstName' => $_POST['firstName'],
                    'lastName' => $_POST['lastName'],
                    'lastUpdateTime' => $time,
                ));
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }
            if (!$updated) {
                Log::error(__METHOD__ . '() - unable to update ' . $user);
                $response->addData('error', 'Unable to update staff.');
                return 500;
            }
            if (!$db->update($user)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $user);
                $response->addData('error', 'DB Unable to update staff.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Staff',
                'modelID' => $user->getID(),
                'modelType' => 'Staff',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' Edited Staff"');
            }
            StaffAPI::storeLastActivityTime($time, 'edited staff');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY
        $response->addData('staff', $user->toOutputArray());
        return 200;
    }

//editStaff()




//editFabricWeave()

    /*     * *
     * Gets activities by a customer that have been created from a certain time.
     * - Note: if 'toTime' is not specified, curent time will be used.
     * @input: $_GET['customerID, fromTime, toTime']
     * @output: Array of activities
     * * */

    function getCustomerActivities($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Param check
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'illegal value for fromTime: ' . $_GET['fromTime']);
            return 400;
        }
        if (!isUnsignedInt($_GET['toTime'])) {
            $_GET['toTime'] = getTimeInMs();
        }

        // Read models from DB
        $db = Database::getConnection();
        $activities = $db->readMultiple('Activity', array(
            array('userID', '=', $_GET['customerID']),
            array('userType', '=', 'Customer'),
            array('lastUpdateTime', '>=', $_GET['fromTime']),
            array('lastUpdateTime', '<', $_GET['toTime'])
                ), 0, 0, NULL, 'ASC', FALSE);

        if (!isset($activities)) {
            $response->addData('activities', array());
            return 202;
        }

        // Package into array data
        $data = array();
        foreach ($activities as $activity) {
            $data[] = $activity->toOutputArray();
        }

        // YAY
        $response->addData('activities', $data);
        return 200;
    }

//getCustomerActivities()


    /*     * *
     * Adds testimonial to the system
     * @input: $_GET['fromTime']
     * @output: Array of Testimonials
     * * */

    function getUpdatedTestimonials($response) {
        // Ensure action is legal - check that the user is indeed a staff
        // if (get_class($_SESSION['user']) != 'Staff') {
        // 	$response->addData('error', 'No session staff');
        // 	return 400;
        // }
        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Param check
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'illegal value for fromTime: ' . $_GET['fromTime']);
            return 400;
        }

        // Read models from DB
        $db = Database::getConnection();
        $testimonials = $db->readMultiple('Testimonial', array(
            array('lastUpdateTime', '>=', $_GET['fromTime'])
                ), 0, 0, NULL, 'ASC', FALSE);

        if (!isset($testimonials)) {
            $response->addData('testimonials', array());
            return 202;
        }

        // Package into array data
        $data = array();
        foreach ($testimonials as $testimonial) {
            $data[] = $testimonial->toOutputArray();
        }

        // YAY
        $response->addData('testimonials', $data);
        return 200;
    }//getUpdatedTestimonials()
    
    /***
     * Adds testimonial to the system
     * @input: $_POST['name, profession, message, displayPriority']
     * @output: Testimonial
     * * */

    function addTestimonial($response) {
        //Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }
        
        // Check for file errors
        if ($_FILES['img']['error'] > 0) {
            $response->addData('error', $_FILES['img']['error']);
            return 400;
        }
        
        if (!isset($_FILES['img']['tmp_name'])) {
            $response->addData('error', 'No tmp file uploaded.');
            return 400;
        } else {
            $image = getimagesize($_FILES["img"]["tmp_name"]);
            $image_width = $image[0];
            $image_height = $image[1];
            // removed the validation with the new changes requirement by rupesh on dated 23-sep 2016
//            if($image_width!=280 || $image_height!=180){
//                $response->addData('error', 'The image size must be 280*180px');
//                return 400;
//            }
        }

        // Generate timestamp
        $time = getTimeInMs();
        // Cache variables
        $userID = $_SESSION['user']->getID();

        // Create testimonial - can be done outside transaction as we don't need persistence (yet)
        try {
            $testimonial = new Testimonial(array(
                'name' => $_POST['name'],
                'message' => $_POST['message'],
                'profession' => $_POST['profession'],
                'displayPriority' => $_POST['displayPriority'],
                'createdTime' => $time,
                'lastUpdateTime' => NULL,
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }


        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Create in DB
            if (!$db->create($testimonial)) {
                Log::error(__METHOD__ . '() - DB unable to create testimonial for: ' . $_POST['name']);
                $response->addData('error', 'DB unable to create testimonial.');
                return 500;
            }
            
             // Move upload file
            $target_path = WWW_ROOT . '/img/testimonials/' . $testimonial->getID() . '.png';
            if (!move_uploaded_file($_FILES['img']['tmp_name'], $target_path)) {
                Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $target_path);
                $response->addData('error', 'Image file could not be uploaded. Possibly no write permissions.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $userID,
                'userType' => 'Staff',
                'action' => 'Added Testimonial',
                'modelID' => $testimonial->getID(),
                'modelType' => 'Testimonial',
                'createdTime' => NULL
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' added testimonial"');
            }
            StaffAPI::storeLastActivityTime($time, 'added testimonial');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }
            }
        } // while not committed
        // YAY!

        $response->addData('testimonial', $testimonial->toOutputArray());
        return 200;
    }

    /** *
     * Edits a testimonial
     * @input: $_POST['id, name, profession, message, displayPriority']
     * @output: Testimonial
     * * */

    function editTestimonial($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }
        
        // Check for file errors
        $hasFile = TRUE;
        if ($_FILES['img']['error'] > 0 || !isset($_FILES['img']['tmp_name'])) {
            $hasFile = FALSE;
        }
        
        if ($hasFile) {
            $image = getimagesize($_FILES["img"]["tmp_name"]);
            $image_width = $image[0];
            $image_height = $image[1];
//            if($image_width!=180 || $image_height!=180){
//                $response->addData('error', 'The image size must be 280*180px');
//                return 400;
//            }
        }

        // Generate timestamp
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find from DB
            $testimonial = $db->readSingle('Testimonial', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($testimonial)) {
                $response->addData('error', 'No such testimonial ' . $_POST['id']);
                return 400;
            }

            // Update it
            $updated = $testimonial->update(array(
                'name' => $_POST['name'],
                'profession' => $_POST['profession'],
                'message' => $_POST['message'],
                'displayPriority' => $_POST['displayPriority'],
                'lastUpdateTime' => $time
            ));

            // Update it in DB
            if (!$db->update($testimonial)) {
                Log::error(__METHOD__ . '() - DB unable to update ' . $testimonial);
                $response->addData('error', 'DB unable to update testimonial.');
                return 500;
            }
            
            // Move upload file (if file was uploaded)
            if ($hasFile) {
                $target_path = WWW_ROOT . '/img/testimonials/' . $testimonial->getID() . '.png';
                if (!move_uploaded_file($_FILES['img']['tmp_name'], $target_path)) {
                    Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $target_path);
                    $response->addData('error', 'Image file could not be uploaded. Possibly no write permissions.');
                    return 500;
                }
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Testimonial',
                'modelID' => $testimonial->getID(),
                'modelType' => 'Testimonial',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited testimonial"');
            }
            StaffAPI::storeLastActivityTime($time, 'edited testimonial');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }
            }
        } // while not committed
        // YAY!
        $response->addData('testimonial', $testimonial->toOutputArray());
        return 200;
    }

//editTestimonial()





    /*     * *
     * Adds partner to the system
     * @input: $_GET['fromTime']
     * @output: Array of Partners
     * * */

    function getUpdatedPartners($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Param check
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'illegal value for fromTime: ' . $_GET['fromTime']);
            return 400;
        }

        // Read models from DB
        $db = Database::getConnection();
        $partners = $db->readMultiple('Partner', array(
            array('lastUpdateTime', '>=', $_GET['fromTime'])
                ), 0, 0, NULL, 'ASC', FALSE);

        if (!isset($partners)) {
            $response->addData('partners', array());
            return 202;
        }


        // Package into array data
        $data = array();
        foreach ($partners as $tmp) {
            $data[] = $tmp->toOutputArray();
        }

        // YAY
        $response->addData('partners', $data);
        return 200;
    }

//getUpdatedPartners()

    /*     * *
     * Edits a Partner
     * - A PNG of the partner may also be uploaded (overwrite previous).
     * @input: $_POST['id, message, displayPriority'], $_FILES['img']
     * @output: Partner
     * * */

    function editPartner($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Check for file errors
        $hasFile = TRUE;
        if ($_FILES['img']['error'] > 0 || !isset($_FILES['img']['tmp_name'])) {
            $hasFile = FALSE;
        }

        if ($hasFile) {
            $image = getimagesize($_FILES["img"]["tmp_name"]);
            $image_width = $image[0];
            $image_height = $image[1];
            if ($image_width != 200 || $image_height != 200) {
//                $response->addData('error', 'The image size must be 200*200px');
//                return 400;
            }
        }

        // Generate timestamp
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find from DB
            $partner = $db->readSingle('Partner', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($partner)) {
                $response->addData('error', 'No such Partner ' . $_POST['id']);
                return 400;
            }

            // Edit it
            $updated = $partner->update(array(
                'name' => $_POST['name'],
                'email' => '',
                'description' => $_POST['description'],
                'websiteURL' => null,
                'displayPriority' => $_POST['displayPriority'],
                'lastUpdateTime' => $time
            ));

            // Update in DB
            if (!$db->update($partner)) {
                Log::error(__METHOD__ . '() - DB unable to update partner for: ' . $_POST['description']);
                $response->addData('error', 'DB unable to update partner.');
                return 500;
            }

            // Move upload file (if file was uploaded)
            if ($hasFile) {
                $target_path = WWW_ROOT . '/img/partners/' . $partner->getID() . '.png';
                if (!move_uploaded_file($_FILES['img']['tmp_name'], $target_path)) {
                    Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $target_path);
                    $response->addData('error', 'Image file could not be uploaded. Possibly no write permissions.');
                    return 500;
                }
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Capability',
                'modelID' => $partner->getID(),
                'modelType' => 'Partner',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited Partner"');
            }
            StaffAPI::storeLastActivityTime($time, 'Edited Partner');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        $response->addData('partner', $partner->toOutputArray());
        return 200;
    }

//editPartner()



    /*     * *
     * Adds partner to the system
     * @output: Partner
     * * */

    function addPartner($response) {
//print_r($_POST);
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Check for file errors
        if ($_FILES['img']['error'] > 0) {
            $response->addData('error', $_FILES['img']['error']);
            return 400;
        }

        if (!isset($_FILES['img']['tmp_name'])) {
            $response->addData('error', 'No tmp file uploaded.');
            return 400;
        } else {
            $image = getimagesize($_FILES["img"]["tmp_name"]);
            $image_width = $image[0];
            $image_height = $image[1];
            if ($image_width != 200 || $image_height != 200) {
//                $response->addData('error', 'The image size must be 200*200px');
//                return 400;
            }
        }



        // Generate timestamp
        $time = getTimeInMs();
//        echo $time;
        // Create Partner - can be done outside transaction as we don't need persistence (yet)
        try {
            $partner = new Partner(array(
                'email' => '',
                'name' => $_POST['name'],
                'description' => $_POST['description'],
                'websiteURL' => null,
                'displayPriority' => $_POST['displayPriority'],
                'createdTime' => $time,
                'lastUpdateTime' => $time,
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }


        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }
//            print_r($partner);exit;
            // Create in DB
            if (!$db->create($partner)) {
                Log::error(__METHOD__ . '() - DB unable to create partner for: ' . $_POST['description']);
                $response->addData('error', 'DB unable to create partner.');
                return 500;
            }

            // Move upload file
            $target_path = WWW_ROOT . '/img/partners/' . $partner->getID() . '.png';
            if (!move_uploaded_file($_FILES['img']['tmp_name'], $target_path)) {
                Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $target_path);
                $response->addData('error', 'Image file could not be uploaded. Possibly no write permissions.');
                return 500;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Added Capability',
                'modelID' => $partner->getID(),
                'modelType' => 'Partner',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' added Partner"');
            }
            StaffAPI::storeLastActivityTime($time, 'Added Partner');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        $response->addData('partner', $partner->toOutputArray());
        return 200;
    }

//addPartner()


    /*     * *
     * Adds project to the system
     * @output: Partner
     * * */

    function addProject($response) {//print_r($_POST);die;
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Check for file errors
        if ($_FILES['img']['error'] > 0) {
            $response->addData('error', $_FILES['img']['error']);
            return 400;
        }
        //code added for home page feature display with new requirement on date 23rd sep 2016 by rupesh manandhar
        $isFeaturedInStaticSection = 0;
        $isFeaturedInPanoramicSection = 0;
        if ($_POST['home_page_feature'] == 0) {
            $isFeaturedInStaticSection = 1;
            $isFeaturedInPanoramicSection = 0;
        } else if ($_POST['home_page_feature'] == 1) {
            $isFeaturedInStaticSection = 0;
            $isFeaturedInPanoramicSection = 1;
        } else {
            $isFeaturedInStaticSection = 1;
            $isFeaturedInPanoramicSection = 1;
        }
        if ($_POST['home_page_feature'] == 3) {
            $isFeaturedInStaticSection = 0;
            $isFeaturedInPanoramicSection = 0;
        }


        if (!isset($_FILES['img']['tmp_name'])) {
            $response->addData('error', 'No tmp file uploaded.');
            return 400;
        } else {
            $image = getimagesize($_FILES["img"]["tmp_name"]);
            $image_width = $image[0];
            $image_height = $image[1];
            // removed the validation with the new changes requirement by rupesh on dated 23-sep 2016
//            if($image_width!=280 || $image_height!=180){
//                $response->addData('error', 'The image size must be 280*180px');
//                return 400;
//            }
        }


        
        // Create Project - can be done outside transaction as we don't need persistence (yet)
        try {

            $project = new Project(array(
                'name' => $_POST['name'],
                'location' => $_POST['location'],
                'longitude' => $_POST['longitude'],
                'latitude' => $_POST['latitude'],
                'description' => $_POST['description'],
                'displayPriority' => $_POST['displayPriority'],
                'isFeaturedInStaticSection' => $isFeaturedInStaticSection,
                'isFeaturedInPanoramicSection' => $isFeaturedInPanoramicSection,
                'createdTime' => NULL,
                'lastUpdateTime' => NULL,
            ));
        } catch (Exception $e) {
            $response->addData('error', $e->getMessage());
            return 400;
        }

        // Generate timestamp
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }
//            print_r($project);exit;
            // Create in DB
            if (!$db->create($project)) {
                Log::error(__METHOD__ . '() - DB unable to create project for: ' . $_POST['description']);
                $response->addData('error', 'DB unable to create project.');
                return 500;
            }


            // Move upload file
            $target_path = WWW_ROOT . '/img/projects/icons/' . $project->getID() . '.png';
            if (!move_uploaded_file($_FILES['img']['tmp_name'], $target_path)) {
                Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $target_path);
                $response->addData('error', 'Image file could not be uploaded. Possibly no write permissions.');
                return 500;
            }
            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Added Project',
                'modelID' => $project->getID(),
                'modelType' => 'Project',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' added Project"');
            }
            StaffAPI::storeLastActivityTime($time, 'Added Project');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        $response->addData('project', $project->toOutputArray());
        return 200;
    }

//addProject()

    function editProject($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Check for file errors
        $hasFile = TRUE;
        if ($_FILES['img']['error'] > 0 || !isset($_FILES['img']['tmp_name'])) {
            $hasFile = FALSE;
        }

        if ($hasFile) {
            $image = getimagesize($_FILES["img"]["tmp_name"]);
            $image_width = $image[0];
            $image_height = $image[1];
//            if($image_width!=180 || $image_height!=180){
//                $response->addData('error', 'The image size must be 280*180px');
//                return 400;
//            }
        }

        //code added for home page feature display with new requirement on date 23rd sep 2016 by rupesh manandhar
        $isFeaturedInStaticSection = 0;
        $isFeaturedInPanoramicSection = 0;
        if ($_POST['home_page_feature'] == 0) {
            $isFeaturedInStaticSection = 1;
            $isFeaturedInPanoramicSection = 0;
        } else if ($_POST['home_page_feature'] == 1) {
            $isFeaturedInStaticSection = 0;
            $isFeaturedInPanoramicSection = 1;
        } else {
            $isFeaturedInStaticSection = 1;
            $isFeaturedInPanoramicSection = 1;
        }

        if ($_POST['home_page_feature'] == 3) {
            $isFeaturedInStaticSection = 0;
            $isFeaturedInPanoramicSection = 0;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find from DB
            $project = $db->readSingle('Project', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($project)) {
                $response->addData('error', 'No such Project ' . $_POST['id']);
                return 400;
            }
            // Edit it
            $updated = $project->update(array(
                'name' => $_POST['name'],
                'location' => $_POST['location'],
                'longitude' => $_POST['longitude'],
                'latitude' => $_POST['latitude'],
                'description' => $_POST['description'],
                'displayPriority' => $_POST['displayPriority'],
                'isFeaturedInStaticSection' => $isFeaturedInStaticSection,
                'isFeaturedInPanoramicSection' => $isFeaturedInPanoramicSection,
                'lastUpdateTime' => NULL,
            ));
//            print_r($project);exit;
            // Generate timestamp
            $time = getTimeInMs();

            // Update in DB
            if (!$db->update($project)) {
                Log::error(__METHOD__ . '() - DB unable to update project for: ' . $_POST['description']);
                $response->addData('error', 'DB unable to update project.');
                return 500;
            }

            // Move upload file (if file was uploaded)
            if ($hasFile) {
                $target_path = WWW_ROOT . '/img/projects/icons/' . $project->getID() . '.png';
                if (!move_uploaded_file($_FILES['img']['tmp_name'], $target_path)) {
                    Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $target_path);
                    $response->addData('error', 'Image file could not be uploaded. Possibly no write permissions.');
                    return 500;
                }
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Project',
                'modelID' => $project->getID(),
                'modelType' => 'Project',
                'createdTime' => NULL
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited Project"');
            }
            StaffAPI::storeLastActivityTime($time, 'Edited Project');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        $response->addData('project', $project->toOutputArray());
        return 200;
    }

//editProject()

    /*     * *
     * Gets updates projects from the system
     * @input: $_GET['fromTime']
     * @output: Array of Partners
     * * */

    function getUpdatedProjects($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Param check
        if (!isUnsignedInt($_GET['fromTime'])) {
            $response->addData('error', 'illegal value for fromTime: ' . $_GET['fromTime']);
            return 400;
        }

        // Read models from DB
        $db = Database::getConnection();
        $projects = $db->readMultiple('Project', array(
            array('lastUpdateTime', '>=', $_GET['fromTime'])
                ), 0, 0, NULL, 'ASC', FALSE);

        if (!isset($projects)) {
            $response->addData('projects', array());
            return 202;
        }


        // Package into array data
        $data = array();
        foreach ($projects as $tmp) {
            $data[] = $tmp->toOutputArray();
        }

        // YAY
        $response->addData('projects', $data);
        return 200;
    }

//getUpdatedProjects()

    /*     * *
     * Gets updated project blue prints from the system
     * @input: $_GET['fromTime']
     * @output: Array of Partners
     * * */

    function getProjectBluePrints($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }


        // Find from DB
        $db = Database::getConnection();
        $projectBluePrint = $db->readSingle('ProjectBluePrint', array(
            array('projectID', '=', $_GET['id'])
                ), TRUE);
//        print_r($projectBluePrint);
        if (!isset($projectBluePrint)) {
            $a = array(
                'id' => NULL,
                'projectID' => NULL,
                'urlLow1' => NULL,
                'urlLow2' => NULL,
                'urlLow3' => NULL,
                'urlLow4' => NULL,
                'urlHigh1' => NULL,
                'urlHigh2' => NULL,
                'urlHigh3' => NULL,
                'urlHigh4' => NULL,
            );
            $response->addData('projectBluePrints', $a);
            return 202;
        }

        // YAY
        $response->addData('projectBluePrints', $projectBluePrint->toOutputArray());
        return 200;
    }

//getUpdatedProjects()

    /*     * *
     * Gets updated project image from the system
     * @input: $_GET['fromTime']
     * @output: Array of Partners
     * * */

    function getProjectImages($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }


        // Read models from DB
        $db = Database::getConnection();
        $projectImages = $db->readMultiple('ProjectImage', array(
            array('projectID', '=', $_GET['id'])
                ), 0, 0, NULL, 'ASC', FALSE);

        if (!isset($projectImages)) {
            $response->addData('projectImages', array());
            return 202;
        }


        // Package into array data
        $data = array();
        foreach ($projectImages as $tmp) {
            $data[] = $tmp->toOutputArray();
        }

        // YAY
        $response->addData('projectImages', $data);
        return 200;
    }

//getUpdatedProjects()

    /*     * *
     * Edits a Project features
     * @input: $_POST['id, message, displayPriority'], $_FILES['img']
     * @output: Project
     * * */

    function editProjectBluePrint($response) {
//        print_r($_POST);print_r($_FILES);//return false;
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }


        $validSize = TRUE;
        $validSizeErr = array();
        if (isset($_FILES)) {
            foreach ($_FILES as $field => $files) {
                if ($files['error'] == 0 && isset($files['tmp_name'])) {
                    $image = getimagesize($files["tmp_name"]);
                    $image_width = $image[0];
                    $image_height = $image[1];
                    if (strpos($field, 'low') !== false) {
//                        if($image_width!=720 || $image_height!=320){
//                            $validSize = FALSE;
//                            $validSizeErr[] = str_replace('url-','Blue Print ',$field.' image size is not valid. (720*320px)');
//                        }
                    } else if (strpos($field, 'high') !== false) {
//                         if($image_width!=1440 || $image_height!=640){
//                            $validSize = FALSE;
//                            $validSizeErr[] = str_replace('url-','Blue Print ',$field.' image size is not valid. (1440*640px)');
//                        }
                    }
                }
            }
        }
        if ($validSize == FALSE) {
            $response->addData('error', $validSizeErr);
            return 400;
        }

        $db = Database::getConnection();
        // Find from DB
        $checkProjectBluePrintExists = $db->readSingle('ProjectBluePrint', array(
            array('projectID', '=', $_POST['id'])
                ), TRUE);
        if (!isset($checkProjectBluePrintExists)) {
            //perpare for insert
            $insertData = array();
            $path = 'img/projects/blueprints';
            $insertData['projectID'] = $_POST['id'];
            $insertData['urlHigh1'] = isset($_FILES['url-high-1']) ? $this->simply_upload_file($_FILES['url-high-1']['tmp_name'], $_FILES['url-high-1']['name'], $path) : NULL;
            $insertData['urlHigh2'] = isset($_FILES['url-high-2']) ? $this->simply_upload_file($_FILES['url-high-2']['tmp_name'], $_FILES['url-high-2']['name'], $path) : NULL;
            $insertData['urlHigh3'] = isset($_FILES['url-high-3']) ? $this->simply_upload_file($_FILES['url-high-3']['tmp_name'], $_FILES['url-high-3']['name'], $path) : NULL;
            $insertData['urlHigh4'] = isset($_FILES['url-high-4']) ? $this->simply_upload_file($_FILES['url-high-4']['tmp_name'], $_FILES['url-high-4']['name'], $path) : NULL;
            $insertData['urlLow1'] = isset($_FILES['url-low-1']) ? $this->simply_upload_file($_FILES['url-low-1']['tmp_name'], $_FILES['url-low-1']['name'], $path) : NULL;
            $insertData['urlLow2'] = isset($_FILES['url-low-2']) ? $this->simply_upload_file($_FILES['url-low-2']['tmp_name'], $_FILES['url-low-2']['name'], $path) : NULL;
            $insertData['urlLow3'] = isset($_FILES['url-low-3']) ? $this->simply_upload_file($_FILES['url-low-3']['tmp_name'], $_FILES['url-low-3']['name'], $path) : NULL;
            $insertData['urlLow4'] = isset($_FILES['url-low-4']) ? $this->simply_upload_file($_FILES['url-low-4']['tmp_name'], $_FILES['url-low-4']['name'], $path) : NULL;


            // Create Project blueprint - can be done outside transaction as we don't need persistence (yet)
            try {
                $projectBluePrint = new ProjectBluePrint($insertData);
            } catch (Exception $e) {
                $response->addData('error', $e->getMessage());
                return 400;
            }

            // Generate timestamp
            $time = getTimeInMs();

            // Transaction for ACID-ity
            $committed = FALSE;
            while (!$committed) {
                // Begin DB transaction
                if (!$db->beginTransaction()) {
                    Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                    $response->addData('error', 'DB unable to begin transaction.');
                    return 500;
                }
                //            print_r($project);exit;
                // Create in DB
                if (!$db->create($projectBluePrint)) {
                    Log::error(__METHOD__ . '() - DB unable to create project for: ' . $_POST['id']);
                    $response->addData('error', 'DB unable to create project.');
                    return 500;
                }
                // Create and store this activity
                $activity = new Activity(array(
                    'userID' => $_SESSION['user']->getID(),
                    'userType' => 'Staff',
                    'action' => 'Added Project Blueprint',
                    'modelID' => $projectBluePrint->getID(),
                    'modelType' => 'Project',
                    'createdTime' => $time
                ));
                if (!$db->create($activity)) {
                    Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' added Project"');
                }
                StaffAPI::storeLastActivityTime($time, 'Added Project Blue Print');

                // Commit transaction
                $committed = $db->commit();
                if (!$committed) {
                    Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                    if (!$db->rollback()) {
                        Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                        $response->addData('error', 'DB unable to commit transaction');
                        return 500;
                    }
                }
            } // while not committed
            // YAY!
            $response->addData('projectBluePrint', $projectBluePrint->toOutputArray());
            return 200;
        } else {
            //prepare for update

            $updateData = array();
            $path = 'img/projects/blueprints';
            $updateData['urlHigh1'] = isset($_FILES['url-high-1']) ? $this->simply_upload_file($_FILES['url-high-1']['tmp_name'], $_FILES['url-high-1']['name'], $path, $_POST['url-high-1-prev']) : NULL;
            $updateData['urlHigh2'] = isset($_FILES['url-high-2']) ? $this->simply_upload_file($_FILES['url-high-2']['tmp_name'], $_FILES['url-high-2']['name'], $path, $_POST['url-high-2-prev']) : NULL;
            $updateData['urlHigh3'] = isset($_FILES['url-high-3']) ? $this->simply_upload_file($_FILES['url-high-3']['tmp_name'], $_FILES['url-high-3']['name'], $path, $_POST['url-high-3-prev']) : NULL;
            $updateData['urlHigh4'] = isset($_FILES['url-high-4']) ? $this->simply_upload_file($_FILES['url-high-4']['tmp_name'], $_FILES['url-high-4']['name'], $path, $_POST['url-high-4-prev']) : NULL;
            $updateData['urlLow1'] = isset($_FILES['url-low-1']) ? $this->simply_upload_file($_FILES['url-low-1']['tmp_name'], $_FILES['url-low-1']['name'], $path, $_POST['url-low-1-prev']) : NULL;
            $updateData['urlLow2'] = isset($_FILES['url-low-2']) ? $this->simply_upload_file($_FILES['url-low-2']['tmp_name'], $_FILES['url-low-2']['name'], $path, $_POST['url-low-2-prev']) : NULL;
            $updateData['urlLow3'] = isset($_FILES['url-low-3']) ? $this->simply_upload_file($_FILES['url-low-3']['tmp_name'], $_FILES['url-low-3']['name'], $path, $_POST['url-low-3-prev']) : NULL;
            $updateData['urlLow4'] = isset($_FILES['url-low-4']) ? $this->simply_upload_file($_FILES['url-low-4']['tmp_name'], $_FILES['url-low-4']['name'], $path, $_POST['url-low-4-prev']) : NULL;


            $committed = FALSE;
            while (!$committed) {
                // Begin DB transaction
                if (!$db->beginTransaction()) {
                    Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                    $response->addData('error', 'DB unable to begin transaction.');
                    return 500;
                }
                $ProjectBluePrintUpdate = $db->readSingle('ProjectBluePrint', array(
                    array('projectID', '=', $_POST['id'])
                        ), TRUE);

                // Edit it
                $ProjectBluePrintUpdate->update($updateData);
//                    print_r($ProjectBluePrintUpdate);exit;
                //  Generate timestamp
                $time = getTimeInMs();

                // Update in DB
                if (!$db->update($ProjectBluePrintUpdate)) {
//                    Log::error(__METHOD__ . '() - DB unable to update project blue print for: ' . $_POST['id']);
//                    $response->addData('error', 'DB unable to update project blue print.');
//                    return 500;
                }

                // Create and store this activity
                $activity = new Activity(array(
                    'userID' => $_SESSION['user']->getID(),
                    'userType' => 'Staff',
                    'action' => 'Edited Project Blueprint',
                    'modelID' => $ProjectBluePrintUpdate->getID(),
                    'modelType' => 'ProjectBluePrint',
                    'createdTime' => NULL
                ));
                if (!$db->create($activity)) {
                    Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited Project"');
                }
                StaffAPI::storeLastActivityTime($time, 'Edited Project');

                // Commit transaction
                $committed = $db->commit();
                if (!$committed) {
                    Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                    if (!$db->rollback()) {
                        Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                        $response->addData('error', 'DB unable to commit transaction');
                        return 500;
                    }
                }
            } // while not committed
            // YAY!
            $response->addData('projectBluePrint', $ProjectBluePrintUpdate->toOutputArray());
            return 200;
        }
    }

//editProjectBluePrint()

    function deleteProjectBluePrint($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }


        $validSize = TRUE;
        $validSizeErr = array();


        if ($validSize == FALSE) {
            $response->addData('error', $validSizeErr);
            return 400;
        }
        $db = Database::getConnection();
        // print_r($_POST);die("test");
        // Find from DB

        $checkProjectBluePrintExists = $db->readSingle('ProjectBluePrint', array(
            array('id', '=', $_POST['id']),
            array($_POST['level'], '!=', 'null')
                ), FALSE);

        if ($checkProjectBluePrintExists != '') {
            //prepare for update

            $updateData = array();
            $urlimg = $_POST['level'];
            $path = 'img/projects/blueprints';
            $updateData[$urlimg] = null;

            $committed = FALSE;
            while (!$committed) {
                // Begin DB transaction
                if (!$db->beginTransaction()) {
                    Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                    $response->addData('error', 'DB unable to begin transaction.');
                    return 500;
                }
                $ProjectBluePrintUpdate = $db->readSingle('ProjectBluePrint', array(
                    array('id', '=', $_POST['id'])
                        ), TRUE);

                // Edit it
                //print_r($updateData); die("test");
                $ProjectBluePrintUpdate->update($updateData);
                //  Generate timestamp
                $time = getTimeInMs();

                // Update in DB
                if (!$db->update($ProjectBluePrintUpdate)) {
                    Log::error(__METHOD__ . '() - DB unable to update project blue print for: ' . $_POST['id']);
                    $response->addData('error', 'DB unable to update project blue print.');
                    return 500;
                }

                // Create and store this activity
                $activity = new Activity(array(
                    'userID' => $_SESSION['user']->getID(),
                    'userType' => 'Staff',
                    'action' => 'Edited Project Blueprint',
                    'modelID' => $ProjectBluePrintUpdate->getID(),
                    'modelType' => 'ProjectBluePrint',
                    'createdTime' => NULL
                ));
                if (!$db->create($activity)) {
                    Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited Project"');
                }
                StaffAPI::storeLastActivityTime($time, 'Edited Project');

                // Commit transaction
                $committed = $db->commit();
                if (!$committed) {
                    Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                    if (!$db->rollback()) {
                        Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                        $response->addData('error', 'DB unable to commit transaction');
                        return 500;
                    }
                }
            } // while not committed
            // YAY!
            $response->addData('projectBluePrint', $ProjectBluePrintUpdate->toOutputArray());
            return 200;
        } else {
            return 200;
        }
    }

//delete ProjectBluePrint()

    function simply_upload_file($temp_name, $name, $folder, $prev_image = NULL) {
        if ($name != '') {
            $file = time();
            $tempFile = $temp_name;
            $targetPath = WWW_ROOT . '/' . $folder;
            $new_name = $file . '_' . $name;
            $targetFile = rtrim($targetPath, '/') . '/' . $new_name;

            // Validate the file type
            $fileTypes = array('png', 'jpg', 'JPG', 'jpeg', 'JPEG', 'PNG', 'svg'); // File extensions
            $fileParts = pathinfo($name);

            if (in_array($fileParts['extension'], $fileTypes)) {
                if (!move_uploaded_file($tempFile, $targetFile)) {
                    Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $targetFile);
                    return $prev_image;
                } else {
                    //delete previous image
                    if ($prev_image != '') {
                        $path = WWW_ROOT . '/' . $folder . '/' . $prev_image;
                        if (file_exists($path)) {
                            unlink($path);
                        }
                    }
                    return $new_name;
                }
            } else {
                return $prev_image;
            }
        } else {
            return $prev_image;
        }
    }

    /*     * *
     * Edits a Project blue print
     * @input: $_POST['id, message, displayPriority'], $_FILES['img']
     * @output: Project
     * * */

    function editProjectFeature($response) {
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Check for file errors
        $hasFile = TRUE;
        if ($_FILES['img']['error'] > 0 || !isset($_FILES['img']['tmp_name'])) {
            $hasFile = FALSE;
        }

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find from DB
            $project = $db->readSingle('Project', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($project)) {
                $response->addData('error', 'No such Project ' . $_POST['id']);
                return 400;
            }

            $startMonth = null;
            $startYear = null;
            if ($_POST['startDate']) {
                $startdate = $_POST['startDate'];
                $datetime = new DateTime($startdate);
                $startMonth = $datetime->format('m');
                $startYear = $datetime->format('Y');
            }

            $endMonth = null;
            $endYear = null;
            if ($_POST['endDate']) {
                $enddate = $_POST['endDate'];
                $datetime = new DateTime($enddate);
                $endMonth = $datetime->format('m');
                $endYear = $datetime->format('Y');
            }

            // Edit it
            $updated = $project->update(array(
                'leaseHold' => $_POST['leaseHold'],
                'buildUpArea' => $_POST['buildUpArea'],
                'buildInArea' => $_POST['buildInArea'],
                'bedRoom' => $_POST['bedRoom'],
                'shower' => $_POST['shower'],
                'startMonth' => $startMonth,
                'startYear' => $startYear,
                'endMonth' => $endMonth,
                'endYear' => $endYear,
                'estimatedCost' => $_POST['estimatedCost'],
                'demolitionWork' => $_POST['demolitionWork'],
                'structureWork' => $_POST['structureWork'],
                'meWork' => $_POST['meWork'],
                'externalWork' => $_POST['externalWork'],
                'architecturalWork' => $_POST['architecturalWork'],
                'lastUpdateTime' => NULL,
            ));
//            print_r($project);exit;
            // Generate timestamp
            $time = getTimeInMs();

            // Update in DB
            if (!$db->update($project)) {
                Log::error(__METHOD__ . '() - DB unable to update project for: ' . $_POST['description']);
                $response->addData('error', 'DB unable to update project.');
                return 500;
            }

            // Move upload file (if file was uploaded)
            if ($hasFile) {
                $target_path = WWW_ROOT . '/img/projects/icons/' . $project->getID() . '.png';
                if (!move_uploaded_file($_FILES['img']['tmp_name'], $target_path)) {
                    Log::error(__METHOD__ . '() - Image file could not be uploaded. Possibly no write permissions. Path: ' . $target_path);
                    $response->addData('error', 'Image file could not be uploaded. Possibly no write permissions.');
                    return 500;
                }
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Project Info',
                'modelID' => $project->getID(),
                'modelType' => 'Project',
                'createdTime' => NULL
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited Project"');
            }
            StaffAPI::storeLastActivityTime($time, 'Edited Project');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        $response->addData('project', $project->toOutputArray());
        return 200;
    }

//editProjectFeature()


    /*     * *
     * Adds project to the system
     * @output: Partner
     * * */

    function addProjectImage($response) {
//        print_r($_FILES);exit;
        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        if (!isset($_FILES['img']['tmp_name'])) {
            $response->addData('error', 'No image file uploaded.');
            return 400;
        } else {
            $image = getimagesize($_FILES["img"]["tmp_name"]);
            $image_width = $image[0];
            $image_height = $image[1];
            if ($_POST['panoramic'] == 1) {
                if ($image_width != 1920 || $image_height != 1280) {
//                    $response->addData('error', 'The image size must be 1920*1280px');
//                    return 400;
                }
            } else {
                if ($image_width != 1920 || $image_height != 1280) {
//                    $response->addData('error', 'The image size must be 1920*1280px');
//                    return 400;
                }
            }
        }


        // Create Project - can be done outside transaction as we don't need persistence (yet)
        $path = 'img/projects/images';
        $imgUrl = isset($_FILES['img']) ? $this->simply_upload_file($_FILES['img']['tmp_name'], $_FILES['img']['name'], $path) : '';
        $isProjectImage = 0;
        $isStaticImgFeature = 0;
        $isPanaromaImgFeature = 0;

        try {
            if ($_POST['panoramic'] == 1) {
                $isPanaromaImgFeature = $_POST['isFeatured'];
            } else {
                $isStaticImgFeature = $_POST['isFeatured'];
            }

            $projectImage = new ProjectImage(array(
                'projectID' => $_POST['projectId'],
                'displayPriority' => $_POST['displayPriority'],
                'name' => $_POST['name'],
                'imgUrl' => $imgUrl,
                'isPanorama' => $_POST['panoramic'],
                'isStaticSectionFeaturedImage' => $isStaticImgFeature,
                'isPanoramicSectionFeaturedImage' => $isPanaromaImgFeature,
                'createdTime' => NULL,
                'lastUpdateTime' => NULL,
            ));
        } catch (Exception $e) {// print_R($e);
            $response->addData('error', $e->getMessage());
            return 400;
        }

//        print_r($projectImage);exit;
        // Generate timestamp
        $time = getTimeInMs();

        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }
//            print_r($projectImage);exit;
            // Create in DB
            if (!$db->create($projectImage)) {
                Log::error(__METHOD__ . '() - DB unable to create project image data for: ' . $_POST['description']);
                $response->addData('error', 'DB unable to create project image data.');
                return 500;
            }
            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Added Project Gallery',
                'modelID' => $projectImage->getID(),
                'modelType' => 'Project Image',
                'createdTime' => $time
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' added Project"');
            }
            StaffAPI::storeLastActivityTime($time, 'Added Project');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!

        $response->addData('insertId', $projectImage->getID());
        $response->addData('projectImage', $projectImage->toOutputArray());
        return 200;
    }

//addProjectImage()

    function editProjectImage($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }

        // Check for file errors
        $hasFile = TRUE;
        if ($_FILES['img']['error'] > 0 || !isset($_FILES['img']['tmp_name'])) {
            $hasFile = FALSE;
        }

        if ($hasFile) {
            $image = getimagesize($_FILES["img"]["tmp_name"]);
            $image_width = $image[0];
            $image_height = $image[1];
            if ($_POST['panoramic'] == 1) {
                if ($image_width != 1920 || $image_height != 1280) {
//                    $response->addData('error', 'The image size must be 1920*1280px');
//                    return 400;
                }
            } else {
                if ($image_width != 1920 || $image_height != 1280) {
//                    $response->addData('error', 'The image size must be 1920*1280px');
//                    return 400;
                }
            }
        }


        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find from DB
            $projectImage = $db->readSingle('ProjectImage', array(
                array('id', '=', $_POST['id'])
                    ), TRUE);
            if (!isset($projectImage)) {
                $response->addData('error', 'No such Project ' . $_POST['id']);
                return 400;
            }
            // Edit it

            $path = 'img/projects/images';
            $imgUrl = isset($_FILES['img']) ? $this->simply_upload_file($_FILES['img']['tmp_name'], $_FILES['img']['name'], $path, $_POST['prevImg']) : '';
            $isProjectImage = 0;
            $isStaticImgFeature = 0;
            $isPanaromaImgFeature = 0;
            if ($_POST['panoramic'] == 1) {
                $isPanaromaImgFeature = $_POST['isFeatured'];
            } else {
                $isStaticImgFeature = $_POST['isFeatured'];
            }
            $updated = $projectImage->update(array(
                'projectID' => $_POST['projectId'],
                'displayPriority' => $_POST['displayPriority'],
                'name' => $_POST['name'],
                'imgUrl' => $imgUrl,
//                'description' => '',
                'isPanorama' => $_POST['panoramic'],
                'isStaticSectionFeaturedImage' => $isStaticImgFeature,
                'isPanoramicSectionFeaturedImage' => $isPanaromaImgFeature,
//                'isProjectImage' => $isProjectImage,
                'lastUpdateTime' => NULL,
            ));
//            print_r($project);exit;
            // Generate timestamp
            $time = getTimeInMs();

            // Update in DB
            if (!$db->update($projectImage)) {
                Log::error(__METHOD__ . '() - DB unable to update project for: ' . $_POST['description']);
                $response->addData('error', 'DB unable to update project.');
                return 500;
            }


            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Project Gallery',
                'modelID' => $projectImage->getID(),
                'modelType' => 'Project',
                'createdTime' => NULL
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited Project"');
            }
            StaffAPI::storeLastActivityTime($time, 'Edited Project');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!

        $response->addData('projectImage', $projectImage->toOutputArray());
        return 200;
    }

//editProject()


    function getUpdatedPanoramaProjectImages($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }


        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find from DB

            $projectImages = $db->readMultiple('ProjectImage', array(
                array('id', '!=', $_POST['id']),
                array('projectID', '=', $_POST['pid']),
                array('isPanorama', '=', 1),
                array('isPanoramicSectionFeaturedImage', '=', 1)
                    ), TRUE);

//	    print_r($projectImages);die;

            if (empty($projectImages)) {
                //$response->addData('error', 'No such Project ' . $_POST['id']);
                return 200;
            }
            foreach ($projectImages as $images) {
                $data = $images->toOutputArray();

                $projectImage = $db->readSingle('ProjectImage', array(
                    array('id', '=', $data['id']),
                        ), TRUE);

                if (empty($projectImage)) {
                    return 200;
                }
                $time = getTimeInMs();
                $updated = $projectImage->update(array(
                    'isPanoramicSectionFeaturedImage' => 0,
                    'lastActivityTime' => $time, // use this instead of the class static method to save 1 x UPDATE query
                ));
//            
                // Update in DB
                if (!$db->update($projectImage)) {
                    Log::error(__METHOD__ . '() - DB unable to update project for: ' . $data['name']);
                    $response->addData('error', 'DB unable to update project.');
                    return 500;
                }
                //print_r($projectImage);die;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Project Gallery',
                'modelID' => $projectImage->getID(),
                'modelType' => 'Project',
                'createdTime' => NULL
            ));

            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited Project"');
            }
            StaffAPI::storeLastActivityTime($time, 'Edited Project');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        //$response->addData('projectImage', $projectImage->toOutputArray());
        return 200;
    }

    function getUpdatedBoundaryProjectImages($response) {

        // Ensure action is legal - check that the user is indeed a staff
        if (get_class($_SESSION['user']) != 'Staff') {
            $response->addData('error', 'No session staff');
            return 400;
        }

        // Check CSRF
        if (!Session::checkCSRFToken()) {
            Log::warning(__METHOD__ . '() - CSRF Detected on ' . $_SESSION['user']);
            $response->setData(array('error' => 'CSRF detected'));
            return 401;
        }


        // Transaction for ACID-ity
        $db = Database::getConnection();
        $committed = FALSE;
        while (!$committed) {
            // Begin DB transaction
            if (!$db->beginTransaction()) {
                Log::fatal(__METHOD__ . '() - DB unable to begin transaction');
                $response->addData('error', 'DB unable to begin transaction.');
                return 500;
            }

            // Find from DB

            $projectImages = $db->readMultiple('ProjectImage', array(
                array('id', '!=', $_POST['id']),
                array('projectID', '=', $_POST['pid']),
                array('isPanorama', '=', 0),
                array('isStaticSectionFeaturedImage', '=', 1)
                    ), TRUE);
            if (empty($projectImages)) {
                //$response->addData('error', 'No such Project ' . $_POST['id']);
                return 200;
            }
            foreach ($projectImages as $images) {
                $data = $images->toOutputArray();

                $projectImage = $db->readSingle('ProjectImage', array(
                    array('id', '=', $data['id']),
                        ), TRUE);

                if (empty($projectImage)) {
                    return 200;
                }
                $time = getTimeInMs();
                $updated = $projectImage->update(array(
                    'isStaticSectionFeaturedImage' => 0,
                    'lastActivityTime' => $time, // use this instead of the class static method to save 1 x UPDATE query
                ));
//            
                // Update in DB
                if (!$db->update($projectImage)) {
                    Log::error(__METHOD__ . '() - DB unable to update project for: ' . $data['name']);
                    $response->addData('error', 'DB unable to update project.');
                    return 500;
                }
                //print_r($projectImage);die;
            }

            // Create and store this activity
            $activity = new Activity(array(
                'userID' => $_SESSION['user']->getID(),
                'userType' => 'Staff',
                'action' => 'Edited Project Gallery',
                'modelID' => $projectImage->getID(),
                'modelType' => 'Project',
                'createdTime' => NULL
            ));
            if (!$db->create($activity)) {
                Log::error(__METHOD__ . '() - DB Unable to create activity "' . $_SESSION['user'] . ' edited Project"');
            }
            StaffAPI::storeLastActivityTime($time, 'Edited Project');

            // Commit transaction
            $committed = $db->commit();
            if (!$committed) {
                Log::warning(__METHOD__ . '() - DB unable to commit transaction');
                if (!$db->rollback()) {
                    Log::fatal(__METHOD__ . '() - DB unable to rollback transaction');
                    $response->addData('error', 'DB unable to commit transaction');
                    return 500;
                }

                $time = getTimeInMs();
            }
        } // while not committed
        // YAY!
        //$response->addData('projectImage', $projectImage->toOutputArray());
        return 200;
    }

    /*     * *
     * Stores last activity time onto this session staff.
     * - Note: should be done in the context of a transaction.
     * - Note: must have a session staff
     * @param: time (int), activity name (string)
     * @return: none
     * * */

    private static function storeLastActivityTime($time, $activity_name = '') {
        $db = Database::getConnection();

        $query = 'UPDATE WB_Staff SET lastActivityTime = ' . $time . ' WHERE id = ' . $_SESSION['user']->getID() . ' LIMIT 1';
        // Log::test($query);
        if (!$db->query($query)) {
            Log::error(__METHOD__ . '() - unable to update last activity time of ' . $_SESSION['user'] . ' to ' . $time . ' (Activity: ' . $activity_name . ')');
        }
    }

//storeLastActivityTime()
}

//class StaffAPI
