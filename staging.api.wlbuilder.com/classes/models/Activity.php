<?php
// Record of things done on the system, e.g. Staff login, logout, product added, category edited
class Activity {
	// Compulsory fields
	private $id;					// int (DEFAULT: 0)
	private $createdTime;			// int timestamp (UNIX Timestamp) (DEFAULT: now)
	private $userID;				// int
	private $userType;				// string (class name)
	private $action;				// string
	
	// Optional fields (DEFAULT: NULL)
	private $modelID;				// int
	private $modelType;				// string (class name)
	
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists('Activity', $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in Activity class: method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->id;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()
	
	// Converts pre-selected data members into array
	function toOutputArray() {
		// Include everything
		return get_object_vars($this);
	} //toOutputArray()
	
	// Table that this model should be stored
	static function getTableName() {
		return 'WB_Activity';
	} //getTableName()
	
	/***********
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Only data members defined by this class will be processed; unrecognised args will be ignored.
	 ***********/
	function __construct($arg) {
		if (!is_array($arg)) throw new Exception('Error creating Activity: arg is not array');

		foreach ($arg as $key => $value) {
			if (property_exists('Activity', $key)) {
				$this->$key = $value;
			} //property exists
		} //parse each arg in the array

		$this->checkActivityData();
	} //construct()
	
	/***********
	 * Updates model with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * Params: an array of new data (key => value)
	 * Return: TRUE on success update, FALSE if no changes were made
	 ***********/
	function update($arg) {
		if (!is_array($arg)) throw new Exception('Error updating Activity: arg is not array');
		
		//$hasChanged = parent::update($arg);
		foreach ($arg as $key => $value) {
			if (property_exists('Activity', $key)) {
				if ((string) $this->$key != $value) {
					//Log::test($key.' changed from '.$this->$key.' to '.$value);
					$this->$key = $value;
					$hasChanged = true;
				} //update only if different
			} //property exists
		} //parse each arg in the array
		
		$this->checkActivityData();
		
		return $hasChanged;
	} //update()
	
	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	private function checkActivityData() {	
		if (!isUnsignedInt($this->id)){ 
			$this->id = 0;
		}
		else {
			$this->id = (int) $this->id;
		}
		
		if (!isUnsignedInt($this->createdTime)) {
			$this->createdTime = getTimeInMs();
		}
		else {
			$this->createdTime = $this->createdTime;   //#bkesh : remmoved (int)
		}
		
		if (!isUnsignedInt($this->userID)) {
		 	throw new Exception('Error in Activity data: userID not set');
		}
		else {
			$this->userID = (int) $this->userID;
		}
		
		if (!isset($this->userType) || strlen(trim($this->userType)) == 0) {
		 	throw new Exception('Error in Activity data: userType not set');
		}
		
		if (!isset($this->action) || strlen(trim($this->action)) == 0) {
		 	throw new Exception('Error in Activity data: action not set');
		}
		
		if (!isUnsignedInt($this->modelID)){ 
			$this->modelID = NULL;
		}
		else {
			$this->modelID = (int) $this->modelID;
		}
		
		if (!isset($this->modelType) || strlen(trim($this->modelType)) == 0) {
		 	$this->modelType = NULL;
		}
	} //checkActivityData()
	
	/*******************
	 *   MODEL LOGIC   *
	 *******************/
	//For DB to set ID after creation
	function setID($input) {
		if (!isUnsignedInt($input)){
			throw new Exception ('Error in Activity setID: invalid id');
		}
		$this->id = (int) $input;
	} //setID()
	
	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->id;
	}
	
	function getCreatedTime() {
		return $this->createdTime;
	}
	
	function getUserID() {
		return $this->userID;
	}
	
	function getUserType() {
		return $this->userType;
	}
	
	function getAction() {
		return $this->action;
	}
	
	function getModelID() {
		return $this->modelID;
	}
	
	function getModelType() {
		return $this->modelType;
	}
} // class Activity
