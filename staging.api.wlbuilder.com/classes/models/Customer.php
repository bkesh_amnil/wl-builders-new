<?php
class Customer extends User {
	// Compulsory fields
	private $id;					// int (DEFAULT: 0)
	private $email;					// string
	private $emailVerified;			// bool: access to login (DEFAULT: false)
	private $emailVerificationHash;	// string (hash of 6-char code used in activation)
	private $referrerID;			// int (DEFAULT: NULL)
	private $referralCount;			// int (DEFAULT: 0)
	private $credits;				// int (DEFAULT: 0)
	
	// Other optional fields (DEFAULT: NULL)
	private $firstName;				// string
	private $lastName;				// string
	private $profession;			// string
	private $company;				// string
	private $residenceCountryID;	// int
	private $residenceStateID;		// int
	private $residencePhone;		// string (better choice than int due to max int limitation)
	private $homeCountryID;			// int
	private $homeStateID;			// int (3-char code ISO-3166-2)
	private $homePostalCode;		// string
	private $homeAddress;			// string
	private $homePhone;				// string (better choice than int due to max int limitation)
	private $officeCountryID;		// int (2-char code ISO-3166-1)
	private $officeStateID;			// int (3-char code ISO-3166-2)
	private $officePostalCode;		// string
	private $officeAddress;			// string
	private $officePhone;			// string (better choice than int due to max int limitation)
	private $birthTime;				// int (UNIX timestamp)
	
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists('Customer', $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in Customer class: method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->id;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()
	
	// Converts pre-selected data members into array
	function toOutputArray() {
		// Take everything except sensitive stuff (password)
		$array = get_object_vars($this);
		unset($array['passwordHash']);
		unset($array['persistentLoginHashes']);
		unset($array['emailVerificationHash']);
		
		// Add user type
		$array['userType'] = 'Customer';
		return $array;
	} //toOutputArray()
	
	// Table that this model should be stored
	static function getTableName() {
		return 'EB_Customer';
	} //getTableName()
	
	//Gets the default access level of this class of User (for use during parent object creation)
	static function getDefaultAccessLevel() {
		return CUSTOMER_ACCESS_LEVEL;
	} //getDefaultAccessLevel()
	
	/***********
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Only data members defined by this class will be processed; unrecognised args will be ignored.
	 ***********/
	function __construct($arg) {
		if (!is_array($arg)) throw new Exception('Error creating Customer: arg is not array');

		foreach ($arg as $key => $value) {
			if (property_exists('Customer', $key)) {
				$this->$key = $value;
			} //property exists
		} //parse each arg in the array

		$this->checkUserData();
		$this->checkCustomerData();
	} //construct()
	
	/***********
	 * Updates Customer with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * Params: an array of new data (key => value)
	 * Return: TRUE on success update, FALSE if no changes were made
	 ***********/
	function update($arg) {
		if (!is_array($arg)) throw new Exception('Error updating Customer: arg is not array');
		
		//$hasChanged = parent::update($arg);
		foreach ($arg as $key => $value) {
			if (property_exists('Customer', $key)) {
				if ((string) $this->$key != $value) {
					//Log::test($key.' changed from '.$this->$key.' to '.$value);
					$this->$key = $value;
					$hasChanged = true;
				} //update only if different
			} //property exists
		} //parse each arg in the array
		
		$this->checkUserData();
		$this->checkCustomerData();
		
		return $hasChanged;
	} //update()
	
	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	private function checkCustomerData() {
		if (!isUnsignedInt($this->id)){ 
			$this->id = 0;
		}
		else {
			$this->id = (int) $this->id;
		}
		
		if (!isValidEmail($this->email)) {
			throw new Exception('Error in Customer data: invalid email: '.$this->email);
		}
		
		if (!isset($this->emailVerified) || $this->emailVerified != TRUE) {
			$this->emailVerified = FALSE;
		}
		else {
			$this->emailVerified = TRUE;
		}
		
		if (!isset($this->emailVerificationHash) || strlen(trim($this->emailVerificationHash)) == 0) {
			$this->emailVerificationHash = NULL;
		}
		
		if (!isUnsignedInt($this->referrerID)) {
			$this->referrerID = NULL;
		}
		
		if (!isUnsignedInt($this->referralCount)) {
			$this->referralCount = 0;
		}
		
		if (!isUnsignedInt($this->credits)) {
			$this->credits = 0;
		}
		
		if (!isset($this->firstName) || strlen(trim($this->firstName)) == 0){
		 	$this->firstName = NULL;
		}
		if (!isset($this->lastName) || strlen(trim($this->lastName)) == 0){
		 	$this->lastName = NULL;
		}
		if (!isset($this->profession) || strlen(trim($this->profession)) == 0){
		 	$this->profession = NULL;
		}
		if (!isset($this->company) || strlen(trim($this->company)) == 0){
		 	$this->company = NULL;
		}
		
		if (!isUnsignedInt($this->residenceCountryID)) {
			$this->residenceCountryID = NULL;
		}
		if (!isUnsignedInt($this->residenceStateID)) {
			$this->residenceStateID = NULL;
		}
		if (!isUnsignedInt($this->residencePhone)) {
			$this->residencePhone = NULL;
		}
		
		if (!isUnsignedInt($this->homeCountryID)){
			$this->homeCountryID = NULL;
		}
		
		if (!isUnsignedInt($this->homeStateID)) {
			$this->homeStateID = NULL;
		}
		
		if (!isset($this->homePostalCode) || strlen(trim($this->homePostalCode)) == 0) {
			$this->homePostalCode = NULL;
		}
				
		if (!isset($this->homeAddress) || strlen(trim($this->homeAddress)) == 0){
			$this->homeAddress = NULL;
		}
		
		if (!isUnsignedInt($this->homePhone)) {
			$this->homePhone = NULL;
		}
		
		if (!isUnsignedInt($this->officeCountryID)){
			$this->officeCountryID = NULL;
		}
		
		if (!isUnsignedInt($this->officeStateID)) {
			$this->officeStateID = NULL;
		}
		
		if (!isset($this->officePostalCode) || strlen(trim($this->officePostalCode)) == 0) {
			$this->officePostalCode = NULL;
		}
				
		if (!isset($this->officeAddress) || strlen(trim($this->officeAddress)) == 0){
			$this->officeAddress = NULL;
		}
		
		if (!isUnsignedInt($this->officePhone)) {
			$this->officePhone = NULL;
		}
		
		if (!isInt($this->birthTime)) {
			$this->birthTime = NULL;
		}
		else {
			$this->birthTime = (int) $this->birthTime;
		}
		
		if (!isUnsignedInt($this->referrerID)) {
			$this->referrerID = NULL;
		}
		else {
			$this->referrerID = (int) $this->referrerID;
		}
		
		if (!isUnsignedInt($this->referralCount)) {
			$this->referralCount = NULL;
		}
		else {
			$this->referralCount = (int) $this->referralCount;
		}
		
		if (!isUnsignedInt($this->credits)) {
			$this->credits = NULL;
		}
		else {
			$this->credits = (int) $this->credits;
		}
	} //checkCustomerData()
	
	
	/*******************
	 *   MODEL LOGIC   *
	 *******************/
	//For DB to set ID after creation
	function setID($input) {
		if (!isUnsignedInt($input)){
			throw new Exception ('Error in Customer setID: invalid id');
		}
		$this->id = (int) $input;
	} //setID()
	
	/***
	 * Checks if an input email verification code is correct and time-valid (within expiry time).
	 * @param: string of code
	 * @return: TRUE on correct and valid, FALSE otherwise
	 ***/
	function checkEmailVerificationCode($code) {
		return Encryptor::checkTimeSensitiveCode($code, $this->emailVerificationHash, EMAIL_VERIFICATION_EXPIRY);
	}
	
	/***
	 * Checks if there's a credit reward for hitting a certain number of referrals.
	 * @param: int of referral number
	 * @return: int  of cents of credits
	 ***/
	function checkReferralReward($count) {
		if ($count == REFERRAL_MILESTONE_1_THRESHOLD) return REFERRAL_MILESTONE_1_REWARD;
		if ($count == REFERRAL_MILESTONE_2_THRESHOLD) return REFERRAL_MILESTONE_2_REWARD;
		if ($count == REFERRAL_MILESTONE_3_THRESHOLD) return REFERRAL_MILESTONE_3_REWARD;
		if ($count == REFERRAL_MILESTONE_4_THRESHOLD) return REFERRAL_MILESTONE_4_REWARD;
		return 0;
	}
	
	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->id;
	}
	function getFirstName() {
		return $this->firstName;
	}
	function getLastName() {
		return $this->lastName;
	}
	function getProfession() {
		return $this->profession;
	}
	function getName() {
		if (is_null($this->firstName) && is_null($this->lastName)) {
			return NULL;
		}
		return $this->firstName . ' ' . $this->lastName;
	}
	function getEmail() {
		return $this->email;
	}
	function isEmailVerified() {
		return $this->emailVerified;
	}
	function getEmailVerificationHash() {
		return $this->emailVerificationHash;
	}
	function getReferrerID() {
		return $this->referrerID;
	}
	function getReferralCount() {
		return $this->referralCount;
	}
	function getCredits() {
		return $this->credits;
	}
	function getResidenceCountryID() {
		return $this->residenceCountryID;
	}
	function getResidenceStateID() {
		return $this->residenceStateID;
	}
	function getResidencePhone() {
		return $this->residencePhone;
	}
	function getHomeCountryID() {
		return $this->homeCountryID;
	}
	function getHomeStateID() {
		return $this->homeStateID;
	}
	function getHomeAddress() {
		return $this->homeAddress;
	}
	function getHomePostalCode() {
		return $this->homePostalCode;
	}
	function getHomePhone() {
		return $this->homePhone;
	}
	function getOfficeCountryID() {
		return $this->officeCountryID;
	}
	function getOfficeStateID() {
		return $this->officeStateID;
	}
	function getOfficeAddress() {
		return $this->officeAddress;
	}
	function getOfficePostalCode() {
		return $this->officePostalCode;
	}
	function getOfficePhone() {
		return $this->officePhone;
	}
	function getBirthTime() {
		return $this->birthTime;
	}
	function getUserType() {
		return 'Customer';
	}
} //class Customer