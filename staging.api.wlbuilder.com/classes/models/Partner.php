<?php
class Partner {
	// Compulsory fields
	private $id;					// int (DEFAULT: 0)
	private $description='';				// string
	private $email='';
	private $name;
	private $displayPriority;		// int (DEFAULT: 0)
	private $createdTime;			// int (UNIX timestamp)
	private $lastUpdateTime;		// int (UNIX timestamp)
	
	// Optional
	 private $url;					// string
	 private $websiteURL=null;
	
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists('Partner', $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in Partner class: method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->id;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()
	
	// Converts pre-selected data members into array
	function toOutputArray() {
		return get_object_vars($this);
	}
	
	// Table that this model should be stored
	static function getTableName() {
		return 'WB_Partner';
	} //getTableName()
	
	/***********
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Only data members defined by this class will be processed; unrecognised args will be ignored.
	 ***********/
	function __construct($arg) {
		if (!is_array($arg)) throw new Exception('Error creating Partner: arg is not array');

		foreach ($arg as $key => $value) {
			if (property_exists('Partner', $key)) {
				$this->$key = $value;
			} //property exists
		} //parse each arg in the array

		$this->checkPartnerData();
	} //construct()
	
	/***********
	 * Updates Partner with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * Params: an array of new data (key => value)
	 * Return: TRUE on success update, FALSE if no changes were made
	 ***********/
	function update($arg) {
		if (!is_array($arg)) throw new Exception('Error updating Partner: arg is not array');
		
		foreach ($arg as $key => $value) {
			if (property_exists('Partner', $key)) {
				if ((string) $this->$key != $value) {
					//Log::test($key.' changed from '.$this->$key.' to '.$value);
					$this->$key = $value;
					$hasChanged = true;
				} //update only if different
			} //property exists
		} //parse each arg in the array
		
		$this->checkPartnerData();
		
		return $hasChanged;
	} //update()
	
	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	private function checkPartnerData() {		
		if (!isUnsignedInt($this->id)){ 
			$this->id = 0;
		}
		else {
			$this->id = (int) $this->id;
		}
		
		if (!isset($this->description) || strlen(trim($this->description))==0) {
			//throw new Exception('Error in Partner data: description not set');
		    $this->description = '';
		}
                if (!isset($this->email) || strlen(trim($this->email))==0) {
			$this->email = '';
			
		}
//		if (!isset($this->email) || strlen(trim($this->email))==0) {
//			throw new Exception('Error in Partner data: email not set');
//		}
//		if (!isset($this->name) || strlen(trim($this->name))==0) {
//			throw new Exception('Error in Partner data: name not set');
//		}
				
		if (!isUnsignedInt($this->displayPriority)) {
			$this->displayPriority = 0;
		}
		else {
			$this->displayPriority = (int) $this->displayPriority;
		}
		
		if (!isUnsignedInt($this->createdTime)) {
			$this->createdTime = getTimeInMs();
		}
		else {
			$this->createdTime = $this->createdTime;
		}
		
		if (!isUnsignedInt($this->lastUpdateTime)) {
			$this->lastUpdateTime = $this->createdTime;
		}
		else {
			$this->lastUpdateTime = $this->lastUpdateTime;
		}
		
		// Optional fields
		if (!isset($this->url) || strlen(trim($this->url))==0) {
			$this->url = NULL;
			
		}
	} //checkPartnerData()
	
	/*******************
	 *   MODEL LOGIC   *
	 *******************/
	//For DB to set ID after creation
	function setID($input) {
		if (!isUnsignedInt($input)){
			throw new Exception ('Error in Partner setID: invalid id');
		}
		$this->id = (int) $input;
	} //setID()
	
	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->id;
	}
	function getDescription() {
		return $this->description;
	}
	function getWebsite() {
		return $this->websiteURL;
	}
	function getEmail() {
		return $this->email;
	}
	function getName() {
		return $this->name;
	}
	
	function getDisplayPriority() {
		return $this->displayPriority;
	}
	function getCreatedTime() {
		return $this->createdTime;
	}
	function getLastUpdateTime() {
		return $this->lastUpdateTime;
	}
	
	function getURL() {
		return $this->url;
	}
} //class Partner