<?php
class Project {
	// Compulsory fields
	private $id;					// int (DEFAULT: 0)
	private $name;				// string
	private $location;				// string
	private $longitude;				// string
	private $latitude;				// string
	private $description;				// string
	private $leaseHold;
	private $buildUpArea;
	private $buildInArea;
	private $bedRoom;
	private $shower;
	private $estimatedCost;
	private $startMonth;
	private $startYear;
	private $endMonth;
	private $endYear;
	private $demolitionWork;
	private $structureWork;
	private $meWork;
	private $externalWork;
	private $architecturalWork;
	private $isFeaturedInStaticSection;
	private $isFeaturedInPanoramicSection;
	private $displayPriority;		// int (DEFAULT: 0)
	private $createdTime;			// int (UNIX timestamp)
	private $lastUpdateTime;		// int (UNIX timestamp)
	
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists('Project', $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in Project class: method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->id;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()
	
	// Converts pre-selected data members into array
	function toOutputArray() {
		return get_object_vars($this);
	}
	
	// Table that this model should be stored
	static function getTableName() {
		return 'WB_Project';
	} //getTableName()
	
	/***********
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Only data members defined by this class will be processed; unrecognised args will be ignored.
	 ***********/
	function __construct($arg) {
		if (!is_array($arg)) throw new Exception('Error creating Project: arg is not array');

		foreach ($arg as $key => $value) {
			if (property_exists('Project', $key)) {
				$this->$key = $value;
			} //property exists
		} //parse each arg in the array

		$this->checkProjectData();
	} //construct()
	
	/***********
	 * Updates Project with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * Params: an array of new data (key => value)
	 * Return: TRUE on success update, FALSE if no changes were made
	 ***********/
	function update($arg) {
		if (!is_array($arg)) throw new Exception('Error updating Project: arg is not array');
		
		foreach ($arg as $key => $value) {
			if (property_exists('Project', $key)) {
				if ((string) $this->$key != $value) {
					//Log::test($key.' changed from '.$this->$key.' to '.$value);
					$this->$key = $value;
					$hasChanged = true;
				} //update only if different
			} //property exists
		} //parse each arg in the array
		
		$this->checkProjectData();
		
		return $hasChanged;
	} //update()
	
	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	private function checkProjectData() {
		if (!isUnsignedInt($this->id)){
			$this->id = 0;
		}
		else {
			$this->id = (int) $this->id;
		}
		if (!isset($this->name) || strlen(trim($this->name))==0) {
			throw new Exception('Error in Project data: name not set');
		}
                if (!isset($this->location) || strlen(trim($this->location)) == 0){
		 	$this->location = NULL;
		}
                if (!isset($this->longitude) || strlen(trim($this->longitude)) == 0){
		 	$this->longitude = NULL;
		}
                if (!isset($this->latitude) || strlen(trim($this->latitude)) == 0){
		 	$this->latitude = NULL;
		}
                if (!isset($this->description) || strlen(trim($this->description)) == 0){
		 	$this->description = NULL;
		}
                if (!isset($this->leaseHold) || strlen(trim($this->leaseHold)) == 0){
		 	$this->leaseHold = NULL;
		}
                if (!isset($this->buildUpArea) || strlen(trim($this->buildUpArea)) == 0){
		 	$this->buildUpArea = NULL;
		}
                if (!isset($this->buildInArea) || strlen(trim($this->buildInArea)) == 0){
		 	$this->buildInArea = NULL;
		}
                if (!isset($this->bedRoom) || strlen(trim($this->bedRoom)) == 0){
		 	$this->bedRoom = NULL;
		}
                if (!isset($this->shower) || strlen(trim($this->shower)) == 0){
		 	$this->shower = NULL;
		}
                if (!isset($this->estimatedCost) || strlen(trim($this->estimatedCost)) == 0){
		 	$this->estimatedCost = NULL;
		}
                if (!isset($this->startMonth) || strlen(trim($this->startMonth)) == 0){
		 	$this->startMonth = NULL;
		}
                if (!isset($this->startYear) || strlen(trim($this->startYear)) == 0){
		 	$this->startYear = NULL;
		}
                if (!isset($this->endMonth) || strlen(trim($this->endMonth)) == 0){
		 	$this->endMonth = NULL;
		}
                if (!isset($this->endYear) || strlen(trim($this->endYear)) == 0){
		 	$this->endYear = NULL;
		}
                if (!isset($this->demolitionWork) || strlen(trim($this->demolitionWork)) == 0){
		 	$this->demolitionWork = NULL;
		}
                if (!isset($this->structureWork) || strlen(trim($this->structureWork)) == 0){
		 	$this->structureWork = NULL;
		}
                if (!isset($this->meWork) || strlen(trim($this->meWork)) == 0){
		 	$this->meWork = NULL;
		}
                if (!isset($this->externalWork) || strlen(trim($this->externalWork)) == 0){
		 	$this->externalWork = NULL;
		}
                if (!isset($this->architecturalWork) || strlen(trim($this->architecturalWork)) == 0){
		 	$this->architecturalWork = NULL;
		}
                if (!isset($this->isFeaturedInStaticSection) || strlen(trim($this->isFeaturedInStaticSection)) == 0){
		 	$this->isFeaturedInStaticSection = 0;
		}
                if (!isset($this->isFeaturedInPanoramicSection) || strlen(trim($this->isFeaturedInPanoramicSection)) == 0){
		 	$this->isFeaturedInPanoramicSection = 0;
		}
                
				
		if (!isUnsignedInt($this->displayPriority)) {
			$this->displayPriority = 0;
		}
		else {
			$this->displayPriority = (int) $this->displayPriority;
		}
		
		if (!isUnsignedInt($this->createdTime)) {
			$this->createdTime = getTimeInMs();
		}
		else {
			$this->createdTime =  $this->createdTime; //#bkesh : remmoved (int)
		}
		
		if (!isUnsignedInt($this->lastUpdateTime)) {
			$this->lastUpdateTime = getTimeInMs();
		}
		else {
			$this->lastUpdateTime =  $this->lastUpdateTime; //#bkesh : remmoved (int)
		}
                
	} //checkProjectData()
        
	
	/*******************
	 *   MODEL LOGIC   *
	 *******************/
	//For DB to set ID after creation
	function setID($input) {
		if (!isUnsignedInt($input)){
			throw new Exception ('Error in Project setID: invalid id');
		}
		$this->id = (int) $input;
	} //setID()
	
	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->id;
	}
	function getName() {
		return $this->name;
	}
	function getLocation() {
		return $this->location;
	}
	function getLongitute() {
		return $this->longitude;
	}
	function getLatitude() {
		return $this->latitude;
	}
        function getDescription() {
		return $this->description;
	}
	function getLeaseHold() {
		return $this->leaseHold;
	}
	
	function getBuildUpArea() {
		return $this->buildUpArea;
	}
	function getBuildInArea() {
		return $this->buildInArea;
	}
	
	function getBedRoom() {
		return $this->bedRoom;
	}
	function getShower() {
		return $this->shower;
	}
	function getEstimatedCost() {
		return $this->estimatedCost;
	}
	function getStartMonth() {
		return $this->startMonth;
	}
	function getStartYear() {
		return $this->startYear;
	}
	function getEndMonth() {
		return $this->endMonth;
	}
	function getEndYear() {
		return $this->endYear;
	}
	function getDemolitionWork() {
		return $this->demolitionWork;
	}
	function getStructureWork() {
		return $this->structureWork;
	}
	function getMeWork() {
		return $this->meWork;
	}
	function getExternalWork() {
		return $this->externalWork;
	}
	function getArchitecturalWork() {
		return $this->architecturalWork;
	}
	function getIsFeaturedInStaticSection() {
		return $this->isFeaturedInStaticSection;
	}
	function getIsFeaturedInPanoramicSection() {
		return $this->isFeaturedInPanoramicSection;
	}
	
	function getCreatedTime() {
		return $this->createdTime;
	}
	function getLastUpdateTime() {
		return $this->lastUpdateTime;
	}
	function getDisplayPriority() {
		return $this->displayPriority;
	}
} //class Project