<?php
class ProjectBluePrint {
	// Compulsory fields
	private $id;					// int (DEFAULT: 0)
	private $projectID;				// string
	private $urlLow1;				// string
	private $urlLow2;				// string
	private $urlLow3;				// string
	private $urlLow4;				// string
	private $urlHigh1;
	private $urlHigh2;
	private $urlHigh3;
	private $urlHigh4;
	
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists('ProjectBluePrint', $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in ProjectBluePrint class: method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->id;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()
	
	// Converts pre-selected data members into array
	function toOutputArray() {
		return get_object_vars($this);
	}
	
	// Table that this model should be stored
	static function getTableName() {
		return 'WB_Project_blueprint';
	} //getTableName()
	
	/***********
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Only data members defined by this class will be processed; unrecognised args will be ignored.
	 ***********/
	function __construct($arg) {
		if (!is_array($arg)) throw new Exception('Error creating ProjectBluePrint: arg is not array');

		foreach ($arg as $key => $value) {
			if (property_exists('ProjectBluePrint', $key)) {
				$this->$key = $value;
			} //property exists
		} //parse each arg in the array

		$this->checkProjectData();
	} //construct()
	
	/***********
	 * Updates Project with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * Params: an array of new data (key => value)
	 * Return: TRUE on success update, FALSE if no changes were made
	 ***********/
	function update($arg) {
		if (!is_array($arg)) throw new Exception('Error updating Project: arg is not array');
		
		foreach ($arg as $key => $value) {
			if (property_exists('ProjectBluePrint', $key)) {
				if ((string) $this->$key != $value) {
					//Log::test($key.' changed from '.$this->$key.' to '.$value);
					$this->$key = $value;
					$hasChanged = true;
				} //update only if different
			} //property exists
		} //parse each arg in the array
		
		$this->checkProjectData();
		
		return $hasChanged;
	} //update()
	
	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	private function checkProjectData() {
		if (!isUnsignedInt($this->id)){
			$this->id = 0;
		}
		else {
			$this->id = (int) $this->id;
		}
		if (!isUnsignedInt($this->projectID)){
			$this->projectID = 0;
		}
		else {
			$this->projectID = (int) $this->projectID;
		}
                if (!isset($this->urlLow1) || strlen(trim($this->urlLow1)) == 0){
		 	$this->urlLow1 = NULL;
		}
                if (!isset($this->urlLow2) || strlen(trim($this->urlLow2)) == 0){
		 	$this->urlLow2 = NULL;
		}
                if (!isset($this->urlLow3) || strlen(trim($this->urlLow3)) == 0){
		 	$this->urlLow3 = NULL;
		}
                if (!isset($this->urlLow4) || strlen(trim($this->urlLow4)) == 0){
		 	$this->urlLow4 = NULL;
		}
                if (!isset($this->urlHigh1) || strlen(trim($this->urlHigh1)) == 0){
		 	$this->urlHigh1 = NULL;
		}
                if (!isset($this->urlHigh2) || strlen(trim($this->urlHigh2)) == 0){
		 	$this->urlHigh2 = NULL;
		}
                if (!isset($this->urlHigh3) || strlen(trim($this->urlHigh3)) == 0){
		 	$this->urlHigh3 = NULL;
		}
                if (!isset($this->urlHigh4) || strlen(trim($this->urlHigh4)) == 0){
		 	$this->urlHigh4 = NULL;
		}
                
	} //checkProjectData()
        
	
	/*******************
	 *   MODEL LOGIC   *
	 *******************/
	//For DB to set ID after creation
	function setID($input) {
		if (!isUnsignedInt($input)){
			throw new Exception ('Error in Project setID: invalid id');
		}
		$this->id = (int) $input;
	} //setID()
	
	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->id;
	}
	function getProjectID() {
		return $this->projectID;
	}
	function getUrlLow1() {
		return $this->urlLow1;
	}
	function getUrlLow2() {
		return $this->urlLow2;
	}
	function getUrlLow3() {
		return $this->urlLow3;
	}
	function getUrlLow4() {
		return $this->urlLow4;
	}
        function getUrlHigh1() {
		return $this->urlHigh1;
	}
	function getUurlHigh2() {
		return $this->urlHigh2;
	}	
	function geturlHigh3() {
		return $this->urlHigh3;
	}
	function geturlHigh4() {
		return $this->urlHigh4;
	}
} //class Project