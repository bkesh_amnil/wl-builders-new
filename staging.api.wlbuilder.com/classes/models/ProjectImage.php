<?php
class ProjectImage {
	// Compulsory fields
	private $id;					// int (DEFAULT: 0)
	private $projectID;				// string
	private $name;				// string
	private $imgUrl;				// string
//	private $description;				// string
//	private $level;				// string
	private $isPanorama;				// string
	private $isStaticSectionFeaturedImage;
	private $isPanoramicSectionFeaturedImage;
//	private $isProjectImage;
	private $createdTime;
	private $lastUpdateTime;
	private $displayPriority;
	
	// Default function: checks if this class has a property
	static function hasProperty($a) {
		return property_exists('ProjectImage', $a);
	} //hasProperty()
	
	// Default function: make invalid method calls throw Exceptions
	function __call($name, $arg) {
		throw new Exception ('Error in ProjectImage class: method '.$name.'() does not exist');
	} //call()
	
	// Default function: make this a printable string
	function __toString() {
		return get_class($this).' '.$this->id;
	} //toString()
	
	// Default function: converts ALL data members into array
	function toArray() {
		return get_object_vars($this);
	} //toArray()
	
	// Converts pre-selected data members into array
	function toOutputArray() {
		return get_object_vars($this);
	}
	
	// Table that this model should be stored
	static function getTableName() {
		return 'WB_Project_image';
	} //getTableName()
	
	/***********
	 * Constructor takes an array of args, and maps each arg into a data member
	 * Only data members defined by this class will be processed; unrecognised args will be ignored.
	 ***********/
	function __construct($arg) {
		if (!is_array($arg)) throw new Exception('Error creating ProjectImage: arg is not array');

		foreach ($arg as $key => $value) {
			if (property_exists('ProjectImage', $key)) {
				$this->$key = $value;
			} //property exists
		} //parse each arg in the array

		$this->checkProjectData();
	} //construct()
	
	/***********
	 * Updates Project with an array of new data (e.g. from $_POST)
	 * Note: only data members defined by the class will be processed; unrecognised args will be ignored.
	 *		 Throws exceptions on invalid compulsory data, e.g. non-numeric id, empty names
	 * Params: an array of new data (key => value)
	 * Return: TRUE on success update, FALSE if no changes were made
	 ***********/
	function update($arg) {
		if (!is_array($arg)) throw new Exception('Error updating Project: arg is not array');
		
		foreach ($arg as $key => $value) {
			if (property_exists('ProjectImage', $key)) {
				if ((string) $this->$key != $value) {
					//Log::test($key.' changed from '.$this->$key.' to '.$value);
					$this->$key = $value;
					$hasChanged = true;
				} //update only if different
			} //property exists
		} //parse each arg in the array
		
		$this->checkProjectData();
		
		return $hasChanged;
	} //update()
	
	/*
	 * Checks that each data member is valid:
	 * - throws Exceptions when critical data is invalid
	 * - sets other compulsory fields with invalid data to their default values
	 * - sets optional fields with invalid data to NULL
	 */
	private function checkProjectData() {
		if (!isUnsignedInt($this->id)){
			$this->id = 0;
		}
		else {
			$this->id = (int) $this->id;
		}
		if (!isUnsignedInt($this->projectID)){
			$this->projectID = 0;
		}
		else {
			$this->projectID = (int) $this->projectID;
		}
                if (!isset($this->name) || strlen(trim($this->name))==0) {
			throw new Exception('Error in Project Image data: name not set');
		}
                if (!isset($this->imgUrl) || strlen(trim($this->imgUrl)) == 0){
		 	$this->imgUrl = NULL;
		}
//                if (!isset($this->description) || strlen(trim($this->description)) == 0){
//		 	$this->description = '';
//		}
//                if (!isset($this->level) || strlen(trim($this->level)) == 0){
//		 	$this->level = 0;
//		}
                if (!isset($this->isPanorama) || strlen(trim($this->isPanorama)) == 0){
		 	$this->isPanorama = 0;
		}
                if (!isset($this->isStaticSectionFeaturedImage) || strlen(trim($this->isStaticSectionFeaturedImage)) == 0){
		 	$this->isStaticSectionFeaturedImage = 0;
		}
                if (!isset($this->isPanoramicSectionFeaturedImage) || strlen(trim($this->isPanoramicSectionFeaturedImage)) == 0){
		 	$this->isPanoramicSectionFeaturedImage = 0;
		}
//		if (!isset($this->isProjectImage) || strlen(trim($this->isProjectImage)) == 0){
//		 	$this->isProjectImage = 0;
//		}
                if (!isUnsignedInt($this->displayPriority)) {
			$this->displayPriority = 0;
		}
		else {
			$this->displayPriority = (int) $this->displayPriority;
		}
                if (!isUnsignedInt($this->createdTime)) {
			$this->createdTime = getTimeInMs();
		}
		else {
			$this->createdTime =  $this->createdTime; //#bkesh : remmoved (int)
		}
		
		if (!isUnsignedInt($this->lastUpdateTime)) {
			$this->lastUpdateTime = getTimeInMs();
		}
		else {
			$this->lastUpdateTime =  $this->lastUpdateTime; //#bkesh : remmoved (int)
		}
                
	} //checkProjectData()
        
	
	/*******************
	 *   MODEL LOGIC   *
	 *******************/
	//For DB to set ID after creation
	function setID($input) {
		if (!isUnsignedInt($input)){
			throw new Exception ('Error in Project setID: invalid id');
		}
		$this->id = (int) $input;
	} //setID()
	
	/***************
	 *   GETTERS   *
	 ***************/
	function getID() {
		return $this->id;
	}
	function getProjectID() {
		return $this->projectID;
	}
	function getName() {
		return $this->name;
	}
	function getImgUrl() {
		return $this->imgUrl;
	}
//        function getDescription() {
//		return $this->description;
//	}
//        function getLevel() {
//		return $this->level;
//	}
        function getIsPanorama() {
		return $this->isPanorama;
	}
        function getIsStaticSectionFeaturedImage() {
		return $this->isStaticSectionFeaturedImage;
	}
        function getIsPanoramicSectionFeaturedImage() {
		return $this->isPanoramicSectionFeaturedImage;
	}
//	function getIsProjectImage() {
//		return $this->isProjectImage;
//	}
	function getCreatedTime() {
		return $this->createdTime;
	}
	function getLastUpdateTime() {
		return $this->lastUpdateTime;
	}
	function getDisplayPriority() {
		return $this->displayPriority;
	}
} //class Project