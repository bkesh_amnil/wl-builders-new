<?php
/****************************************************
 * Collection of functions to process images.       *
 ****************************************************/
class ImageProcessor {
	//Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in ImageProcessor class: method '.$name.'() does not exist');
	} //call()
	
	/***
	 * Resizes an image and saves it in a new image file
	 * @input: {string} src file path, 
	 *         {array} options,
	 *         {string} new file name (optional),
	 * @return: new file path, NULL on error
	 * - Options:
	 *       width {int}: desired width - if omitted, will use height and resize keeping aspect ratio,
	 *       height {int}: desired height - if omitted, will use width and resize keeping aspect ratio,
	 *       minWidth, minHeight {int}: only used if width and height are both not given - will keep aspect ratio, and ensure resultant image is at least minWidth x minHeight
	 *       quality [1-100] (optional),
	 * - Tip: useful for creating small thumbnails, but don't use it to create expanded images.
	 * - Note: directory must be read/writeable
	 * - Note: if new file path is not specified, it will overwrite old file.
	 * - Note: if destination file already exists, it will be overwritten.
	 ***/
	private static function resizeImage($file_path, $options, $new_file_path=NULL, $image_create_func, $image_save_func) {
		// Set default quality
		if (!isset($options['quality'])) {
			if ($image_save_func == 'imagejpeg') {
				$quality = 90;
			}
			else if ($image_save_func == 'imagepng') {
				$quality = 9;
			}
			else if ($image_save_func == 'imagegif') {
				$quality = NULL;
			}
		}
		else {
			$quality = $options['quality'];
		}
		
		// Calculate new dimensions
		$height=0;
		$width=0;
		list($orig_width, $orig_height) = getimagesize($file_path);
		$aspect_ratio = $orig_width / $orig_height;
		if (isset($options['width'])) {
			if (isset($options['height'])) {
				$width = $options['width'];
				$height = $options['height'];
			} // fixed width and height
			else {
				$width = $options['width'];
				$height = $width / $aspect_ratio;
			}
		} // fixed witdh
		else if (isset($options['height'])) {
			$height = $options['height'];
			$width = $height * $aspect_ratio;
		} // fixed height
		else if (isset($options['minWidth']) && isset($options['minHeight'])) {
			$option_ratio = $options['minWidth'] / $options['minHeight'];
			if ($aspect_ratio > $option_ratio) {
				$height = $options['minHeight'];
				$width = $height * $aspect_ratio;
			}
			else {
				$width = $options['minWidth'];
				$height = $width / $aspect_ratio;
			}
		} // minWidth and minHeight
		else {
			Log::error('ImageProcessor::resizeImage() - Nothing specified for: '.$new_file_path);
			return NULL;
		} // nothing specified: error case
		
		// Trivial case: simply copy file if required and return
		if ($orig_width == $width && $orig_height == $height) {
			// Check if we need to copy file
			if (is_null($new_file_path)) {
				return $file_path;
			}
			
			// copy file over to a new file
			copy($file_path, $new_file_path);
			return $new_file_path;
		}
		
		// Create new image
		$image_new = imagecreatetruecolor($width, $height);
		
		// For transparency in PNG
		if ($image_create_func == 'imagecreatefrompng') {
			imagealphablending($image_new, FALSE);
			imagesavealpha($image_new, TRUE);
		}
		
		// Copy image into new image with size
		$image = $image_create_func($file_path);	
		imagecopyresampled($image_new, $image, 0, 0, 0, 0, $width, $height, $orig_width, $orig_height);
		
		// Save to file
		if (is_null($new_file_path)) {
			$new_file_path = $file_path;
		}
		if ($image_save_func == 'imagegif') {
			$image_save_func($image_new, $new_file_path);
		}
		else {
			$image_save_func($image_new, $new_file_path, $quality);
		}
		
		return $new_file_path;
	} //resizeImage()
	
	static function resizeJPG($file_path, $options, $new_file_path=NULL) {
		return self::resizeImage($file_path, $options, $new_file_path, 'imagecreatefromjpeg', 'imagejpeg');
	}
	static function resizePNG($file_path, $options, $new_file_path=NULL) {
		return self::resizeImage($file_path, $options, $new_file_path, 'imagecreatefrompng', 'imagepng');
	}
	static function resizeGIF($file_path, $options, $new_file_path=NULL) {
		return self::resizeImage($file_path, $options, $new_file_path, 'imagecreatefromgif', 'imagegif');
	}
	
	/***
	 * Crops a JPG and saves it as a new JPG in the same directory
	 * @param: {string} src file path, 
	 *         {array} options,
	 *         {string} new file name (optional),
	 * @return: new file path
	 * - Options:
	 *       fromX, fromY (2 x int): crop settings (DEFAULT: 0)
	 *       widtth, height (2 x int): crop settings
	 *       quality (int): [1-100],
	 * - Note: if height==0 or not specified, image will be auto-height keeping aspect ratio
	 * - Note: directory must be read/writeable
	 * - Note: if new file name is not specified, it will overwrite old file
	 * - Note: if destination file already exists, it will be overwritten.
	 ***/
	static function cropJPG($file_path, $options, $new_file_path=NULL) {
		// Set default quality
		if (!isset($options['quality'])) {
			$quality = 90;
		}
		else {
			$quality = $options['quality'];
		}
		
		// Set default crop position
		if (!isset($options['fromX'])) {
			$from_x = 0;
		}
		else {
			$from_x = $options['fromX'];
		}
		if (!isset($options['fromY'])) {
			$from_y = 0;
		}
		else {
			$from_y = $options['fromY'];
		}
		
		// Calculate crop dimensions
		list($orig_width, $orig_height) = getimagesize($file_path);
		$aspect_ratio = $orig_width / $orig_height;
		if (isset($options['width'])) {
			if (isset($options['height'])) {
				$width = $options['width'];
				$height = $options['height'];
			} // fixed width and height
			else {
				$width = $options['width'];
				$height = $width / $aspect_ratio;
			}
		} // fixed witdh
		else if (isset($options['height'])) {
			$height = $options['height'];
			$width = $height * $aspect_ratio;
		} // fixed height
		else {
			return NULL;
		} // both width and height not specified: error case
		
		// Trivial case: simply copy file if required and return
		if ($orig_width == $width && $orig_height == $height && $from_x == 0  && $from_y == 0) {
			// Check if we need to copy file
			if (is_null($new_file_path)) {
				return $file_path;
			}
			
			// copy file over to a new file
			copy($file_path, $new_file_path);
			return $new_file_path;
		}		
		// Create new image
		$image = imagecreatefromjpeg($file_path);
		$image_new = imagecreatetruecolor($width, $height);
		imagecopyresampled($image_new, $image, 0, 0, $from_x, $from_y, $width, $height, $width, $height);
		
		// Save to file
		if (is_null($new_file_path)) {
			$new_file_path = $file_path;
		}
		imagejpeg($image_new, $new_file_path, $quality);
		
		return $new_file_path;
	} //cropJPG()
	
	/***
	 * Converts a PNG to JPG and stores it in the same directory.
	 * @return: new file path
	 * - Note: if destination file already exists, it will be overwritten.
	 ***/
	static function PNG2JPG($png_file_path, $quality=90) {
		// Read image from PNG file
		$image = imagecreatefrompng($png_file_path);
		
		// Fill it with white bg (to replace transparency)
		$bg = imagecreatetruecolor(imagesx($image), imagesy($image));
		imagefill($bg, 0, 0, imagecolorallocate($bg, 255, 255, 255));
		imagealphablending($bg, TRUE);
		
		// Copy image onto the bg
		imagecopy($bg, $image, 0, 0, 0, 0, imagesx($image), imagesy($image));
		imagedestroy($image);
		
		// Create the JPG from bg
		$path_info = pathinfo($png_file_path);
		$new_file_path = $path_info['dirname'].'/'.$path_info['filename'].'.jpg';
		imagejpeg($bg, $new_file_path, $quality);
		imagedestroy($bg);
		
		return $new_file_path;
	} //PNG2JPG()
} //ImageProcessor
