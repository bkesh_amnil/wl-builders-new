<?php
/****************************************************
 *   Collection of functions for logging purposes   *
 ****************************************************/
class Log {
	//Default function: make invalid method calls throw Exceptions
	static function __callStatic($name, $arguments) {
		throw new Exception ('Error in Log class: method '.$name.'() does not exist');
	} //call()
	
	/*****************
	 * Log functions *
	 *****************/
	static function fatal($content) {
		$time = time();
		self::appendLog('['.date('Y-m-d H:i:s', $time).'] [FATAL] '.$content."\r\n");
	}
	
	static function error($content) {
		$time = time();
		self::appendLog('['.date('Y-m-d H:i:s', $time).'] [ERROR] '.$content."\r\n");
	}
	
	static function warning($content) {
		$time = time();
		self::appendLog('['.date('Y-m-d H:i:s', $time).'] [WARN] '.$content."\r\n");
	}
	
	static function info($content) {
		$time = time();
		self::appendLog('['.date('Y-m-d H:i:s', $time).'] [INFO] '.$content."\r\n");
	}
	
	static function test($content) {
		$time = time();
		self::appendLog('['.date('Y-m-d H:i:s', $time).'] [TEST] '.$content."\r\n");
	}
	
	static function debug($content) {
		$time = time();
		self::appendLog('['.date('Y-m-d H:i:s', $time).'] [DEBUG] '.$content."\r\n");
	}
	
	/***
	 * Appends message to log file
	 ***/
	private static function appendLog($content) {
		$time = time();
		$file_name = 'server-'.date('Y-m-d', $time).'.log';
		$file_path = APPROOT.'/logs/'.$file_name;
		
		if (!file_put_contents($file_path, $content, FILE_APPEND)) {
			throw new Exception('Unable to log to '.$file_path.' value: '.$content);
		}
	} //appendLog()
} //class Log