<?php
/*******************************/
/*  Set up pathing constants   */
/*******************************/
define('PROJECT_NAME', 'WL-Builders');

define('SERVER_PATH', '');// live


define('STAFF_URL', 'wl.staff.dev');//bkesh host
//define('STAFF_URL', 'staging.staff.wlbuilder.com');//staging

// Derive paths related to web server
define('PROTOCOL', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']) ? 'https://' : 'http://'));
define('DOMAIN', $_SERVER['SERVER_NAME'] . SERVER_PATH);
define('BASE_URL', PROTOCOL . DOMAIN);
define('WEBROOT', $_SERVER['DOCUMENT_ROOT'] . SERVER_PATH);
define('APPROOT', __DIR__);

// Mobile domain accessing this server (defining it will cause re-direction for phone devices)
//define ('MOBILE_DOMAIN', 'm.wlbuilder.com');

/*******************************/
/*  Set up Database constants  */
/*******************************/


// bkesh-localhost - remove the 2 preceding slashes to comment out this whole section
///*
define('WWW_ROOT', dirname(APPROOT).'/staging.www.wlbuilder.com');
define('DB_HOST', 'localhost'); // Use 127.0.0.1 instead of localhost (seems to cause slowness)
define('DB_NAME', 'wl-builder');
define('DB_USER', 'root');
define('DB_PASS', '');
//*/

// Staging - remove the 2 preceding slashes to comment out this whole section
/*
define('WWW_ROOT', dirname(API_ROOT).'/staging.www.wlbuilder.com');
define('DB_HOST', '10.240.44.227'); // Use 127.0.0.1 instead of localhost (seems to cause slowness)
define('DB_NAME', 'staging_wlbuilder');
define('DB_USER', 'wlbuilder_user');
define('DB_PASS', 'd#g4uRr+7B');
//*/



/*******************************/
/*      Names and Emails       */
/*******************************/
define('SESSION_DURATION', 3600);
// define('SESSION_NAME', 'wlbuilder');
define('SESSION_NAME', 'WLBUILDER');
define('SITE_NAME', 'WL Builder');
define('WEBMASTER_FIRST_NAME', 'Wl');
define('WEBMASTER_LAST_NAME', 'Builder');
define('WEBMASTER_EMAIL', 'isen@techplusart.com');
define('NO_REPLY_EMAIL', 'noreply@wlbuilder.com');
define('CONTACT_EMAIL', 'isen@techplusart.com');
define('ORDER_EMAIL', '');


// Facebook
define('FACEBOOK_APP_ID', 'to be filled');
define('FACEBOOK_APP_SECRET', 'to be filled');

/*******************************/
/*    Accounts and Passwords   */
/*******************************/
define('MIN_NAME_LENGTH', 3);
define('EMAIL_VERIFICATION_CODE_LENGTH', 6);
define('EMAIL_VERIFICATION_EXPIRY', 300000); // 5 mins == 300,000 milliseconds
define('CSRF_TOKEN_LENGTH', 32);
define('MIN_PASSWORD_LENGTH', 6);
define('PERSISTENT_LOGIN_COOKIE_LENGTH', 32);
define('PERSISTENT_LOGIN_PERIOD', 5184000000); // 60 days == 5184,000,000 milliseconds


/*******************************/
/*     User Access Levels      */
/*******************************/
define('DEFAULT_ACCESS_LEVEL', 0);
define('CUSTOMER_ACCESS_LEVEL', 1);
define('GENERAL_STAFF_ACCESS_LEVEL', 20);
define('ADMIN_ACCESS_LEVEL', 90);
define('SUPER_ADMIN_ACCESS_LEVEL', 99);
