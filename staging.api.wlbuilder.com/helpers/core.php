<?php
/***
 * IMPORTANT for older PHP versions (<5.4)
 * Get all HTTP request headers
 * Note: only works with Apache
 ***/
if (!function_exists('apache_request_headers')) {
	function apache_request_headers() {
		$headers = array();
		$regex_http = '/\AHTTP_/';
		foreach($_SERVER as $key => $val) {
			// ignore keys not starting with HTTP
			if(!preg_match($regex_http, $key) ) {
				continue;
			}
			
			// restore the original letter case (should work in most cases)
			$header_key = preg_replace($regex_http, '', $key); // remove starting HTTP_
			$regex_matches = explode('_', $header_key);
			if (count($regex_matches) > 0 && strlen($header_key) > 2) {
				foreach($regex_matches as $ak_key => $ak_val) {
					$regex_matches[$ak_key] = ucfirst($ak_val);
				}
				$header_key = implode('-', $regex_matches);
			}
			$headers[$header_key] = $val;
		}
		
		return $headers;
	} //apache_request_headers()
}

/***
 * Returns current system time accurate to milliseconds
 ***/
function getTimeInMs() {
	list($ms, $s) = explode(' ',microtime());
	$ms = round($ms * 1000);
	return $s*1000 + $ms;
}