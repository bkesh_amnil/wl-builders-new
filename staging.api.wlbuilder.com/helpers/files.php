<?php
/***
 * Creates a zip file from a file / directory
 * @param: file paths of source and destination,
 *			e.g. zip("/folder/to/compress/", "./compressed.zip") 
 * - File will be created at destination
 * @return: TRUE on success
 ***/
function zip($source, $destination) {
	// Check source exists
	if (!file_exists($source)) {
		return FALSE;
	}

	// Create zip
	$zip = new ZipArchive();
	if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
		return FALSE;
	}

	$source = str_replace('\\', '/', realpath($source));
	if (is_dir($source) === true) {
		$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

		foreach ($files as $file) {
			$file = str_replace('\\', '/', $file);

			//ignore "." and ".." folders
			if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) ) {
				continue;
			}
			
			//Add files
			$file = realpath($file);
			if (is_dir($file) === true) {
				$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
			}
			else if (is_file($file) === true) {
				$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
			}
		}
	} //source is directory
	else if (is_file($source) === true) {
		$zip->addFromString(basename($source), file_get_contents($source));
	} //source is file

	return $zip->close();
} //zip()

/***
 * Searches a file for a string and replaces it with a replacement.
 * - File must be in a read-writable directory (e.g. "777")
 * @return: the number of bytes that were written to the file, or FALSE on failure.
 ***/
function replaceStringInFile($file_path, $search, $replacement, $max_replacement_count = 1) {
	$str = file_get_contents($file_path);
	$str = str_replace($search, $replacement, $str, $max_replacement_count);
	
	return file_put_contents($file_path, $str, LOCK_EX);
} //replaceStringInFile()
