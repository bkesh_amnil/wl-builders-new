<!-- Breadcrumb section-->
<div class="breadcrumbs"> <a href="#" class="breadcrumb active-link"  data-for="staff-dashboard">Overview</a><!--
    --><span class="breadcrumb current">Account Settings<a href="#" class="icon-download"></a></span> </div>
<!-- Breadcrumb section ends-->

<section class="setting-section">
    <h2>Edit account information</h2>
    <form id="edit-account-info-form">
	<div class="form-message" id="edit-account-info-form-message"></div>
	<table id="edit-table" class="setting-table">
	    <tr>
		<td class="label"><label for="name">Name</label></td>
		<td class="input"><input type="text" value="" name="firstName" id="first-name" placeholder="Change Name" /><!--
		    --><label class="error"></label></td>
	    </tr>
	    <tr>
		<td class="label"><label for="phone_number">Phone Number</label></td>
		<td class="input"><input type="text" value="" name="phoneNumber" id="phone-number" placeholder="Phone Number" />
<!--		    <label class="error"></label>-->
                </td>
	    </tr>
	    <tr>
		<td class="label"><label for="email">Email</label></td>
		<td class="input"><input type="email" name="email" value="" id="email" readonly="readonly" placeholder="Change Email"  /><!--
		    -->
<!--                    <label class="error"></label>-->
                </td>
	    </tr>
<!--	    <tr>
		<td class="label"><label for="">Image</label></td>
		<td>
		    <div class="upload-instructions ">
			<input type="file" id="add-account-file" name="img" />
			<label for="add-account-file">Browse</label>
			<output for="add-account-file"></output>
			<div class="uploaded-name-holder">
			    <label>Image URL</label>
			    <input disabled="" type="text" placeholder="(Max. 128 characters)" />
			    <span class="confirm-delete">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
			    <a href="javascript:void(0)" class="deleteAccountImage btn-delete" id="edit-partner-image-delete">×</a>
			</div>
		    </div>
		</td>

	    </tr>-->
	</table>
	<button class="save-button" type="submit">Save</button><!--
	--><button id="edit-account-undo-button" class="undo-button" type="button">Reset</button>
        <output class="edit-account-loader"></output>
    </form>
</section>
<section class="setting-section">
    <h2>Change Password</h2>
    <form id="change-password-form">
	<div class="form-message" id="change-password-form-message"></div>
	<table id="edit-change-password-table" class="setting-table">
	    <tr>
		<td class="label"><label for="old-password">Current Password</label></td>
		<td class="input"><input type="password" name="oldPassword" value="" id="old-password"  /><!--
		    --><label class="error"></label></td>
	    </tr>
	    <tr>
		<td class="label"><label for="new-password">New Password</label></td>
		<td class="input"><input type="password" name="newPassword" value=""  id="new-password" /><!--
		    --><label class="error"></label></td>
	    </tr>
	    <tr>
		<td class="label"><label for="new-password-2">Confirm Password</label></td>
		<td class="input"><input type="password" name="confirmPassword" value="" id="new-password-2" /><!--
		    --><label class="error"></label></td>
	    </tr>
	</table>
	<button class="change-password-button" type="submit">Save</button><!--
	--><button id="change-password-undo-button" class="undo-button" type="button">Reset</button>
        <output class="change-password-loader"></output>
    </form>
</section>
<!-- Section for adding new testimonial ends --> 

<!-- Breadcrumb on footer-->
<div class="breadcrumbs"> <a href="#" class="breadcrumb active-link" data-for="staff-dashboard" >Overview</a><!--
    --><span class="breadcrumb current">Account settings<a href="#" class="icon-download"></a></span>
</div>
<!-- Breadcrumb on footer ends-->