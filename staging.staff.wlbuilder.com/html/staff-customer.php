﻿<!-- Breadcrumb section-->
<div class="breadcrumbs">
  <a href="#" class="breadcrumb active-link"  data-for="staff-dashboard">Overview</a><!--
	--><span class="breadcrumb current">Customer</span><!--
	--><form id="search-form">
		<input id="search-text" type="text" placeholder="Search Here">
		<button id="search-button" type="submit"></button>
	</form>
</div>
<!-- Breadcrumb section ends-->

<!-- section for listing out the data.-->
<section id="result-section">
  <nav class="simple-pagination"></nav>
  <div class="results-per-page">
    <span>Results per page</span><!--
		--><a href="#" class="current per-page" data-perPage="10">10</a><!--
		--><a href="#" class="per-page" data-perPage="20">20</a><!--
		--><a href="#" class="per-page" data-perPage="50">50</a>
  </div>
  <table id="result-table">
    <thead>
      <tr>
        <td class="email">Email<a class="sort-button" data-sort="email" href="#"></a></td>
        <td class="name">Name<a class="sort-button" data-sort="firstName" href="#"></a></td>
        <td class="profession">Profession<a class="sort-button" data-sort="profession" href="#"></a></td>
        <td class="action"></td>
      </tr>
    </thead>
    <tbody>
      <!--			Dynamic Generation of TestimonialList-->
    </tbody>
  </table>
  <nav class="simple-pagination"></nav>
  <div class ="results-per-page">
    <span>Results per page</span><!--
		--><a href="#" class="current per-page" data-perPage="10">10</a><!--
		--><a href="#" class="per-page" data-perPage="20">20</a><!--
		--><a href="#" class="per-page" data-perPage="50">50</a>
  </div>
</section>
<!-- section for listing out the data.-->

<!-- Section for editing customer -->
<section class="setting-section" id ="edit-customer">
	<h2>Edit selected customer</h2>
	<form action="#" name="edit-customer" data-pre="">
		<input type="hidden" value="" name="id"/>
		<table class="setting-table" id="edit-table">
			<tbody>
				<tr>
					<td class="label"><label for="edit-customer-email">Email</label></td>
					<td class="input"><input type="text" name="email" value="" id="edit-customer-email"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-first-name">First Name</label></td>
					<td class="input"><input type="text" name="firstName" value="" id="edit-customer-first-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-last-name">Last Name</label></td>
					<td class="input"><input type="text" name="lastName" value="" id="edit-customer-last-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-profession">Profession</label></td>
					<td class="input"><input type="text" name="profession" value="" id="edit-customer-profession"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-company-name">Company Name</label></td>
					<td class="input"><input type="text" name="company" value="" id="edit-customer-company-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label">
        <label for="edit-customer-dob">D.O.B</label></td>
        <td class="input">
            <select id="edit-customer-dob-day">
             <?php for($i = 1; $i <= 31; $i++){
              if($i > 0 && $i <= 9){
                 $i = "0{$i}"; // Because date format should be in yyyy:mm:dd
              }
             ?>
                 <option value="<?php echo $i ?>"><?php echo $i; ?></option>
            <?php } ?>
            </select>

            <select id="edit-customer-dob-month">
               <option value="01">Jan</option>
                <option value="02">Feb</option>
                <option value="03">Mar</option>
                <option value="04">Apr</option>
                <option value="05">May</option>
                <option value="06">Jun</option>
                <option value="07">Jul</option>
                <option value="08">Aug</option>
                <option value="09">Sep</option>
                <option value="10">Oct</option>
                <option value="11">Nov</option>
                <option value="12">Dec</option>
            </select>

            <select id="edit-customer-dob-year">
            <?php for($i = 1971; $i <= date(Y);$i++){ ?>
            <option value="<?php echo $i ?>"><?php echo $i; ?></option>
            <?php } ?>
            </select>
        </td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-residence-country">Residence  Country</label></td>
					<td class="input"><select id="edit-customer-residence-country" data-state-control="edit-customer-residence-state" name="residenceCountryID">
							<option value="">Select</option>
						</select>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-residence-state">Residence State</label></td>
					<td class="input"><select id="edit-customer-residence-state" name="residenceStateID">
							<option value="">Select</option>
						</select>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-mobile-number">Contact Number (Mobile)</label></td>
					<td class="input"><input type="text" name="residencePhone" value="" id="edit-customer-mobile-number"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-home-number">Contact Number (Home)</label></td>
					<td class="input"><input type="text" name="homePhone" value="" id="edit-customer-home-number"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-office-number">Contact Number (Office)</label></td>
					<td class="input"><input type="text" name="officePhone" value="" id="edit-customer-office-number"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-home-country">Home Country</label></td>
					<td class="input"><select id="edit-customer-home-country" data-state-control="edit-customer-home-state" name="homeCountryID">
							<option value="">Select</option>
						</select>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-home-state">Home State</label></td>
					<td class="input"><select id="edit-customer-home-state" name="homeStateID">
							<option value="">Select</option>
						</select>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-home-postal-code">Home Postal Code</label></td>
					<td class="input"><input type="text" name="homePostalCode" value="" id="edit-customer-home-postal-code"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-home-address">Home Address</label></td>
					<td class="input"><input type="text" name="homeAddress" value="" id="edit-customer-home-address"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-office-country">Office Country</label></td>
					<td class="input">
       <select name="officeCountryID" value="" id="edit-customer-office-country" data-state-control="edit-customer-office-state"></select>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-office-state">Office State</label></td>
					<td class="input">
       <select name="officeStateID" value="" id="edit-customer-office-state">
         <option value="">Select</option>
       </select>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-office-postal-code">Office Postal Code</label></td>
					<td class="input"><input type="text" name="officePostalCode" value="" id="edit-customer-office-postal-code"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-customer-office-address">Office Address</label></td>
					<td class="input"><input type="text" name="officeAddress" value="" id="edit-customer-office-address"/>
						<label></label></td>
				</tr>
			</tbody>
		</table>
		<div class="group-button">
			<button class="save-btn" type="submit">Save</button><!--
			--><button class="undo-btn" type="button">Undo</button><!--
      		--><button class="manage-fitting-profile-btn" id="manage-fitting-profile-btn" type="button" data-for="staff-shirt-fitting-profile">Manage fitting profile</button>
		</div>
	</form>
</section>
<!-- Section for editing testimonial ends-->

<!-- Section for refferal section -->
<section class="setting-section" id ="referral-information">
	<h2>Referral information</h2>
	<form action="#" name="referral-information" data-pre="">
		<input type="hidden" value="" name="id"/>
		<table id="add-information-table" class="setting-table">
			<tbody>
				<tr>
					<td class="label"><label for="referral-information-email">Email</label></td>
					<td class="input"><input type="text" name="email" value="" id="referral-information-email"/>
						<label></label></td>
				</tr>
			</tbody>
		</table>
		<div class="group-button">
			<button class="save-btn" type="submit">Save</button><!--
			--><button class="undo-btn" type="button">Undo</button>
		</div>
	</form>
</section>
<!-- Section for refferal section -->
<section class="setting-section" id ="referrals">
	<h2>Referrals</h2>
	<form action="#" name="referrals" data-pre="">
		<input type="hidden" value="" name="id"/>
		<table id="add-referrals-table" class="setting-table">
			<tbody>
				<tr>
					<td class="label"><label for="referrals-number">Total Number Of Referrals</label></td>
					<td class="input"><input type="text" name="total-number" value="" id="referrals-number"/>
						<label></label></td>
				</tr>
			</tbody>
		</table>
		<div class="group-button">
			<button class="save-btn" type="submit">Save</button><!--
			--><button class="undo-btn" type="button">Undo</button>
		</div>
	</form>
</section>
<!-- Section for credits section -->
<section class="setting-section" id ="credits-customer">
	<h2>Credit management</h2>
	<form action="#" name="credits-customer" data-pre="">
		<input type="hidden" value="" name="id"/>
		<table id="add-credits-table" class="setting-table">
			<tbody>
				<tr>
					<td class="label"><label for="credits-change-number">Credit Amount</label></td>
					<td class="input"><input type="text" name="credits-number" value="" id="credits-change-number"/>
						<label></label></td>
				</tr>
			</tbody>
		</table>
		<div class="group-button">
			<button class="save-btn" type="submit">Save</button><!--
			--><button class="undo-btn" type="button">Undo</button><!--
			<button class="view-credit-history-btn" id="view-credit-history-btn" type="button" data-for="view-credit-history">View Credit History</button> -->

		</div>
	</form>
</section>

<!-- Section for adding customer -->
<section id ="add-customer" class="setting-section">
	<h2>Add new customer</h2>
	<form action="#" name="add-customer" data-pre="">
		<table id="add-customer-table" class="setting-table">
			<tbody>
				<tr>
					<td class="label"><label for="add-customer-email">Email</label></td>
					<td class="input"><input type="text" name="email" value="" id="add-customer-email"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-customer-password">Password</label></td>
					<td class="input"><input type="password" name="password" value="" id="add-customer-password"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-customer-confirm-password">Confirm Password</label></td>
					<td class="input"><input type="password" name="confirmPassword" value="" id="add-customer-confirm-password" data-target="add-customer-password" />
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-customer-first-name">First Name</label></td>
					<td class="input"><input type="text" name="firstName" value="" id="add-customer-first-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-customer-last-name">Last Name</label></td>
					<td class="input"><input type="text" name="lastName" value="" id="add-customer-last-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-customer-profession">Profession</label></td>
					<td class="input"><input type="text" name="profession" value="" id="add-customer-profession"/>
						<label></label></td>
				</tr>
			</tbody>
		</table>
		<div class="group-button">
			<button class="save-btn" type="submit">Save</button>
			<!--
			-->
			<button class="undo-btn" type="button">Undo</button>
		</div>
	</form>
</section>
<!-- Section for adding testimonial ends-->

<section id="credit-section">
    <nav class="credit-list-pagination">
        <span class="credit-list-current">Previous</span>
        <span class="credit-list-current">1</span>
        <a class="credit-list-page-link" data-page="2" href="#">2</a>
        <a class="credit-list-page-link" data-page="2" href="#">Next</a>
    </nav>
    <span id="credit-left">Credit Balance $50</span>
    <table id="credit-listing">
     <thead>
      <tr>
       <td class="credit-date">Date</td>
       <td class="credit-activity">Activity</td>
       <td class="credit-added">Added</td>
       <td class="credit-used">Used</td>
      </tr>
     </thead>
     <tbody>
     </tbody>
    </table>
</section>

<!-- Breadcrumb on footer-->
<div class="breadcrumbs">
  <a href="#" class="breadcrumb active-link" data-for="staff-dashboard">Overview</a><!--
	--><span class="breadcrumb current">Customer</span><!--
	--><span id="back-to-top-button">Back to top</span>
</div>
<!-- Breadcrumb on footer ends-->

