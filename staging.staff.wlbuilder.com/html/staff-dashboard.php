<aside id="activity-module">
		<div id="activity-log" data-page="0" data-activities-per-page="6">
			<h2>Activity Log</h2>
				<article class="activity"></article>
			<!-- Staff Activity Listing -->
		</div>
		<button id="newer-log-button" class="activity-nav" data-page="0" disabled="disabled">Newer</button><!--
		--><button id="older-log-button" class="activity-nav" data-page="0" disabled="disabled">Older</button>
	</aside><!--
	--><section id="modules"><!--
         --><a class="module" href="<?php echo SERVER_PATH;?>/staff-testimonial" data-for="staff-testimonial" data-name="Testimonial" draggable="true">
            <h2>Testimonials</h2>
            <img src="<?php echo SERVER_PATH;?>/img/staff/icon-testimonial.svg" alt="testimonial" title="Testimonial">
        </a><!--
        --><a class="module" href="<?php echo SERVER_PATH;?>/staff-project" data-for="staff-project" data-name="Project" draggable="true">
            <h2>Projects</h2>
            <img src="<?php echo SERVER_PATH;?>/img/staff/module-icon-categories.svg" alt="project" title="Project">
        </a><!--
     -->
<!--     <button id="change-layout-button">Change Module Layout</button>-->
        <div id="user_id"></div>
	</section>