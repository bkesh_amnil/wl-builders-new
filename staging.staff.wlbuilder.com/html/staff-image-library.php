<div class="breadcrumbs">
	<a class="breadcrumb" href="<?php echo SERVER_PATH;?>/staff-dashboard" data-for="staff-dashboard">Overview</a><!--
	--><span class="breadcrumb current">Image library</span><!--
	--><button class="download-button"></button>
	
	<form id="search-form">
		<input type="text" id="search-text" placeholder="Search Here" />
		<button id="search-button"></button>
	</form>
</div>

<section id="result-section">
	<nav class="simple-pagination"></nav>
	<div class="results-per-page">
		Results per page<!--
		--><a href="#" class="current">10</a><!--
		--><a href="#">20</a><!--
		--><a href="#">50</a>
    </div>
	<section id="image-library">
		<figure>
			<img src="<?php echo SERVER_PATH;?>/img/products/library-image.jpg" alt="" />
			<figcaption class="file-name">stanley_measuring.jpg</figcaption>
			<div class="actions-overlay">
				<button class="edit"></button><button class="delete"></button>
			</div>
		</figure><!--
		--><figure>
			<img src="<?php echo SERVER_PATH;?>/img/products/thumbnails/default.jpg" alt="" />
			<figcaption class="file-name">stanley_measuring.jpg</figcaption>
			<div class="actions-overlay">
				<button class="edit"></button><button class="delete"></button>
			</div>
		</figure><!--
		--><figure>
			<img src="<?php echo SERVER_PATH;?>/img/products/thumbnails/default.jpg" alt="" />
			<figcaption class="file-name">stanley_measuring.jpg</figcaption>
			<div class="actions-overlay">
				<button class="edit"></button><button class="delete"></button>
			</div>
		</figure><!--
		--><figure>
			<img src="<?php echo SERVER_PATH;?>/img/products/thumbnails/default.jpg" alt="" />
			<figcaption class="file-name">stanley_measuring.jpg</figcaption>
			<div class="actions-overlay">
				<button class="edit"></button><button class="delete"></button>
			</div>
		</figure><!--
		--><figure>
			<img src="<?php echo SERVER_PATH;?>/img/products/thumbnails/default.jpg" alt="" />
			<figcaption class="file-name">stanley_measuring.jpg</figcaption>
			<div class="actions-overlay">
				<button class="edit"></button><button class="delete"></button>
			</div>
		</figure><!--
		--><figure>
			<img src="<?php echo SERVER_PATH;?>/img/products/thumbnails/default.jpg" alt="" />
			<figcaption class="file-name">stanley_measuring.jpg</figcaption>
			<div class="actions-overlay">
				<button class="edit"></button><button class="delete"></button>
			</div>
		</figure><!--
		--><figure>
			<img src="<?php echo SERVER_PATH;?>/img/products/thumbnails/default.jpg" alt="" />
			<figcaption class="file-name">stanley_measuring.jpg</figcaption>
			<div class="actions-overlay">
				<button class="edit"></button><button class="delete"></button>
			</div>
		</figure><!--
		--><figure class="inactive"></figure>
	</section>
	<nav class="simple-pagination"></nav>
	<div class="results-per-page">
		Results per page<!--
		--><a href="#" class="current">10</a><!--
		--><a href="#">20</a><!--
		--><a href="#">50</a>
    </div>
</section>

<section class="setting-section" id="edit-image-section">
	<h2>Edit Image</h2>
	<form id="edit-image-form">
		<label for="edit-image-file-name">Filename</label><!--
		--><input type="text" value="" id="edit-image-file-name" readonly="readonly"><!--
		--><label class="error" for="edit-image-file-name"></label>
		
		<section id="edit-image-img-holder" class="img-holder">
			<img id="edit-image-img">
			<div class="crop-border"></div>	
		</section><!--
		--><ol class="steps">
			<li>Use an image from the internet (right click > copy image location) or upload from your local computer.<br />
				<input type="text" id="edit-image-url" class="url-input" placeholder="http://www.copy_paste_here.com/image.jpg" />
				
				<button id="edit-image-fetch-url-button" type="button">Fetch from URL</button><!--
				--><button id="edit-image-upload-local-button" type="button">Upload from local</button><!--
				--><input type="file" name="file" id="edit-image-browse-input" />
			</li>
			
			<li>Adjust image size using slider below and/or reposition image using canvas on the left.<br />
				<input id="edit-image-zoomer" type="range" min="260" max="1040" value="260" step="1">
			</li>
			
			<li>Save the edited image.<br />
				<button id="edit-image-save-button" type="submit">Save</button><!--
				--><button id="edit-image-undo-button" type="button">Close</button>
			</li>
		</ol>
	</form>
</section>

<section class="setting-section" id="add-image-section">
	<h2>Add New Image</h2>
	<form id="add-image-form">
		<label for="add-image-file-name">Filename</label><!--
		--><input type="text" value="" id="add-image-file-name" placeholder="Enter filename here."><!--
		--><label class="error" for="add-image-file-name"></label>
		
		<section id="add-image-img-holder" class="img-holder">
			<img id="add-image-img">
			<div class="crop-border"></div>	
		</section><!--
		--><ol class="steps">
			<li>Use an image from the internet (right click > copy image location) or upload from your local computer.<br />
				<input type="text" id="add-image-url" class="url-input" placeholder="http://www.copy_paste_here.com/image.jpg" />
				
				<button id="add-image-fetch-url-button" type="button">Fetch from URL</button><!--
				--><button id="add-image-upload-local-button" type="button">Upload from local</button><!--
				--><input type="file" name="file" id="add-image-browse-input" />
			</li>
			
			<li>Adjust image size using slider below and/or reposition image using canvas on the left.<br />
				<input id="add-image-zoomer" type="range" min="260" max="1040" value="260" step="1">
			</li>
			
			<li>Save the edited image.<br />
				<button id="add-image-save-button" type="submit">Save</button><!--
				--><button id="add-image-clear-button" type="button">Clear</button>
			</li>
		</ol>
	</form>
</section>

<div class="breadcrumbs">
	<a class="breadcrumb" href="<?php echo SERVER_PATH;?>/staff-dashboard" data-for="staff-dashboard">Overview</a><!--
	--><span class="breadcrumb current">Image Library</span><!--
	--><button class="download-button"></button>
	
	<button id="back-to-top-button">Back to top</button>
</div>
