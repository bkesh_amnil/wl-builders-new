<!-- Breadcrumb section-->

<div class="breadcrumbs"> <a href="#" class="breadcrumb active-link" data-for="staff-dashboard">Overview</a><!--
    --><span class="breadcrumb current">Capabilities</span><!--
    --><form id="search-form">
        <input id="search-text" type="text" placeholder="Search Here">
        <button id="search-button" type="submit"></button>
    </form>
</div>
<!-- Breadcrumb section ends--> 

<!-- Section for listing out the data.-->
<section id="result-section">
    <nav class="simple-pagination"><span class="current">Previous</span><span class="current">1</span><span class="current">Next</span></nav><!--
    --><div class ="results-per-page"></div>
    <table id="result-table">
        <thead>
            <tr>
                <th class="message"><a class="sort-button" data-sort="name" href="#">Name</a></th>
                <th class="message"><a class="sort-button" data-sort="description" href="#">Description</a></th>
<!--		<td class="message">Email<a class="sort-button" data-sort="email" href="#"></a></td>
                <td class="message">Website<a class="sort-button" data-sort="website" href="#"></a></td>-->
                <th class="priority"><a class="sort-button" data-sort="displayPriority" href="#">Priority</a></th>

                <th  class="action"></th>
            </tr>
        </thead>
        <tbody>
            <!-- Dynamic Generation of partnerList -->
        </tbody>
    </table>
    <nav class="simple-pagination"><span class="current">Previous</span><span class="current">1</span><span class="current">Next</span></nav><!--
    --><div class ="results-per-page"></div>
</section>
<!-- section for listing out the data.--> 

<!-- Section for editing partner -->
<section class="setting-section" id ="edit-partner">
    <h2>Edit Capabilities</h2>
    <form action="#" name="edit-partner" data-pre="">
        <input type="hidden" value="" name="id"/>
        <table class="setting-table" id="edit-table">
            <tbody>
                <tr>
                    <td class="label"><label for="edit-partner-name">Name</label></td>
                    <td class="input"><input type="text" name="name" value="" id="edit-partner-name" />
                        <label></label></td>
                </tr>
<!--				<tr>
                        <td class="label"><label for="edit-partner-email">Email</label></td>
                        <td class="input"><input type="text" name="email" value="" id="edit-partner-email" />
                                <label></label></td>
                </tr>-->

                <tr>
                    <td class="label"><label for="edit-partner-description">Description</label></td>
                    <td class="input">
                        <textarea placeholder="Description" maxlength="1024" name="description" id="edit-partner-description"></textarea>
                        <label></label></td>
                </tr>
<!--				<tr>
                        <td class="label"><label for="edit-partner-website">Website</label></td>
                        <td class="input"><input type="text" name="website" value="" id="edit-partner-website" />
                                <label></label></td>
                </tr>-->
                <tr>
                    <td class="label"><label for="edit-partner-priority">Display Priority</label></td>
                    <td class="input"><input type="text" value="" name="displayPriority" id="edit-partner-priority" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="input" colspan="2">
                        <div id="edit-partner-image-preview-area" class="image-preview-area">
                            <img id="edit-partner-image-preview" src="" alt="" />
                        </div><!--
                        --><div class="upload-instructions ">
                            <p>Upload image for desktop (400 x 400 pixels)</p>
                            </p>
                            <input type="file" id="edit-partner-file" name="img" />
                            <label for="edit-partner-file">Browse</label>                           
                            <output for="edit-partner-file"></output>
                            <div class="uploaded-name-holder">
                                <label>Image URL</label><!--
                                --><input disabled="" type="text" placeholder="(Max. 128 characters)" />
                                <span class="confirm-delete">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                <a href="javascript:void(0)" class="deleteCapabilityImg btn-delete" id="edit-partner-image-delete">×</a>
                            </div>
                        </div>                        
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
            <button class="save-btn" type="submit">Submit</button><!-- 
            --><button class="undo-btn" type="button">Cancel</button>
        </div>

        <div class="edit-error "></div>
    </form>
</section>
<!-- Section for editing announcement ends--> 

<!-- Section for adding new announcement -->
<section class="setting-section" id="add-partner">
    <h2>Add Capabilities</h2>
    <form action="#" name="add-partner">
        <table id="add-table" class="setting-table">
            <tbody>

                <tr>
                    <td class="label"><label for="add-partner-name">Name</label></td>
                    <td class="input"><input type="text" value="" placeholder="Name" name="name" id="add-partner-name" />
                        <label></label></td>
                </tr>

<!--				<tr>
                                        <td class="label"><label for="add-partner-email">Email</label></td>
                                        <td class="input"><input type="text" value="" placeholder="Email" name="email" id="add-partner-email" />
                                        <label></label></td>
                                </tr>-->
                <tr>
                    <td class="label"><label for="add-partner-description">Description</label></td>
                    <td class="input">

                        <textarea placeholder="Description" maxlength="1024" name="description" id="add-partner-description"></textarea>

                        <label></label></td>
                </tr>

<!--				<tr>
                                        <td class="label"><label for="add-partner-website">Website</label></td>
                                        <td class="input"><input type="text" name="website" placeholder="website" value="" id="add-partner-website" />
                                                <label></label></td>
                                </tr>-->
                <tr>
                    <td class="label"><label for="add-partner-priority">Display Priority</label></td>
                    <td class="input"><input type="text" value="" placeholder="Display Priority Number" name="displayPriority" id="add-partner-priority" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="input" colspan="2">
                        <div id="add-partner-image-preview-area" class="image-preview-area">
                            <img id="add-partner-image-preview" src="" alt="" />
                        </div><!--
                        --><div class="upload-instructions ">
                            <p>Upload image for desktop (400 x 400 pixels)</p>
                            </p>
                            <input type="file" id="add-partner-file" name="img" />
                            <label for="add-partner-file">Browse</label>                       
                            <output for="add-partner-file"></output>
                            <div class="uploaded-name-holder">
                                <label>Image URL</label><!--
                                --><input disabled="" type="text" placeholder="(Max. 128 characters)" />
                                <span class="confirm-delete">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                <a href="javascript:void(0)" class="deleteCapabilityImg btn-delete" id="edit-partner-image-delete">×</a>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
            <button class="add-btn" type="submit"> Add </button><!-- 
            --><button class="undo-btn" type="button"> Clear </button>
        </div>
        <div class="add-error "></div>
    </form>
</section>
<!-- Section for adding new announcement ends --> 
<div id="error-overlay" class="hide"></div>
<!-- Breadcrumb on footer-->
<div class="breadcrumbs"> <a href="#" class="breadcrumb active-link" data-for="staff-dashboard" >Overview</a><!--
    --><span class="breadcrumb current">Capabilities</span><!--
    -->
<!--    <span id="back-to-top-button">Back to top</span> -->
</div>
<!-- Breadcrumb on footer ends-->