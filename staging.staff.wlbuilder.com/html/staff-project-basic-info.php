<!-- Breadcrumb section-->
<div class="breadcrumbs">
  <a href="#" class="breadcrumb active-link"  data-for="staff-dashboard">Overview</a><!--
--><span class="breadcrumb current">Projects</span><!--
	--><form id="search-form">
		<input id="search-text" type="text" placeholder="Search Here">
		<button id="search-button" type="submit"></button>
	</form>
</div>
<!-- Breadcrumb section ends-->

<section id="staff-project-sub-menu" class="project">
</section>

<!-- section for listing out the data.-->
<section id="result-section">
    <nav class="simple-pagination"><span class="current">Previous</span><span class="current">1</span><span class="current">Next</span></nav><!--
    --><div class ="results-per-page"></div>
    <table id="result-table">
        <thead>
            <tr>
                <th class="message"><a class="sort-button" data-sort="name" href="#">Address</a></th>
                <th class="message"><a class="sort-button" data-sort="location" href="#">District</a></th>
                <th class="message"><a class="sort-button" data-sort="isFeaturedInStaticSection" href="#">Homepage Feature</a></th>
                <th class="priority"><a class="sort-button" data-sort="displayPriority" href="#">Display Priority</a></th>
                <th  class="action"></th>
            </tr>
        </thead>
        <tbody>
            <!-- Dynamic Generation of projectList -->
        </tbody>
    </table>
    <nav class="simple-pagination"><span class="current">Previous</span><span class="current">1</span><span class="current">Next</span></nav><!--
    --><div class ="results-per-page"></div>
</section>
<!-- section for listing out the data.-->

<!-- Section for editing project basic info -->
<section class="setting-section" id ="edit-project">
	<h2>Edit listing</h2>
	<form action="#" name="edit-project">
        <table id="edit-table" class="setting-table">
            <tbody>

                <tr>
                    <td class="label"><label for="edit-project-name">Address</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Max. 128 characters)" name="name" id="edit-project-name" />
                        <label></label></td>
                </tr>

                <tr>
                    <td class="label"><label for="edit-project-location">District</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Max. 128 characters)" name="location" id="edit-project-location" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-longitude">Longitude</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Max. 28 characters)" name="longitude" id="edit-project-longitude" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-latitude">Latitude</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Max. 28 characters)" name="latitude" id="edit-project-latitude" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-description">Description</label></td>
                    <td class="input">
                        <textarea placeholder="(Max. 512 characters)"  name="description" id="edit-project-description"></textarea>
						
                        <label></label></td>
                </tr>

<!--                <tr>
                    <td class="label"><label for="edit-project-featured-static">Is Featured In Static Section</label></td>
                    <td class="input">
                        <select name="isFeaturedInStaticSection" class="add-project-featured-static" id="edit-project-featured-static">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <label></label>
                    </td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-featured-panoromic">Is Featured In Panoramic Section</label></td>
                    <td class="input">
                        <select name="isFeaturedInPanoramicSection" class="add-project-featured-panoromic" id="edit-project-featured-panoromic">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <label></label>
                    </td>
                </tr>-->
                
                <tr>
                    <td class="label"><label for="edit-home-page-feature">Homepage Feature</label></td>
                    <td class="input">
                        <select name="home_page_feature" class="edit-home-page-feature" id="edit-home-page-feature">
                            <option value="0">Static</option>
                            <option value="1">Panorama</option>
                            <option value="2">Static and Panaroma</option>
                            <option value="3">None</option>
                        </select>
                        <label></label>
                    </td>
                </tr>
                
                <tr>
                    <td class="label"><label for="edit-project-priority">Display Priority</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Higher priority will be shown first; set to 0 to hide.)" name="displayPriority" id="edit-project-priority" />
                        <label></label></td>
                </tr>
                <tr>
<!--                        <td class="label"><label for="edit-project-image">Image</label></td>-->
                    <td class="input" colspan="2">
                        <div id="edit-project-image-preview-area" class="image-preview-area">
                                <img id="edit-project-image-preview" src="" alt="" />
                        </div><!--
                        --><div class="upload-instructions ">
                                <p>Upload Project Profile Picture (200 x 200 Pixels)</p>
                                <input type="file" id="edit-project-file" name="img" />
                                <label for="edit-project-file">Browse</label><!--
                                --><output for="edit-project-file"></output>
                                <div class="uploaded-name-holder">
                                <label>Image Name</label><!--
                                --><input disabled="" id="edit-partner-image-url" type="text" placeholder="" />
                                <span class="confirm-delete">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                <a href="javascript:void(0)" class="deleteInfoImg btn-delete" id="edit-partner-image-delete">×</a>
                            </div>
                        </div>
                        </td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
            <button class="save-btn" type="submit"> Submit </button><!-- 
            --><button class="undo-btn" type="button"> Cancel </button>
            <output class="edit-listing-loader"></output>
        </div>
            
            <div class="edit-error "></div>
    </form>
</section>
<!-- Section for editing project-basic-info ends-->

<!-- Section for adding new project-basic-info -->
<section class="setting-section" id="add-project">
    <h2>Add listing</h2>
    <form action="#" name="add-project">
        <table id="add-table" class="setting-table">
            <tbody>

                <tr>
                    <td class="label"><label for="add-project-name">Address</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Max. 128 characters)" name="name" id="add-project-name" />
                        <label></label></td>
                </tr>

                <tr>
                    <td class="label"><label for="add-project-location">District</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Max. 128 characters)" name="location" id="add-project-location" />
                        <label></label></td>
                </tr>                
                <tr>
                    <td class="label"><label for="add-project-latitude">Latitude</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Max. 28 characters)" name="latitude" id="add-project-latitude" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="add-project-longitude">Longitude</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Max. 28 characters)" name="longitude" id="add-project-longitude" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="add-project-description">Description</label></td>
                    <td class="input">
                        
                        <textarea placeholder="(Max. 512 characters)"  name="description" id="add-project-description"></textarea>
                        <label></label></td>
                </tr>
                
                
                <!-- old-feature display  Starts-->
<!--                <tr>
                    <td class="label"><label for="add-project-featured-static">Is Featured In Static Section</label></td>
                    <td class="input">
                        <select name="isFeaturedInStaticSection" class="add-project-featured-static" id="add-project-featured-static">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <label></label>
                    </td>
                </tr>
                <tr>
                    <td class="label"><label for="add-project-featured-panoromic">Is Featured In Panoramic Section</label></td>
                    <td class="input">
                        <select name="isFeaturedInPanoramicSection" class="add-project-featured-panoromic" id="add-project-featured-panoromic">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <label></label>
                    </td>
                </tr>-->
                <!-- old-feature display Ends-->
                
                <!-- displayed  of homepage featured with the new requirement by Rupesh on-dated sep-23 2016 Starts -->
                <tr>
                    <td class="label"><label for="add-home-page-feature">Homepage Feature</label></td>
                    <td class="input">
                        <select name="home_page_feature" class="add-home-page-feature" id="add-home-page-feature">
                            <option value="0">Static</option>
                            <option value="1">Panorama</option>
                            <option value="2">Static and Panaroma</option>
                            <option value="3">None</option>
                        </select>
                        <label></label>
                    </td>
                </tr>
                <!-- homepage display Ends-->
                
                <tr>
                    <td class="label"><label for="add-project-priority">Display Priority</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Higher priority will be shown first; set to 0 to hide.)" name="displayPriority" id="add-project-priority" />
                        <label></label></td>
                </tr>
                <tr>
<!--                        <td class="label"><label for="add-project-image">Image</label></td>-->
                    <td class="input" colspan="2">
                        <div id="add-project-image-preview-area" class="image-preview-area">
                                <img id="add-project-image-preview" src="" alt="" />
                        </div><!--
                        --><div class="upload-instructions ">
                                <p>Upload Project Profile Picture (200 x 200 Pixels)</p>
                                <input type="file" id="add-project-file" name="img" />
                                <label for="add-project-file">Browse</label><!--
                                --><output for="add-project-file"></output>
                                <div class="uploaded-name-holder">
                                <label>Image Name</label><!--
                                --><input disabled="" type="text" placeholder="" />
                                <span class="confirm-delete">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                <a href="javascript:void(0)" class="deleteInfoImg btn-delete" id="edit-partner-image-delete">×</a>
                            </div>
                        </div>
                        </td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
            <button class="add-btn" type="submit"> Submit </button><!-- 
            --><button class="undo-btn" type="button"> Cancel </button>
            <output class="add-listing-loader"></output>
        </div>
        
            <div class="add-error "></div>
    </form>
</section>
<!-- Section for adding new project-basic-info -->

<!-- Breadcrumb on footer-->
<div class="breadcrumbs">
  <a href="#" class="breadcrumb active-link" data-for="staff-dashboard" >Overview</a><!--
	--><span class="breadcrumb current">Projects</span><!--
	-->
<!--        <span id="back-to-top-button">Back to top</span>-->
</div>
<!-- Breadcrumb on footer ends-->
