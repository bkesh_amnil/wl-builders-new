<!-- Breadcrumb section-->
<div class="breadcrumbs">
    <a href="#" class="breadcrumb active-link"  data-for="staff-dashboard">Overview</a><!--
    --><span class="breadcrumb current">Projects</span><!--
    --><form id="search-form">
        <input id="search-text" type="text" placeholder="Search Here">
        <button id="search-button" type="submit"></button>
    </form>
</div>
<!-- Breadcrumb section ends-->

<section id="staff-project-sub-menu" class="project">
</section>

<!-- section for listing out the data.-->
<section id="result-section">
    <nav class="simple-pagination"></nav><!--
    --><div class="results-per-page">
        <span>Results per page</span><!--
	--><a href="#" class="current per-page" data-perPage="5">5</a><!--
        --><a href="#" class="current per-page" data-perPage="10">10</a><!--
        --><a href="#" class="per-page" data-perPage="20">20</a><!--
        --><a href="#" class="per-page" data-perPage="50">50</a>
    </div>
    <table id="result-table">
        <thead>
            <tr>
                <th class="message"><a class="sort-button" data-sort="name" href="#">Address</a></th>
                <th class="message"><a class="sort-button" data-sort="location" href="#">District</a></th>
                <th class="message"><a class="sort-button" data-sort="isFeaturedInStaticSection" href="#">Homepage Feature</a></th>
                <th class="priority"><a class="sort-button" data-sort="displayPriority" href="#">Display Priority</a></th>
                <th  class="action"></th>
            </tr>
        </thead>
        <tbody>
            <!-- Dynamic Generation of projectList -->
        </tbody>
    </table>
    <nav class="simple-pagination"></nav><!--
    --><div class ="results-per-page">
        <span>Results per page</span><!--
	--><a href="#" class="current per-page" data-perPage="5">5</a><!--
        --><a href="#" class="current per-page" data-perPage="10">10</a><!--
        --><a href="#" class="per-page" data-perPage="20">20</a><!--
        --><a href="#" class="per-page" data-perPage="50">50</a>
    </div>
</section>
<!-- section for listing out the data.-->

<!-- section for listing out the data.-->

<div id="manage-blueprints-image" class="setting-section">
    <h2 id="popup-response-title">BluePrint Image</h2>

    <p id="header-error"></p>

    <section id="result-section-blueprint" class="popup-setting-section">
        <form action="#" name="edit-project-blue-print" data-pre="">
            <table class="pop-result-table">
                <tbody id="result-table-blueprint" width="100%" data-pre="">
                    <tr>
                        <td colspan="5">
                            <div id="add-blueprint-image-low-1-preview-area" class="image-preview-area url-low-1-complete">
                                <img class="add-project-blueprint-image-preview" src="" alt="" width="200" height="" />
                            </div>
                            <div class="upload-instructions ">
                                <p>Upload Blueprint Thumbnail For Level One</p>
                                <div class="pop-result-action">
                                    <input class="hiddenImg" type="hidden" name="url-low-1-prev">
                                    <input id="url-low-1" type="file" name="url-low-1"> 
                                    <label for="url-low-1">Browse</label>
                                    <label class="error"></label>                                                                     
                                </div>
                                <output for="add-project-blueprint-image-file"></output>
                                <div class="uploaded-name-holder">
                                    <label>Image Name</label><!--
                                    --><input disabled="" id="url-low-1-url" type="text" placeholder="" />
                                    <span class="confirm-delete" data-delete="urlLow1">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                    <a href="javascript:void(0)" class="deleteBlueprintImg btn-delete" id="url-low-1-delete">×</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    
                    <tr>
                        <td colspan="5">
                            <div id="add-blueprint-image-high-1-preview-area" class="image-preview-area url-high-1-complete">
                                <img class="add-project-blueprint-image-preview" src="" width="200" height="" alt="" />
                            </div>
                            <div class="upload-instructions ">
                                <p>Upload Enlarged Blueprint For Level One</p>
                                <div class="pop-result-action">
                                    <input class="hiddenImg" type="hidden" name="url-high-1-prev"><input id="url-high-1" type="file" name="url-high-1"> <label for="url-high-1">Browse</label><label class="error"></label>
                                </div>
                                <output for="add-project-blueprint-image-file"></output>                                
                                <div class="uploaded-name-holder">
                                    <label>Image Name</label><!--
                                    --><input disabled="" id="url-high-1-url" type="text" placeholder="" />
                                    <span class="confirm-delete" data-delete="urlHigh1">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                    <a href="javascript:void(0)" class="deleteBlueprintImg btn-delete" id="url-high-1-delete">×</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div id="add-blueprint-image-low-2--preview-area" class="image-preview-area url-low-2-complete">
                                <img class="add-project-blueprint-image-preview" src="" width="200" height="" alt="" />
                            </div>
                            <div class="upload-instructions ">
                                <p>Upload Blueprint Thumbnail For Level Two</p>
                                <div class="pop-result-action">
                                    <input class="hiddenImg" type="hidden" name="url-low-2-prev"><input id="url-low-2" type="file" name="url-low-2"> <label for="url-low-2">Browse</label><label class="error"></label>
                                </div>
                                <output for="add-project-blueprint-image-file"></output>
                                <div class="uploaded-name-holder">
                                    <label>Image Name</label><!--
                                    --><input disabled="" id="url-low-2-url" type="text" placeholder="" />
                                    <span class="confirm-delete" data-delete="urlLow2">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                    <a href="javascript:void(0)" class="deleteBlueprintImg btn-delete" id="url-low-2-delete">×</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div id="add-blueprint-image-high-2-preview-area" class="image-preview-area url-high-2-complete">
                                <img class="add-project-blueprint-image-preview" src="" width="200" height="" alt="" />
                            </div>
                            <div class="upload-instructions ">
                               <p>Upload Enlarged Blueprint For Level Two</p>
                                <div class="pop-result-action">
                                    <input class="hiddenImg" type="hidden" name="url-high-2-prev"><input id="url-high-2" type="file" name="url-high-2"> <label for="url-high-2">Browse</label><label class="error"></label>
                                </div>
                                <output for="add-project-blueprint-image-file"></output>
                                <div class="uploaded-name-holder">
                                    <label>Image Name</label><!--
                                    --><input disabled="" id="url-high-2-url" type="text" placeholder="" />
                                    <span class="confirm-delete" data-delete="urlHigh2">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                    <a href="javascript:void(0)" class="deleteBlueprintImg btn-delete" id="url-high-2-delete">×</a>
                                </div>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="5">
                            <div id="add-blueprint-image-low-3-preview-area" class="image-preview-area url-low-3-complete">
                                <img class="add-project-blueprint-image-preview" src="" width="200" height="" alt="" />
                            </div>
                            <div class="upload-instructions ">
                                <p>Upload Blueprint Thumbnail For Level Three</p>
                                <div class="pop-result-action">
                                    <input  class="hiddenImg" type="hidden" name="url-low-3-prev"><input id="url-low-3" type="file" name="url-low-3"> <label for="url-low-3">Browse</label><label class="error"></label>
                                </div>
                                <output for="add-project-blueprint-image-file"></output>
                                <div class="uploaded-name-holder">
                                    <label>Image Name</label><!--
                                    --><input disabled=""  id="url-low-3-url" type="text" placeholder="" />
                                    <span class="confirm-delete" data-delete="urlLow3">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                    <a href="javascript:void(0)" class="deleteBlueprintImg btn-delete" id="url-low-3-delete">×</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div id="add-blueprint-image-high-3preview-area" class="image-preview-area url-high-3-complete">
                                <img class="add-project-blueprint-image-preview" src="" width="200" height="" alt="" />
                            </div>
                            <div class="upload-instructions ">
                                <p>Upload Enlarged Blueprint For Level Three</p>
                                <div class="pop-result-action">
                                    <input  class="hiddenImg" type="hidden" name="url-high-3-prev"><input id="url-high-3" type="file" name="url-high-3"> <label for="url-high-3">Browse</label><label class="error"></label>
                                </div>
                                <output for="add-project-blueprint-image-file"></output>
                                <div class="uploaded-name-holder">
                                    <label>Image Name</label><!--
                                    --><input disabled="" id="url-high-3-url" type="text" placeholder="" />
                                    <span class="confirm-delete" data-delete="urlHigh3">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                    <a href="javascript:void(0)" class="deleteBlueprintImg btn-delete" id="url-high-3-delete">×</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div id="add-blueprint-image-low-4-preview-area" class="image-preview-area url-low-4-complete">
                                <img class="add-project-blueprint-image-preview" src="" alt="" />
                            </div>
                            <div class="upload-instructions ">
                                <p>Upload Blueprint Thumbnail For Level Four</p>
                                <div class="pop-result-action">
                                    <input  class="hiddenImg" type="hidden" name="url-low-4-prev"><input id="url-low-4" type="file" name="url-low-4"> <label for="url-low-4">Browse</label><label class="error"></label>
                                </div>
                                <output for="add-project-blueprint-image-file"></output>
                                <div class="uploaded-name-holder">
                                    <label>Image Name</label><!--
                                    --><input disabled="" id="url-low-4-url" type="text" placeholder="" />
                                    <span class="confirm-delete" data-delete="urlLow4">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                    <a href="javascript:void(0)" class="deleteBlueprintImg btn-delete" id="url-low-4-delete">×</a>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <div id="add-blueprint-image-high-4-preview-area" class="image-preview-area url-high-4-complete">
                                <img class="add-project-blueprint-image-preview" src="" width="200" height="" alt="" />
                            </div>
                            <div class="upload-instructions">
                                <p>Upload Enlarged Blueprint For Level Three</p>
                                <div class="pop-result-action">
                                    <input  class="hiddenImg" type="hidden" name="url-high-4-prev"><input type="file" name="url-high-4" id="url-high-4"> <label for="url-high-4">Browse</label><label class="error"></label>

                                </div>
                                <output for="add-project-blueprint-image-file"></output>
                                <div class="uploaded-name-holder">
                                    <label>Image Name</label><!--
                                    --><input disabled="" id="url-high-4-url" type="text" placeholder="" />
                                    <span class="confirm-delete" data-delete="urlHigh4">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                    <a href="javascript:void(0)" class="deleteBlueprintImg btn-delete" id="url-high-4-delete">×</a>
                                </div>
                            </div>
                        </td>
                    </tr>


                </tbody>
            </table>
            <div class="group-button">
                <button type="submit" class="save-blueprint-images">Save</button><!--
                --><button type="button" class="undo-btn">Cancel</button>
                <output class="edit-blueprint-loader"></output>
            </div>
            <div class="edit-error"></div>
        </form>
    </section>

</div>

<!-- Section for editing project-feature ends-->

<!-- Breadcrumb on footer-->
<div class="breadcrumbs">
    <a href="#" class="breadcrumb active-link" data-for="staff-dashboard" >Overview</a><!--
    --><span class="breadcrumb current">Projects</span><!--
    -->
<!--    <span id="back-to-top-button">Back to top</span>-->
</div>
<!-- Breadcrumb on footer ends-->
