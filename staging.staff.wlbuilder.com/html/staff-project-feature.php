<!-- Breadcrumb section-->
<div class="breadcrumbs">
    <a href="#" class="breadcrumb active-link"  data-for="staff-dashboard">Overview</a><!--
    --><span class="breadcrumb current">Projects</span><!--
    --><form id="search-form">
        <input id="search-text" type="text" placeholder="Search Here">
        <button id="search-button" type="submit"></button>
    </form>
</div>
<!-- Breadcrumb section ends-->

<section id="staff-project-sub-menu" class="project">
</section>

<!-- section for listing out the data.-->
<section id="result-section">
    <nav class="simple-pagination"></nav><!--
    --><div class="results-per-page">
        <span>Results per page</span><!--
	--><a href="#" class="current per-page" data-perPage="5">5</a><!--
        --><a href="#" class="current per-page" data-perPage="10">10</a><!--
        --><a href="#" class="per-page" data-perPage="20">20</a><!--
        --><a href="#" class="per-page" data-perPage="50">50</a>
    </div>
    <table id="result-table">
        <thead>
            <tr>
                <th class="message"><a class="sort-button" data-sort="name" href="#">Address</a></th>
                <th class="message"><a class="sort-button" data-sort="location" href="#">District</a></th>
                <th class="message"><a class="sort-button" data-sort="isFeaturedInStaticSection" href="#">Homepage Feature</a></th>
                <th class="priority"><a class="sort-button" data-sort="displayPriority" href="#">Display Priority</a></th>
                <th  class="action"></th>
            </tr>
        </thead>
        <tbody>
            <!-- Dynamic Generation of projectList -->
        </tbody>
    </table>
    <nav class="simple-pagination"></nav><!--
    --><div class ="results-per-page">
        <span>Results per page</span><!--
	--><a href="#" class="current per-page" data-perPage="5">5</a><!--
        --><a href="#" class="current per-page" data-perPage="10">10</a><!--
        --><a href="#" class="per-page" data-perPage="20">20</a><!--
        --><a href="#" class="per-page" data-perPage="50">50</a>
    </div>
</section>
<!-- section for listing out the data.-->

<!-- Section for editing fabric care -->
<section class="setting-section" id ="edit-project">
    <h2 class="edit-project-heading"></h2>
    <form action="#" name="edit-project" data-pre="">
        <input type="hidden" value="" name="id"/>
        <table class="setting-table" id="edit-table">
            <tbody>
                <tr>
                    <td class="label"><label for="edit-project-lease-hold">Leasehold (Years)</label></td>
                    <td class="input"><input type="text" name="leaseHold"  value="" id="edit-project-lease-hold" maxlength="3"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-build-up-area">Built-up Area (SQF)</label></td>
                    <td class="input"><input type="text" name="buildUpArea" value="" id="edit-project-build-up-area" maxlength="6"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-build-in-area">Built-in Area (SQF)</label></td>
                    <td class="input"><input type="text" name="buildInArea" value="" id="edit-project-build-in-area" maxlength="6"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-bed-room">Bedrooms</label></td>
                    <td class="input"><input type="text" name="bedRoom" value="" id="edit-project-bed-room" maxlength="3"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-shower">Showers</label></td>
                    <td class="input"><input type="text" name="shower" value="" id="edit-project-shower" maxlength="2"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-estimated-cost">Estimated Cost (Per SQF)</label></td>
                    <td class="input"><input type="text" name="estimatedCost" value="" id="edit-project-estimated-cost" />
                        <label></label></td>
                </tr>
                <tr><td></td><td></td></tr>
                <tr>
                    <td class="label"><label for="edit-project-start-time">Timeline</label></td>
                    <td>
                        <label for="startDate">Start Date</label>
                        <input type="text" id="startDate" name="startDate" value="" placeholder=""/>
                        <label for="endDate">End Date</label>
                        <input type="text" id="endDate" name="endDate" value="" placeholder=""/>
                    </td>
                </tr>
           
                <tr>
                    <td class="label"><label for="edit-project-demolition-work">Demolition (Weeks)</label></td>
                    <td class="input"><input type="text" name="demolitionWork" value="" id="edit-project-demolition-work" maxlength="3"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-structure-work">Structure Works (Weeks)</label></td>
                    <td class="input"><input type="text" name="structureWork" value="" id="edit-project-structure-work" maxlength="3"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-me-work">M&E Works (Weeks)</label></td>
                    <td class="input"><input type="text" name="meWork" value="" id="edit-project-me-work" maxlength="3"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-external-work">External Works (Weeks)</label></td>
                    <td class="input"><input type="text" name="externalWork" value="" id="edit-project-external-work" maxlength="3"   />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-architectural-work">Architectural Works (Weeks)</label></td>
                    <td class="input"><input type="text" name="architecturalWork" value="" id="edit-project-architectural-work" maxlength="3"   />
                        <label></label></td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
            <button class="save-btn" type="submit">Save</button><!--
            --><button class="undo-btn" type="button">Cancel</button>
            <output class="edit-info-loader"></output>
        </div>
    </form>
</section>
<!-- Section for editing project-feature ends-->

<!-- Breadcrumb on footer-->
<div class="breadcrumbs">
    <a href="#" class="breadcrumb active-link" data-for="staff-dashboard" >Overview</a><!--
    --><span class="breadcrumb current">Projects</span><!--
    -->
<!--    <span id="back-to-top-button">Back to top</span>-->
</div>
<!-- Breadcrumb on footer ends-->
