<!-- Breadcrumb section-->
<div class="breadcrumbs">
    <a href="#" class="breadcrumb active-link"  data-for="staff-dashboard">Overview</a><!--
    --><span class="breadcrumb current">Projects</span><!--
    --><form id="search-form">
        <input id="search-text" type="text" placeholder="Search Here">
        <button id="search-button" type="submit"></button>
    </form>
</div>
<!-- Breadcrumb section ends-->

<section id="staff-project-sub-menu" class="project">
</section>

<!-- section for listing out the data.-->
<section id="result-section">
    <nav class="simple-pagination"></nav><!--
    --><div class="results-per-page"><!--
        --><span>Results per page</span><!--
        --><a href="#" class="current per-page" data-perPage="5">5</a><!--
        --><a href="#" class="current per-page" data-perPage="10">10</a><!--
        --><a href="#" class="per-page" data-perPage="20">20</a><!--
        --><a href="#" class="per-page" data-perPage="50">50</a>
    </div>
    <table id="result-table">
        <thead>
            <tr>
                <th class="message"><a class="sort-button" data-sort="name" href="#">Address</a></th>
                <th class="message"><a class="sort-button" data-sort="location" href="#">District</a></th>
                <th class="message"><a class="sort-button" data-sort="isFeaturedInStaticSection" href="#">Homepage Feature</a></th>
                <th class="priority"><a class="sort-button" data-sort="displayPriority" href="#">Display Priority</a></th>
                <th  class="action"></th>
            </tr>
            </tr>
        </thead>
        <tbody>
            <!-- Dynamic Generation of projectList -->
        </tbody>
    </table>
    <nav class="simple-pagination"></nav><!--
    --><div class ="results-per-page">
        <span>Results per page</span><!--
        --><a href="#" class="current per-page" data-perPage="5">5</a><!--
        --><a href="#" class="current per-page" data-perPage="10">10</a><!--
        --><a href="#" class="per-page" data-perPage="20">20</a><!--
        --><a href="#" class="per-page" data-perPage="50">50</a>
    </div>
</section>
<!-- section for listing out the data ends.-->

<!-- section for listing out the project image data.-->

<div id="manage-project-image" class="setting-section">    

    <p id="header-error"></p>

    <section id="result-section-image" class="popup-setting-section" data-callbackUrl="staff-project-image-list" data-parameter="">
        <h2 id="popup-response-title" class="multigrid-header">Gallery Image</h2>
        
        <nav class="simple-pagination"></nav><!--
    --><div class ="results-per-page">
        <span>Results per page</span><!--
        --><a href="#" class="current per-page" data-perPage="5">5</a><!--
        --><a href="#" class="current per-page" data-perPage="10">10</a><!--
        --><a href="#" class="per-page" data-perPage="20">20</a><!--
        --><a href="#" class="per-page" data-perPage="50">50</a>
    </div>
        <table class="pop-result-table">
            <thead>
                <tr>
                    <th class="pop-result-name"><a class="sort-button" data-sort="name" href="#">Name</a></th>
                    <th class="pop-result-name"><a class="sort-button" data-sort="isPanorama" href="#">Type</a></th>
                    <th class="pop-result-name"><a class="sort-button" data-sort="isStaticSectionFeaturedImage" href="#">Featured</a></th>
                    <th class="pop-result-name"><a class="sort-button" data-sort="displayPriority" href="#">Display Priority</a></th>
                    <th class="action"></th>        
                </tr>
            </thead>
            <tbody id="result-table-image">
                <!-- Dynamic Content of project image -->
            </tbody>
        </table>
    <nav class="simple-pagination"></nav><!--
    --><div class ="results-per-page">
        <span>Results per page</span><!--
        --><a href="#" class="current per-page" data-perPage="5">5</a><!--
        --><a href="#" class="current per-page" data-perPage="10">10</a><!--
        --><a href="#" class="per-page" data-perPage="20">20</a><!--
        --><a href="#" class="per-page" data-perPage="50">50</a>
    </div>
    </section>

</div>
<!-- Section for editproject-images-->
<section class="setting-section" id="edit-project-image">
    <h2>Edit Gallery Image</h2>
    <form action="#" name="edit-project-image">
        <table id="edit-table" class="setting-table">
            <tbody>

                <tr>
                    <td class="label"><label for="edit-project-image-name">Name</label></td>
                    <td class="input"><input type="text" value="" placeholder="Name" name="name" id="edit-project-image-name" />
                        <label></label></td>
                </tr>
                <tr id="panaromicRow">
                    <td class="label"><label for="edit-project-image-panoramic">Type</label></td>
                    <td class="input">
                        <select name="panoramic" class="edit-project-image-panoramic" id="edit-project-image-panoramic">
                            <option value="0">Static</option>
                            <option value="1">Panorama</option>                            
                        </select>
                        <label></label>
                    </td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-image-section-featured">Is Featured</label></td>
                    <td class="input">
                        <select name="isFeatured" class="edit-project-image-section-featured" id="edit-project-image-section-featured">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                        <label></label>
                    </td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-project-image-priority">Display Priority</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Higher priority will be shown first; set to 0 to hide.)" name="displayPriority" id="edit-project-image-priority" />
                        <label></label></td>
                </tr>
                <tr>
<!--                        <td class="label"><label for="edit-project-image-image">Image</label></td>-->
                    <td class="input" colspan="2">
                        <input type="hidden" name="prevImg" >
                        <div id="edit-project-image-image-preview-area" class="image-preview-area">
                            <img id="edit-project-image-image-preview" src="" alt="" />
                        </div><!--
                        --><div class="upload-instructions ">
                            <p>Upload Image <br> For Panorama (4000 x 2000 Pixels)<br>  For Static (1440 x 960 Pixels) </p>
                            <input type="file" id="edit-project-image-file" name="img" />
                            <label for="edit-project-image-file">Browse</label><!--
                            --><output for="edit-project-image-file"></output>
                            <div class="uploaded-name-holder">
                                <label>Image Name</label><!--
                                --><input disabled="" id="edit-project-image-url" type="text" placeholder="" />
                                <span class="confirm-delete" data-delete="">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                <a href="javascript:void(0)" id="edit-project-image-delete" class="deleteProjectImage btn-delete" >×</a>
                            </div>
                        </div>
                        <label></label>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
            <button class="save-btn" type="submit"> Save </button><!-- 
            --><button class="undo-btn" type="button"> Cancel </button>
            <output class="edit-gallery-loader"></output>
        </div>
        <div class="edit-error "></div>
    </form>
</section>
<!-- Section for edit project images -->

<!-- Section for adding new project-images-->
<section class="setting-section" id="add-project-image">
    <h2>Add Gallery Image</h2>
    <form action="#" name="add-project-image">
        <input type="hidden" id="add-hidden-projectId" name="projectId" value="">
        <table id="add-table" class="setting-table">
            <tbody>

                <tr>
                    <td class="label"><label for="add-project-image-name">Name</label></td>
                    <td class="input"><input type="text" value="" placeholder="Name" name="name" id="add-project-image-name" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="add-project-image-panoramic">Type</label></td>
                    <td class="input">
                        <select name="panoramic" class="add-project-image-panoramic" id="add-project-image-panoramic">
                            <option value="0">Static</option>
                            <option value="1">Panorama</option>
                        </select>
                        <label></label>
                    </td>
                </tr>
                <tr>
                    <td class="label"><label for="add-project-image-section-featured">Is Featured</label></td>
                    <td class="input">
                        <select name="isFeatured" class="add-project-image-section-featured" id="add-project-image-section-featured">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                        <label></label>
                    </td>
                </tr>
                <tr>
                    <td class="label"><label for="add-project-image-priority">Display Priority</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Higher priority will be shown first; set to 0 to hide.)"  name="displayPriority" id="add-project-image-priority" />
                        <label></label></td>
                </tr>
                <tr>
                    <td class="input" colspan="2">
                        <div id="add-project-image-image-preview-area" class="image-preview-area">
                            <img id="add-project-image-image-preview" src="" alt="" />
                        </div><!--
                        --><div class="upload-instructions ">
                            <p>Upload Image <br> For Panorama (4000 x 2000 Pixels)<br>  For Static (1440 x 960 Pixels) </p>
                            <input type="file" id="add-project-image-file" name="img" />
                            <label for="add-project-image-file">Browse</label><!--
                            --><output for="add-project-image-file"></output>
                            <div class="uploaded-name-holder">
                                <label>Image Name</label><!--
                                --><input disabled="" type="text" placeholder="" />
                                <span class="confirm-delete" data-delete="">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                <a href="javascript:void(0)" class="deleteProjectImage btn-delete" >×</a>
                            </div>
                        </div>
                        <label></label>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
            <button class="add-btn" type="submit"> Submit </button><!-- 
            --><button class="undo-btn" type="button"> Cancel </button>
            <output class="add-gallery-loader"></output>
        </div>
        <div class="add-error "></div>
    </form>
</section>
<!-- Section for adding new project images -->

<!-- Breadcrumb on footer-->
<div class="breadcrumbs">
    <a href="#" class="breadcrumb active-link" data-for="staff-dashboard" >Overview</a><!--
    --><span class="breadcrumb current">Projects</span><!--
    -->
<!--    <span id="back-to-top-button">Back to top</span>-->
</div>
<!-- Breadcrumb on footer ends-->
