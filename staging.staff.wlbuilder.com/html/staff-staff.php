﻿<!-- Breadcrumb section-->
<div class="breadcrumbs">
  <a href="#" class="breadcrumb active-link"  data-for="staff-dashboard">Overview</a><!--
	--><span class="breadcrumb current">Staff</span><!--
	--><form id="search-form">
		<input id="search-text" type="text" placeholder="Search Here">
		<button id="search-button" type="submit"></button>
	</form>
</div>
<!-- Breadcrumb section ends-->


<!-- section for listing out the data.-->
<section id="result-section">
  <nav class="simple-pagination"></nav>
  <div class="results-per-page">
    <span>Results per page</span><!--
		--><a href="#" class="current per-page" data-perPage="10">10</a><!--
		--><a href="#" class="per-page" data-perPage="20">20</a><!--
		--><a href="#" class="per-page" data-perPage="50">50</a>
  </div>
  <table id="result-table">
    <thead>
      <tr>
        <td class="access-level">Access Level<a class="sort-button" data-sort="accessLevel" href="#"></a></td>
        <td class="name">Name<a class="sort-button" data-sort="name" href="#"></a></td>
		<td class="email">Email<a class="sort-button" data-sort="email" href="#"></a></td>
        <td class="action"></td>
      </tr>
    </thead>
    <tbody><!--	Dynamic Generation of Staff List-->
    </tbody>
  </table>
  <nav class="simple-pagination"></nav>
  <div class ="results-per-page">
    <span>Results per page</span><!--
		--><a href="#" class="current per-page" data-perPage="10">10</a><!--
		--><a href="#" class="per-page" data-perPage="20">20</a><!--
		--><a href="#" class="per-page" data-perPage="50">50</a>
  </div>
</section>
<!-- section for listing out the data.-->

<!-- Section for editing Staff -->
<section class="setting-section" id ="edit-staff">
	<h2>Edit selected staff</h2>
	<form action="#" name="edit-staff" data-pre="">
		<input type="hidden" value="" name="id"/>
		<table class="setting-table" id="edit-table">
			<tbody>
				<tr>
					<td class="label"><label for="edit-staff-email">Email</label></td>
					<td class="input"><input type="text" name="email" placeholder="Staff Email" value="" id="edit-staff-email"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-staff-first-name">First Name</label></td>
					<td class="input"><input type="text" name="firstName" placeholder="First Name" value="" id="edit-staff-first-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-staff-last-name">Last Name</label></td>
					<td class="input"><input type="text" name="lastName" placeholder="Last Name" value="" id="edit-staff-last-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="edit-staff-access-level">Access Level</label></td>
					<td class="input"><input type="text" value="" placeholder="Access Level Number" name="accessLevel" id="edit-staff-access-level"/>
						<label></label></td>
				</tr>
			</tbody>
		</table>
		<div class="group-button">
			<button class="save-btn" type="submit">Save</button><!--
			--><button class="undo-btn" type="button">Undo</button>
		</div>
	</form>
</section>
<!-- Section for editing staff ends-->

<!-- Section for adding new staff -->
<section class="setting-section" id="add-staff">
	<h2>Add new staff</h2>
	<form action="#" name="add-staff">
		<table id="add-table" class="setting-table">
			<tbody>
				<tr>
					<td class="label"><label for="add-staff-email">Email</label></td>
					<td class="input"><input type="text" name="email" placeholder="Staff Email" value="" id="add-staff-email"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-staff-password">Password</label></td>
					<td class="input"><input type="password" name="password" placeholder="Staff Password" value="" id="add-staff-password"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-staff-confirm-password">Confirm Password</label></td>
					<td class="input"><input type="password" name="confirmPassword" placeholder="Confirm Password" value="" id="add-staff-confirm-password"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-staff-first-name">First Name</label></td>
					<td class="input"><input type="text" name="firstName" placeholder="First Name" value="" id="add-staff-first-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-staff-last-name">Last Name</label></td>
					<td class="input"><input type="text" name="lastName" placeholder="Last Name" value="" id="add-staff-last-name"/>
						<label></label></td>
				</tr>
				<tr>
					<td class="label"><label for="add-staff-access-level">Access Level</label></td>
					<td class="input"><input type="text" value="" placeholder="Access Level Number" name="accessLevel" id="add-staff-access-level"/>
						<label></label></td>
				</tr>
			</tbody>
		</table>
		<div class="group-button">
			<button class="add-btn" type="submit">Add</button><!--
			--><button class="undo-btn" type="button">Clear</button>
		</div>
	</form>
</section>
<!-- Section for adding new Staff ends -->

<!-- Breadcrumb on footer-->
<div class="breadcrumbs">
  <a href="#" class="breadcrumb active-link" data-for="staff-dashboard" >Overview</a><!--
	--><span class="breadcrumb current">Staff</span><!--
	--><span id="back-to-top-button">Back to top</span>
</div>
<!-- Breadcrumb on footer ends-->
