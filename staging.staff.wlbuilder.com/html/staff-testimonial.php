﻿<!-- Breadcrumb section-->
<div class="breadcrumbs">
    <a href="#" class="breadcrumb active-link" data-for="staff-dashboard">Overview</a><!--
    --><span class="breadcrumb current">Testimonials</span><!--
    --><form id="search-form">
        <input id="search-text" type="text" placeholder="Search Here">
        <button id="search-button" type="submit"></button>
    </form>
</div>
<!-- Breadcrumb section ends-->

<!-- section for listing out the data.-->
<section id="result-section">
    <nav class="simple-pagination"><span class="current">Previous</span><span class="current">1</span><span class="current">Next</span></nav><!--
    --><div class ="results-per-page"></div>
    <table id="result-table">
        <thead>
            <tr>
                <th class="name"><a class="sort-button" data-sort="name" href="#">Name</a></th>
                <th class="message"><a class="sort-button" data-sort="message" href="#">Message</a></th>
                <th class="priority"><a class="sort-button" data-sort="displayPriority" href="#">Display Priority</a></th>
                <th class="action"><a href="#"></a></th>

            </tr>
        </thead>
        <tbody><!--			Dynamic Generation of TestimonialList-->
        </tbody>
    </table>
    <nav class="simple-pagination"><span class="current">Previous</span><span class="current">1</span><span class="current">Next</span></nav><!--
    --><div class ="results-per-page"></div>
</section>
<!-- section for listing out the data.-->

<!-- Section for editing testimonial -->
<section class="setting-section" id ="edit-testimonial">
    <h2>Edit testimonial</h2>
    <form action="#" name="edit-testimonial" data-pre="">
        <input type="hidden" value="" name="id"/>
        <table class="setting-table" id="edit-table">
            <tbody>
                <tr>
                    <td class="label"><label for="edit-testimonial-name">Name</label></td>
                    <td class="input"><input type="text" placeholder="(Max. 128 characters)" name="name" value="" id="edit-testimonial-name"/>
                        <label></label></td>
                </tr>

                <tr>
                    <td class="label"><label for="edit-testimonial-message">Message</label></td>
                    <td class="input">


                        <textarea placeholder="(Max. 512 characters)"  name="message" id="edit-testimonial-message"></textarea>
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="edit-testimonial-priority">Display Priority</label></td>
                    <td class="input"><input type="text" placeholder="(Higher priority will be shown first; set to 0 to hide.)" value="" name="displayPriority" id="edit-testimonial-priority"/>
                        <label></label></td>
                </tr>
                <tr>
                    <td class="input" colspan="2">
                        <div id="edit-testimonial-image-preview-area" class="image-preview-area">
                                <img id="edit-testimonial-image-preview" src="" alt="" />
                        </div><!--
                        --><div class="upload-instructions ">
                                <p>Upload Picture (400 x 400 Pixels)</p>
                                <input type="file" id="edit-testimonial-file" name="img" />
                                <label for="edit-testimonial-file">Browse</label><!--
                                --><output for="edit-testimonial-file"></output>
                                <div class="uploaded-name-holder">
                                <label>Image Name</label><!--
                                --><input disabled="" id="edit-testimonial-image-url" type="text" placeholder="" />
                                <span class="confirm-delete">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                <a href="javascript:void(0)" class="deleteInfoImg btn-delete" id="edit-testimonial-image-delete">×</a>
                            </div>
                        </div>
                        </td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
            <button class="save-btn" type="submit">Save</button><!-- 
            --><button class="undo-btn" type="button">Cancel</button>
            <output class="edit-testimonial-loader"></output>
        </div>
        <div class="edit-error "></div>
    </form>
</section>
<!-- Section for editing testimonial ends-->

<!-- Section for adding new testimonial -->
<section class="setting-section" id="add-testimonial">
    <h2>Add testimonial</h2>
    <form action="#" name="add-testimonial">
        <table id="add-table" class="setting-table">
            <tbody>
                <tr>
                    <td class="label"><label for="add-testimonial-name">Name</label></td>
                    <td class="input"><input type="text" name="name" placeholder="(Max. 128 characters)" value="" id="add-testimonial-name"/>
                        <label></label></td>
                </tr>

                <tr>
                    <td class="label"><label for="add-testimonial-message">Message</label></td>
                    <td class="input">
                        <textarea placeholder="(Max. 512 characters)"  name="message" id="add-testimonial-message"></textarea>
                        <label></label></td>
                </tr>
                <tr>
                    <td class="label"><label for="add-testimonial-priority">Display Priority</label></td>
                    <td class="input"><input type="text" value="" placeholder="(Higher priority will be shown first; set to 0 to hide.)" name="displayPriority" id="add-testimonial-priority"/>
                        <label></label></td>
                </tr>
                <tr>
                    <td class="input" colspan="2">
                        <div id="add-testimonial-image-preview-area" class="image-preview-area">
                                <img id="add-testimonial-image-preview" src="" alt="" />
                        </div><!--
                        --><div class="upload-instructions ">
                                <p>Upload Picture (400 x 400 Pixels)</p>
                                <input type="file" id="add-testimonial-file" name="img" />
                                <label for="add-testimonial-file">Browse</label><!--
                                --><output for="add-testimonial-file"></output>
                                <div class="uploaded-name-holder">
                                <label>Image Name</label><!--
                                --><input disabled="" type="text" placeholder="" />
                                <span class="confirm-delete">Confirm delete? <i class="confirm-yes"> Y</i> / <i class="confirm-no">N</i></span>                           
                                <a href="javascript:void(0)" class="deleteInfoImg btn-delete" id="edit-testimonial-image-delete">×</a>
                            </div>
                        </div>
                        </td>
                </tr>
            </tbody>
        </table>
        <div class="group-button">
        <button class="add-btn" type="submit">Submit</button><!--
        --><button class="undo-btn" type="button">Cancel</button>
        <output class="add-testimonial-loader"></output>
        
        </div>
        
            <div class="add-error "></div>
    </form>
</section>
<!-- Section for adding new testimonial ends -->

<!-- Breadcrumb on footer-->
<div class="breadcrumbs">
    <a href="#" class="breadcrumb active-link" data-for="staff-dashboard" >Overview</a><!--
    --><span class="breadcrumb current">Testimonials</span>
</div>
<!-- Breadcrumb on footer ends-->