<div class="breadcrumbs">
	<a class="breadcrumb" href="<?php echo SERVER_PATH;?>/staff-dashboard" data-for="staff-dashboard">Overview</a><!--
	--><span class="breadcrumb current">Change theme</span>
</div>

<section class="setting-section">
	<h2>Change theme</h2>
	<form id="edit-theme-form">
		<ul id="theme-list">
			<li class="theme">
				<label id="blue-palette" class="palette" for="blue"></label>
				<input type="radio" name="theme" value="blue" id="blue" checked="checked" />
				<label for="blue">Blue</label>
			</li><!--
			--><!--<li class="theme">
				<label id="green-palette" class="palette" for="green"></label>
				<input type="radio" name="theme" value="green" id="green" />
				<label for="green">Green</label>
			</li>--><!--
			--><!--<li class="theme">
				<label id="orange-palette" class="palette" for="orange"></label>
				<input type="radio" name="theme" value="orange" id="orange" />
				<label for="orange">Orange</label>
			</li>--><!--
			--><!--<li class="theme">
				<label id="gray-palette" class="palette" for="gray"></label>
				<input type="radio" name="theme" value="gray" id="gray" />
				<label for="gray">Gray</label>
			</li>-->
		</ul>
	</form>
</section>

<div class="breadcrumbs">
	<a class="breadcrumb" href="<?php echo SERVER_PATH;?>/staff-dashboard" data-for="staff-dashboard">Overview</a><!--
	--><span class="breadcrumb current">Change theme</span>
	
	<button id="back-to-top-button">Back to top</button>
</div>

	
