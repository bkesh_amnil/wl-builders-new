<!DOCTYPE HTML>
<html lang="<?php echo $this->lang; ?>">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="<?php echo $_SESSION['CSRF_TOKEN'];?>">
<?php
foreach ($this->meta as $key => $value) {
	echo '	<meta name="'.$key.'" content="'.$value.'">'."\r\n";
}
?>
	<title><?php echo $this->title;?></title>
	<link rel="canonical" href="<?php echo PROTOCOL.DOMAIN.'/'.$this->slug; ?>" />
	<link rel="shortcut icon" href="<?php echo SERVER_PATH;?>/img/favicon.ico" type="image/x-icon" />
	<link rel="stylesheet" href="<?php echo SERVER_PATH;?>/css/common/reset.css" />
	<link rel="stylesheet" href="<?php echo SERVER_PATH;?>/css/common/staff.css" />
        <link rel="stylesheet" href="<?php echo SERVER_PATH;?>/css/common/sweetalert.css" />
        <link rel="stylesheet" href="<?php echo SERVER_PATH;?>/css/bootstrap-material-datetimepicker.css" />
        

<?php
foreach ($this->css as $css) {
	echo '	<link rel="stylesheet" href="'.SERVER_PATH.'/css/'.$css.'" data-page="'.$this->name.'" />'."\r\n";
}
?>
<link rel="stylesheet" href="<?php echo SERVER_PATH;?>/css/theme/blue-theme.css" id="theme-css" />
</head>
<body>
