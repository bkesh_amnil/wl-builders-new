</div>
<!-- end of content -->
<footer>
  <div class="container">
      <a id="footer-logo" href="http://www.techplusart.com"><img src="img/staff/logo-t+a.png" alt="Tech + Art"></a>
		Making stuff simple since 2012. 
	</div>
</footer>
<div id="error-overlay" class="hide"></div>
<div id="login-overlay" class="hide">
<form id="login-form" class="overlay-form hide" method="post">
  <label for="login-email">Email</label>
  <input type="text" name="email" id="login-email" placeholder="Your email" />

  <label for="login-password">Password</label>
  <input type="password" name="password" id="login-password" placeholder="Your password" />

  <input type="checkbox" class="css-checkbox" name="persistent" id="login-persistent" checked="checked" />
  <label for="login-persistent">Remember me on this computer</label>

  <a href="#forgot-password-form hide" id="forgot-password-link">Forgot your password?</a>

  <button type="submit" class="login_button">Log In</button>
  
    <output class=""></output>
</form>

<form id="forgot-password-form" class="overlay-form hide" method="post">
  <label for="forgotten-email">Email</label>
  <input type="text" name="email" id="forgotten-email" placeholder="Your email" />

  <p id="forgot-password-message">
    Not to worry. Just enter your<br />
    email address and click Send<br />
    to retrieve your new password.
  </p>
  <a href="#login-form" id="login-link">Return to login screen.</a>

  <button type="submit" class="forgot_pass_button">Send</button>
    <output class=""></output>
</form>
</div>
<!-- Start of overlay for drag and drop module positions-->
<div id="overlay">
 <div class="container">
  <aside id="save-layout-panel">
   <p>Drag and drop the modules to rearrange order of listing.</p>
   <button type="button" id="save-layout-button">Save</button>
   <button type="button" id="clear-layout-changes-button">Exit</button>
  </aside><!--
  --><section id="overlay-modules">
   <!-- JS -->
  </section>
 </div>
</div>
<!-- End of overlay for drag and drop module positions-->
<div id="popup-response" class="overlay-bg"></div>

<!--[if IE]>
<p id="browsehappy">We detected that you are using an <strong>outdated</strong> browser.<br>Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->

<!-- include libraries -->
<script src="js/vendor/jquery-2.1.1.min.js"></script><!--fallback if Google CDN fails-->
<script src="js/vendor/sweetalert.min.js"></script>
<script src="js/vendor/jquery-easings.1.3.min.js"></script>
<script src="js/vendor/hand-1.3.8.js"></script>
<script src="js/vendor/jquery.selectric.min.js"></script>
<script src="js/vendor/jquery.selectric.placeholder.min.js"></script>

<!-- include helpers -->
<script src="js/helpers/pointerclick.js"></script>
<script src="js/helpers/string.js"></script>
<script src="js/helpers/array.js"></script>
<script src="js/helpers/date.js"></script>
<script src="js/helpers/form.js"></script>
<script src="js/helpers/conversion.js"></script>
<script src="js/helpers/number.js"></script>
<script src="js/helpers/amd.js"></script>

<!-- include application scripts-->
<script src="js/common/config.js"></script>
<script src="js/common/ajaxloader.js"></script>
<!-- config must be included first -->
<script src="js/common/ajax-customer-api.js"></script>
<script src="js/common/ajax-staff-api.js"></script>
<script src="js/common/ajax-public-api.js"></script>
<script src="js/common/ajaxpageswitcher.js"></script>
<script src="js/common/db.js"></script>
<script src="js/common/window.js"></script>
<script src="js/common/init.js"></script>
<script src="js/common/staff.js"></script>
<script src="js/common/paginator.js"></script>


<?php
foreach ($this->js as $js) {
	if (startsWith($js, 'http://') || startsWith($js, 'https://') || startsWith($js, '//')) {
		echo '<script src="' . $js . '" data-page="' . $this->name . '"></script>' . "\r\n";
	} else {
		echo '<script src="' . SERVER_PATH . '/js/' . $js . '" data-page="' . $this->name . '"></script>' . "\r\n";
	}
}
?>
</body>
</html>
