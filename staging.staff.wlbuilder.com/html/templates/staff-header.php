<header>
	<div class="container">
	<a href="#" id="header-logo-link">
		<img id="header-logo" src="<?php echo SERVER_PATH;?>/img/staff/logo-t+a-dashboard.png" alt="Tech+Art Dashboard"  data-for="staff-dashboard" />
	</a>
      	<section id="user-controls">
			<a href="<?php echo SERVER_PATH;?>/staff-account-settings" id="profile-link" data-for="staff-account-settings">isen.majennt@techplusart.com</a>
			<a href="#" id="logout-link">Logout</a>
		</section>
             <button type="button" id="start-button">Get Started</button>
            
	</div>
</header>
<div id="content" data-page="<?php echo $this->name;?>">
