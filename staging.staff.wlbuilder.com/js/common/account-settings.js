/***
 * This Script incorporates following features:
 * - Change Password
 * - Edit Personal Profile
 * - Edit Addresses
 * - Change Newsletter Settings
 *
 * Before use:
 * - ajax-customer-api is init
 * - ensure localDB & sessionDB are init
 ***/

(function(app, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.AccountSetting = function() {
        $.fn.extend({
            // Adds class login-fadein and removes class login-fadeout
            showPopup: function() {
                return this.each(function() {
                    $(this).css("display", "block");
                    var el = $("#content");
                    var elOffset = el.offset().top;
                    var elHeight = el.height();
                    var windowHeight = $(window).height();
                    var offset;
                    if (elHeight < windowHeight) {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
                    } else {
                        offset = elOffset - ((windowHeight / 2) - (elHeight / 2));
                    }
                    $("html, body").animate({
                        scrollTop: offset
                    }, POPUP_MESSAGE_ENTER_DURATION);
                });
            },
            // Adds class login-fadeout and removes class login-fadein
            hidePopup: function() {
                return this.each(function() {
                    $(this).css("display", "none");
                });
            },
        });
        var personal_data,
            country_elms = $("#personal-country, #shipping-home-country, #shipping-office-country");

        /***
         * Removes all events: click, change, submit
         ***/
        function resetEvent() {
            $("form[name='change-password']").off("submit");
            $("form[name='newsletter-option']").off("submit");
            $("#reset-newsletter-option-form").suppressClick();
            $("#reset-newsletter-option-form").off("pointerclick");
            $("form[name='personal-profile']").off("submit");
            $("#reset-personal-profile").suppressClick();
            $("#reset-personal-profile").off("pointerclick");
            $("form[name='shipping-address']").off("submit");
            $("#reset-shipping-address").suppressClick();
            $("#reset-shipping-address").off("pointerclick");
            $("#account-settings input").off("input");
            $("#account-settings select").off("change");
            country_elms.off("change");
            $("#popup-close").off("change");
            $("#response-popup-close").suppressClick();
        } // resetEvent()

        /***
         * Populates all the country dropdowns with data from localDB
         ***/
        function initUI() {
            var countries = localDB.countries;
            var country_options = '<option value="0">Select Country</option>';
            $.each(countries, function(i, v) {
                country_options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            country_elms.html(country_options);
            $('select').selectric();
        } // initUI()

        /***
         * Fills the forms with customer datas from sessionDB
         ***/
        function fillCustomerData() {
            personal_data = sessionDB.user;
            var subscription_status = sessionDB.subscriptionStatus;

            //Values for Personal Profile Form
            $("#personal-first-name").val(personal_data.firstName);
            $("#personal-last-name").val(personal_data.lastName);
            $("#personal-profession").val(personal_data.profession);
            $("#personal-company").val(personal_data.company);
            if (personal_data.residenceCountryID) {
                $("#personal-country").val(personal_data.residenceCountryID);
                if (personal_data.residenceStateID) {
                    populateStateDropdown(personal_data.residenceCountryID, "#personal-country");
                    $("#personal-state").val(personal_data.residenceStateID);
                    $("#personal-state").selectric("refresh");
                }
            }
            $("#personal-mobile").val(personal_data.residencePhone);
            var birth_date = new Date(personal_data.birthTime);
            var day = birth_date.getDate();
            if (day <= 9) {
                day = "0" + day; //requres format 00
            }
            $("#personal-birth-date-day").val(day);
            $("#personal-birth-date-day").selectric("refresh");
            var month = birth_date.getMonth() + 1;
            if (month <= 9) {
                month = "0" + month; //requres format 00
            }
            $("#personal-birth-date-mon").val(month);
            $("#personal-birth-date-mon").selectric("refresh");
            $("#personal-birth-date-year").val(birth_date.getFullYear());
            $("#personal-birth-date-year").selectric("refresh");

            //Values for Newsletter Options
            //if(subscription_status) {
            $("#newsletter-option1").prop("checked", true);
            //}else{
            //    $("#newsletter-option2").prop("checked",true);
            //}

            //Values for Shipping Address Form
            if (personal_data.homeCountryID) {
                $("#shipping-home-country").val(personal_data.homeCountryID);
                if (personal_data.homeStateID) {
                    populateStateDropdown(personal_data.homeCountryID, "#shipping-home-country");
                    $("#shipping-home-state").val(personal_data.homeStateID);
                    $("#shipping-home-state").selectric("refresh");
                }
            }
            $("#shipping-home-post-code").val(personal_data.homePostalCode);
            $("#shipping-home-address").val(personal_data.homeAddress);
            $("#shipping-home-contact").val(personal_data.homePhone);
            if (personal_data.officeCountryID) {
                $("#shipping-office-country").val(personal_data.officeCountryID);
                if (personal_data.officeStateID) {
                    populateStateDropdown(personal_data.officeCountryID, "#shipping-office-country");
                    $("#shipping-office-state").val(personal_data.officeStateID);
                    $("#shipping-office-state").selectric("refresh");
                }
            }
            $("#shipping-office-post-code").val(personal_data.officePostalCode);
            $("#shipping-office-address").val(personal_data.officeAddress);
            $("#shipping-office-contact").val(personal_data.officePhone);
        } // fillCustomerData()

        /*
         * Attaches various event handlers
         */
        function initEvent() {
            $(window.app).off("ready");
            $(window.app).on("ready", function() {
                if (!window.app.Module.Login.isUserLoggedIn()) {
                    var target_page = DEFAULT_PAGE;
                    $.when(window.app.Module.PageSwitcher.switchPage(target_page)).done(function(data) {
                        // Remember history (state, title, URL)
                        window.history.pushState("nav", "Page: " + target_page, DOMAIN_URL + "/" + target_page);
                    }).fail(function() {});
                }
                fillCustomerData();
                $(window.app).off("ready");
            });

            /*
             * Listens to logout event and redirects to home page
             */
            $(window.app).off("loggedOutAccountSettings");
            $(window.app).on("loggedOutAccountSettings", function() {
                if ($("#content").attr("data-page") === "account-settings") {
                    var target_page = DEFAULT_PAGE;
                    $.when(window.app.Module.PageSwitcher.switchPage(target_page)).done(function(data) {
                        // Remember history (state, title, URL)
                        window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                    }).fail(function() {

                    }).always(function() {

                    });
                }
            });

            //Saves change password Form
            $("form[name='change-password']").on("submit", function(e) {
                e.preventDefault();
                $("#popup-response-title").text("Change Password");
                var oldPasswordElement = $("#acc-old-password");
                var newPasswordElement = $("#acc-new-password");
                var confirmPasswordElement = $("#acc-confirm-password");
                if (oldPasswordElement.val() === "") {
                    showErrorTicker(oldPasswordElement, "Old password required.");
                    return;
                }
                if (newPasswordElement.val() === "") {
                    showErrorTicker(newPasswordElement, "New password required.");
                    return;
                }
                if (newPasswordElement.val().length < 6) {
                    showErrorTicker(newPasswordElement, "Length insufficient.");
                    return;
                }
                if (newPasswordElement.val() !== confirmPasswordElement.val()) {
                    showErrorTicker(confirmPasswordElement, "Passwords do not match.");
                    return;
                }
                var change_pass_data = $("input[id!=acc-confirm-password]", this).serializeArray();
                $.when(customerAPI.changePassword(change_pass_data)).done(function() {
                    $("form[name='change-password']")[0].reset();
                    $("#popup-response-message").text("Password successfully updated.");
                    return;
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    $("#popup-response-message").text(responseTxt.error);
                    return;
                });
                $("#popup-response").showPopup();
            });

            //Saves E-Newsletter Options Form
            $("form[name='newsletter-option']").on("submit", function(e) {
                e.preventDefault();
                $("#popup-response-title").text("E-Newsletter Options");
                var newsletter_option = $('input[name="newsletterOption"]:checked');
                if (newsletter_option.val() == 1) {
                    $.when(customerAPI.subscribeMailList()).done(function(data) {
                        if (data.info) {
                            $("#popup-response-message").text("E-Newsletter has already been subscribed.");
                        } else {
                            $("#popup-response-message").text("You have successfully  subscribed  E-Newsletter.");
                        }
                        $("#newsletter-option1").prop("checked", true);
                    }).fail(function(data) {
                        var responseTxt = $.parseJSON(data.responseText);
                        $("#popup-response-message").text(responseTxt.error);
                        return;
                    });
                } else {
                    $.when(customerAPI.unsubscribeMailList()).done(function(data) {
                        if (data.info) {
                            $("#popup-response-message").text("E-Newsletter has already been unsubscribed.");
                        } else {
                            $("#popup-response-message").text("You have successfully  unsubscribed  E-Newsletter.");
                        }
                        $("#newsletter-option1").prop("checked", true);
                    }).fail(function(data) {
                        var responseTxt = $.parseJSON(data.responseText);
                        $("#form-message").html(responseTxt.error);
                        return;
                    });
                }
                $("#popup-response").showPopup();
            });

            // Resets E-Newsletter Options Form
            listenForPointerClick($("#reset-newsletter-option-form"));
            $("#reset-newsletter-option-form").on("pointerclick", function(e) {
                $("form[name='newsletter-option']")[0].reset();
                $("#newsletter-option1").prop("checked", true);
            });

            // Resets the events            
            /*$("form[name='change-password']").on("pointerclick", ".account-btn", function(e) {
                $(".input-error").html('').removeClass('input-error');
                $("#acc-old-password").focus();
            });*/

            //Saves Personal Profile Form
            $("form[name='personal-profile']").on("submit", function(e) {
                e.preventDefault();
                $(this).find(".input-type-error").removeClass("input-type-error");
                $(this).find(".input-error").remove();
                $("#popup-response-title").text("Personal Profile");
                var first_name_elm = $("#personal-first-name");
                var last_name_elm = $("#personal-last-name");
                var country_elm = $("#personal-country");
                var state_elm = $("#personal-state");
                var mobile_elm = $("#personal-mobile");
                var alphaNumericRegEx = /\d/;
                var specialCharacterReg = /^\s*[a-zA-Z0-9,\s]+\s*$/;

                if (first_name_elm.val() === "" || alphaNumericRegEx.test(first_name_elm.val()) || !specialCharacterReg.test(first_name_elm.val())) {
                    showErrorTicker(first_name_elm, "Invalid first name.");
                    return;
                }
                if (last_name_elm.val() === "" || alphaNumericRegEx.test(last_name_elm.val()) || !specialCharacterReg.test(last_name_elm.val())) {
                    showErrorTicker(last_name_elm, "Invalid last name.");
                    return;
                }
                if (country_elm.val() == 0) {
                    showErrorTicker(country_elm.parents(".selectricWrapper"), "Invalid country name.");
                    return;
                }
                if (state_elm.val() == 0) {
                    showErrorTicker(state_elm.parents(".selectricWrapper"), "Invalid state name.");
                    return;
                }
                if (mobile_elm.val() === "") {
                    showErrorTicker(mobile_elm, "Invalid mobile no.");
                    return;
                }
                if (!$.isNumeric(mobile_elm.val())) {
                    showErrorTicker(mobile_elm, "Invalid mobile no.");
                    return;
                }

                var birth_date = $("#personal-birth-date-year").val() + "-" + $("#personal-birth-date-mon").val() + "-" + $("#personal-birth-date-day").val();
                var birthTime = new Date(birth_date).getTime();
                var form_data = $(this).serializeArray();
                form_data.push({
                    name: "birthTime",
                    value: birthTime
                });

                //for passing entire object
                form_data.push({
                    name: "homeCountryID",
                    value: personal_data.homeCountryID
                });
                form_data.push({
                    name: "homeStateID",
                    value: personal_data.homeStateID
                });
                form_data.push({
                    name: "homePostalCode",
                    value: personal_data.homePostalCode
                });
                form_data.push({
                    name: "homeAddress",
                    value: personal_data.homeAddress
                });
                form_data.push({
                    name: "homePhone",
                    value: personal_data.homePhone
                });
                form_data.push({
                    name: "officeCountryID",
                    value: personal_data.officeCountryID
                });
                form_data.push({
                    name: "officeStateID",
                    value: personal_data.officeStateID
                });
                form_data.push({
                    name: "officePostalCode",
                    value: personal_data.officePostalCode
                });
                form_data.push({
                    name: "officeAddress",
                    value: personal_data.officeAddress
                });
                form_data.push({
                    name: "officePhone",
                    value: personal_data.officePhone
                });

                $.when(customerAPI.editProfile(form_data)).done(function() {
                    $("#popup-response-message").text("You have successfully updated your profile information.");
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    $("#popup-response-message").text(responseTxt.error);
                    return;
                });
                $("#popup-response").showPopup();
            });

            // Resets Personal Profile Form
            listenForPointerClick($("#reset-personal-profile"));
            $("#reset-personal-profile").on("pointerclick", function(e) {
                personal_data = sessionDB.user;

                //Values for Personal Profile Form
                $("#personal-first-name").val(personal_data.firstName);
                $("#personal-last-name").val(personal_data.lastName);
                $("#personal-profession").val(personal_data.profession);
                $("#personal-company").val(personal_data.company);
                if (personal_data.residenceCountryID) {
                    $("#personal-country").val(personal_data.residenceCountryID);
                    if (personal_data.residenceStateID) {
                        populateStateDropdown(personal_data.residenceCountryID, "#personal-country");
                        $("#personal-state").val(personal_data.residenceStateID);
                        $("#personal-state").selectric("refresh");
                    }
                }
                $("#personal-mobile").val(personal_data.residencePhone);
                var birth_date = new Date(personal_data.birthTime);
                var day = birth_date.getDate();
                if (day <= 9) {
                    day = "0" + day; //requres format 00
                }
                $("#personal-birth-date-day").val(day);
                $("#personal-birth-date-day").selectric("refresh");
                var month = birth_date.getMonth() + 1;
                if (month <= 9) {
                    month = "0" + month; //requres format 00
                }
                $("#personal-birth-date-mon").val(month);
                $("#personal-birth-date-mon").selectric("refresh");
                $("#personal-birth-date-year").val(birth_date.getFullYear());
                $("#personal-birth-date-year").selectric("refresh");

                $("form[name='personal-profile'] input").removeClass("input-type-error");
                $("form[name='personal-profile'] .input-error").remove();
            });

            //Saves Shipping Address Form
            $("form[name='shipping-address']").on("submit", function(e) {
                e.preventDefault();
                $(this).find(".input-type-error").removeClass("input-type-error");
                $(this).find(".input-error").remove();
                $("#popup-response-title").text("Shipping Address");
                var shipping_home_country_elm = $("#shipping-home-country");
                var shipping_home_state_elm = $("#shipping-home-state");
                var shipping_home_contact_elm = $("#shipping-home-contact");
                var shipping_office_country_elm = $("#shipping-office-country");
                var shipping_office_state_elm = $("#shipping-office-state");
                var shipping_office_contact_elm = $("#shipping-office-contact");
                // if(shipping_home_country_elm.val() == 0) {
                //     showErrorTicker(shipping_home_country_elm,"Invalid Country Name");
                //     return;
                // }
                // if(shipping_home_state_elm.val() == 0) {
                //     showErrorTicker(shipping_home_state_elm,"Invalid State Name");
                //     return;
                // }
                if (shipping_home_contact_elm.val() !== "" && !$.isNumeric(shipping_home_contact_elm.val())) {
                    showErrorTicker(shipping_home_contact_elm, "Invalid home contact.");
                    return;
                }
                // if(shipping_office_country_elm.val() == 0) {
                //     showErrorTicker(shipping_office_country_elm,"Invalid Country Name");
                //     return;
                // }
                // if(shipping_office_state_elm.val() == 0) {
                //     showErrorTicker(shipping_office_state_elm,"Invalid State Name");
                //     return;
                // }
                if (shipping_office_contact_elm.val() !== "" && !$.isNumeric(shipping_office_contact_elm.val())) {
                    showErrorTicker(shipping_office_contact_elm, "Invalid office contact.");
                    return;
                }
                //for passing entire object 
                var form_data = $(this).serializeArray();
                form_data.push({
                    name: "firstName",
                    value: personal_data.firstName
                });
                form_data.push({
                    name: "lastName",
                    value: personal_data.lastName
                });
                form_data.push({
                    name: "profession",
                    value: personal_data.profession
                });
                form_data.push({
                    name: "company",
                    value: personal_data.company
                });
                form_data.push({
                    name: "birthTime",
                    value: personal_data.birthTime
                });
                form_data.push({
                    name: "residenceCountryID",
                    value: personal_data.residenceCountryID
                });
                form_data.push({
                    name: "residenceStateID",
                    value: personal_data.residenceStateID
                });
                form_data.push({
                    name: "residencePhone",
                    value: personal_data.residencePhone
                });
                $.when(customerAPI.editProfile(form_data)).done(function() {
                    $("#popup-response-message").text("You have successfully updated your address information.");
                    return;
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    $("#popup-response-message").text(responseTxt.error);
                    return;
                });
                $("#popup-response").showPopup();
            });

            // Resets Shipping Address Form
            listenForPointerClick($("#reset-shipping-address"));
            $("#reset-shipping-address").on("pointerclick", function(e) {
                personal_data = sessionDB.user;

                //Values for Shipping Address Form
                if (personal_data.homeCountryID) {
                    $("#shipping-home-country").val(personal_data.homeCountryID);
                    if (personal_data.homeStateID) {
                        populateStateDropdown(personal_data.homeCountryID, "#shipping-home-country");
                        $("#shipping-home-state").val(personal_data.homeStateID);
                        $("#shipping-home-state").selectric("refresh");
                    }
                }
                $("#shipping-home-post-code").val(personal_data.homePostalCode);
                $("#shipping-home-address").val(personal_data.homeAddress);
                $("#shipping-home-contact").val(personal_data.homePhone);
                if (personal_data.officeCountryID) {
                    $("#shipping-office-country").val(personal_data.officeCountryID);
                    if (personal_data.officeStateID) {
                        populateStateDropdown(personal_data.officeCountryID, "#shipping-office-country");
                        $("#shipping-office-state").val(personal_data.officeStateID);
                        $("#shipping-office-state").selectric("refresh");
                    }
                }
                $("#shipping-office-post-code").val(personal_data.officePostalCode);
                $("#shipping-office-address").val(personal_data.officeAddress);
                $("#shipping-office-contact").val(personal_data.officePhone);

                $("form[name='shipping-address'] input").removeClass("input-type-error");
                $("form[name='shipping-address'] .input-error").remove();
            });

            /*
             * Attaches Event Handlers function input event on form input box
             * so that it removes it's respective error message
             */
            $("#account-settings input").on("input", function() {
                $(this).parent().find(".input-error").remove();
                $(this).removeClass("input-type-error");
            });

            /*
             * Attaches event handlers on dropdowns higlighted with errors to remove its respective error
             * bubble and its errror message
             */
            $("#account-settings select").on("change", function() {
                $(this).parents().find(".input-error").remove();
                $(this).parents(".selectricWrapper").removeClass("input-type-error");
            });

            /*
             * Attaches event handlers on country dropdowns to change next state dropdowns
             */
            country_elms.on("change", function() {
                var country_id = $("option:selected", this).val();
                populateStateDropdown(country_id, this);
            });

            /*
             * Attaches event handles on close button of popup to hide the poup
             */
            listenForPointerClick($("#response-popup-close"));
            $("#response-popup-close").off("pointerclick");
            $("#response-popup-close").on("pointerclick", function(e) {
                e.preventDefault();
                $("#popup-response").hidePopup();
                $("#popup-response-title").text("");
                $("#popup-response-message").text("Loading...");
            });

        } // initEvent()

        /*
         * @param {String} Element where the error bubble is to be displayed
         * @param {String} Message to be displayed
         * Displays error bubble to show error message on provided element
         */
        function showErrorTicker(element, message) {
            element.addClass("input-type-error");
            element.focus();
            element.select();
            element.addClass("input-type-error");
            element.parent().find(".input-error").remove();
            element.parent().append("<p class='input-error'>" + message + "</p>");
        } // showErrorTicker()

        /*
         * @param {Integer} Country Id
         * @param {String} Country Element
         * Populates the Next State Dropdown to provided country dropdown Id
         */
        function populateStateDropdown(country_id, country_element) {
            var states = localDB.states;
            var state_options = '<option value="0">Select State</option>';
            var state_elms = $("#" + $(country_element).attr("data-state-control"));
            $.each(states, function(i, v) {
                if (v.countryID == country_id) {
                    state_options += '<option value="' + v.id + '">' + v.name + '</option>';
                }
            });
            state_elms.html(state_options);
            $("select").selectric("refresh");
        } // populateStateDropdown()

        /*
         * Initialize Module
         */
        function init() {
            //redirect if the user is not logged in
            // To Do : make this a common function to be shared among modules
            if (app.ready) {
                if (!window.app.Module.Login.isUserLoggedIn()) {
                    var target_page = DEFAULT_PAGE;
                    $.when(window.app.Module.PageSwitcher.switchPage(target_page)).done(function(data) {
                        // Remember history (state, title, URL)
                        window.history.pushState("nav", "Page: " + target_page, DOMAIN_URL + "/" + target_page);
                    }).fail(function() {});
                }
            }
            resetEvent();
            initUI();
            initEvent();
            if (app.ready) {
                fillCustomerData();
            }
        } // init()

        return {
            init: init
        }
    }();
}(window.app = window.app || {}, jQuery));

//// Init account setting
app.Module.AccountSetting.init();