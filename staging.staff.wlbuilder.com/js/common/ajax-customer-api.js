/***
 * This script defines functions to make AJAX calls to server's Customer API.
 * - Each function correspond to one function in the API.
 * - Each function returns a jQuery Promise, so functions can be chained.
 *
 * Before use:
 * - CUSTOMER_API_URL is defined
 * - ensure localDB & sessionDB are init
 ***/

var customerAPI = {
    login: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/login',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = data.customer;
            sessionDB.save('user');
            if (formData.persistent == 1) {
                localDB.persistentLoginCookie = data.persistentLoginCookie;
                localDB.persistentLoginEmail = data.customer.email;
            } else {
                localDB.persistentLoginCookie = "";
                localDB.persistentLoginEmail = "";
            }
            localDB.save('persistentLoginCookie');
            localDB.save('persistentLoginEmail');
        });
    },
    persistentLogin: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/persistentlogin',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = data.customer;
            sessionDB.save('user');
            localDB.persistentLoginCookie = data.persistentLoginCookie;
            localDB.save('persistentLoginCookie');
        });
    },
    logout: function(formData) {
        return $.ajax({
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/logout',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = {};
            sessionDB.save('user');
            localDB.persistentLoginCookie = '';
            localDB.save('persistentLoginCookie');
            localDB.persistentLoginEmail = '';
            localDB.save('persistentLoginEmail');
        });
    },
    register: function(formData) {
        return $.ajax({
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/register',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {

        });
    },
    resendEmailVerificationCode: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/resendEmailVerificationCode',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {

        });
    },
    verifyEmail: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/verifyEmail',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {

        });
    },
    setPassword: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/setPassword',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {

        });
    },
    forgetPassword: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/forgetPassword',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {

        });
    },
    editProfile: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/editProfile',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = data.customer;
            sessionDB.save('user');
        });
    },
    changeEmail: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/changeEmail',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = data.customer;
            sessionDB.save('user');
        });
    },
    changePassword: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/changePassword',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = data.customer;
            sessionDB.save('user');
        });
    },
    addShirtFittingProfile: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/addShirtFittingProfile',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.shirtFittingProfiles.mergeUniqueByID([data.shirtFittingProfile]);
            sessionDB.save('shirtFittingProfiles');
        });
    },
    editShirtFittingProfile: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/editShirtFittingProfile',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.shirtFittingProfiles.mergeUniqueByID([data.shirtFittingProfile]);
            sessionDB.save('shirtFittingProfiles');
        });
    },
    getMailListSubscriptionStatus: function() {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/getMailListSubscriptionStatus',
            type: 'POST',
            dataType: "json",
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.subscriptionStatus = data.subscribed;
            sessionDB.save('subscriptionStatus');
        });
    },
    subscribeMailList: function() {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/subscribeMailList',
            type: 'POST',
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            return;
            sessionDB.subscriptionStatus = true;
            sessionDB.save('subscriptionStatus');
        });
    },
    unsubscribeMailList: function() {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/unsubscribeMailList',
            type: 'POST',
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.subscriptionStatus = false;
            sessionDB.save('subscriptionStatus');
        });
    },

    /***
     * Gets shirt fitting profiles from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedShirtFittingProfiles: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: CUSTOMER_API_URL + '/getUpdatedShirtFittingProfiles',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.shirtFittingProfiles.mergeUniqueByID(data.shirtFittingProfiles);
            sessionDB.save('shirtFittingProfiles');
        });
    },

    /***
     * Gets a list of order items added by this customer into shopping cart (persistent cart).
     * - Must be logged in.
     ***/
    getCart: function() {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: CUSTOMER_API_URL + '/getCart',
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.shirtOrderItems = data.shirtOrderItems;
            sessionDB.tailorKitOrderItems = data.tailorKitOrderItems;
            sessionDB.discountOrderItems = data.discountOrderItems;
            sessionDB.save('shirtOrderItems');
            sessionDB.save('tailorKitOrderItems');
            sessionDB.save('tailorKitOrderItems');
        });
    },

    /***
     * Equivalent of removing all items from cart.
     * - Must be logged in.
     ***/
    clearCart: function() {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: CUSTOMER_API_URL + '/clearCart',
            type: 'POST',
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            // TODO clear sessionDB.shirtOrderItems and sessionDB.tailorKitOrderItems but this will also clear any current order items. WHAT TO DO?
        });
    },

    /***
     * Gets orders from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedOrders: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: CUSTOMER_API_URL + '/getUpdatedOrders',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.orders.mergeUniqueByID(data.orders);
            sessionDB.save('orders');
        });
    },

    /***
     * Gets orders from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getOrderItems: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: CUSTOMER_API_URL + '/getOrderItems',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {

        });
    },


    /***
     * Changes the measurement of a shirt order item that has already been ordered..
     ***/
    changeShirtOrderItemMeasurements: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: CUSTOMER_API_URL + '/changeShirtOrderItemMeasurements',
            type: "POST",
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.shirtOrderItems.mergeUniqueByID(data.shirtOrderItems);
            sessionDB.save('shirtOrderItems');
        });
    },

    /***
     * Adds a tailor kit item to cart
     * Must be logged in
     ***/
    addTailorKitOrderItem: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            type: "POST",
            url: CUSTOMER_API_URL + '/addTailorKitOrderItem',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.tailorKitOrderItems.mergeUniqueByID([data.tailorKitOrderItem]);
            sessionDB.save('tailorKitOrderItems');
        });
    },

    /***
     * Edits a tailor kit item in the cart.
     * Must be logged in
     ***/
    editTailorKitOrderItem: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            type: "POST",
            url: CUSTOMER_API_URL + '/editTailorKitOrderItem',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.tailorKitOrderItems.mergeUniqueByID([data.tailorKitOrderItem]);
            sessionDB.save('tailorKitOrderItems');
        });
    },

    /***
     * Removes a shirt item from cart.
     * Must be logged in
     ***/
    removeTailorKitOrderItem: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            type: "POST",
            url: CUSTOMER_API_URL + '/removeTailorKitOrderItem',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.tailorKitOrderItems.removeByID(formData.id);
            sessionDB.save('tailorKitOrderItems');
        });
    },

    /***
     * Adds a shirt item to cart.
     * Must be logged in
     ***/
    addShirtOrderItem: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            type: "POST",
            url: CUSTOMER_API_URL + '/addShirtOrderItem',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.shirtOrderItems.pushUniqueByID(data.shirtOrderItem);
            sessionDB.save('shirtOrderItems');
        });
    },

    /***
     * Edits a shirt item in the cart.
     * Must be logged in
     ***/
    editShirtOrderItem: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            type: "POST",
            url: CUSTOMER_API_URL + '/editShirtOrderItem',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.shirtOrderItems.mergeUniqueByID([data.shirtOrderItem]);
            sessionDB.save('shirtOrderItems');
        });
    },

    /***
     * Removes a tailor kit item from cart.
     * Must be logged in
     ***/
    removeShirtOrderItem: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            type: "POST",
            url: CUSTOMER_API_URL + '/removeShirtOrderItem',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.shirtOrderItems.removeByID(formData.id);
            sessionDB.save('shirtOrderItems');
        });
    },

    /***
     * Puts the shopping cart's order items into an order with status "Created".
     * If credits were used, the customer's credits will be deducted.
     * - Must be logged in.
     ***/
    makeOrder: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            type: "POST",
            url: CUSTOMER_API_URL + '/makeOrder',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.orders.mergeUniqueByID([data.order]);
            sessionDB.save("orders");
            if (data.shirtOrderItems != undefined && data.shirtOrderItems != null) {
                sessionDB.shirtOrderItems.mergeUniqueByID(data.shirtOrderItems);
                sessionDB.save("shirtOrderItems");
            }
            if (data.tailorKitOrderItems != undefined && data.tailorKitOrderItems != null) {
                sessionDB.tailorKitOrderItems.mergeUniqueByID(data.tailorKitOrderItems);
                sessionDB.save("tailorKitOrderItems");
            }
        });
    },

    /***
     * Cancels a particular created order.
     * Call this function after PayPal Cancel.
     * If credits were used, the customer's
     * credits will be refunded.
     * Must be logged in.
     ***/
    cancelOrder: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            type: "POST",
            url: CUSTOMER_API_URL + '/cancelOrder',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.orders.mergeUniqueByID([data.order]);
            sessionDB.save("orders");
        });
    },

    /***
     * Gets shirt credit activities from the server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890',toTime: '2234567890'}
     ***/
    getCreditActivities: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: CUSTOMER_API_URL + '/getCreditActivities',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.credits.mergeUniqueByID(data.creditActivities);
            sessionDB.save('credits');
        });
    },

    /***
     * Resends invoice email of an order to customer
     ***/
    requestInvoice: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: CUSTOMER_API_URL + '/requestInvoice',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {});
    }
}
