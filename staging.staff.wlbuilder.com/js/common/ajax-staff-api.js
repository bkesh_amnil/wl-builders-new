/***
 * This script defines functions to make AJAX calls to server's Staff API.
 * - Each function correspond to one function in the API.
 * - Each function returns a jQuery Promise, so functions can be chained.
 *
 * Before use:
 * - PUBLIC_API_URL is defined
 * - ensure localDB & sessionDB are init
 ***/

var staffAPI = {
    /***
     * Attempts to login a staff and saves staff in session DB.
     * @param: e.g. {email: 'staff1@example.com', password: 'PaS5w0rD'}
     ***/

    login: function(formData) {
        console.log(STAFF_API_URL);
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/login',
            dataType: "json",
            data: formData,
            beforeSend:function(){
                $('#login-form').find('output').removeClass('error').text('Please wait.');
            }
        }).done(function(data, textStatus, jqXHR) {
            console.log(data);
            sessionDB.user = data.staff;
            sessionDB.save('user');
            if (data.persistentLoginCookie) {
                localDB.staffPersistentLoginCookie = data.persistentLoginCookie;
                localDB.staffPersistentLoginEmail = data.staff.email;
            } else {
                localDB.staffPersistentLoginCookie = '';
                localDB.staffPersistentLoginEmail = '';
            }
            localDB.save('staffPersistentLoginCookie');
            localDB.save('staffPersistentLoginEmail');
        });
    }, //login()
    /***
     * Logins a customer using email and a persistent login cookie
     **/
    persistentLogin: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: STAFF_API_URL + '/persistentlogin',
            type: 'POST',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = data.staff;
            sessionDB.save('user');
            localDB.staffPersistentLoginCookie = data.persistentLoginCookie;
            localDB.save('staffPersistentLoginCookie');
        });
    },
    /***
     * Logs session staff out, and clears all session data in session DB.
     ***/
    logout: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/logout',
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = {};
            sessionDB.save('user');
            localDB.staffPersistentLoginCookie = '';
            localDB.staffPersistentLoginEmail = '';
            localDB.save('staffPersistentLoginCookie');
            localDB.save('staffPersistentLoginEmail');
        });
    }, //logout()

    /***
     * Adds a customer to the system. All customers added are automatically verified.
     * @param: e.g. {email: 'customer1@hotmail.com', password: 'Pa5Sw0rD', firstName: 'Robbie', lastName: 'Fowler', profession: 'Footballer'}
     ***/
    addCustomer: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/addCustomer',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            if (localDB.customers == null) {
                localDB.customers = [];
            }
            localDB.customers.mergeUniqueByID([data.customer]);
            localDB.save('customers');
        });
    },
    /***
     *Gets customers from server that have been updated since a certain time. Use this function to sync the client-side local DB with the server.
     * @param: e.g. {fromTime:'1234567890'}
     ***/
    getUpdatedCustomers: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "GET",
            url: STAFF_API_URL + '/getUpdatedCustomers',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.customers.mergeUniqueByID(data.customers);
            localDB.save('customers');
        });
    },
    /***
     * Updates a customer's profile. Also used to save home and office addresses to this customer's profile.
     * @param: e.g. {id: '14', firstName: 'Johnny', lastName: 'Evans', profession: 'Footballer', homeCountry: 'SG', homeState: 'SG'
     homePostalCode: '987654', homeAddress: '654 Republic Ave #12-34', officeCountry: 'null', officeState: 'null',
     officePostalCode: 'null', officeAddress: 'null'
     }
     ***/
    editCustomerProfile: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editCustomerProfile',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.customers.mergeUniqueByID([data.customer]);
            localDB.save('customers');
        });
    },
    /***
     * Updates a customer's profile. Also used to save home and office addresses to this customer's profile.
     * @param: e.g. {id: '14', firstName: 'Johnny', lastName: 'Evans', profession: 'Footballer', homeCountry: 'SG', homeState: 'SG'
     homePostalCode: '987654', homeAddress: '654 Republic Ave #12-34', officeCountry: 'null', officeState: 'null',
     officePostalCode: 'null', officeAddress: 'null'
     }
     ***/
    editCustomerCredits: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editCustomerCredits',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.customers.mergeUniqueByID([data.customer]);
            localDB.save('customers');
        });
    },
    /***
     * Updates referrer id of the customer
     * @param: e.g. {id:14,referrerID:8}
     ***/
    editCustomerReferrerID: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editCustomerReferrerID',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.customers.mergeUniqueByID([data.customer]);
            localDB.save('customers');
        });
    },
    /****
     * Adds an email to the mail list
     * @param: e.g. {id: '14'}
     ****/
    addMailListEntry: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/addMailListEntry',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
        });
    },
    /****
     * Removes an email to the mail list
     * @param: e.g. {email: 'abc@mail.com',list: 'Customer'}
     ****/
    removeMailListEntry: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/removeMailListEntry',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
        });
    },
    /****
     * Gets mail list subscription status
     * @param: e.g. {email: 'asdas', list: "Customer"}
     ****/
    getMailListSubscriptionStatus: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/getMailListSubscriptionStatus',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
        });
    },
    /***
     * Edits the session's staff profile.
     * - must be logged in
     ***/
    editProfile: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editProfile',
            data: formData,
            dataType: "json",
            beforeSend:function(){
                $('.edit-account-loader').html('Please Wait...').show();
                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.edit-account-loader').html('Changes Saved.').delay(3000).fadeOut();
                        
            sessionDB.user = data.staff;
            sessionDB.save('user');
        });
    },
    /***
     * Old password is required as mitigation for persistent login exploit.
     * @param: e.g. {oldPassword: 'lousyPass' , newPassword: 'g0oDPa5S'}
     ***/
    changePassword: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/changePassword',
            data: formData,
            dataType: "json",
            beforeSend:function(){
                $('.change-password-loader').html('Please Wait...');
                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.change-password-loader').html('Changes Saved.').delay(3000).fadeOut();
        });
    },
    /***
     * Gets activities from the server that have been created from a certain time. Use this function to sync the client-side local DB with the server.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getNewActivities: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "GET",
            url: STAFF_API_URL + '/getNewActivities',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {

        });
    },
    /***
     * Adds testimonial to the system
     * @param: e.g. {name: 'John Doe', profession: 'Doctor', message: 'Fantastic service!', displayPriority: '7'}
     ***/
    addTestimonial: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/addTestimonial',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.add-testimonial-loader').html('Please Wait...');
               
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.add-testimonial-loader').html('').delay(1000).fadeOut();
            localDB.testimonials.mergeUniqueByID([data.testimonial]);
            localDB.save("testimonials");
        });
    },
    /***
     * Get testimonials that have been updated since a certain time. Use this function to sync the client-side local DB with the server.Use this function to sync the client-side local DB with the server.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedTestimonials: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "GET",
            url: STAFF_API_URL + '/getUpdatedTestimonials',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.testimonials.mergeUniqueByID(data.testimonials);
            localDB.save('testimonials');
        });
    },
    /***
     * Edits an existing testimonial
     * @param: e.g. {id: '4', name: 'John Doe', profession: 'Lawyer', message: 'Fantastic service!', displayPriority: '5'}
     ***/
    editTestimonial: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editTestimonial',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.edit-testimonial-loader').html('Please Wait...');
                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.edit-testimonial-loader').html('').delay(1000).fadeOut();
            
            localDB.testimonials.mergeUniqueByID([data.testimonial]);
            localDB.save("testimonials");
        });
    },
    /***
     * Adds an announcement to the system
     * @param: e.g. {message: 'Site-wide 10% discount', img: 'discount.jpg', displayPriority: '1'}
     ***/
    addAnnouncement: function(formData) {
        console.log(formData);
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/addAnnouncement',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.announcements.mergeUniqueByID([data.announcement]);
            localDB.save("announcements");
        });
    },
    /***
     * Adds an partner to the system
     
     ***/
    addPartner: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/addPartner',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.add-partner-loader').html('Please Wait...');
                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.add-partner-loader').html('Changes Saved.').delay(1000).fadeOut();
            localDB.partners.mergeUniqueByID([data.partner]);
            localDB.save("partners");
        });
    },
    /***
     * Get partners from the server that have been created from a certain time. Use this function to sync the client-side local DB with the server.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedPartners: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "GET",
            url: STAFF_API_URL + '/getUpdatedPartners',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.partners.mergeUniqueByID(data.partners);
            localDB.save('partners');
        });
    },
    /***
     * Edits partners to the system
     
     ***/
    editPartner: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editPartner',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.edit-partner-loader').html('Please Wait...');
                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.edit-partner-loader').html('').delay(1000).fadeOut();
            localDB.partners.mergeUniqueByID([data.partner]);
            localDB.save("partners");
        });
    },
    /***
     * Adds an project to the system
     
     ***/
    addProject: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/addProject',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.add-listing-loader').html('Please Wait...');
                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.add-listing-loader').html('');
            localDB.projects.mergeUniqueByID([data.project]);
            localDB.save("projects");
        });
    },
    
     /***
     * Edits project to the system
     
     ***/
    editProject: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editProject',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.edit-listing-loader').html('Please Wait...');
                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.edit-listing-loader').html('');
            localDB.projects.mergeUniqueByID([data.project]);
            localDB.save("projects");
        });
    },
     /***
     * Edits project feature to the system
     
     ***/
    editProjectFeature: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editProjectFeature',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.edit-info-loader').html('Please Wait...');                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.edit-info-loader').html('');
            localDB.projects.mergeUniqueByID([data.project]);
            localDB.save("projects");
        });
    },
     /***
     * Edits project blue print to the system
     
     ***/
    editProjectBluePrint: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editProjectBluePrint',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.edit-blueprint-loader').html('Please Wait...');                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.edit-blueprint-loader').html('');
//            localDB.projects.mergeUniqueByID([data.project]);
//            localDB.save("projects");
        });
    },
    
     /***
     * Edits project blue print to the system
     
     ***/
    deleteProjectBluePrint: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/deleteProjectBluePrint',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
//            localDB.projects.mergeUniqueByID([data.project]);
//            localDB.save("projects");
        });
    },
    /***
     * Get projects from the server that have been created from a certain time. Use this function to sync the client-side local DB with the server.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedProjects: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "GET",
            url: STAFF_API_URL + '/getUpdatedProjects',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
//            console.log(data);
            localDB.projects.mergeUniqueByID(data.projects);
            localDB.save('projects');
        });
    },
    /***
     * Get project blue prints from the server that have been created from a certain time. Use this function to sync the client-side local DB with the server.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getProjectBluePrints: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "GET",
            url: STAFF_API_URL + '/getProjectBluePrints',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            //console.log(data.projectBluePrints);
            localDB.projectBluePrints = data.projectBluePrints; 
            localDB.save("projectBluePrints");
//            localStorage.setItem('projectBluePrints',JSON.stringify(data.projectBluePrints));
//            localDB.save('projectBluePrints');
        });
    },
    /***
     * Get project images from the server that have been created from a certain time. Use this function to sync the client-side local DB with the server.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getProjectImages: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "GET",
            url: STAFF_API_URL + '/getProjectImages',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.projectImages = data.projectImages; 
            localDB.save("projectImages");
        });
    },
    
    /***
     * Get project images from the server that have been created from a certain time. Use this function to sync the client-side local DB with the server.
     * @param: e.g. {pid: pid,  id: projectImgId}
     ***/
    getUpdatedPanoramaProjectImages: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/getUpdatedPanoramaProjectImages',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            //localDB.projectImages = data.projectImages; 
            //localDB.save("projectImages");
        });
    },
    
    /***
     * Get project images from the server that have been created from a certain time. Use this function to sync the client-side local DB with the server.
     * @param: e.g. {pid: pid,  id: projectImgId}
     ***/
    getUpdatedBoundaryProjectImages: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/getUpdatedBoundaryProjectImages',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            //localDB.projectImages = data.projectImages; 
            //localDB.save("projectImages");
        });
    },   
    
    
    /***
     * Adds an project image to the system
     
     ***/
    addProjectImage: function(formData) {

        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/addProjectImage',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.add-gallery-loader').html('Please Wait...');                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.add-gallery-loader').html('');
            localDB.projectImages.mergeUniqueByID([data.projectImage]);
            localDB.save("projectImages");
            return data.insertId;
            
        });
    },
     /***
     * Edits project to the system
     
     ***/
    editProjectImage: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/editProjectImage',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json",
            beforeSend:function(){
                $('.edit-gallery-loader').html('Please Wait...');                
            }
        }).done(function(data, txtStatus, jqXHR) {
            $('.edit-gallery-loader').html('');
            localDB.projectImages.mergeUniqueByID([data.projectImage]);
            localDB.save("projectImages");
        });
    },
    
    /***     
     * Gets a list of activiteies by staff
     * users within a certain time frame.
     * If 'toTime' is not specified, curent
     * time will be used.
     * - must be logged in
     ***/
    getStaffActivities: function(formData) {


        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: STAFF_API_URL + '/getStaffActivities',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            console.log(data);
            localDB.staffActivities.mergeUniqueByID(data.activities);
            localDB.save("staffActivities");
        });
    },
    /***     
     * Gets a list of activiteies by staff
     * users within a certain time frame.
     * If 'toTime' is not specified, curent
     * time will be used.
     * - must be logged in
     ***/
    getUpdatedStaffs: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: STAFF_API_URL + '/getUpdatedStaffs',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.staffs.mergeUniqueByID(data.staffs);
            localDB.save("staffs");
        });
    },
    /***
     * Adds a staff to the system.- must be logged in as Admin
     * @param: e.g. {email: new_staff@hotmail.com
     password: Pa5Sw0rD
     firstName: Fernando
     lastName: Torres
     accessLevel: 50}
     ***/

    addStaff: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: STAFF_API_URL + '/addStaff',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.staffs.mergeUniqueByID([data.staff]);
            localDB.save("staffs");
        });
    },
    /***
     * Gets staffs from server that have been updated since a certain time.
     * @param: e.g. {fromTime: 1234567890}
     ***/

    getUpdatedStaffs: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "GET",
            url: STAFF_API_URL + '/getUpdatedStaffs',
            data: formData,
            dataType: "json"
        }).done(function(data, txtStatus, jqXHR) {
            localDB.staffs.mergeUniqueByID(data.staffs);
            localDB.save("staffs");
        });
    },
            /***
             * Edits a staff.- must be logged in
             * @param: e.g. {id: 18
             email: new_staff@hotmail.com
             firstName: Fernando
             lastName: Torres
             accessLevel: 90}
             ***/

            editStaff: function(formData) {
                return $.ajax({
                    xhrFields: {
                        'withCredentials': true
                    },
                    crossDomain: true,
                    type: "POST",
                    url: STAFF_API_URL + '/editStaff',
                    data: formData,
                    dataType: "json"
                }).done(function(data, txtStatus, jqXHR) {
                    localDB.staffs.mergeUniqueByID([data.staff]);
                    localDB.save("staffs");
                });
            }


}
