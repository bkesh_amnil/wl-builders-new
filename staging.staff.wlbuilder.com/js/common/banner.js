﻿/***
 * Module - app.Controls.Slider
 *
 * This Script incorporates following features:
 * - Creates a cut effect rotating banner
 * - Create a fading in background for banner
 * - Number of images in rotating banner must be equal to number of images in banner background
 * - Animates banner and banner background with same animation duration
 *
 * Dependencies :
 *  - jQuery
 *
 * Before use:
 *  - none
 *
 * Init instructions:
 *  - app.Controls.Slider.initSlider(options);
 *
 * Usage examples:
 * window.app.Controls.Slider.initSlider({
 *    el: 'about-banner',
 *    bannerImageSelector: "#hero-slider"
 *});
 ***/
var globalSlideInterval;
(function(app, $) {
    "use strict";

    var App = window.app = window.app || {};
    App.Controls = App.Controls = App.Controls || {};
    app.Controls.Slider = function() {
        var
            mainContainer = "",
            slidingInterval = 0,
            currentSlide = 0,
            maxSlides = 0,
            imgWidth = 0,
            imgHeight = 0,
            numberOfSlider = 4,
            config = {},
            sliderImages = [],
            slideIntervalDuration = 6000;

        function renderSlide() {
            var container = $(mainContainer);
            //create container
            var wrapper = $("<div>").addClass("slider-container");
            $(mainContainer).append(wrapper);

            //render slide image
            var mainImage = createImage(sliderImages[0].url, sliderImages[0].alt);
            mainImage.addClass("main-image");
            mainImage.css("display", "inline");
            wrapper.append(mainImage);

            // Init Slide Config
            initSliderConfig();

            // Clone boxes
            var topBox = createBoxClone();
            topBox.addClass("top-box");
            topBox.css({
                top: 0,
                left: 0,
                width: imgWidth,
                height: imgHeight / 2
            });
            wrapper.append(topBox);

            var bottomBox = createBoxClone();
            bottomBox.addClass("bottom-box");
            bottomBox.css({
                top: imgHeight / 2,
                left: 0,
                width: imgWidth,
                height: imgHeight / 2
            });
            wrapper.append(bottomBox);

            //Show first Slide

            //start sliding
            startSliding();
        }

        /***
         * Initializes Slider Configuration
         ***/
        function initSliderConfig() {
            maxSlides = sliderImages.length;
            // Show current background image
            $($(".background")[0]).addClass("current");
        }

        /***
         * Starts slide animation
         ***/
        function startSliding() {
            if (globalSlideInterval !== undefined && globalSlideInterval > 0) {
                clearInterval(globalSlideInterval);
            }
            globalSlideInterval = setInterval(nextSlide, slideIntervalDuration);
        }

        /***
         * Animates to next slide
         ***/
        function nextSlide() {
            var prevSlide = currentSlide;
            currentSlide++;
            if (currentSlide >= maxSlides) {
                currentSlide = 0;
            }

            $(".top-box").children().remove();
            $(".bottom-box").children().remove();
            // initui
            var currentTopImg = createImage(sliderImages[prevSlide].url, sliderImages[prevSlide].alt);
            var currentBottomImg = createImage(sliderImages[prevSlide].url, sliderImages[prevSlide].alt);
            currentTopImg.css("display", "block");
            currentBottomImg.css("display", "block");
            var nextImageIndex = prevSlide + 1;
            if (nextImageIndex === maxSlides) {
                nextImageIndex = 0;
            }
            var nextTopImg = createImage(sliderImages[nextImageIndex].url, sliderImages[nextImageIndex].alt);
            var nextBottomImg = createImage(sliderImages[nextImageIndex].url, sliderImages[nextImageIndex].alt);
            nextTopImg.css("display", "block");
            nextBottomImg.css("display", "block");

            currentTopImg.addClass("current-img");
            currentBottomImg.addClass("current-img");

            nextTopImg.addClass("next-img");
            nextBottomImg.addClass("next-img");

            currentTopImg.css({
                left: 0,
                top: 0,
                position: "relative"
            });
            currentBottomImg.css({
                left: 0,
                top: -imgHeight * 1.5,
                position: "relative"
            });

            nextTopImg.css({
                left: imgWidth,
                top: -imgHeight,
                position: "relative"
            });
            nextBottomImg.css({
                left: -imgWidth,
                top: -imgHeight / 2,
                position: "relative"
            });

            $(".top-box").append(currentTopImg);
            $(".top-box").append(nextTopImg);
            $(".bottom-box").append(nextBottomImg);
            $(".bottom-box").append(currentBottomImg);
            $(".main-image").hide();

            // top box
            // current img left: 0 imgWidth
            // next-img left: imgWidth 0
            $(".top-box .current-img").animate({
                left: -imgWidth
            }, {
                duration: BANNER_ANIMATION_DURATION,
                easing: "easeInOutExpo"
            }, function() {

            });

            $(".top-box .next-img").animate({
                left: 0
            }, {
                duration: BANNER_ANIMATION_DURATION,
                easing: "easeInOutExpo"
            }, function() {
                //alert("all done");
            });

            // bottom box
            // current img left: 0 imgWidth
            // next-img left: -imgWidth 0
            $(".bottom-box .current-img").animate({
                left: imgWidth
            }, {
                duration: BANNER_ANIMATION_DURATION,
                easing: "easeInOutExpo"
            }, function() {

            });

            $(".bottom-box .next-img").animate({
                left: 0
            }, BANNER_ANIMATION_DURATION, "easeInOutExpo", function() {
                $(".main-image").attr("src", sliderImages[currentSlide].url);
                $(".main-image").show();
            });

            var slideContents = $(".background");
            $(slideContents[prevSlide]).removeClass('current');
            $(slideContents[currentSlide]).addClass('current');
        }

        /***
         * Initializes Slider Control
         ***/
        function initSlider($config) {
            config = config || {};
            config = $.extend({}, config, $config);

            config.el = config.el || "slider";
            config.baseCss = config.baseCss || "slider";
            mainContainer = ["#", config.el].join("");
            imgHeight = config.imgHeight || 522;
            imgWidth = config.imgWidth || 1024;
            sliderImages = [];
            $(config.bannerImageSelector).find("img").each(function(index, item) {
                sliderImages.push({
                    url: $(item).attr("src"),
                    alt: $(item).attr("alt")
                });
            });
            renderSlide();
        };

        /***
         * Creates jQuery object with image tag
         * params: string of imageUrl
         * returns: jQuery object
         ***/
        function createImage(imageUrl, altText) {
            var img = $("<img src='" + imageUrl + "' />");
            img.attr({
                alt: altText
            });
            return img;
        };

        /***
         * Creates jQuery object with div tag having css properties: display: block, overflow: hidden
         * returns: jQuery object
         ***/
        function createBoxClone() {
            var box = $("<div>");
            box.css("display", "block");
            box.css("overflow", "hidden");
            return box;
        }

        return {
            initSlider: initSlider
        }
    }();
}(window.app = window.app || {}, jQuery));