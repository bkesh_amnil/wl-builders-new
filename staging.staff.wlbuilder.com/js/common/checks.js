/***
 * This script performs:
 * - checks to the user device app can support the app
 * - avoids console errors in browsers without console
 * - appends CSRF token to every AJAX request
 *
 * Before use, things to ensure/edit:
 * - ensure browsehappy prompt is styled in CSS
 ***/

/***
 * Checks that the browser is cookie and HTML5 storage capable.
 * - If not, display "browsehappy" message.
 ***/
(function() {
    // Check cookies
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;
    if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) {
        document.cookie = "testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
    }

    // Show browsehappy overlay
    if (!cookieEnabled) {
        $('body').append('<p id="browsehappy">Your browser does not support cookies and will not work properly with our website. Please check your browser settings.<br><br>If you are using an <strong>outdated</strong> browser, please consider <a href="http://browsehappy.com/">upgrading your browser</a> to improve your experience.</p>');
        return;
    }

    // Check HTML5 storage
    var storageEnabled = (typeof Storage === "undefined") ? false : true;
    if (storageEnabled) {
        try {
            localStorage.setItem('testStorage', 'test');
            localStorage.removeItem('testStorage');
            sessionStorage.setItem('testStorage', 'test');
            sessionStorage.removeItem('testStorage');
        } catch (e) {
            storageEnabled = false;
        }
    }

    // Show browsehappy overlay
    if (!storageEnabled) {
        $('body').append('<p id="browsehappy">Your browser does not support storage and will not work properly with our website. Please check your browser settings (some browsers may not work in private browsing mode).<br><br>If you are using an <strong>outdated</strong> browser, please consider <a href="http://browsehappy.com/">upgrading your browser</a> to improve your experience.</p>');
        return;
    }
})();

/***
 * Checks that the browser has a console.
 * - If not, define a empty placeholder for each method to avoid `console` errors.
 ***/
(function() {
    var method;
    var noop = function() {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});
    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
})();

/***
 * Appends CSRF Token to all AJAX requests
 ***/
(function() {
    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    if (typeof csrf_token === "undefined") return;

    jQuery.ajaxPrefilter(function(options, _, xhr) {
        if (!xhr.crossDomain) {
            xhr.setRequestHeader('CSRF_TOKEN', csrf_token);
        }
    });
})();