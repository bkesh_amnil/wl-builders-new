/***
 * Makes use of local (persistent in internet cache) or session storage (persistent across page refreshes) to enable storage of Objects.
 *
 * Script defines:
 * - localDB, sessionDB objects (HTML5 storage)
 *
 * Before use:
 * - set localDB and sessionDB default data and fields in init()
 *
 * To init, either:
 *    localDB.init();
 *    sessionDB.init();
 *
 * Usage example (localDB):
 *    var user = localDB.session_user; // to access data
 *    localDB.products = Array(); // to create/mutate data
 *    localDB.save('products'); // to save data to DB
 ***/
var localDB = {
        init: function() {
            // TODO: fill in with default data

            // Testimonials
            this.testimonials = [];
            var stored = localStorage.getItem("testimonials");
            if (stored != null) {
                this.testimonials = JSON.parse(stored);
            }
            
            // Front Testimonials
            this.WLtestimonials = [];
            var stored = localStorage.getItem("WLtestimonials");
            if (stored != null) {
                this.WLtestimonials = JSON.parse(stored);
            }

            // Announcements
            this.announcements = [];
            var stored = localStorage.getItem("announcements");
            if (stored != null) {
                this.announcements = JSON.parse(stored);
            }

            // Partners
            this.partners = [];
            var stored = localStorage.getItem("partners");
            if (stored != null) {
                this.partners = JSON.parse(stored);
            }
            
            // front Partners
            this.WLpartners = [];
            var stored = localStorage.getItem("WLpartners");
            if (stored != null) {
                this.WLpartners = JSON.parse(stored);
            }
            
             // Projects
            this.projects = [];
            var stored = localStorage.getItem("projects");
            if (stored != null) {
                this.projects = JSON.parse(stored);
            }
            
             //front Projects
            this.WLprojects = [];
            var stored = localStorage.getItem("WLprojects");
            if (stored != null) {
                this.WLprojects = JSON.parse(stored);
            }
            
             // ProjectBluePrints
            this.projectBluePrints = [];
            var stored = localStorage.getItem("projectBluePrints");
            if (stored != null) {
                this.projectBluePrints = JSON.parse(stored);
            }
            
             //front ProjectBluePrints
            this.WLprojectBluePrints = [];
            var stored = localStorage.getItem("WLprojectBluePrints");
            if (stored != null) {
                this.WLprojectBluePrints = JSON.parse(stored);
            }            
            
            // projectImages
            this.projectImages = [];
            var stored = localStorage.getItem("projectImages");
            if (stored != null) {
                this.projectImages = JSON.parse(stored);
            }
            
            // Front projectImages
            this.WLprojectImages = [];
            var stored = localStorage.getItem("WLprojectImages");
            if (stored != null) {
                this.WLprojectImages = JSON.parse(stored);
            }
            
            // Announcement Pictures
            this.announcementPictures = [];
            var stored = localStorage.getItem("announcementPictures");
            if (stored != null) {
                this.announcementPictures = JSON.parse(stored);
            }

            //Countries
            this.countries = [];
            var stored = localStorage.getItem("countries");
            if (stored != null) {
                this.countries = JSON.parse(stored);
            }

            //States
            this.states = [];
            var stored = localStorage.getItem("states");
            if (stored != null) {
                this.states = JSON.parse(stored);
            }

            // Persistent Login Cookie
            this.persistentLoginCookie = "";
            var stored = localStorage.getItem("persistentLoginCookie");
            if (stored != null) {
                this.persistentLoginCookie = JSON.parse(stored);
            }

            // Persistent Login Email
            this.persistentLoginEmail = "";
            var stored = localStorage.getItem("persistentLoginEmail");
            if (stored != null) {
                this.persistentLoginEmail = JSON.parse(stored);
            }


            /*** Dashboard ***/
            // Persistent Login Cookie
            this.staffPersistentLoginCookie = "";
            var stored = localStorage.getItem("staffPersistentLoginCookie");
            if (stored != null) {
                this.staffPersistentLoginCookie = JSON.parse(stored);
            }

            // Persistent Login Email
            this.staffPersistentLoginEmail = "";
            var stored = localStorage.getItem("staffPersistentLoginEmail");
            if (stored != null) {
                this.staffPersistentLoginEmail = JSON.parse(stored);
            }
            // Theme
            this.staffTheme = {
                "name": "blue",
                "css": "blue-theme.css"
            };
            var stored = localStorage.getItem("staffTheme");
            if (stored != null) {
                this.staffTheme = JSON.parse(stored);
            }

            // Cookie for persistent login
            var stored = localStorage.getItem("staffLoginCookie");
            if (stored != null) {
                this.staffLoginCookie = JSON.parse(stored);
            }

            // Module Layout
            this.staffModuleLayout = [];
            var stored = localStorage.getItem("staffModuleLayout");
            if (stored != null) {
                this.staffModuleLayout = JSON.parse(stored);
            }

            // Staff activity
            this.staffActivities = [];
            var stored = localStorage.getItem("staffActivities");
            if (stored != null) {
                this.staffActivities = JSON.parse(stored);
            }

            // Staffs
            this.staffs = [];
            var stored = localStorage.getItem("staffs");
            if (stored != null) {
                this.staffs = JSON.parse(stored);
            }

            //Shirt Fitting-Profile
            this.staffShirtFittingProfiles = [];
            var stored = localStorage.getItem("staffShirtFittingProfiles");
            if (stored != null) {
                this.staffShirtFittingProfiles = JSON.parse(stored);
            }

            // Per Page
            this.perPage = {
                "value": 10
            };
            var stored = localStorage.getItem("perPage");
            if (stored != null) {
                this.perPage = JSON.parse(stored);
            }

            // Current Page
            this.currentPage = {
                "value": 1
            };
            var stored = localStorage.getItem("currentPage");
            if (stored != null) {
                this.currentPage = JSON.parse(stored);
            }

            // Staff Per Page
            this.staffPerPage = {
                "value": localDB.perPage.value
            };
            var stored = localStorage.getItem("staffPerPage");
            if (stored != null) {
                this.staffPerPage = JSON.parse(stored);
            }

            // Staff Current Page
            this.staffCurrentPage = {
                "value": 1
            };
            var stored = localStorage.getItem("staffCurrentPage");
            if (stored != null) {
                this.staffCurrentPage = JSON.parse(stored);
            }

            // Announcement Per Page
            this.announcementPerPage = {
                "value": localDB.perPage.value
            };
            var stored = localStorage.getItem("announcementPerPage");
            if (stored != null) {
                this.announcementPerPage = JSON.parse(stored);
            }

            // Announcement Current Page
            this.announcementCurrentPage = {
                "value": localDB.currentPage.value
            };
            var stored = localStorage.getItem("announcementCurrentPage");
            if (stored != null) {
                this.announcementCurrentPage = JSON.parse(stored);
            }

             // Partner Per Page
            this.partnerPerPage = {
                "value": localDB.perPage.value
            };
            var stored = localStorage.getItem("partnerPerPage");
            if (stored != null) {
                this.partnerPerPage = JSON.parse(stored);
            }

            // Partner Current Page
            this.partnerCurrentPage = {
                "value": localDB.currentPage.value
            };
            var stored = localStorage.getItem("partnerCurrentPage");
            if (stored != null) {
                this.partnerCurrentPage = JSON.parse(stored);
            }
             // Project Per Page
            this.projectPerPage = {
                "value": localDB.perPage.value
            };
            var stored = localStorage.getItem("projectPerPage");
            if (stored != null) {
                this.projectPerPage = JSON.parse(stored);
            }
             // Project Per Page
            this.projectImagePerPage = {
                "value": localDB.perPage.value
            };
            var stored = localStorage.getItem("projectImagePerPage");
            if (stored != null) {
                this.projectImagePerPage = JSON.parse(stored);
            }
            

            // Project Current Page
            this.projectCurrentPage = {
                "value": localDB.currentPage.value
            };
            var stored = localStorage.getItem("projectCurrentPage");
            if (stored != null) {
                this.projectCurrentPage = JSON.parse(stored);
            }
            
            // Project Current Page
            this.projectImageCurrentPage = {
                "value": localDB.currentPage.value
            };
            var stored = localStorage.getItem("projectImageCurrentPage");
            if (stored != null) {
                this.projectImageCurrentPage = JSON.parse(stored);
            }
            
            // Testimonial Per Page
            this.testimonialPerPage = {
                "value": localDB.perPage.value
            };
            var stored = localStorage.getItem("testimonialPerPage");
            if (stored != null) {
                this.testimonialPerPage = JSON.parse(stored);
            }

            // Testimonial Current Page
            this.testimonialCurrentPage = {
                "value": localDB.currentPage.value
            };
            var stored = localStorage.getItem("testimonialCurrentPage");
            if (stored != null) {
                this.testimonialCurrentPage = JSON.parse(stored);
            }


            // Staff perPageArray
            this.perPageArray = [5, 10, 20, 50];
            var stored = localStorage.getItem("perPageArray");
            if (stored != null) {
                this.perPageArray = JSON.parse(stored);
            }

            // Staff maxPageLinks
            this.maxPageLinks = 3;
            var stored = localStorage.getItem("maxPageLinks");
            if (stored != null) {
                this.maxPageLinks = JSON.parse(stored);
            }

            this.hasInit = true;
        },
        // Flags whether localDB has init
        hasInit: false,
        // Start afresh
        clear: function() {
            localStorage.clear();
            this.init();
        },
        // Writes an item to local DB
        save: function(key) {
            var data = eval("this." + key);
            localStorage.setItem(key, JSON.stringify(data));
        },
        // Reverts to saved copy of an item
        revert: function(key) {
            var stored = localStorage.getItem(key);
            if (stored != null) {
                this[key] = JSON.parse(stored);
            }
        }
    } //var localDB

var sessionDB = {
        // Inits session DB
        init: function() {
            // TODO: fill in with default session data
            // 
            // Session User
            this.user = {
                "id": "",
                "email": "",
                "emailVerified": false,
                "userType": "Customer",
                "accessLevel": 1,
                "banned": true,
                "language": "en",
                "isAdmin":"",
                "createdTime": 1234567890,
                "lastUpdateTime": 1234567980,
                "lastActivityTime": 1234567980,
                "firstName": "",
                "lastName": "",
                "profession": "",
                "residenceCountry": "",
                "residencePhone": "",
                "homeCountry": "",
                "homeState": "",
                "homePostalCode": "",
                "homePhone": "",
                "officeCountry": null,
                "officeState": null,
                "officePostalCode": null,
                "officePhone": ""
            }
            var stored = sessionStorage.getItem("user");
            if (stored != null) {
                this.user = JSON.parse(stored);
            }

            this.subscriptionStatussubscribed = 0;
            var stored = sessionStorage.getItem("subscriptionStatus");
            if (stored != null) {
                this.subscriptionStatus = stored;
            }

            //Shirt Fitting Profiles
            this.shirtFittingProfiles = [];
            var stored = sessionStorage.getItem("shirtFittingProfiles");
            if (stored != null) {
                this.shirtFittingProfiles = JSON.parse(stored);
            }

            //Orders
            this.orders = [];
            var stored = sessionStorage.getItem("orders");
            if (stored != null) {
                this.orders = JSON.parse(stored);
            }

            // Credits
            this.credits = [];
            var stored = sessionStorage.getItem("credits");
            if (stored != null) {
                this.credits = JSON.parse(stored);
            }

            //temp session variable to store measurement details on fitting profile
            this.tempMeasurement = {
                name: ""
            };
            var stored = sessionStorage.getItem("tempMeasurement");
            if (stored != null) {
                this.tempMeasurement = JSON.parse(stored);
            }

            // tailorKitDesign
            this.tailorKitDesigns = [];
            var stored = sessionStorage.getItem("tailorKitDesigns");
            if (stored != null) {
                this.tailorKitDesigns = JSON.parse(stored);
            }

            // tailorKitOrderItem
            this.tailorKitOrderItems = [];
            var stored = sessionStorage.getItem("tailorKitOrderItems");
            if (stored != null) {
                this.tailorKitOrderItems = JSON.parse(stored);
            }

            // tailorKitOrderItem
            this.discountOrderItems = [];
            var stored = sessionStorage.getItem("discountOrderItems");
            if (stored != null) {
                this.discountOrderItems = JSON.parse(stored);
            }

            // shirtOrderItem
            this.shirtOrderItems = [];
            var stored = sessionStorage.getItem("shirtOrderItems");
            if (stored != null) {
                this.shirtOrderItems = JSON.parse(stored);
            }


            /*** Dashboard ***/
            // Session staff
            this.staff = {
                id: 0,
                email: "staff1@example.com",
                emailVerified: true,
                userType: "Super Admin",
                accessLevel: 99,
                banned: false,
                language: "en",
                createdTime: 1234567890,
                lastUpdateTime: 1234567980,
                lastActivityTime: 1234567980,
                firstName: "John",
                lastName: "Doe"
            }

            // Staff Cart
            this.staffOrderCarts = [];
            var stored = sessionStorage.getItem("staffOrderCarts");
            if (stored != null) {
                this.staffOrderCarts = JSON.parse(stored);
            }

            // Staff Orders
            this.staffCredits = [];
            var stored = sessionStorage.getItem("staffCredits");
            if (stored != null) {
                this.staffCredits = JSON.parse(stored);
            }

            var stored = sessionStorage.getItem("staff");
            if (stored != null) {
                this.staff = JSON.parse(stored);
            }
            this.fabricImageStatus = {};

            // Results Per Page
            this.staffResultSettings = {
                "productsPerPage": 10,
                "maxPageLinks": 4
            };

            var stored = sessionStorage.getItem("staffResultSettings");
            if (stored != null) {
                this.staffResultSettings = JSON.parse(stored);
            }

            this.hasInit = true;
        },
        // Flags whether sessionDB has init
        hasInit: false,
        // Start afresh
        clear: function() {
            sessionStorage.clear();
            this.init();
        },
        // Writes an item to local DB
        save: function(key) {
            var data = eval("this." + key);
            sessionStorage.setItem(key, JSON.stringify(data));
        },
        // Reverts to saved copy of an item
        revert: function(key) {
            var stored = sessionStorage.getItem(key);
            if (stored != null) {
                this.key = JSON.parse(stored);
            }
        }
    } //var sessionDB