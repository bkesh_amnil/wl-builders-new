/***
 * Module - app.Controls.InspirationSelection
 *
 * Renders Selection Picks
 *
 * Dependencies
 *  - jQuery
 *
 * Before use
 *  - ensure document is ready
 *
 * Init instructions:
 *  - app.Controls.InspirationSelection.init(config);
 *
 * Usage examples:
 * var inspirationSlectionConfig = {
 *                el: "inspirations",
 *                inspirations : inspirations,
 *                anchorText : "USE THIS DESIGN",
 *                prevBtn : $("#latest-inspiration-pagination-prev"),
 *                nextBtn : $("#latest-inspiration-pagination-next")
 *            }
 *            inspirationSelections.init(inspirationSlectionConfig);
 *
 * On load, performs:
 * - renders the shirts along with pagination logic
 ***/

(function(app, $) {
    "use strict";

    var App = window.app = window.app || {};
    App.Controls = App.Controls = App.Controls || {};
    App.Controls.InspirationSelection = function() {
        var
            config = {}, //
            mainContainer = "",
            inspirations, // Stores all the inspirations
            anchorText, // Text to be shown in the button
            prevBtn, // Slector for the prev button 
            totalPages, // Stores the total pages 
            pageSize = 6, // Current display images in current page
            currentPage, // Shows current page
            nextBtn; // Selector for the next button
        /***
         * Removes all events in this module: click, input, focus, pointerclick
         ***/
        function resetEvent() {
            mainContainer.off('click', 'button');
            mainContainer.on('click', 'button', function(e) {
                e.preventDefault();
            });
            mainContainer.off('pointerclick', 'button');
            $(prevBtn).suppressClick();
            $(prevBtn).off("pointerclick");
            $(nextBtn).suppressClick();
            $(nextBtn).off("pointerclick");
            mainContainer.off("pointerclick", ".use-shirt-design");
        };
        /***
         * Adds all necessary events:
         ***/
        function initEvent() {
            listenForPointerClick($(prevBtn));
            $(prevBtn).on("pointerclick", function(e) {
                /* Act on the event */
                prevPage(); // Calls the previous page
            });

            listenForPointerClick($(nextBtn));
            $(nextBtn).on("pointerclick", function(e) {
                /* Act on the event */
                nextPage(); // Calls the next page 
            });

            // Magnify inspirations on selected 
            listenForPointerClick(mainContainer, ".magnify-btn");
            mainContainer.on("pointerclick", ".magnify-btn", function(e) {
                e.stopImmediatePropagation();
                var element = $(this);
                mainContainer.trigger("magnifyClicked", [element.attr("data-id")]);
            });

            // Use this shirt clicked event
            listenForPointerClick(mainContainer, ".use-shirt-design");
            mainContainer.on("pointerclick", ".use-shirt-design", function(e) {
                mainContainer.trigger("useShirtClicked", $(this).attr("data-id"));
            });
        };
        /***
         * Initializes UI states
         ***/
        function initUI() {
            paginate(currentPage);
        };
        /***
         * Refresh UI states
         ***/
        function refreshUI() {
            mainContainer.html("");
            paginate(currentPage);
        };
        /***
         * Initializes selection shirt and renders the data
         ***/
        function init($config) {
            config = config || {};
            config = $.extend({}, config, $config);
            config.el = config.el || "";
            mainContainer = $(["#", config.el].join(""));
            inspirations = config.inspirations;
            totalPages = Math.ceil(inspirations.length / pageSize);
            anchorText = config.anchorText;
            prevBtn = $(config.prevBtn);
            nextBtn = $(config.nextBtn);
            currentPage = 0;
            resetEvent();
            initUI();
            initEvent();
        };
        // Shirt Refresh
        function refresh($config) {
            config = config || {};
            config = $.extend({}, config, $config);
            inspirations = config.inspirations;
            totalPages = Math.ceil(inspirations.length / pageSize);
            currentPage = 0;
            resetEvent();
            initEvent();
            paginate(currentPage);
        };
        // Pagination logic 
        function paginate(page_no) {
            mainContainer.html("");
            var startIndex = (currentPage) * pageSize;
            var endIndex = ((currentPage + 1) * pageSize) - 1;
            var imagesInPage = (inspirations).filter(function(e, i) {
                return i >= startIndex && i <= endIndex;
            });
            // Rendering Images on the page to show data
            $.each(imagesInPage, function(i, val) {
                var liElement = $("<li>");
                liElement.addClass('inspiration-selection-list')
                var imgElement = $("<img>");
                imgElement.attr("src", "img/shirt-renders/" + val.id + ".png");
                imgElement.attr("width", "280");
                imgElement.attr("height", "420");
                imgElement.attr("alt", "");
                imgElement.attr("inspirations-image-id", val.id);
                var btnImgElement = $("<button>");
                btnImgElement.attr('data-id', val.id);
                btnImgElement.addClass("magnify-btn");
                btnImgElement.attr("data-name", "");
                var btnElement = $("<button>");
                btnElement.attr("data-id", val.id);
                btnElement.attr("data-name", "");
                btnElement.text(anchorText);
                btnElement.addClass("use-shirt-design");
                mainContainer.append(liElement);
                liElement.append(imgElement);
                liElement.append(btnElement);
                liElement.append(btnImgElement);
            });

            // change prev next btn state
            if (currentPage == 0) {
                $(prevBtn).attr("disabled", "disabled");
                $(prevBtn).addClass("inactive");
            } else {
                $(prevBtn).removeAttr("disabled");
                $(prevBtn).removeClass("inactive");
            }

            if (currentPage >= totalPages - 1) {
                $(nextBtn).attr("disabled", "disabled");
                $(nextBtn).addClass("inactive");
            } else {
                $(nextBtn).removeAttr("disabled");
                $(nextBtn).removeClass("inactive");
            }
        };
        // Prev and Next Logic 
        function prevPage() {
            if (currentPage == 0) {
                return;
            }
            currentPage--;
            paginate(currentPage);
        };

        function nextPage() {
            if (currentPage >= totalPages - 1) {
                return;
            }
            currentPage++;
            paginate(currentPage);
        };

        return {
            init: init,
            refresh: refresh
        }
    };
}(window.app = window.app || {}, jQuery))