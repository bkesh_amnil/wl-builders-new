(function() {
    "use strict";

    $(document).ready(function() {
        $.get("html/templates/staff-project-sub-menu.html").done(function(staffProjectMenuHTML) {
            $("#staff-project-sub-menu").html(staffProjectMenuHTML);
            var activeSubMenuPage = $("#content").attr("data-page");
            $(".project-button").removeClass("active");
            $(".project-button[data-for='" + activeSubMenuPage + "']").addClass("active");
        })
    });

    listenForPointerClick($("#staff-project-sub-menu"), '.project-button');
    $("#staff-project-sub-menu").off("pointerclick", '.project-button');
    $("#staff-project-sub-menu").on("pointerclick", '.project-button', function(e) {
        if ($(this).hasClass("current")) {
            return;
        }

        var target_page = $(this).attr('data-for');
        $.when(switchPage(target_page)).done(function() {
            // Remember history (state, title, URL)
            window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
        });
    });
})()