/***
 * Script for staff dashboard - common to all staff pages. To be called once during full page load.
 * Defines these variables/functions/objects for use in the app:
 * - staffAppReady
 *
 * On load, performs:
 * - init of client-side DB
 * - sync client-side DB with server
 *
 * When load execution completes, sets var appready to true and triggers "appready" event on $(document)
 *     - Individual pages should bind their load execution script to "appready" event
 *     - Individual pages should check var appready and manually trigger "appready" event if it's true
 ***/

/*****************************
 *      Execute on load      *
 *****************************/
(function(app, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.Staff = function() {
        var ready = false; //will be set to true when load execution is complete

        function initEvent() {
            $("#content").off('click', '.breadcrumb');
            $("#content").on('click', '.breadcrumb', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#content"), '.breadcrumb');
            $("#content").off("pointerclick", '.breadcrumb');
            $("#content").on("pointerclick", '.breadcrumb', function(e) {
                if ($(this).hasClass("current")) {
                    return;
                }

                var target_page = $(this).attr('data-for');
                if (target_page) {
                    $.when(switchPage(target_page)).done(function() {
                        // Remember history (state, title, URL)
                        window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                    });
                }
            });

            //Return to Logout Button triggers 
            $("#content").off('click', '#back-to-top-button');
            $("#content").on('click', '#back-to-top-button', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#content"), '#back-to-top-button');
            $("#content").off("pointerclick", '#back-to-top-button');
            $("#content").on("pointerclick", '#back-to-top-button', function(e) {
                animateScroll(0, 0);
            });

            // when clicked, do nothing - always handle individual touch events as some mobile devices tend to wait a long time before triggering a "click" event.
            $('footer ul li a').off('click');
            $('footer ul li a').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('footer ul li a'));
            $('footer ul li a').off("pointerclick");
            $('footer ul li a').on("pointerclick", function(e) {
                var element = this;
                var target_page = $(this).data('for');
                $.when(switchPage(target_page)).done(function() {
                    // Remember history (state, title, URL)
                    window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                });
            });

            /**********************
             * Image Logo link Back to dashboard
             ****************/
            $('#header-logo').off('click');
            $('#header-logo').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#header-logo'));
            $('#header-logo').off("pointerclick");
            $('#header-logo').on("pointerclick", function(e) {
                var target_page = $(this).data('for');
                $.when(switchPage(target_page)).done(function() {
                    // Remember history (state, title, URL)
                    window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                });
            });

            /*****************************
             *Pagination Features Events for all the pages.
             ******************************/
            $("#content").off('click', '.page-link');
            $("#content").on('click', '.page-link', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#content"), '.page-link');
            $("#content").off("pointerclick", '.page-link');
            $("#content").on("pointerclick", '.page-link', function(e) {
                var element = $(this);
                var currentPage = element.attr('data-page');
                /* localDB variable to store paginaton data */
                var currentPageVariable = element.parents('section').attr("data-currentpagevariable");
                currentPageVariable = currentPageVariable || 'staffCurrentPage';
                localDB[currentPageVariable] = {
                    'value': currentPage
                };
                localDB.save(currentPageVariable);
                var parameter = '';
                parameter = $(this).parents("section").attr("data-parameter"); 
                var url = $(this).parents("section").attr("data-callbackUrl"); // First checks if the callback url is manually defined for the table
                if (typeof url === 'undefined') { // if callback url is not defined manually find the parameter url from the url
                    var url = (document.URL).replace(DOMAIN_URL, '');
                    url = url.replace('#', '');
                    url = url.substring(1, url.length);
                }
                var functionCall = parseUrlToStorage(url);
                console.log(functionCall);
                functionCall.functionName(parameter);
            });

            // when clicked the older or newer activity button
            $("#content").off('click', '.per-page');
            $("#content").on('click', '.per-page', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#content"), '.per-page');
            $("#content").off("pointerclick", '.per-page');
            $("#content").on("pointerclick", '.per-page', function(e) {
                var element = $(this);
                
                var perPageVariable = element.parents("section").attr("data-perpagevariable");
                
                perPageVariable = perPageVariable || 'staffPerPage';
                localDB[perPageVariable] = {
                    'value': element.attr('data-per-page')
                };
                
                localDB.save(perPageVariable);
                
                // To make the page start at 1 again when per page is changed to avoid blank 
                var currentPageVariable = element.parents('section').attr("data-currentpagevariable");
                currentPageVariable = currentPageVariable || 'staffCurrentPage';
                localDB[currentPageVariable] = {
                    'value': 1
                };
                
                localDB.save(currentPageVariable);
                var url = $(this).parents("section").attr("data-callbackUrl"); 
                var parameter = '';
                
                parameter = $(this).parents("section").attr("data-parameter"); //first check the parameter required to pass in the sortin
                
                // First checks if the callback url is manually defined for the table
                if (typeof url === 'undefined') { // if callback url is not defined manually find the parameter url from the url
                    var url = (document.URL).replace(DOMAIN_URL, '');
                    url = url.replace('#', '');
                    url = url.substring(1, url.length);
                }
                var functionCall = parseUrlToStorage(url);
                
                functionCall.functionName(parameter);
            });

            // when clicked the older or newer activity button
            $("#content").off('click', '.sort-button');
            $("#content").on('click', '.sort-button', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#content"), '.sort-button');
            $("#content").off("pointerclick", '.sort-button');
            $("#content").on("pointerclick", '.sort-button', function(e) {
                var element = $(this);
                var sortOrder = $(element).attr('data-check');
                if (sortOrder == null) {
                    sortOrder = true;
                    $(element).attr('data-check', "asc");
                } else {
                    sortOrder == "desc";
                    if (sortOrder == "desc") {
                        sortOrder = true;
                        $(element).attr('data-check', "asc");
                    } else {
                        sortOrder = false;
                        $(element).attr('data-check', "desc");
                    }
                }

                var url = (document.URL).replace(DOMAIN_URL, '');
                url = url.replace('#', '');
                //      var sortingOrder = element.dat;
                var url = $(this).parents("section").attr("data-callbackUrl"); // First checks if the callback url is manually defined for the table
                var parameter = '';
                parameter = $(this).parents("section").attr("data-parameter"); //first check the parameter required to pass in the sortin
                //alert(url);
                if (typeof url === 'undefined') { // if callback url is not defined manually find the parameter url from the url
                    var url = (document.URL).replace(DOMAIN_URL, '');
                    url = url.replace('#', '');
                    url = url.substring(1, url.length);
                }
                
                
                var functionCall = parseUrlToStorage(url);
                functionCall.object = functionCall.object.sortBy($(element).data('sort'), sortOrder);
                console.log(functionCall);
                functionCall.functionName(parameter);
            });

            // When Search
            listenForPointerClick($("#content"), '.search');
            $("#content").off("pointerclick", '.search');
            $("#content").on("pointerclick", '.search', function(e) {
                var element = $(this);
                // Start listening for touch end    
                $(element).off(touchEndEvent + '.search');
                $(element).on(touchEndEvent + '.search', function(evt) {
                    evt.preventDefault();
                    //evt.stopPropagation();

                    // Unbind 
                    $(element).off(touchMoveEvent + '.search');
                    $(element).off(touchEndEvent + '.search');

                    var searchText = $.trim($('#search-text').val());
                    if (searchText != '') {

                        var regex = new RegExp(searchText, "i");
                        // Filter products using the searchText
                        var categories = categories.filter(function(el) {
                            return (regex.test(el['name']));
                        });
                    }

                    //categories = categories.sort(sortBy($(element).data('search'), true));

                    var url = (document.URL).replace(DOMAIN_URL, '');
                    url = url.replace('#', '');
                   
                    var functionCall = parseUrlToStorage(url.substring(1, url.length));
                    functionCall.functionName();

                });

                // Ignore entire action if user moves before releasing touch/mouse
                $(element).off(touchMoveEvent + '.search');
                $(element).on(touchMoveEvent + '.search', function(evt) {
                    // Unbind
                    $(element).off(touchMoveEvent + '.search');
                    $(element).off(touchEndEvent + '.search');
                });
            });

            //Return to Logout Button triggers 

            $('#logout-link').off('click');
            $('#logout-link').on('click', function(e) {
                e.preventDefault();
            });

            //Return to Logout Button triggers 
            listenForPointerClick($('#logout-link'));
            $('#logout-link').off("pointerclick");
            $('#logout-link').on("pointerclick", function(e) {
                //Logout Section 
                $.when(staffAPI.logout()).done(function(data) {
                    localDB.staffPersistentLoginEmail = '';
                    localDB.save('staffPersistentLoginEmail');
                    localDB.staffPersistentLoginCookie = '';
                    localDB.save('staffPersistentLoginCookie');
                    sessionDB.clear();
                    var target_page = "staff-login";
                    $.when(switchPage(target_page)).done(function(data) {
                        hideLoadingAjaxContainer();
                        // Remember history (state, title, URL)
                        window.history.pushState('staffLogin', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                    }).fail(function() {});

                }).fail(function(data) {
                    sessionDB.clear();
                    localDB.staffPersistentLoginEmail = '';
                    localDB.save('staffPersistentLoginEmail');
                    localDB.staffPersistentLoginCookie = '';
                    localDB.save('staffPersistentLoginCookie');

                    var target_page = "staff-login";
                    $.when(switchPage(target_page)).done(function(data) {
                        hideLoadingAjaxContainer();
                        // Remember history (state, title, URL)
                        window.history.pushState('staffLogin', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                    }).fail(function() {});
                });
            });

            $('#profile-link').off('click');
            $('#profile-link').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#profile-link'));
            $('#profile-link').off("pointerclick");
            $('#profile-link').on("pointerclick", function(e) {
                var target_page = "staff-account-settings";
                $.when(switchPage(target_page)).done(function(data) {
                    // Remember history (state, title, URL)
                    window.history.pushState('staffaccount', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                }).fail(function() {

                });
            });
        }

        function init() {
            // Init storage
            localDB.init();
            sessionDB.init();
            initTheme();
            initEvent();
            $.when(publicAPI.getSessionUser()).done(function() {
                makeAppReady();
                return false;
            });
        }

        function makeAppReady() {
            resizeContent();
            $(window).off('resize.main');
            $(window).on('resize.main', resizeContent);
            $(window.app).trigger("staffAppReady");
            $("#content").addClass("active");
        }

        /***
         * Resizes the page content such that:
         * - page will always span at least one full screen (footer always stick to the bottom of page)
         * - Note: #content must be auto-height
         ***/
        function resizeContent() {
            // Get header/footer dimensions
            var headerHeight = $('header').innerHeight();
            var footerHeight = $('footer').innerHeight();

            // Set content dimensions
            var contentMinHeight = Math.max(window.innerHeight, window.minHeight) - headerHeight - footerHeight;
            $('#content').css({
                'min-width': window.minWidth + 'px',
                'min-height': contentMinHeight + 'px'
            });

            //
        }

        //theme settings;
        function initTheme() {
            if ($.type(localDB.theme) !== "string") {
                if (localDB.theme != null) {
                    var currentTheme = localDB.theme.name;
                    $("#theme-css").attr('href', DOMAIN_URL + "/css/theme/" + currentTheme + "-theme.css");
                }
            } else {
                if (localDB.theme != null) {
                    var currentTheme = $.parseJSON(localDB.theme).name;
                    $("#theme-css").attr('href', DOMAIN_URL + "/css/theme/" + currentTheme + "-theme.css");
                }
            }
        }

        /***
         * switch the page if the user is logged in and is in local session
         ***/
        function checkUserSession(from_page) {
            var loggedIn = sessionDB.user.id !== 0 && sessionDB.user.accessLevel == 99 && sessionDB.user.banned === false;
            var hasPersistentCookie = localDB.staffPersistentLoginCookie != '' && localDB.staffPersistentLoginCookie != '';
            var target_page;
            if (loggedIn && from_page == 'staff-login') {
                // Redirect to Staff Dashboard
                target_page = "staff-dashboard";
                $.when(switchPage(target_page)).done(function(data) {
                    window.history.pushState('staffLogin', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);

                });
            } else if (loggedIn && from_page != 'staff-login') {
                // Dont redirect to anywhere
                return 1;
            } else if (hasPersistentCookie) {
                var loginData = {
                    email: localDB.staffPersistentLoginEmail,
                    persistentLoginCookie: localDB.staffPersistentLoginCookie
                };
                $.when(staffAPI.persistentLogin(loginData)).done(function(data) {
                    if (sessionDB.user.id && from_page == 'staff-login') {
                        // Redirect to staff Dashboard
                        target_page = "staff-dashboard";
                        $.when(switchPage(target_page)).done(function(data) {
                            window.history.pushState('staffLogin', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                        });
                    } else if (sessionDB.user.id && from_page != 'staff-login') {
                        // Dont redirect to anywhere
                        return 1;
                    } else {
                        // Redirect to Staff Login
                        target_page = "staff-login";
                        $.when(switchPage(target_page)).done(function(data) {
                            window.history.pushState('staff Login', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                        });
                    }
                }).fail(function() {
                    localDB.staffPersistentLoginEmail = '';
                    localDB.save('staffPersistentLoginEmail');
                    localDB.staffPersistentLoginCookie = '';
                    localDB.save('staffPersistentLoginCookie');
                    target_page = "staff-login";
                    $.when(switchPage(target_page)).done(function(data) {
                        window.history.pushState('staff Login', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                    });
                });
            } else if (!loggedIn && from_page == 'staff-login') {
                // Dont redirect to anywhere
                return 1;
            } else {
                // Redirect to Staff Login
                target_page = "staff-login";
                $.when(switchPage(target_page)).done(function(data) {
                    window.history.pushState('staff Login', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                });
                return 1;
            }
        } //checkUserSession()

        /***
         * Check which page the user is on, and show/hide elements accordingly
         ***/
        function initHeaderFooter() {
            var page = getCurrentPageName();
            if (page == 'staff-login') {
                // Header elements
                $('#user-controls').hide();
                $('#start-button').show();

                // Footer elements
                $('.footer-section').hide();
                $('footer').css('padding-top', '25px');
                $('footer').css('padding-bottom', '25px');
            } else {
                // Header elements
                $('#user-controls').show();
                $('#start-button').hide();

                // Footer elements
                $('.footer-section').show();
                $('footer').css('padding-top', '40px');
                $('footer').css('padding-bottom', '40px');
            }
        } //initHeaderFooter()


        /***
         *@param string value of the (Name choose for each cateogory/subcategory(local storge))
         *@param strign field (Name for the localDB storage name "" eg: categories, subCategories)
         *@param int Id (ID of the localDB object eg. ID of category object, if(field is categories))
         *@return string of error.
         *If the new name for the category or the subcategories etc.is reserved or not.
         *Id value -1 for the new name for the localDB storage objects.
         *
         ***/
        function isReservedName(value, field, Id) {
            var error = "ok";
            var nameRequest = $.trim(value);
            if (nameRequest === "") {
                error = "*Required (can not be empty)";
                return error;
            }

            if (nameRequest.length >= 20) {
                error = "Name to contain less than 20 characters."
                return error;
                //DEBUG: console.log("nameRequest = " + nameRequest);
            }

            if (Id !== -1) {
                //DEBUG: console.log("id inside isReservedName = " + Id);
                var objectReference = localDB[field].getByID(Id);
                if (nameRequest.toLowerCase() === objectReference.name.toLowerCase()) {
                    return error;
                }
            }

            var objectArray = localDB[field].filter(function(el) {
                return el['displayPriority'] > 0;
            });

            $.each(objectArray, function(i, v) {
                if (v.name.toLowerCase() == nameRequest.toLowerCase()) {
                    error = "Name already exists.";
                    return error;
                }

            });

            return error;

        }


        /*****************
         * Pagination Function to create pagination HTML
         ******************/
        function generatePaginationHTML(currentPage, maxPageLinks, resultsPerPage, perPageArray, arrayLength, resultSectionId) {

            resultSectionId = resultSectionId || "#result-section";
            var resultSection = $(resultSectionId);

            var perPageVariable = resultSection.attr("data-perpagevariable");
            var currentPageVariable = resultSection.attr("data-currentpagevariable");
            perPageVariable = perPageVariable || 'staffPerPage';
            currentPageVariable = currentPageVariable || 'staffCurrentPage';

            if (isNaN(currentPage)) {
                currentPage = 1;
            }
            if (isNaN(resultsPerPage)) {
                resultsPerPage = 10;
            }
            // Ensure input data is clean
            var resultsPerPage = parseInt(resultsPerPage);
            var maxPageLinks = parseInt(maxPageLinks);
            var currentPage = parseInt(currentPage);
            var arrayLength = parseInt(arrayLength);
            var totalPages = Math.ceil(arrayLength / resultsPerPage);

            // Calculate first and last page numbers
            var firstPage = 1;
            if (currentPage == firstPage) {
                firstPage = null;
            }
            var lastPage = totalPages;
            if (currentPage == lastPage) {
                lastPage = null;
            }
            if ((totalPages - 3) <= maxPageLinks) {
                firstPage = null;
                lastPage = null;
                maxPageLinks = totalPages - 1; // don't count current
            }

            // Calculate prev and next page numbers
            var prevPage = currentPage - 1;
            if (prevPage < 1) {
                prevPage = null;
            }
            var nextPage = currentPage + 1;
            if (nextPage > totalPages) {
                nextPage = null;
            }

            // Calculate the page numbers to show as page links
            var startPage = currentPage;
            var endPage = currentPage;
            while (endPage - startPage < maxPageLinks) {
                // Try to find an earlier page to be start page
                startPage--;
                if (startPage <= 1) {
                    startPage = 1;
                    firstPage = null;
                }
                //DEBUG: console.log(endPage + '-' + startPage + '=' + (endPage-startPage));
                if (endPage - startPage == maxPageLinks) {
                    break;
                }

                // Try to find a later page to be end page
                endPage++;
                if (endPage >= totalPages) {
                    endPage = totalPages;
                    lastPage = null;
                }
                //             DEBUG: console.log(endPage + '-' + startPage + '=' + (endPage-startPage));
            }

            // Generate HTML for [Prev] [First] ... [Start] [...] [Current] [...] [End] [Next]
            var paginationHTML = '';
            if (prevPage != null) {
                paginationHTML += '<a class="page-link" href="#" data-page="' + prevPage + '">Previous</a>';
            } else {
                paginationHTML += '<span class="current">Previous</span>';
            }
            if (firstPage != null) {
                paginationHTML += '<a class="page-link" href="#" data-page="' + firstPage + '">' + firstPage + '</a> ... ';
            }
            for (var i = startPage; i < currentPage; i++) {
                paginationHTML += '<a class="page-link" href="#" data-page=' + i + '>' + i + '</a>';
            }
            paginationHTML += '<span class="current active">' + i + '</span>';
            for (var i = currentPage + 1; i <= endPage; i++) {
                paginationHTML += '<a class="page-link" href="#" data-page=' + i + '>' + i + '</a>';
            }
            if (lastPage != null) {
                paginationHTML += '... <a class="page-link" href="#" data-page="' + lastPage + '">' + lastPage + '</a>';
            }
            if (nextPage != null) {
                paginationHTML += '<a class="page-link" href="#" data-page="' + nextPage + '">Next</a>';
            } else {
                paginationHTML += '<span class="current">Next</span>';
            }
            
            resultSection.find(".simple-pagination").html(paginationHTML);

            // Generate HTML for results per page
            if (typeof perPageArray !== "object" || !(perPageArray instanceof Array)) {
                perPageArray = [5, 10, 20, 50];
            }
            var resultsPerPageHTML = 'Results per page';
            $.each(perPageArray, function(i, v) {
                var selected = '';
                if (v == resultsPerPage) {
                    selected = ' current';
                }
                resultsPerPageHTML += '<a href= "#" class="per-page' + selected + '" data-per-page="' + v + '">' + v + '</a>';
            })
            resultSection.find(".results-per-page").html(resultsPerPageHTML);
        }

        //Pagination Section search / sort / Filter
        /*****************************
         * Pagination section function call info + detect with url
         ******************************/
        function parseUrlToStorage(url) {
            switch (url) {
                case 'staff-project-basic-info':
                    return {
                        'object': localDB.projects,
                        'functionName': window.app.Module.StaffProjectBasicInfo.createProjectList
                    };
                case 'staff-project-feature':
                    return {
                        'object': localDB.projects,
                        'functionName': window.app.Module.StaffProjectFeature.createProjectList
                    };
                case 'staff-project-blue-print':
                    return {
                        'object': localDB.projects,
                        'functionName': window.app.Module.StaffProjectBluePrint.createProjectList
                    };
                case 'staff-project-image':
                    return {
                        'object': localDB.projects,
                        'functionName': window.app.Module.StaffProjectImage.createProjectList
                    };
                case 'staff-project-image-list':
                    return {
                        'object': localDB.projectImages,
                        'functionName': window.app.Module.StaffProjectImage.getProjectImageHtmlList
                    };
                case 'staff-testimonial':
                    return {
                        'object': localDB.testimonials,
                        'functionName': window.app.Module.StaffTestimonial.createTestimonialList
                    };

                case 'staff-sub-category':
                    return {
                        'object': localDB.subCategories,
                        'functionName': createSubCategoryList
                    };

                case 'staff-catalogue':
                    return {
                        'object': localDB.catalogues,
                        'functionName': createCatalogueList
                    };

                case 'staff-product':
                    return {
                        'object': localDB.products,
                        'functionName': createProductList
                    };

                case 'staff-image-library':
                    return {
                        'object': localDB.pictures,
                        'functionName': createPicturesList
                    };

                case 'staff-announcement':
                    return {
                        'object': localDB.announcements,
                        'functionName': window.app.Module.StaffAnnouncement.createAnnouncementList
                    };

                case 'staff-capabilities':
                    return {
                        'object': localDB.partners,
                        'functionName': window.app.Module.StaffPartner.createPartnerList
                    };

                case 'staff-button':
                    return {
                        'object': localDB.buttons,
                        'functionName': window.app.Module.StaffButton.createButtonList
                    };

                case 'staff-button-hole':
                    return {
                        'object': localDB.buttonHoles,
                        'functionName': window.app.Module.StaffButtonHole.createButtonHoleList
                    };

                case 'staff-button-thread':
                    return {
                        'object': localDB.buttonThreads,
                        'functionName': window.app.Module.StaffButtonThread.createButtonThreadList
                    };

                case 'staff-shirt-fitting-profile':
                    return {
                        'object': localDB.staffShirtFittingProfiles,
                        'functionName': window.app.Module.StaffShirtFittingProfile.createShirtFittingProfileList
                    };

                case 'staff-fabric':
                    return {
                        'object': localDB.fabrics,
                        'functionName': window.app.Module.StaffFabric.createFabricList
                    };

                case 'staff-customer':
                    return {
                        'object': localDB.customers,
                        'functionName': window.app.Module.StaffCustomer.createCustomerList
                    };

                case 'staff-fabric-care':
                    return {
                        'object': localDB.fabricCares,
                        'functionName': window.app.Module.StaffFabricCare.createFabricCareList
                    };

                case 'staff-fabric-color':
                    return {
                        'object': localDB.fabricColors,
                        'functionName': window.app.Module.StaffFabricColor.createFabricColorList
                    };

                case 'staff-fabric-material':
                    return {
                        'object': localDB.fabricMaterials,
                        'functionName': window.app.Module.StaffFabricMaterial.createFabricMaterialList
                    };

                case 'staff-fabric-pattern':
                    return {
                        'object': localDB.fabricPatterns,
                        'functionName': window.app.Module.StaffFabricPattern.createFabricPatternList
                    };

                case 'staff-fabric-weave':
                    return {
                        'object': localDB.fabricWeaves,
                        'functionName': window.app.Module.StaffFabricWeave.createFabricWeaveList
                    };

                case 'staff-staff':
                    return {
                        'object': localDB.staffs,
                        'functionName': window.app.Module.StaffStaff.createStaffList
                    };

                case 'staff-discount':
                    return {
                        'object': localDB.discounts,
                        'functionName': window.app.Module.StaffDiscount.createDiscountList
                    };

                case 'staff-order-history-customer':
                    return {
                        'object': localDB.customers,
                        'functionName': window.app.Module.StaffCart.createCustomerList
                    };

                case 'staff-shirt-fitting-profile-customer':
                    return {
                        'object': localDB.customers,
                        'functionName': window.app.Module.StaffShirtFittingProfile.createCustomerList
                    };

                case 'staff-fabric-collar':
                    return {
                        'object': localDB.collars,
                        'functionName': window.app.Module.StaffFabric.createCollarList
                    };

                case 'staff-fabric-cuff':
                    return {
                        'object': localDB.cuffs,
                        'functionName': window.app.Module.StaffFabric.createCuffList
                    };

                case 'staff-order':
                    return {
                        'object': localDB.staffOrders,
                        'functionName': window.app.Module.StaffOrder.createOrderList
                    };

                default:
                    return false;
            }
        }

        /***
         * Gets last updated time from the stored database (session storage or local storage) for the db element
         * @param {string} dbElement The database element
         * @param {sessionDB || localDB} storageType Database Type (session or local)
         * @returns {lastUpdateTime} Last updated time of the database element
         */
        function getLastUpdatedTime(dbElement, storageType) {
            if (storageType === 'localDB') {
                //gets last updated time of a localdb element
                var fromTime = 0;
                if (localDB[dbElement] !== null) {
                    var datas = localDB[dbElement];
                    $.each(datas, function(i, v) {
                        if (v.lastUpdateTime > fromTime) {
                            fromTime = v.lastUpdateTime;
                        }
                    });
                    fromTime = fromTime + 1;
                    return fromTime;
                }
            } else if (storageType === 'sessionDB') {
                //gets last updated time of a localdb element
                var fromTime = 0;
                if (sessionDB[dbElement] !== null) {
                    var datas = sessionDB[dbElement];
                    $.each(datas, function(i, v) {
                        if (v.lastUpdateTime > fromTime) {
                            fromTime = v.lastUpdateTime;
                        }
                    });
                    fromTime = fromTime + 1;
                    return fromTime;
                }
            };
        }

        return {
            init: init,
            checkUserSession: checkUserSession,
            headerFooterDisplay: initHeaderFooter,
            getLastUpdatedTime: getLastUpdatedTime,
            generatePaginationHTML: generatePaginationHTML,
            isReservedName: isReservedName,
            ready: ready
        }
    }();

}(window.app = window.app || {}, jQuery));

//animateInPage();
window.app.Module.Staff.init();
