//setup a function that loads a single script
function loadScripts(scripts) {
    var d = $.Deferred();
    //setup object to store results of AJAX requests
    var responses = {};

    //create function that evaluates each response in order
    function eval_scripts() {
        for (var i = 0, len = scripts.length; i < len; i++) {
            window.eval(responses[scripts[i]]);
        }
        d.resolve();
    }

    $.each(scripts, function(index, value) {
        $.ajax({
            url: DOMAIN_URL + '/' + scripts[index],
            //force the dataType to be `text` rather than `script`
            dataType: 'text',
            success: function(textScript) {

                //add the response to the `responses` object
                responses[value] = textScript;

                //check if the `responses` object has the same length as the `scripts` array,
                //if so then evaluate the scripts
                var responseLength = $.map(responses, function(n, i) {
                    return i;
                }).length;
                if (responseLength === scripts.length) {
                    eval_scripts();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) { /*don't forget to handle errors*/ }
        });
    });
    return d.promise();
}