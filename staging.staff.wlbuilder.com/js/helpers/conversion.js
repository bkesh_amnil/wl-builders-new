/***
 * Defines function for various unit conversion
 * Adds the following functions to window
 * -
 ***/

/***
 * Rounds to nearest quarter and returns the corresponding inch
 * @param {int} measurement in milli meters
 * @returns {int} Inch
 * Usage Example
 *  convertToInch(16.77716); // => 16.75
 *  convertToInch(16.892); // => 17
 *  convertToInch(16.999) // => 16.999
 */
function convertToInch(number) {
    var inch = number / 100;
    return inch;
}

/***
 * Converts to Millimeter
 * @param {int} measurement in inch
 * @returns {int} Inch
 * Usage Example
 *  convertToHundredthOfInch(1); // => 25.4
 */
function convertToHundredthOfInch(number) {
    var inch = Math.round(number * 100);
    return inch;
}