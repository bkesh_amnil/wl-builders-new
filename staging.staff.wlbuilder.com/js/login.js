/***
 * Module - app.Module.Login
 *
 * This Script incorporates following features:
 * - login
 * - register
 * - forgot password
 *
 * Dependencies :
 *  - jQuery
 *  - db.js
 *  - ajax-customer-api
 *
 * Before use:
 *  - ensure ajax-customer-api is init
 *  - ensure localDB & sessionDB are init
 *
 * Init instructions:
 *  - app.Module.Login.init();
 *
 * Usage examples:
 * - app.Module.Login.isUserLoggedIn(); // Checks if user is logged in
 * - app.Module.Login.displayLoginForm(); // Displays login form
 * - app.Module.Login.displayRegisterNewAccountForm(); // Displays Register new account form
 ***/

(function(app, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};
    // Jquery extension functions
    $.fn.extend({
        // Removes class "inactive"
        customShow: function() {
            return this.each(function() {
                $(this).removeClass("inactive");
                //$(this).addClass("login-fadein");
                //$(this).removeClass("login-fadeout");
            });
        },
        // Adds class "inactive"
        customHide: function() {
            return this.each(function() {
                $(this).addClass("inactive");
                //$(this).removeClass("login-fadein");
                //$(this).addClass("login-fadeout");
            });
        },
        activateLink: function() {
            return this.each(function() {
                if (!$(this).hasClass("active")) {
                    $(this).addClass("active");
                }
            });
        },
        deactivateLink: function() {
            return this.each(function() {
                $(this).removeClass("active");
            });
        },
        suppressClick: function() {
            return this.each(function() {
                $(this).off("click");
                $(this).on("click", function(e) {
                    e.preventDefault();
                    //e.stopPropagation();
                    return false;
                });
            });
        }
    });

    app.Module.Login = function() {
        /***
         * Removes all events: click, input, focus, pointerclick
         ***/
        function resetEvent() {
            // Sign in and register new account button event
            $("#sign-in").off("pointerclick");
            $("#sign-in").suppressClick();
            $("#register-new-account").off("pointerclick");
            $("#register-new-account").suppressClick();
            // Close button event
            $("#close-login-collapse-panel").off("pointerclick");
            $("#close-login-collapse-panel").suppressClick();
            // Login section events
            $("#login-form-email").off("focus");
            $("#login-form-email").off("input");
            $("#login-form-password").off("input");
            $("#login-form-password").off("focus");
            $("#login-form-password").off("keypress");
            $("#login-button").off("pointerclick");
            $("#login-button").suppressClick();
            // Logout button event
            $("#logout-button").off("pointerclick");
            $("#logout-button").suppressClick();
            $("#logged-in-email").off("pointerclick");
            $("#logged-in-email").suppressClick();
            // Register new account section events
            $("#register-new-account-email").off("focus");
            $("#register-new-account-email").off("input");
            $("#register-new-account-email").off("keypress");
            $("#register-new-account-button").off("pointerclick");
            $("#register-new-account-button").suppressClick();
            $("#register-new-account-code-verification-code").off("input");
            $("#register-new-account-code-verification-code").off("focus");
            $("#register-new-account-code-verification-code").off("keypress");
            $("#register-new-account-code-verification-code-verify-button").off("pointerclick");
            $("#register-new-account-code-verification-code-verify-button").suppressClick();
            $("#register-new-account-code-verification-code-verify-resend-button").off("pointerclick");
            $("#register-new-account-code-verification-code-verify-resend-button").suppressClick();
            $("#register-new-account-password").off("focus");
            $("#register-new-account-password").off("input");
            $("#register-new-account-confirm-password").off("focus");
            $("#register-new-account-confirm-password").off("input");
            $("#register-new-account-confirm-password").off("keypress");
            $("#register-new-account-password-save-button").off("pointerclick");
            $("#register-new-account-password-save-button").suppressClick();
            // Forgot password section events
            $("#forgot-password-link").off("pointerclick");
            $("#forgot-password-link").suppressClick();
            $("#forgot-password-verification-code").off("input");
            $("#forgot-password-verification-code").off("focus");
            $("#forgot-password-verification-code").off("keypress");
            $("#forgot-password-verification-code-verify-button").off("pointerclick");
            $("#forgot-password-verification-code-verify-button").suppressClick();
            $("#forgot-password-verification-code-verify-resend-button").off("pointerclick");
            $("#forgot-password-verification-code-verify-resend-button").suppressClick();
            $("#forgot-password-password").off("focus");
            $("#forgot-password-password").off("input");
            $("#forgot-password-confirm-password").off("focus");
            $("#forgot-password-confirm-password").off("input");
            $("#forgot-password-confirm-password").off("keypress");
            $("#forgot-password-password-save-button").off("pointerclick");
            $("#forgot-password-password-save-button").suppressClick();
        } // resetEvent()

        /***
         * Adds all necessary events: input, focus, pointerclick
         ***/
        function initEvent() {
            // Sign in and register new account button event
            listenForPointerClick($("#sign-in"));
            $("#sign-in").on("pointerclick", function(e) {
                resetAllSigninForms();
                resetAllForgotPasswordForms();

                $(this).activateLink();
                $("#register-new-account").deactivateLink();

                $("#login-form").customShow();
                $("#login-form-email-valid").customHide();
                $("#login-form-email-invalid").customHide();
                $("#register-new-account-form").customHide();
                $("#register-new-account-code-verification-form").customHide();
                $("#register-new-account-set-password-form").customHide();
                $("#register-welcome-section").customHide();

                $("#login-button").customHide();
                $("#forgot-password-link").customHide();
                $("#login-form-password").attr("placeholder", "Your password");

                $("#forgot-password-link").customHide();
                $("#forgot-password-verification-code-form").customHide();
                $("#forgot-password-set-password-form").customHide();
                $("#forgot-password-verification-code-invalid").customHide();
                $("#forgot-password-verification-code-verify-button").customHide();
                $("#forgot-password-set-password-form").customHide();
                $("#forgot-password-password-valid").customHide();
                $("#forgot-password-confirm-password-invalid").customHide();
                $("#forgot-password-password-save-button").customHide();
                $("#login-collapsed-panel").customShow();
                setTimeout(function() {
                    $("#login-form-email").focus();
                }, SET_FOCUS_TIMEOUT);
            });

            listenForPointerClick($("#register-new-account"));
            $("#register-new-account").on("pointerclick", function(e) {
                resetAllSignupForms();

                $(this).activateLink();
                $("#sign-in").deactivateLink();

                $("#login-form").customHide();
                $("#forgot-password-verification-code-form").customHide();
                $("#forgot-password-set-password-form").customHide();

                $("#register-new-account-form").customShow();
                $("#register-new-account-code-verification-form").customShow();
                $("#register-new-account-set-password-form").customShow();
                $("#register-welcome-section").customShow();
                $("#register-new-account-form").customShow();
                $("#register-new-account-email-valid").customHide();
                $("#register-new-account-email-invalid").customHide();
                $("#register-new-account-code-verification-form").customHide();
                $("#register-new-account-set-password-form").customHide();
                $("#register-welcome-section").customHide();
                $("#register-new-account-button").attr("disabled", "disabled");

                $("#login-collapsed-panel").customShow();
                setTimeout(function() {
                    $("#register-new-account-email").focus();
                }, SET_FOCUS_TIMEOUT);
            });

            listenForPointerClick($("#close-login-collapse-panel"));
            $("#close-login-collapse-panel").on("pointerclick", function(e) {
                resetAllSigninForms();
                resetAllSignupForms();
                resetAllForgotPasswordForms();
                $("#login-collapsed-panel").customHide();

                $("#sign-in").deactivateLink();
                $("#register-new-account").deactivateLink();
                $("#login-form-password").attr("placeholder", "Your password");
                $("#login-section :input").blur();
                $('input').blur();
            });

            /******************************
             * Login/Logout process events *
             *******************************/
            // Reset login email placeholder
            $("#login-form-email").on("focus", function(e) {
                $("#login-form-email").attr("placeholder", "Your email address");
            });

            // Check login email 
            $("#login-form-email").on("input", function(e) {
                //e.stopPropagation();
                setLoginButtonVisibility();
                if (!isEmailFieldValid()) {
                    $("#forgot-password-link").customHide();
                }
            });

            // Check login password
            $("#login-form-password").on("input", function(e) {
                //e.stopPropagation();
                setLoginButtonVisibility();
                $("#forgot-password-link").customHide();
            });

            // Reset login password placeholder
            $("#login-form-password").on("focus", function(e) {
                $("#login-form-password").attr("placeholder", "Your password");
            });

            // Trigger Login on enter key
            $("#login-form-password").on("keypress", function(e) {
                if (e.which == 13) {
                    $("#login-button").trigger("pointerclick");
                    e.preventDefault();
                }
            });

            // Login
            listenForPointerClick($("#login-button"));
            $("#login-button").on("pointerclick", function(e) {
                // validate email
                var emailElement = $("#login-form-email");
                var email = emailElement.val();

                if (email === "") {
                    $("#login-form-email").attr("placeholder", "Email is required :(");
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#login-form-email").attr("placeholder", "Invalide Email :(");
                    return;
                }

                // validate password
                var passwordElement = $("#login-form-password");
                var password = passwordElement.val();

                if (password === "") {
                    $("#login-form-password").attr("placeholder", "Password is required :(");
                    return;
                }

                var keepLoggedInElement = $("#login-form-keep-me-logged-in");
                var keepLoggedIn = 0;
                if (keepLoggedInElement.prop("checked")) {
                    keepLoggedIn = 1;
                }

                var formData = {
                    email: email,
                    password: password,
                    persistent: keepLoggedIn
                };

                $.when(customerAPI.login(formData)).done(function(data) {
                    changeStateToLogin();
                    $(window.app).trigger("loggedIn");
                }).fail(function(data) {
                    showInvalidAuthenticationAction();
                });
            });

            // Logout
            listenForPointerClick($("#logout-button"));
            $("#logout-button").on("pointerclick", function(e) {
                logoutUser();
            });

            // Redirect to account-settings page
            listenForPointerClick($("#logged-in-email"));
            $("#logged-in-email").on("pointerclick", function(e) {
                if (isUserLoggedIn()) {
                    var target_page = 'account-settings';
                    $.when(window.app.Module.PageSwitcher.switchPage(target_page)).done(function(data) {
                        // Remember history (state, title, URL)
                        window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                    }).fail(function() {});
                }
            });

            // Initiate forgot password process
            listenForPointerClick($("#forgot-password-link"));
            $("#forgot-password-link").on("pointerclick", function(e) {

                var emailElement = $("#login-form-email");
                var email = $.trim(emailElement.val());

                //check email validation
                if (email === "") {
                    $("#login-form-email").val('');
                    $("#login-form-email").attr("placeholder", "Invalid email :(");
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#login-form-email").val('');
                    $("#login-form-email").attr("placeholder", "Invalid email :(");
                    return;
                }

                var formData = {
                    email: email
                };

                $.when(customerAPI.forgetPassword(formData)).done(function(data) {
                    $("#login-form").customHide();
                    $("#forgot-password-verification-code-form").customShow();
                    $("#forgot-password-set-password-form").customHide();
                    $("#forgot-password-verification-code-invalid").customHide();
                    $("#forgot-password-verification-code-verify-button").customHide();
                    $("#forgot-password-verification-code-verify-resend-button").customShow();

                    $("#forgot-password-verification-code").focus();
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    $("#login-form-email").val('');
                    $("#login-form-email").attr("placeholder", responseTxt.error);
                });
            });

            /******************************
             * Register new account events *
             *******************************/
            // Reset register new account email placeholder
            $("#register-new-account-email").on("focus", function(e) {
                $("#register-new-account-email").attr("placeholder", "Your email address");
            });

            // Check register new account email
            $("#register-new-account-email").on("input", function(e) {
                //e.stopPropagation();
                setRegisterButtonAccessibility();
            });

            // Trigger register new account on enter key
            $("#register-new-account-email").on("keypress", function(e) {
                if (e.which == 13) {
                    $("#register-new-account-button").trigger("pointerclick");
                    e.preventDefault();
                    //e.stopPropagation();
                }
            });

            // Start register new account process
            listenForPointerClick($("#register-new-account-button"));
            $("#register-new-account-button").on("pointerclick", function(e) {
                var emailElement = $("#register-new-account-email");
                var email = $.trim(emailElement.val());

                //check email validation
                if (email === "") {
                    $("#register-new-account-email").attr("placeholder", "Email is required :(");
                    $("#register-new-account-email").val("");
                    $("#register-new-account-email-invalid").customHide();
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#register-new-account-email").attr("placeholder", "Invalid email :(");
                    $("#register-new-account-email").val("");
                    $("#register-new-account-email-invalid").customHide();
                    return;
                }

                var formData = {
                    email: email,
                };

                var referrerId = getParameterByName("uid");
                if (referrerId != null && referrerId != "") {
                    formData.referrerID = referrerId;
                }

                $.when(customerAPI.register(formData)).done(function(data) {
                    $("#register-new-account-form").customHide();
                    $("#register-new-account-code-verification-form").customShow();
                    $("#register-new-account-code-verification-code-verify-resend-button").customShow();

                    $("#register-new-account-code-verification-code").focus();
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    $("#register-new-account-email").val("");
                    $("#register-new-account-email").attr("placeholder", responseTxt.error);
                    $("#register-new-account-email-valid").customHide();
                });
            });

            // Check verification code
            $("#register-new-account-code-verification-code").on("input", function(e) {
                //e.stopPropagation();
                validateVerificationCode();
            });

            // Reset verification code placeholder
            $("#register-new-account-code-verification-code").on("focus", function(e) {
                $("#register-new-account-code-verification-code").attr("placeholder", "Your verification code");
            });

            // Trigger code verification code process
            $("#register-new-account-code-verification-code").on("keypress", function(e) {
                if (e.which == 13) {
                    $("#register-new-account-code-verification-code-verify-button").trigger("pointerclick");
                    e.preventDefault();
                    //e.stopPropagation();
                }
            });

            // Start code verification process
            listenForPointerClick($("#register-new-account-code-verification-code-verify-button"));
            $("#register-new-account-code-verification-code-verify-button").on("pointerclick", function(e) {
                var emailElement = $("#register-new-account-email");
                var email = $.trim(emailElement.val());

                //check email validation
                if (email === "") {
                    $("#register-new-account-code-verification-code").attr("placeholder", "Invalid email. Please restart registration :(");
                    $("#register-new-account-code-verification-code").val("");
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#register-new-account-code-verification-code").attr("placeholder", "Invalide email. Please restart registration :(");
                    $("#register-new-account-code-verification-code").val("");
                    return;
                }

                var verificationCodeElement = $("#register-new-account-code-verification-code");
                var verificationCode = $.trim(verificationCodeElement.val());

                //check verification code validation
                if (verificationCode === "") {
                    $("#register-new-account-code-verification-code").attr("placeholder", "Verfication code is empty :(");
                    $("#register-new-account-code-verification-code").val("");
                    return;
                }

                if (!isValidVerificationCode(verificationCode)) {
                    $("#register-new-account-code-verification-code").attr("placeholder", "Verfication code is invalid :(");
                    $("#register-new-account-code-verification-code").val("");
                    return;
                }

                var formData = {
                    email: email,
                    code: verificationCode
                };

                $.when(customerAPI.verifyEmail(formData)).done(function(data) {
                    $("#register-new-account-code-verification-form").customHide();
                    $("#register-new-account-set-password-form").customShow();

                    $("#register-new-account-password").focus();
                }).fail(function(data) {
                    $("#register-new-account-code-verification-code").val("");
                    $("#register-new-account-code-verification-code").attr("placeholder", "Wrong verfication code :(");
                    $("#register-new-account-code-verification-code-verify-button").customHide();
                });
            });

            // Resend verification code
            listenForPointerClick($("#register-new-account-code-verification-code-verify-resend-button"));
            $("#register-new-account-code-verification-code-verify-resend-button").on("pointerclick", function(e) {
                var emailElement = $("#register-new-account-email");
                var email = $.trim(emailElement.val());

                //check email validation
                if (email === "") {
                    $("#register-new-account-code-verification-code").attr("placeholder", "Invalid email. Please restart registration :(");
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#register-new-account-code-verification-code").attr("placeholder", "Invalid email. Please restart registration :(");
                    return;
                }

                var formData = {
                    email: email
                };
                $.when(customerAPI.resendEmailVerificationCode(formData)).done(function(data) {
                    $("#register-new-account-code-verification-code").val("");
                    $("#register-new-account-code-verification-code").attr("placeholder", "New verfication code has been sent to your email.");
                }).fail(function(data) {
                    $("#register-new-account-code-verification-code").val("");
                    $("#register-new-account-code-verification-code").attr("placeholder", "Error sending verfication code :(");
                });
            });

            // Reset register new account password placeholder
            $("#register-new-account-password").on("focus", function(e) {
                $(this).attr("placeholder", "Your password");
            });

            // Check new password
            $("#register-new-account-password").on("input", function(e) {
                //e.stopPropagation();
                if ($("#register-new-account-password").val() == "") {
                    $("#register-new-account-password-valid").customHide();
                    $("#register-new-account-confirm-password-invalid").customHide();
                    $("#register-new-account-password-save-button").customHide();

                    $("#register-new-account-code-verification-code").focus();
                } else {
                    $("#register-new-account-password-valid").customShow();
                }
            });

            // Reset register new account confirm password placeholder
            $("#register-new-account-confirm-password").on("focus", function(e) {
                $(this).attr("placeholder", "Your password");
            });

            // Check confirm password
            $("#register-new-account-confirm-password").on("input", function(e) {
                //e.stopPropagation();
                if ($("#register-new-account-password").val() == "") {
                    $("#register-new-account-confirm-password-invalid").customHide();
                    $("#register-new-account-password-save-button").customHide();
                } else if ($("#register-new-account-confirm-password").val() == "") {
                    $("#register-new-account-confirm-password-invalid").customHide();
                    $("#register-new-account-password-save-button").customHide();
                } else if ($("#register-new-account-password").val() == $("#register-new-account-confirm-password").val()) {
                    $("#register-new-account-confirm-password-invalid").customHide();
                    $("#register-new-account-password-save-button").customShow();
                } else {
                    $("#register-new-account-confirm-password-invalid").customShow();
                    $("#register-new-account-password-save-button").customHide();
                }
            });

            // Trigger set password process
            $("#register-new-account-confirm-password").on("keypress", function(e) {
                if (e.which == 13) {
                    $("#register-new-account-password-save-button").trigger("pointerclick");
                    e.preventDefault();
                    //e.stopPropagation();
                }
            });

            // Set password
            listenForPointerClick($("#register-new-account-password-save-button"));
            $("#register-new-account-password-save-button").on("pointerclick", function(e) {
                var emailElement = $("#register-new-account-email");
                var email = $.trim(emailElement.val());

                //check email validation
                if (email === "") {
                    $("#register-new-account-password").val("");
                    $("#register-new-account-password").attr("placeholder", "Email is required. Please restart.");
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#register-new-account-password").val("");
                    $("#register-new-account-password").attr("placeholder", "Invalid Email. Please restart.");
                    return;
                }

                var passwordElement = $("#register-new-account-password");
                var password = passwordElement.val();

                //check password
                if (password === "") {
                    $("#register-new-account-password").val("");
                    $("#register-new-account-password").attr("placeholder", "Password is required.");
                    return;
                }

                var confirmPasswordElement = $("#register-new-account-confirm-password");
                var confirmPassword = confirmPasswordElement.val();

                //check confirm password
                if (confirmPassword === "") {
                    $("#register-new-account-confirm-password").val("");
                    $("#register-new-account-confirm-password").attr("placeholder", "Password is required.");
                    return;
                }

                if (password !== confirmPassword) {
                    $("#register-new-account-confirm-password").val("");
                    $("#register-new-account-confirm-password").attr("placeholder", "Password don't match.");
                    return;
                }

                var formData = {
                    email: email,
                    password: password
                };

                $.when(customerAPI.setPassword(formData)).done(function(data) {
                    var loginData = {
                        email: email,
                        password: password,
                        persistent: 0
                    };

                    $.when(customerAPI.login(loginData)).done(function(data) {
                        $("#register-new-account-set-password-form").customHide();
                        $("#register-welcome-section").customShow();
                        $("#sign-in").attr("disabled", "disabled");
                        $("#register-new-account").attr("disabled", "disabled");
                        $("#close-login-collapse-panel").attr("disabled", "disabled");
                        setTimeout(function() {
                            changeStateToLogin();
                            $(window.app).trigger("loggedIn");
                        }, WELCOME_MESSAGE_TIMEOUT);
                    }).fail(function(data) {

                    });
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    $("#register-new-account-confirm-password").val("");
                    $("#register-new-account-confirm-password").attr("placeholder", responseTxt.error);
                });
            });

            /*************************
             * Forgot password events *
             **************************/
            // Check verfication code
            $("#forgot-password-verification-code").on("input", function(e) {
                //e.stopPropagation();
                validateForgotPasswordVerificationCode();
            });

            // Reset verification code placeholder
            $("#forgot-password-verification-code").on("focus", function(e) {
                $("#forgot-password-verification-code").attr("placeholder", "Your verification code");
            });

            // Trigger code verification process
            $("#forgot-password-verification-code").on("keypress", function(e) {
                if (e.which == 13) {
                    $("#forgot-password-verification-code-verify-button").trigger("pointerclick");
                    e.preventDefault();
                    //e.stopPropagation();
                }
            });

            // Start code verification
            listenForPointerClick($("#forgot-password-verification-code-verify-button"));
            $("#forgot-password-verification-code-verify-button").on("pointerclick", function(e) {
                var emailElement = $("#login-form-email");
                var email = $.trim(emailElement.val());

                //check email validation
                if (email === "") {
                    $("#forgot-password-verification-code").attr("placeholder", "Invalid email. Please restart registration :(");
                    $("#forgot-password-verification-code").val("");
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#forgot-password-verification-code").attr("placeholder", "Invalide email. Please restart registration :(");
                    $("#forgot-password-verification-code").val("");
                    return;
                }

                var verificationCodeElement = $("#forgot-password-verification-code");
                var verificationCode = $.trim(verificationCodeElement.val());

                //check verification code validation
                if (verificationCode === "") {
                    $("#forgot-password-verification-code").attr("placeholder", "Verfication code is empty :(");
                    $("#forgot-password-verification-code").val("");
                    return;
                }

                if (!isValidVerificationCode(verificationCode)) {
                    $("#forgot-password-verification-code").attr("placeholder", "Verfication code is invalid :(");
                    $("#forgot-password-verification-code").val("");
                    return;
                }

                var formData = {
                    email: email,
                    code: verificationCode
                };

                $.when(customerAPI.verifyEmail(formData)).done(function(data) {
                    $("#forgot-password-verification-code-form").customHide();
                    $("#forgot-password-set-password-form").customShow();

                    $("#forgot-password-password").focus();
                }).fail(function(data) {
                    $("#forgot-password-verification-code").val("");
                    $("#forgot-password-verification-code").attr("placeholder", "Wrong verfication code :(");
                    $("#forgot-password-verification-code-verify-button").customHide();
                });
            });

            // Resend verification code
            listenForPointerClick($("#forgot-password-verification-code-verify-resend-button"));
            $("#forgot-password-verification-code-verify-resend-button").on("pointerclick", function(e) {
                var emailElement = $("#login-form-email");
                var email = $.trim(emailElement.val());

                //check email validation
                if (email === "") {
                    $("#forgot-password-verification-code").attr("placeholder", "Invalid email. Please restart forgot password process :(");
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#forgot-password-verification-code").attr("placeholder", "Invalid email. Please restart forgot password process :(");
                    return;
                }

                var formData = {
                    email: email
                };

                $.when(customerAPI.resendEmailVerificationCode(formData)).done(function(data) {
                    $("#forgot-password-verification-code").val("");
                    $("#forgot-password-verification-code").attr("placeholder", "New verfication code has been sent to your email.");
                    $("#forgot-password-verification-code-verify-button").customHide();

                    $("#forgot-password-verification-code").focus();
                }).fail(function(data) {
                    $("#forgot-password-verification-code").val("");
                    $("#forgot-password-verification-code").attr("placeholder", "Error sending verfication code :(");
                    $("#forgot-password-verification-code-verify-button").customHide();
                });
            });

            // Reset password placeholder
            $("#forgot-password-password").on("focus", function(e) {
                $(this).attr("placeholder", "Your password");
            });

            // Check password
            $("#forgot-password-password").on("input", function(e) {
                //e.stopPropagation();
                if ($("#forgot-password-password").val() == "") {
                    $("#forgot-password-password-valid").customHide();
                    $("#forgot-password-confirm-password-invalid").customHide();
                    $("#forgot-password-password-save-button").customHide();
                } else {
                    $("#forgot-password-password-valid").customShow();
                }
            });

            // Reset confirm password placeholder
            $("#forgot-password-confirm-password").on("focus", function(e) {
                $(this).attr("placeholder", "Your password");
            });

            // Check confirm password
            $("#forgot-password-confirm-password").on("input", function(e) {
                //e.stopPropagation();
                if ($("#forgot-password-password").val() == "") {
                    $("#forgot-password-confirm-password-invalid").customHide();
                    $("#forgot-password-password-save-button").customHide();
                } else if ($("#forgot-password-confirm-password").val() == "") {
                    $("#forgot-password-confirm-password-invalid").customHide();
                    $("#forgot-password-password-save-button").customHide();
                } else if ($("#forgot-password-password").val() == $("#forgot-password-confirm-password").val()) {
                    $("#forgot-password-confirm-password-invalid").customHide();
                    $("#forgot-password-password-save-button").customShow();
                } else {
                    $("#forgot-password-confirm-password-invalid").customShow();
                    $("#forgot-password-password-save-button").customHide();
                }
            });

            // Trigger set password
            $("#forgot-password-confirm-password").on("keypress", function(e) {
                if (e.which == 13) {
                    $("#forgot-password-password-save-button").trigger("pointerclick");
                    e.preventDefault();
                    //e.stopPropagation();
                }
            });

            // Set password
            listenForPointerClick($("#forgot-password-password-save-button"));
            $("#forgot-password-password-save-button").on("pointerclick", function(e) {
                var emailElement = $("#login-form-email");
                var email = $.trim(emailElement.val());

                //check email validation
                if (email === "") {
                    $("#forgot-password-password").val("");
                    $("#forgot-password-password").attr("placeholder", "Email is required. Please restart.");
                    return;
                }

                if (!isValidEmail(email)) {
                    $("#forgot-password-password").val("");
                    $("#forgot-password-password").attr("placeholder", "Invalid Email. Please restart.");
                    return;
                }

                var passwordElement = $("#forgot-password-password");
                var password = passwordElement.val();

                //check password
                if (password === "") {
                    $("#forgot-password-password").val("");
                    $("#forgot-password-password").attr("placeholder", "Password is required.");
                    return;
                }

                var confirmPasswordElement = $("#forgot-password-confirm-password");
                var confirmPassword = confirmPasswordElement.val();

                //check confirm password
                if (confirmPassword === "") {
                    $("#forgot-password-confirm-password").val("");
                    $("#forgot-password-confirm-password").attr("placeholder", "Password is required.");
                    return;
                }

                if (password !== confirmPassword) {
                    $("#forgot-password-confirm-password").val("");
                    $("#forgot-password-confirm-password").attr("placeholder", "Password don't match.");
                    return;
                }

                var formData = {
                    email: email,
                    password: password
                };

                $.when(customerAPI.setPassword(formData)).done(function(data) {
                    var loginData = {
                        email: email,
                        password: password,
                        persistent: 0
                    };

                    $.when(customerAPI.login(loginData)).done(function(data) {
                        changeStateToLogin();
                    }).fail(function(data) {
                        logoutUser();
                    });
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    $("#forgot-password-confirm-password").val("");
                    $("#forgot-password-confirm-password").attr("placeholder", responseTxt.error);
                });
            });
        } // initEvent()

        /***
         * Initializes UI states for sign in, register new account, forgot password sections, etc
         * Resets all forms
         * If user is logged in hides sign in, register buttons and login, register, forgot password controls. Displays logged in user's email and log out button. If persistentCookie exists, checks for persistentCookie and logs in user if valid.
         * If user is not logged in does opposite of logged in stage
         ***/
        function initUI() {
            // Hide and reset login section initially.
            $("#login-form").customHide();
            $("#login-button").customHide();
            $("#login-form-email-invalid").customHide();
            $("#login-form-email-valid").customHide();
            $("#logged-in-email").text("");
            $("#logged-in-email").customHide();
            $("#logout-button").customHide();
            $("#forgot-password-link").customHide();

            $("#sign-in").deactivateLink();
            $("#register-new-account").deactivateLink();
            resetAllSigninForms();

            // Hide and reset register new account section initially
            $("#register-new-account-form").customHide();
            $("#register-new-account-code-verification-form").customHide();
            $("#register-new-account-set-password-form").customHide();
            $("#register-welcome-section").customHide();
            $("#register-new-account-email-invalid").customHide();
            $("#register-new-account-email-valid").customHide();
            $("#register-new-account-code-verification-code-verify-button").customHide();
            $("#register-new-account-code-verification-code-verify-resend-button").customHide();
            $("#register-new-account-code-verification-code-invalid").customHide();
            $("#register-new-account-password-valid").customHide();
            $("#register-new-account-confirm-password-invalid").customHide();
            $("#register-new-account-password-save-button").customHide();
            $("#register-welcome-section").customHide();
            resetAllSignupForms();

            // Hide and reset forgot password section
            $("#forgot-password-set-password-form").customHide();
            $("#forgot-password-verification-code-form").customHide();
            $("#forgot-password-verification-code-invalid").customHide();
            $("#forgot-password-verification-code-verify-button").customHide();
            $("#forgot-password-verification-code-verify-resend-button").customHide();
            $("#forgot-password-set-password-form").customHide();
            $("#forgot-password-password-valid").customHide();
            $("#forgot-password-confirm-password-invalid").customHide();
            $("#forgot-password-password-save-button").customHide();
            resetAllForgotPasswordForms();


            // On full page refresh check if user is already logged in. If user is logged in, change UI to logged in state
            if (isUserLoggedIn()) {
                changeStateToLogin();
            } else if (localDB.persistentLoginCookie !== "" && localDB.persistentLoginEmail !== "") {
                // On full page refresh check if persistentLoginCookie is set. If it is set, check persistentLogin 
                var loginData = {
                    email: localDB.persistentLoginEmail,
                    persistentLoginCookie: localDB.persistentLoginCookie
                };
                $.when(customerAPI.persistentLogin(loginData)).done(function(data) {
                    if (isUserLoggedIn()) {
                        changeStateToLogin();
                    } else {
                        $("#sign-in").customShow();
                        $("#sign-in-register-new-account-separator").customShow();
                        $("#register-new-account").customShow();

                        if (getParameterByName("uid") != "") {
                            displayRegisterNewAccountForm();
                        }
                    }
                }).fail(function(data) {
                    $("#logged-in-email").text("");
                    $("#logged-in-email").customHide();
                    $("#logout-button").customHide();

                    $("#sign-in").customShow();
                    $("#sign-in-register-new-account-separator").customShow();
                    $("#register-new-account").customShow();

                    $("#sign-in").deactivateLink();
                    $("#register-new-account").deactivateLink();

                    if (getParameterByName("uid") != "") {
                        displayRegisterNewAccountForm();
                    }
                });
            } else {
                $("#sign-in").customShow();
                $("#sign-in-register-new-account-separator").customShow();
                $("#register-new-account").customShow();

                if (getParameterByName("uid") != "") {
                    displayRegisterNewAccountForm();
                }
            }
        } // initUI()

        /***
         * Initialize Module
         ***/
        function init() {
            resetEvent();
            initEvent();
            initUI();
        } // init()

        /***
         * Checks if valid login email has been entered in login email textbox
         * @params: none
         * @return: boolean (True if email is not empty and if email is valid. False otherwise)
         ***/
        function isEmailFieldValid() {
            var emailText = $("#login-form-email").val();
            if (emailText == "") {
                // Hide both images
                $("#login-form-email-invalid").customHide();
                $("#login-form-email-valid").customHide();
                return false;
            } else if (isValidEmail(emailText)) {
                // Hide invalid and Show valid image
                $("#login-form-email-invalid").customHide();
                $("#login-form-email-valid").customShow();
                return true;
            } else {
                // Show invalid and hide valid image
                $("#login-form-email-invalid").customShow();
                $("#login-form-email-valid").customHide();
                return false;
            }
        } // isEmailFieldValid()

        /***
         * Check if login password has been entered in login password textbox
         * params: none
         * return: boolean (True if login password field is not empty. False otherwise)
         ***/
        function isPasswordEntered() {
            if ($("#login-form-password").val() == "") {
                return false;
            } else {
                return true;
            }
        } // isPasswordEntered()

        /*** 
         * Sets login button visibility
         * Login button is visible only if valid email has been entered in login email textbox and password has been entered in login password textbox.
         * params: none
         * return: none
         ***/
        function setLoginButtonVisibility() {
            if (isEmailFieldValid() && isPasswordEntered()) {
                $("#login-button").customShow();
            } else {
                $("#login-button").customHide();
            }
        } // setLoginButtonVisibility()

        /*** 
         * Checks if user is logged in
         * params: none
         * return: boolean
         ***/
        function isUserLoggedIn() {
            if (sessionDB.user === undefined || sessionDB.user.email === undefined || sessionDB.user.id == 0) {
                return false;
            } else if (sessionDB.user.userType !== "Customer" || !sessionDB.user.emailVerified || sessionDB.user.accessLevel !== 1) {
                return false;
            } else {
                return true;
            }
        } // isUserLoggedIn()

        /***
         * If user is logged in, gets logged in user's email
         * params: none
         * return: string of email
         ***/
        function getLoggedInUserEmail() {
            if (isUserLoggedIn()) {
                return sessionDB.user.email;
            } else {
                return "";
            }
        } // getLoggedInUserEmail()

        /***
         * Changes UI to logged in state
         * Resets Forms in login, forgot password and register new account sections
         * Hides all login, forgot password and register new account sections
         * Displays logout button
         * params: none
         * return: none
         ***/
        function changeStateToLogin() {
            resetAllSigninForms();
            resetAllSignupForms();
            resetAllForgotPasswordForms();

            $("#login-collapsed-panel").customHide();

            $("#logged-in-email").text(sessionDB.user.email);
            $("#logged-in-email").customShow();
            $("#logout-button").customShow();

            $("#sign-in").customHide();
            $("#sign-in-register-new-account-separator").customHide();
            $("#register-new-account").customHide();
            $('input').blur();
        } // changeStateToLogin()

        /***
         * If authentication fails, Invalid email or password. text is put into placeholder of login password textbox
         * params: none
         * return: none
         ***/
        function showInvalidAuthenticationAction() {
            $("#login-form-password").val("");
            $("#login-form-password").attr("placeholder", "Invalid email or password.");
            $("#login-button").customHide();
            $("#forgot-password-link").customShow();
        } // showInvalidAuthenticationAction()

        /***
         * Logs out user and change UI state to logged out
         * Hides logout button
         * params: none
         * return: none
         ***/
        function logoutUser() {
            $.when(customerAPI.logout()).done(function(data) {
                $("#logged-in-email").text("");
                $("#logged-in-email").customHide();
                $("#logout-button").customHide();

                $("#sign-in").customShow();
                $("#sign-in-register-new-account-separator").customShow();
                $("#register-new-account").customShow();

                $("#sign-in").removeAttr("disabled");
                $("#register-new-account").removeAttr("disabled");
                $("#close-login-collapse-panel").removeAttr("disabled");

                $("#sign-in").deactivateLink();
                $("#register-new-account").deactivateLink();
                sessionDB.clear();
                $(window.app).trigger("loggedOut");
            }).fail(function(data) {
                $("#logged-in-email").text("");
                $("#logged-in-email").customHide();
                $("#logout-button").customHide();

                $("#sign-in").customShow();
                $("#sign-in-register-new-account-separator").customShow();
                $("#register-new-account").customShow();

                $("#sign-in").removeAttr("disabled");
                $("#register-new-account").removeAttr("disabled");
                $("#close-login-collapse-panel").removeAttr("disabled");

                $("#sign-in").deactivateLink();
                $("#register-new-account").deactivateLink();
                sessionDB.clear();
                $(window.app).trigger("loggedOut");
            });
        } // logoutUser()

        /***
         * Changes Register button's accessibility
         * Register button is enabled if email is not empty and valid email has been entered. Otherwise register button is disabled
         * params: none
         * return: boolean (True if button is enabled. False otherwise)
         ***/
        function setRegisterButtonAccessibility() {
            var emailText = $("#register-new-account-email").val();
            if (emailText == "") {
                // Hide both images
                $("#register-new-account-email-invalid").customHide();
                $("#register-new-account-email-valid").customHide();
                $("#register-new-account-button").attr("disabled", "disabled");
                return false;
            } else {
                if (isValidEmail(emailText)) {
                    // Hide invalid and Show valid image
                    $("#register-new-account-email-invalid").customHide();
                    $("#register-new-account-email-valid").customShow();
                    $("#register-new-account-button").removeAttr("disabled");
                    return true;
                } else {
                    // Show invalid and hide valid image
                    $("#register-new-account-email-invalid").customShow();
                    $("#register-new-account-email-valid").customHide();
                    $("#register-new-account-button").attr("disabled", "disabled");
                    return false;
                }
            }
        } // setRegisterButtonAccessibility()

        /***
         * Checks if valid verification code has been entered in registration process.
         * params: none
         * return: boolean (True if verification code is not empty and 6-digit capital alphabets. False otherwise)
         ***/
        function validateVerificationCode() {
            var verificationCode = $("#register-new-account-code-verification-code").val();
            if (verificationCode == "") {
                // Hide both images
                $("#register-new-account-code-verification-code-invalid").customHide();
                $("#register-new-account-code-verification-code-verify-button").customHide();
                return false;
            } else {
                if (isValidVerificationCode(verificationCode)) {
                    // Hide invalid and Show valid image
                    $("#register-new-account-code-verification-code-invalid").customHide();
                    $("#register-new-account-code-verification-code-verify-button").customShow();
                    return true;
                } else {
                    // Show invalid and hide valid image
                    $("#register-new-account-code-verification-code-invalid").customShow();
                    $("#register-new-account-code-verification-code-verify-button").customHide();
                    return false;
                }
            }
        } // validateVerificationCode()

        // Resets all forms in login section
        function resetAllSigninForms() {
            $("#login-form")[0].reset();
            $("#login-form-password").attr("placeholder", "Your password");
        } // resetAllSigninForms()

        // Resets all forms in register new account section
        function resetAllSignupForms() {
            $("#register-new-account-form")[0].reset();
            $("#register-new-account-code-verification-form")[0].reset();
            $("#register-new-account-set-password-form")[0].reset();

            $("#register-new-account-code-verification-code").attr("placeholder", "Your verification code");
        } // resetAllSignupForms()

        // Resets all forms in forgot password section
        function resetAllForgotPasswordForms() {
            $("#forgot-password-verification-code-form")[0].reset();
            $("#forgot-password-set-password-form")[0].reset();
            $("#forgot-password-verification-code").attr("placeholder", "Your verification code");
        } // resetAllForgotPasswordForms()

        /***
         * Checks if valid verification code has been entered in forgot password process.
         * param: none
         * return: boolean (True if verification code is not empty and 6-digit capital alphabets. False otherwise)
         ***/
        function validateForgotPasswordVerificationCode() {
            var verificationCode = $("#forgot-password-verification-code").val();
            if (verificationCode == "") {
                // Hide both images
                $("#forgot-password-verification-code-invalid").customHide();
                $("#forgot-password-verification-code-verify-button").customHide();
                return false;
            } else {
                if (isValidVerificationCode(verificationCode)) {
                    // Hide invalid and Show valid image
                    $("#forgot-password-verification-code-invalid").customHide();
                    $("#forgot-password-verification-code-verify-button").customShow();
                    return true;
                } else {
                    // Show invalid and hide valid image
                    $("#forgot-password-verification-code-invalid").customShow();
                    $("#forgot-password-verification-code-verify-button").customHide();
                    return false;
                }
            }
        } // validateForgotPasswordVerificationCode()

        /***
         * Displays login form
         ***/
        function displayLoginForm() {
            $("#sign-in").trigger("pointerclick");
        } // displayLoginForm()

        /***
         * Displays Register new account form
         ***/
        function displayRegisterNewAccountForm() {
            $("#register-new-account").trigger("pointerclick");
        } // displayRegisterNewAccountForm()

        return {
            init: init,
            isUserLoggedIn: isUserLoggedIn,
            displayLoginForm: displayLoginForm,
            displayRegisterNewAccountForm: displayRegisterNewAccountForm
        }
    }();
}(window.app = window.app || {}, jQuery));