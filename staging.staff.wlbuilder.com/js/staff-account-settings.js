/***
 * Module - app.Module.StaffAccountSettings
 *
 * This Script incorporates following features:
 *
 * Dependencies :
 *  - jQuery
 *  - db.js
 *
 * Before use:
 *  - ensure ajax-customer-api is init
 *  - ensure localDB & sessionDB are init
 *
 * Init instructions:
 *  - app.Module.StaffDashboard.init();
 *
 * Usage examples:
 * - none
 ***/

(function (app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};
    App.Module.StaffAccountSettings = function () {

        function setUserProfile() {
            var fromTime = 0;
            if (localDB.staffs != null) {
                var staffs = localDB.staffs;
                $.each(staffs, function (i, v) {
                    if (v.lastUpdateTime > fromTime) {
                        fromTime = v.lastUpdateTime;
                    }
                });
            }

            var formData = {
                'fromTime': fromTime
            };

            $.when(staffAPI.getUpdatedStaffs(formData)).done(function (data) {
                if (sessionDB.user.id !== "") {
                    $("#first-name").val(sessionDB.user.firstName);
                    $("#phone-number").val(sessionDB.user.phoneNumber);
                    $('#email').val(sessionDB.user.email);
                }
            });
        }

        /***
         * Adds all necessary events: input, focus, pointerclick
         ***/
        function initEvent() {

            //check if the user name and the email is already used by other staffs.
            // check for first name
            $('#first-name').off('change, blur');
            $('#first-name').on('change, blur', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var element = $(this);
                var staffName = $.trim(element.val());
                if ((staffName === "")) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Name of place is Required</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
                }
                element.next('label').html("");
            });
            //old password can't be null
            $('#old-password').off('change, blur');
            $('#old-password').on('change, blur', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var element = $(this);
                if ($.trim(element.val()) === "") {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Password Required.</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
//                    element.next('label').html("*Password Required").addClass('error');
//                    ;
//                    return;
                }
                element.next('label').html("");
            });
            //new password can not be null
            $('#new-password').off('change, blur');
            $('#new-password').on('change, blur', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var returndata = true;
                var element = $(this);
                if ($.trim(element.val()) === "") {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>New Password Required.</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
//                    element.next('label').html("*Password Required").addClass('error');
//                    ;
//                    return;
                }
                element.next('label').html("");
            });
            //new confirm password should be equal to new password 

            $('#new-password-2').off('change, blur');
            $('#new-password-2').on('change, blur', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var element = $(this);
                var newPassword = $.trim(element.val());
                if (newPassword === "") {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Confirm password Required.</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
//                    element.next('label').html("*Confirm password Required").addClass('error');
//                    return;
                } else {
                    if (newPassword !== $.trim($("#edAccountNewPass").val())) {
                        swal({
                            title: "",
                            type: "warning",
                            text: '<div class="error"><span>Passwords do not match.</span></div>',
                            confirmButtonColor: '#18589f',
                            html: true
                        });
                        return;
                        //                    element.next('label').html("*Passwords do not match").addClass('error');
                        //                    return;
                    }
                }
                element.next('label').html("");
            });
            //submit then change.
            $('#edit-account-info-form').off('submit');
            $('#edit-account-info-form').on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();
                //**  Name Validation
                $('.error').html('');
                var staffNameEmail = $('#first-name');
                var staffName = $.trim(staffNameEmail.val());
                if ((staffName === "")) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Name of place is Required.</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
//                    staffNameEmail.next('label').html("*First name is Required").addClass('error');
//                    return;
                }

                var staffNamePhoneNumber = $("#phone-number");
                if ($.trim(staffNamePhoneNumber.val()) == '') {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Phone Number is Required.</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
//                    $(projectPriority).next('label').html("Display priority is required.").addClass('error');
//                    projectPriority.focus();
//                    return;
                } else {
                    if (isNumber(staffNamePhoneNumber.val()) == false || (staffNamePhoneNumber.val().length) != '8') {
                        swal({
                            title: "",
                            type: "warning",
                            text: '<div class="error"><span>Invalid Phone Number.</span></div>',
                            confirmButtonColor: '#18589f',
                            html: true
                        });
                        return;
//                    $(projectPriority).next('label').html("Invalid display priority.").addClass('error');
//                    projectPriority.focus();
//                    return;
                    }
                }
                function isNumber(str) {
                    if (typeof str !== 'number' && typeof str !== 'string') {
                        return false;
                    }

                    return !isNaN(parseFloat(str)) && isFinite(str);

                    // jQuery
                    //return (str - parseFloat(str) + 1) >= 0;
                }



//                staffNameEmail = $('#last-name');
//                staffName = $.trim(staffNameEmail.val());
//                if ((staffName === "")) {
//                    staffNameEmail.next('label').html("*First name is Required").addClass('error');
//                    return;
//                }

                //** Email Validation
                var element = $("#email");
                var staffEmail = $.trim(element.val());
                if ((staffEmail === "")) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Email is Required</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
//                    element.next('label').html("*Email is Required").addClass('error');
//                    return;
                } else {

                    if (!isValidEmail(staffEmail)) {
                        swal({
                            title: "",
                            type: "warning",
                            text: '<div class="error"><span>Invalid Email.</span></div>',
                            confirmButtonColor: '#18589f',
                            html: true
                        });
                        return;
//                    element.next('label').html("Invalid email").addClass('error');
//                    return;
                    }

                    if (staffEmail === sessionDB.user.email) {
                        element.next('label').html("");
                    } else {
                        $.each(localDB.staffs, function (i, v) {
                            if (v.email == staffEmail) {
                                swal({
                                    title: "",
                                    type: "warning",
                                    text: '<div class="error"><span>Email Reserved.</span></div>',
                                    confirmButtonColor: '#18589f',
                                    html: true
                                });
                                return;
//                            $(element).next('label').html("Email Reserved").addClass('error');
//                            return;
                            }
                        });
                    }
                }


                var element = $(this);
                var formData = $(element).serialize();

                $.when(staffAPI.editProfile(formData)).done(function () {

                }).fail(function (data) {
                    if (typeof data.responseText == "object") {
                        var responseTxt = $.parseJSON(data.responseText);
                        $('#error').html(responseTxt.error);
                        return;
                    } else {
                        $('#error').html(data.responseText);
                    }
                });
            });

            //submit then change.
            $('#change-password-form').off('submit');
            $('#change-password-form').on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var checkError = 0;
                var swapError = '';

                if ($.trim($('#old-password').val()) === "") {
                    swapError += '<span>Password Required.</span>';
                    checkError = 1;
//                    $('#old-password').next('label').html("*Password Required").addClass('error');
                }

                //Confirm Password
                var newPasswordElement = $("#new-password-2");
                var newPassword = $.trim(newPasswordElement.val());
                if (newPassword === "") {
                    swapError += '<span>Confirm password Required.</span>';
                    checkError = 1;
//                    newPasswordElement.next('label').html("*Confirm password Required").addClass('error');
//                    return;
                } else {
                    if (newPassword !== $.trim($("#new-password").val())) {
                        swapError += '<span>Passwords do not match.</span>';
                        checkError = 1;
//                    $("#new-password").next('label').html("*Passwords do not match").addClass('error');
//                    return;
                    }
                }
                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    //swal({title:"Error",text: '',html: true});
                    return;

                }
                var element = $(this);
                var formData = $(element).serialize();
                $.when(staffAPI.changePassword(formData)).done(function () {

                }).fail(function (data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + responseTxt.error + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    //swal({title:"Error",text: '',html: true});
                    return;
                    // $('#error').html(responseTxt.error);
                    //return;
                });
            });

            //Undo Button Click Section
            $('#edit-account-undo-button').off('click');
            $('#edit-account-undo-button').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#edit-account-undo-button'));
            $('#edit-account-undo-button').off("pointerclick");
            $('#edit-account-undo-button').on("pointerclick", function (e) {
                var element = $(this);
                var form = $(element).closest('form');

                $('#edit-account-info-form').find(':input').each(function () {
                    $(this).val('');
                });
                $('#edit-account-info-form label.error').html('');
                setUserProfile();
            });

            //Undo Button Click Section
            $('#change-password-undo-button').off('click');
            $('#change-password-undo-button').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#change-password-undo-button'));
            $('#change-password-undo-button').off("pointerclick");
            $('#change-password-undo-button').on("pointerclick", function (e) {
                var element = $(this);
                var form = $(element).closest('form');

                $('#change-password-form').find(':input').each(function () {
                    $(this).val('');
                });
                $('#change-password-form label.error').html('');
            });
            /************************************
             * Breadcum Link to the Page referred
             ***********************************/

            $('.breadcrumb').off('click');
            $('.breadcrumb').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('.breadcrumb'));
            $('.breadcrumb').off("pointerclick");
            $('.breadcrumb').on("pointerclick", function (e) {
                if ($(this).hasClass("current")) {
                    return;
                }

                var target_page = $(this).attr('data-for');
                $.when(switchPage(target_page)).done(function () {
                    // Remember history (state, title, URL)
                    window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                });
            });


            //Return to Logout Button triggers 
            $('#back-to-top-button').off('click');
            $('#back-to-top-button').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#back-to-top-button'));
            $('#back-to-top-button').off("pointerclick");
            $('#back-to-top-button').on("pointerclick", function (e) {
                animateScroll(0, 0);
            });

            // Clear cache
            $('#clear-cache-link').off("click");
            $('#clear-cache-link').on("click", function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#clear-cache-link'));
            $('#clear-cache-link').off("pointerclick");
            $('#clear-cache-link').on("pointerclick", function (e) {
                localDB.clear();
                $('#edit-account-info-form-message').removeClass('error').html('Cleared');
            });

            $("#add-account-file").off("change");
            $("#add-account-file").on("change", function () {
                readURL(this, $(this).parent().prev().find('img'));
                if ($(this).val() == "") {
                    $(this).next().next('output').html("Project image is required").addClas("error");
                }
                $(this).next().next('output').html("");
                $(this).closest('td').find('.deleteAccountImage').css('display', 'inline-block');
            });

        }
        /***
         * Initializes UI
         ***/
        function initUI() {
            //check for the user session to switch the page to dashboard if logged in.
            staff.checkUserSession('staff-account-settings');
            staff.headerFooterDisplay();
            setUserProfile();
            $("#overlay").addClass("hide");
        }

        /***
         * Initialize Module
         ***/
        function init() {
            initEvent();
            initUI();
        }
        return {
            init: init
        }
    }();
}(window.app = window.app || {}, window.app.Module.Staff, jQuery))


window.app.Module.StaffAccountSettings.init();