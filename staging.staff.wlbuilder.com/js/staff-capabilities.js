/***
 * This Script incorporates following features:
 * - Add partner
 * - Edit partner
 * - Delete partner
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
(function (app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffPartner = function () {
        var
                perPage,
                currentPage;

        // Image Validation for PNG type 
        function validateFileExtension(fileName) {
            var exp = /^.*\.(png|PNG)$/;
            return exp.test(fileName);
        }

        /***
         * Initialize various UI states
         ***/
        function initUI() {
            $("#result-section").attr("data-perpagevariable", "partnerPerPage"); // variable to holde per page pagination
            $("#result-section").attr("data-currentpagevariable", "partnerCurrentPage"); // variable to hold current pagination
        }

        function readURL(input, fieldID) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    fieldID.closest('td').find('.uploaded-name-holder input').val(input.files[0].name);
                    fieldID
                            .attr('src', e.target.result)
//                        .width(150)
//                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
//        $("#imgInp").change(function(){
//            readURL(this);
//        });

        /***
         * Listens the various events
         **/
        function initEvent() {

            /**********************
             * Delete Partner Event
             ***********************/

            // Turns off click event delete button
            $("#result-section").off('click', '.delete');
            $("#result-section").on('click', '.delete', function (e) {
                e.preventDefault();
            });

            // Displays delete confirmation row when delete is clicked
            listenForPointerClick($("#result-section"), '.delete');
            $("#result-section").off("pointerclick", '.delete');
            $("#result-section").on("pointerclick", '.delete', function (e) {
                var element = this;
                var partnerRow = $(element).closest('tr');
                var partnerID = $(element).attr('data-pre');
                $(partnerRow).after('<tr><td colspan="2" align="center">Hide this?  <span class="confirm-yes" data-pre="' + partnerID + '">Yes</span> / <span class="confirm-no">No</span><td></tr>');
                $(partnerRow).addClass("hide");
            });

            /**********************
             * Conformation Yes click
             ***********************/

            // Turns off clicke event confirm yes button
            $("#result-section").off('click', '.confirm-yes');
            $("#result-section").on('click', '.confirm-yes', function (e) {
                e.preventDefault();
            });

            // Deletes the clicked partner when delete action is confirmed.
            listenForPointerClick($("#result-section"), '.confirm-yes');
            $("#result-section").off("pointerclick", '.confirm-yes');
            $("#result-section").on("pointerclick", '.confirm-yes', function (e) {
                var confirmationRow = $(this).parents("tr");
                var partnerID = $(this).attr('data-pre');
                var partner = localDB.partners.getByID(partnerID);
                // Creates a Form Data object and appends data to it.
                var formData = new FormData(); // form data object
                formData.append('description', partner.description);
                formData.append('id', partnerID);
                formData.append('displayPriority', 0);
                formData.append('email', partner.email);
                formData.append('name', partner.name);
                formData.append('website', partner.websiteURL);
                $.when(staffAPI.editPartner(formData)).done(function () {
                    createPartnerList();
                    repositionContent();
                    $(confirmationRow).prev("tr").removeClass("hide");
                    $(confirmationRow).remove();
                });
            });

            /**********************
             * Conformation No click
             ***********************/

            // Turns off click event confirm no button
            $("#result-section").off('click', '.confirm-no');
            $("#result-section").on('click', '.confirm-no', function (e) {
                e.preventDefault();
            });

            // Removes the confirmation action row when delete action is not cofirmed
            listenForPointerClick($("#result-section"), '.confirm-no');
            $("#result-section").off("pointerclick", '.confirm-no');
            $("#result-section").on("pointerclick", '.confirm-no', function (e) {
                var confirmationRow = $(this).closest('tr');
                $((confirmationRow).prev('tr')).removeClass("hide");
                $(confirmationRow).remove();
            });

            /**********************
             * Edit Partner 
             ***********************/

            // when clicked on edit button
            $("#result-section").off('click', '.edit');
            $("#result-section").on('click', '.edit', function (e) {
                e.preventDefault();
            });

            // Renders the edit partner form for the clicked partner 
            listenForPointerClick($("#result-section"), '.edit');
            $("#result-section").off("pointerclick", '.edit');
            $("#result-section").on("pointerclick", '.edit', function (e) {
                //$('#login-form').removeClass('hide').addClass('show');               


                var element = $(this);
                // Set the values for the form display 
                renderEditPartnerForm($(element).attr('data-pre'));
                $('#edit-partner').addClass("active");
                var position = $("#edit-partner").position();
                animateScroll(position.left, position.top);
            });

            // Turns of click event for undo button
            $('.undo-btn').off('click');
            $('.undo-btn').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.undo-btn'));
            $('.undo-btn').off("pointerclick");
            $('.undo-btn').on("pointerclick", function (e) {
                var element = $(this);
                var form = $(element).closest('form');
                
                if (form.attr('name') === 'edit-partner') {
                    var partnerId = $(form).attr('data-pre');
                    $('form[name="edit-partner"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    renderEditPartnerForm(partnerId);
                    $('form[name="edit-partner"] label.errorTxt').html('');

                } else {
                    $('.image-preview-area img').attr('src','');
                    $('form[name="add-partner"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    $('form[name="add-partner"] label.errorTxt').html('');
                    $('form[name="add-partner"] span.contenteditable').text("");
                    $('#add-partner-preview-img').attr("src", '');
                }
            });


            //Add section Submit
            $('form[name="add-partner"]').off('submit');
            $('form[name="add-partner"]').on('submit', function (e) {
                console.log("add form");
                e.preventDefault();
                
                //$(this).find('td').removeClass('error');

                var swapError = '';
                var checkError = 0;

                $(this).find("label.error").html("");
                /*** Validation starts ***/

                var partnerName = $("#add-partner-name");

                if ($.trim(partnerName.val()) == '') {
                    swapError += '<span>Name is required.<span>';
                    checkError = 1;
                }

                var partnerDescription = $("#add-partner-description");

                if ($.trim(partnerDescription.val()) == '') {
                    swapError += '<span>Description is required.</span>';
                    checkError = 1;
                }

                var partnerPriority = $("#add-partner-priority");
                if ($.trim(partnerPriority.val()) == '') {
                    swapError += "<span>Display priority is required.</span>";
                    checkError = 1;
                } else { 
                    if (!$.isNumeric(partnerPriority.val())) {
                        swapError += "<span>Invalid display priority entered.</span>";
                        checkError = 1;
                    }
                }

                var partnerImage = $("#add-partner-file");

                if ($.trim(partnerImage.val()) == '') {
                    swapError += "<span>Capability image is required.</span>";
                    checkError = 1;
                } else { 
                        if (!validateFileExtension(partnerImage.val())) {
                            swapError += "<span>Only PNG image is required.<span>";
                            checkError = 1;
                        }
                    }
                /*** Validation ends ***/
                var formData = new FormData($(this)[0]);

                // formData.append('img', partnerImage.get(0).files[0]);
                formData.append('img', partnerImage.get(0).files[0]);

                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;

                }
                $.when(staffAPI.addPartner(formData)).done(function () {
                    createPartnerList();
                    repositionContent();
                    $('form[name="add-partner"]').find(':input').val('');
                    $('form[name="add-partner"] label.errorTxt').html('');
                    $('form[name="add-partner"] img#add-partner-image-preview').attr('src', '');
                }).fail(function (data) {
                    $('.add-error').html(data.responseJSON.error).show().delay(10000).fadeOut();

                });
            });

            //Edit section Submit
            $('form[name="edit-partner"]').off('submit');
            $('form[name="edit-partner"]').on('submit', function (e) {
                console.log('edit-form');
                e.preventDefault();
                
                $(this).find('td').removeClass('error');

                var swapError = '';
                var checkError = 0;

                var partnerID = $(this).attr('data-pre');
                e.preventDefault();
                e.stopPropagation();
                /*** Validation starts ***/

                var partnerName = $("#edit-partner-name");

                if ($.trim(partnerName.val()) == '') {
                    swapError += "<span>Name is required.</span>";
                    checkError = 1;
                }

                var partnerPriority = $("#edit-partner-priority");
                if ($.trim(partnerPriority.val()) == '') {
                    swapError += "<span>Display priority is required.</span>";
                    checkError = 1;

                } else { 
                    if (!$.isNumeric(partnerPriority.val())) {
                        swapError += "<span>Invalid display priority entered.</span>";
                        checkError = 1;
                    }
                }
                var partnerImage = $("#edit-partner-file");
                var partnerImagePrev = $("#edit-partner-image-preview");

                if (partnerImagePrev.attr('src') == "") {
                    if ($.trim(partnerImage.val()) == '') {
                        swapError += "<span>Capability image is required.</span>";
                        checkError = 1;
                    } else {

                        if (!validateFileExtension(partnerImage.val())) {
                            swapError += "<span>Only PNG image is Supported.<span>";
                            checkError = 1;
                        }
                    }
                }

                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;

                }

                var formData = new FormData($(this)[0]);
                formData.append('img', partnerImage.get(0).files[0]);
                formData.append('id', partnerID);
                $.when(staffAPI.editPartner(formData)).done(function () {
                    createPartnerList();
                    repositionContent();
                    $('form[name="edit-partner"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    $('#edit-partner').removeClass("active");
                    $('form[name="edit-partner"] span.contenteditable').text("");

                }).fail(function (data) {
                    $('.edit-error').html(data.responseJSON.error).show().delay(10000).fadeOut();

                });
            });

            /*************************
             *    Search Partners    *
             *************************/
            $('#search-text').off('click keypress');
            $('#search-text').on('keypress', function (e) {
                console.log("press");
                e.stopPropagation();
                var code = e.keyCode || e.which;

                if (code == 13) {
                    var searchedText = $.trim($(this).val());
                    createPartnerList();
                }
            });
            $('#search-button').off('click');
            $('#search-button').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#search-button'));
            $('#search-button').off("pointerclick");
            $('#search-button').on("pointerclick", function (e) {
                var searchedText = $.trim($('#search-text').val());
                createPartnerList();
            });

            $("#add-partner-file,#edit-partner-file").off("change");
            $("#add-partner-file,#edit-partner-file").on("change", function () {
                readURL(this, $(this).parent().prev().find('img'));
                
                if ($(this).val() == "") {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Partner image is required</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
                }
                $(this).closest('td').find('.deleteCapabilityImg').css('display', 'inline-block');
                $(this).next().next('output').html("");

            });

            /***
             * Turns off click event for confirmation' button
             ***/

            $('.deleteCapabilityImg').off('click');
            $('.deleteCapabilityImg').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.deleteCapabilityImg'));
            $('.deleteCapabilityImg').off("pointerclick");
            $('.deleteCapabilityImg').on("pointerclick", function (e) {
                var element = $(this);

                $('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.confirm-delete').css('display', 'block');

            });


            // Turns off clicke event confirm yes button
            $('.setting-section').off('click', '.confirm-yes');
            $(".setting-section").on('click', '.confirm-yes', function (e) {
                e.preventDefault();
            });

            // Deletes the clicked partner when delete action is confirmed.
            listenForPointerClick($(".setting-section"), '.confirm-yes');
            $('.setting-section').off("pointerclick", '.confirm-yes');
            $('.setting-section').on("pointerclick", '.confirm-yes', function (e) { //alert("test");
                var element = $(this);
                $(element).closest('td').find('input[type="file"]').val('');
                $(element).closest('td').find('img').attr('src', '');
                $(element).closest('td').find('.uploaded-name-holder input').val('');

                $(element).closest('td').find('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.deleteCapabilityImg').css('display', 'none');
            });

            /**********************
             * Conformation No click
             ***********************/

            // Turns off click event confirm no button
            $('.setting-section').off('click', '.confirm-no');
            $('.setting-section').on('click', '.confirm-no', function (e) {
                e.preventDefault();
            });

            // Removes the confirmation action row when delete action is not cofirmed
            listenForPointerClick($('.setting-section'), '.confirm-no');
            $('.setting-section').off("pointerclick", '.confirm-no');
            $('.setting-section').on("pointerclick", '.confirm-no', function (e) {
                $(this).closest('td').find('.confirm-delete').css('display', 'none');
            });

        } //initEvent

        /**
         * Init module
         */
        function init() {
            repositionContent();
            initUI();
            //check for the user session to switch the page to dashboard if logged in.
            staff.checkUserSession('staff-partner');
            staff.headerFooterDisplay();
            getPartners();
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);
            initEvent();
        } //init

        /***
         * Positions any content element
         * @param {boolean} animate defaults to true, if set to false the screen is not animated to top
         ***/
        function repositionContent(animate) {
            animate = typeof animate !== 'undefined' ? animate : true;
            var addForm = $("form[name='add-partner']");
            addForm.find("label.error").text("");
            addForm.find("input").val("");
            $("#edit-partner").removeClass("active");
            if (animate) {
                animateScroll(0, 0);
            }
        } //repositionContent

        /***********************
         * from staff api call getUpdatedpartners set data to localDB
         * creates partners.
         ************************/
        function getPartners() {
            var partnerFromTime = 0;
            if (localDB.partners != null) {
                var partners = localDB.partners;
                $.each(partners, function (i, v) {
                    if (v.updatedTime > partnerFromTime) {
                        partnerFromTime = v.updatedTime;
                    }
                });
            }

            var formData = {
                'fromTime': partnerFromTime
            };

            $.when(staffAPI.getUpdatedPartners(formData)).done(function (data) {
                localDB.partners.sortBy("displayPriority", false);
                createPartnerList();
            });
        } //getPartners

        /***
         * Creates the list of partners in the display partner grid
         ***/
        function createPartnerList() {
            perPage = localDB.partnerPerPage.value;
            currentPage = localDB.partnerCurrentPage.value;
            var partners = (localDB.partners).filter(function (el) {
                //return el['displayPriority'] > 0;
                return 1 == 1;
            });
            var searchedText = $.trim($('#search-text').val());
            if (searchedText != '') {
                var regex = new RegExp(searchedText, "i");
                partners = (partners).filter(function (el) {
                    return regex.test(el['name']);
                });
            }
            var paginateData = paginate(perPage, currentPage, partners.length);
            staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, partners.length);
            var staffPartnerHtml = "";
            $.each(partners.slice(paginateData.displayStart, paginateData.displayEnd), function (i, v) {
                staffPartnerHtml += '<tr data-pre="' + v.id + '">' + '<td class="message">' + v.name + '</td>' + '<td class="priority">' + v.description + '</td>' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></button>' + '</td>' + '</tr>';
            });
            //////<button data-pre="' + v.id + '" class="delete"></button>
            $('#result-table tbody').html(staffPartnerHtml);
        } //staffPartnerHtml

        /***
         *  Renders and displays edit partner form for the clicked partner
         * @param {int} partnerID 
         ***/
        function renderEditPartnerForm(partnerID) {
            $('form[name="edit-partner"]').find(':input').each(function () {
                $(this).val('');
            });
            var editForm = $("form[name='edit-partner']");
            editForm.find("label.error").text("");
            var partnerReference = localDB.partners.getByID(partnerID);

            // console.log(partnerReference);
            $('form[name="edit-partner"]').attr('data-pre', partnerID);
            $('form[name="edit-partner"] input[name="id"]').val(partnerID);
            $('#edit-partner-priority').val(partnerReference.displayPriority);
            $('#edit-partner-description').val(partnerReference.description);
            $('#edit-partner-name').val(partnerReference.name);
            $('#edit-partner-email').val(partnerReference.email);
            $('#edit-partner-website').val(partnerReference.websiteURL);
            $('#edit-partner-image-preview').attr("src", WWW_URL + "/img/partners/" + partnerID + ".png?time=" + $.now());
            $('#edit-partner-image-delete').css('display', 'inline-block');
            $('#edit-partner-image-url').val(partnerID + ".png");
            $('form[name="edit-partner"] input[name="layout"][value="' + partnerReference.layout + '"]').attr('checked', 'checked').trigger('click');
        } //renderEditpartnerForm

        return {
            init: init,
            createPartnerList: createPartnerList
        }; //return

    }(); //StaffPartner

}(window.app = window.app || {}, window.app.Module.Staff, jQuery));
window.app.Module.StaffPartner.init();
