/***
 * This Script incorporates following features:
 *-
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
(function(app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffCustomer = function() {
        var
            perPageCustomer,
            currentPageCustomer,
            country_elms = $("#edit-customer-residence-country, #edit-customer-home-country,#edit-customer-office-country"),
            custId,
            credits,
            itemsInPage,
            totalPage,
            allCustomers,
            customerReference // Will hold the reference to the customer which is edited
        ;

        /***
         * loading country list for the country combo box.
         ***/
        function loadCountry() {
            var countries = localDB.countries;
            var country_options = '<option value="">Select Country</option>';
            $.each(countries, function(i, v) {
                country_options += '<option value="' + v.id + '">' + v.name + '</option>';
            });
            country_elms.html(country_options);
            //$('select').selectric();
        } //loadCountry

        function isValidEmailAddress(emailAddress) {
            var pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
            return pattern.test(emailAddress);
        };

        /***
         * Displays error message on passed label element
         * @param {JQuery Selector} labelElm The element where error message is to be displayed
         * @param {string} errorMessage The error message that is to be displayed
         **/
        function showErrorMessage(labelElm, errorMessage) {
            labelElm.html(errorMessage).addClass('error');
            animateScroll(0, labelElm.parents("tr").offset().top);
            labelElm.prev("input").focus();
        } // showErrorMessage

        /*
         * @param {Integer} Country Id
         * @param {String} Country Element
         * Populates the Next State Dropdown to provided country dropdown Id
         */
        function populateStateDropdown(country_id, country_element) {
            var states = localDB.states;
            var state_options = '<option value="">Select State</option>';
            var state_elm = $("#" + $(country_element).attr("data-state-control"));
            $.each(states, function(i, v) {
                if (v.countryID == country_id) {
                    state_options += '<option value="' + v.id + '">' + v.name + '</option>';
                }
            });
            state_elm.html(state_options);
            $("select").selectric("refresh");
        }

        /**
         * Hides customer editable form and displays only add new form
         */
        function repositionContent(animate) {
            animate = typeof animate !== 'undefined' ? animate : true;
            $('#add-customer').addClass('active');
            $('#edit-customer, #referral-information, #referrals, #credits-customer').removeClass('active');
            $("#overlay").addClass("hide")
            if (animate) {
                animateScroll(0, 0);
            };
        } //repositionContent

        /***
         * Updates customers list from the server and updates localDB.customers,
         * then calls createCustomerList to render customer list on the table
         ***/
        function getCustomer() {
            var fromTime = staff.getLastUpdatedTime("customers", "localDB");
            var formData = {
                'fromTime': fromTime
            };
            $.when(staffAPI.getUpdatedCustomers(formData)).done(function(data) {
                createCustomerList();
            });
        } //getCustomer()

        /***
         * create customer list to display on table
         *
         * for the searching purpose,
         * filters by inputted text on 'name' column of table.
         ***/
        function createCustomerList() {
            perPageCustomer = localDB.customerPerPage.value;
            currentPageCustomer = localDB.customerCurrentPage.value;
            var customers = localDB.customers;
            var searchedText = $.trim($('#search-text').val());
            if (searchedText !== '') {
                var regex = new RegExp(searchedText, "i");
                customers = (customers).filter(function(el) {
                    return regex.test(el['email']);
                });
            }
            var paginateData = paginate(perPageCustomer, currentPageCustomer, customers.length);
            staff.generatePaginationHTML(currentPageCustomer, localDB.maxPageLinks, perPageCustomer, localDB.perPageArray, customers.length);
            $('#result-table tbody').html("");
            $.when(getAllCustomerSubscriptionStatus(customers)).done(function(customerMailSubcriptionStatus) {
                var staffCustomerHtml = "";
                var profession;
                var firstName;
                var lastName;
                $.each(customers.slice(paginateData.displayStart, paginateData.displayEnd), function(i, v) {
                    profession = (v.profession == null) ? "" : v.profession;
                    firstName = (v.firstName == null) ? "" : v.firstName;
                    lastName = (v.lastName == null) ? "" : v.lastName;
                    var customerName = firstName + ' ' + lastName;
                    // Show ban customer or un ban customer button
                    var bannedHtml;
                    if (v.banned === true) {
                        bannedHtml = '<button data-pre="' + v.id + '" class="unban-customer" title="Unban Customer"></button>';
                        customerName = customerName + " (banned)";
                    } else {
                        bannedHtml = '<button data-pre="' + v.id + '" class="ban-customer" title="Ban Customer"></button>';
                    }
                    // Show add customer to subscription list button or unsubscribe button
                    var subscribeHtml;
                    if (customerMailSubcriptionStatus[v.id]) {
                        subscribeHtml = '<button data-pre="' + v.id + '" class="add-mail-list-entry-btn" title="Add to mail list entry"></button>';
                    } else {
                        subscribeHtml = '<button data-pre="' + v.id + '" class="remove-mail-list-entry-btn" title="Remove from mail list entry"></button>';
                    }
                    staffCustomerHtml = '<tr>' + '<td class="email">' + v.email + '</td>' + '<td class="name">' + customerName + '</td>' + '<td class="proffession">' + profession + '</td>' + '<td class="action">' + '<button data-pre="' + v.id + '" class="edit"></button>' + bannedHtml + subscribeHtml + '</td>' + '</tr>';
                    $('#result-table tbody').append(staffCustomerHtml);
                });
            })
        } // createCustomerList()

        function getAllCustomerSubscriptionStatus(customers) {
            var d = $.Deferred();
            var totalRequest = customers.length;
            var customerMailSubcriptionStatus = {};
            $.each(customers, function(i, v) {
                $.when(staffAPI.getMailListSubscriptionStatus({
                    email: v.email,
                    list: "Customer"
                })).done(function(data) {
                    if (!data.subscribed) {
                        customerMailSubcriptionStatus[v.id] = true;
                    } else {
                        customerMailSubcriptionStatus[v.id] = false;
                    }
                    totalRequest--;
                    if (totalRequest == 0) {
                        d.resolve(customerMailSubcriptionStatus);
                    }
                });
            });
            return d;
        }

        /***
         * selecting particular customer record by ID
         * used to display fetched record on edit-form
         ***/
        function renderEditCustomer() {
            $('form[name="edit-customer"]').find("label.error").html("");
            customerReference = localDB.customers.getByID(custId);
            $('form[name="edit-customer"]').attr('data-pre', custId);
            $('form[name="edit-customer"] input[name="id"]').val(custId);
            $('#edit-customer-email').val(customerReference.email);
            $('#edit-customer-first-name').val(customerReference.firstName);
            $('#edit-customer-last-name').val(customerReference.lastName);
            $('#edit-customer-profession').val(customerReference.profession);
            $('#edit-customer-company-name').val(customerReference.company);
            var birth_date = new Date(customerReference.birthTime);
            var day = birth_date.getDate();
            day = (day <= 9) ? "0" + day : day;
            $("#edit-customer-dob-day").val(day);
            var month = birth_date.getMonth() + 1;
            month = (month <= 9) ? "0" + month : month;
            $("#edit-customer-dob-month").val(month);
            $("#edit-customer-dob-year").val(birth_date.getFullYear());
            $("#edit-customer-dob-year").selectric("refresh");

            $("#edit-customer-residence-country").val(customerReference.residenceCountryID);
            if (customerReference.residenceCountryID) {
                populateStateDropdown(customerReference.residenceCountryID, "#edit-customer-residence-country");
                $('#edit-customer-residence-state').val(customerReference.residenceStateID);
            }

            $('#edit-customer-mobile-number').val(customerReference.residencePhone);
            $('#edit-customer-home-number').val(customerReference.homePhone);
            $('#edit-customer-office-number').val(customerReference.officePhone);
            $("#edit-customer-home-country").val(customerReference.homeCountryID);
            if (customerReference.homeCountryID) {
                populateStateDropdown(customerReference.homeCountryID, "#edit-customer-home-country");
                $('#edit-customer-home-state').val(customerReference.homeStateID);
            }
            $('#edit-customer-home-postal-code').val(customerReference.homePostalCode);
            $('#edit-customer-home-address').val(customerReference.homeAddress);
            $("#edit-customer-office-country").val(customerReference.officeCountryID);
            if (customerReference.officeCountryID) {
                populateStateDropdown(customerReference.officeCountryID, "#edit-customer-office-country");
                $('#edit-customer-office-state').val(customerReference.officeStateID);
            }
            $('#edit-customer-office-postal-code').val(customerReference.officePostalCode);
            $('#edit-customer-office-address').val(customerReference.officeAddress);
            $('#manage-fitting-profile-btn').attr('data-pre', custId);
            $("#edit-customer-dob-day, #edit-customer-dob-month, #edit-customer-dob-year, #edit-customer-residence-state, #edit-customer-residence-country, #edit-customer-home-state, #edit-customer-home-country, #edit-customer-office-state, #edit-customer-office-country").selectric("refresh");
            $("#credits-change-number").val(customerReference.credits / 100)
        } //renderEditCustomer

        /**
         * Initialise various event listeners
         */
        function initEvent() {
            /*************************
             *    Search Customers    *
             *************************/
            $('#search-text').off('click keypress');
            $('#search-text').on('keypress', function(e) {
                e.stopPropagation();
                var code = e.keyCode || e.which;
                if (code == 13) {
                    var searchedText = $.trim($(this).val());
                    //if (searchedText != '') {
                    createCustomerList();
                    //}
                }
            });

            $('#search-button').off('click');
            $('#search-button').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#search-button'));
            $('#search-button').off("pointerclick");
            $('#search-button').on("pointerclick", function(e) {
                createCustomerList();
            });

            /***
             * Turns off click event for ban customer button
             ***/
            $("#result-table").off('click', '.ban-customer');
            $("#result-table").on('click', '.ban-customer', function(e) {
                e.preventDefault();
            });

            /***
             * Asks for confirmation before banning the customer (Yes|No)
             ***/
            listenForPointerClick($("#result-table"), '.ban-customer');
            $("#result-table").off("pointerclick", '.ban-customer');
            $("#result-table").on("pointerclick", '.ban-customer', function(e) {
                var element = this;
                var tabletd = $(element).closest('tr');
                var trId = $(element).data('pre');
                $(tabletd).after('<tr><td colspan="3" align="center">Confirm Ban Customer? ' +
                    '<span class="confirm-yes" data-pre="' + trId + '">Yes</span> / <span class="confirm-no">No</span><td></tr>');
                $(tabletd).addClass("hide");
            });

            /***
             * Turns of click event for 'confirm yes' button
             ***/
            $("#result-table").off('click', '.confirm-yes');
            $("#result-table").on('click', '.confirm-yes', function(e) {
                e.preventDefault();
            });

            /***
             * When 'confirm yes' is clicked,
             * Bans the customer
             ***/
            listenForPointerClick($("#result-table"), '.confirm-yes');
            $("#result-table").off("pointerclick", '.confirm-yes');
            $("#result-table").on("pointerclick", '.confirm-yes', function(e) {
                var element = $(this);
                var customerId = $(element).attr('data-pre');
                var formData = {
                    id: customerId
                };
                $.when(staffAPI.banCustomer(formData)).done(function() {
                    createCustomerList();
                    repositionContent();
                });
            });

            /**********************
             * Conformation No click
             ***********************/

            // when clicked the older or newer activity button
            $("#result-table").off('click', '.confirm-no');
            $("#result-table").on('click', '.confirm-no', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#result-table"), '.confirm-no');
            $("#result-table").off("pointerclick", '.confirm-no');
            $("#result-table").on("pointerclick", '.confirm-no', function(e) {
                var element = this;
                var tabletd = $(element).closest('tr');
                $((tabletd).prev('tr')).removeClass("hide");
                $(tabletd).remove();
            });

            /***
             * Turns off click event for 'un ban' button
             ***/
            $("#result-table").off("click", ".unban-customer");
            $("#result-table").on('click', '.unban-customer', function(e) {
                e.preventDefault();
            });

            /***
             * Unbans the customer
             ***/
            listenForPointerClick($("#result-table"), '.unban-customer');
            $("#result-table").off("pointerclick", '.unban-customer');
            $("#result-table").on("pointerclick", '.unban-customer', function(e) {
                var element = this;
                var customerId = $(element).attr('data-pre');
                var formData = {
                    id: customerId
                };
                $.when(staffAPI.unbanCustomer(formData)).done(function() {
                    createCustomerList();
                    repositionContent();
                });
            });

            listenForPointerClick($("#result-table"), ".add-mail-list-entry-btn");
            $("#result-table").off("pointerclick", ".add-mail-list-entry-btn");
            $("#result-table").on("pointerclick", ".add-mail-list-entry-btn", function(e) {
                var customerId = $(this).attr("data-pre");
                var customer = localDB.customers.getByID(customerId);
                var formData = {
                    email: customer.email,
                    list: "Customer"
                };
                $.when(staffAPI.addMailListEntry(formData)).done(function() {
                    createCustomerList();
                    repositionContent();
                });
            });

            listenForPointerClick($("#result-table"), ".remove-mail-list-entry-btn");
            $("#result-table").off("pointerclick", ".remove-mail-list-entry-btn");
            $("#result-table").on("pointerclick", ".remove-mail-list-entry-btn", function(e) {
                var customerId = $(this).attr("data-pre");
                var customer = localDB.customers.getByID(customerId);
                var formData = {
                    email: customer.email,
                    list: "Customer"
                };
                $.when(staffAPI.removeMailListEntry(formData)).done(function() {
                    createCustomerList();
                    repositionContent();
                });
            });

            /**********************
             * Edit Testimonial Event
             ***********************/

            // when clicked on edit button of each customer data.
            $("#result-table").off('click', '.edit');
            $("#result-table").on('click', '.edit', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#result-table"), '.edit');
            $("#result-table").off("pointerclick", '.edit');
            $("#result-table").on("pointerclick", '.edit', function(e) {
                var element = $(this);
                custId = $(element).data('pre');
                renderEditCustomer(); // Set the values for the form display 
                $('#add-customer').removeClass('active');
                // Referral Information
                var referrer = localDB.customers.getByID(customerReference.referrerID);
                if (referrer !== null) {
                    $("#referral-information-email").val(referrer.email);
                } else {
                    $("#referral-information-email").val("");
                }
                $("#credits-change-number").val(customerReference.credits / 100);
                $('#edit-customer, #referral-information, #referrals, #credits-customer').addClass('active');
                var position = $("#edit-customer").position();
                animateScroll(position.left, position.top);
            });

            // Undo Button Section
            $('.undo-btn').off('click');
            $('.undo-btn').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('.undo-btn'));
            $('.undo-btn').off("pointerclick");
            $('.undo-btn').on("pointerclick", function(e) {
                var element = $(this);
                var form = $(element).closest('form');
                if (form.attr('name') === 'edit-customer') {
                    var customerId = $(form).data('pre');
                    renderEditCustomer(customerId);
                    $('form[name="edit-customer"] label.errorTxt').html('');
                }
                // Edit Referrals 
                else if (form.attr('name') === 'referral-information') {
                    var referrer = localDB.customers.getByID(customerReference.referrerID);
                    if (referrer !== null) {
                        $("#referral-information-email").val(referrer.email);
                    } else {
                        $("#referral-information-email").val("");
                    }
                }
                // Credits 
                else if (form.attr('name') === 'credits-customer') {
                    $("#credits-change-number").val(customerReference.credits / 100);
                } else if (form.attr('name') == 'add-customer') {
                    form.find("input").val("");
                    form.find("label.error").html("");
                }
            });

            //Add section Submit
            $('form[name="add-customer"]').off('submit');
            $('form[name="add-customer"]').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).find("label.error").html("");
                // Email validation
                var customerEmail = $("#add-customer-email");
                if ($.trim(customerEmail.val()) == '') {
                    showErrorMessage(customerEmail.next('label'), "Email is required.");
                    return;
                }
                var emailExists = false;
                $.each(localDB.customers, function(i, customer) {
                    if (customerEmail.val() == customer.email) {
                        emailExists = true;
                        return false;
                    }
                });
                if (emailExists) {
                    showErrorMessage(customerEmail.next('label'), "Email already used.");
                    return;
                }
                // Password validation
                var customerPassword = $("#add-customer-password");
                var customerConfirmPassword = $("#add-customer-confirm-password");
                if (customerPassword.val() == '') {
                    showErrorMessage(customerPassword.next('label'), "Password is required.");
                    return;
                }
                if ($.trim(customerConfirmPassword.val()) == '') {
                    showErrorMessage(customerPassword.next('label'), "Confirm Password is required.");
                    return;
                }
                if (customerConfirmPassword.val().length < 6) {
                    showErrorMessage(customerPassword.next('label'), "Password must be at least 6 characters.");
                    return;
                }
                if (customerConfirmPassword.val() !== customerPassword.val()) {
                    showErrorMessage(customerPassword.next('label'), "Password does not match.");
                    return;
                }
                // First name validation
                var customerFirstName = $("#add-customer-first-name");
                if ($.trim(customerFirstName.val()) == '') {
                    showErrorMessage(customerFirstName.next("label"), "First Name is required.");
                    return;
                }
                var formData = $('form[name="add-customer"]').serializeFormObject();
                $.when(staffAPI.addCustomer(formData)).done(function() {
                    createCustomerList();
                    repositionContent();
                }).fail(function() {
                    createCustomerList();
                    repositionContent();
                });
            });
            // Referrals 
            $('form[name="referral-information"]').off('submit');
            $('form[name="referral-information"]').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).find("label.error").html("");
                var referralEmail = $("#referral-information-email").val();
                if ($.trim(referralEmail) === '') {
                    $("#referral-information-email").next('label').html("Referral email is required.").addClass('error');
                    return;
                }
                if (!isValidEmailAddress(referralEmail)) {
                    $("#referral-information-email").next('label').html("Invalid Email.").addClass('error');
                    return;
                }
                var referralId = getUserIdFromEmail(referralEmail);
                // If referrer email is same as the customer email
                if (referralId === custId) {
                    $("#referral-information-email").next('label').html("Invalid Email.").addClass('error');
                    return;
                }
                // If the email does not exists
                if (!referralId) {
                    $("#referral-information-email").next('label').html("This email does not exist.").addClass('error');
                    return;
                }
                var formData = {};
                formData.referrerID = referralId;
                formData.id = custId;
                $.when(staffAPI.editCustomerReferrerID(formData)).done(function() {
                    createCustomerList();
                    $('form[name="referrals"] label.errorTxt').html('');
                    $('form[name="add-customer"]').find("label.error").html("");
                    $('form[name="add-customer"]').find("input").val("");
                    repositionContent();
                    animateScroll(0, 0);
                }).fail(function() {
                    // Error data  
                });
            });
            // Credit section Submit
            $('form[name="credits-customer"]').off('submit');
            $('form[name="credits-customer"]').on('submit', function(e) {
                e.preventDefault();
                var creditChangeNumElm = $("#credits-change-number");
                var currentCredit = customerReference.credits;
                var creditChangeNum = creditChangeNumElm.val() * 100;
                if ($.trim(creditChangeNum) == '') {
                    $(creditChangeNumElm).next('label').html("Credit change value is required.").addClass('error');
                    return;
                }
                if (!$.isNumeric(creditChangeNum)) {
                    $(creditChangeNumElm).next('label').html("Not a number.").addClass('error');
                    return;
                }
                var formData = {
                    id: custId,
                    credits: creditChangeNum
                };
                $.when(staffAPI.editCustomerCredits(formData)).done(function() {
                    createCustomerList();
                    $('form[name="credits-customer"] label.errorTxt').html('');
                    repositionContent();
                });
            });
            //Edit section Submit
            $('form[name="edit-customer"]').off('submit');
            $('form[name="edit-customer"]').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();
                // Email validation
                var customerEmail = $("#edit-customer-email");
                if ($.trim(customerEmail.val()) == '') {
                    showErrorMessage(customerEmail.next('label'), "Email is required.");
                    return;
                }
                var emailExists = false;
                var customerID = $(this).attr("data-pre");
                $.each(localDB.customers, function(i, customer) {
                    if (customerEmail.val() === customer.email && customerID != customer.id) {
                        emailExists = true;
                        return false;
                    }
                });
                if (emailExists) {
                    showErrorMessage(customerEmail.next('label'), "Email already used.");
                    return;
                }
                // First name validation
                var customerFirstName = $("#edit-customer-first-name");
                if ($.trim(customerFirstName.val()) == '') {
                    showErrorMessage(customerFirstName.next("label"), "First Name is required.");
                    return;
                }
                // mobile no validation
                var mobileNo = $("#edit-customer-mobile-number").val();
                if (!$.isNumeric(mobileNo) && mobileNo != "") {
                    showErrorMessage($("#edit-customer-mobile-number").next('label'), "Mobile no can contain only numbers.");
                    return;
                }
                // home no validation
                var homeNo = $("#edit-customer-home-number").val();
                if (!$.isNumeric(homeNo) && homeNo != "") {
                    showErrorMessage($("#edit-customer-home-number").next('label'), "Home no can contain only numbers.");
                    return;
                }
                // office no validation
                var officeNo = $("#edit-customer-office-number").val();
                if (!$.isNumeric(officeNo) && officeNo != "") {
                    showErrorMessage($("#edit-customer-office-number").next('label'), "Office no can contain only numbers.");
                    return;
                }
                var formData = $('form[name="edit-customer"]').serializeArray();
                var birth_date = $("#edit-customer-dob-year").val() + "-" + $("#edit-customer-dob-month").val() + "-" + $("#edit-customer-dob-day").val();
                var birthTime = new Date(birth_date).getTime();
                formData.push({
                    name: "birthTime",
                    value: birthTime
                });
                $.when(staffAPI.editCustomerProfile(formData)).done(function() {
                    createCustomerList();
                    repositionContent();
                    $("#edit-customer").removeClass("active");
                }).fail(function() {
                    createCustomerList();
                    repositionContent();
                    $("#edit-customer").removeClass("active");
                });
            });

            /*****************************
             * required validation for #add-customer-first-name
             ******************************/
            $('#add-customer-first-name, #edit-customer-first-name').off('input');
            $('#add-customer-first-name, #edit-customer-first-name').on('input', function(e) {
                var customerFirstName = $(this);
                if ($.trim(customerFirstName.val()) == '') {
                    $(customerFirstName).next('label').html("First Name is required.").addClass('error');
                    return;
                }
                $(customerFirstName).next('label').html("");
            });

            /*****************************
             * required validation for #add-customer-email
             ******************************/
            $('#add-customer-email, #edit-customer-email').off('input');
            $('#add-customer-email, #edit-customer-email').on('input', function(e) {
                var customerEmail = $(this);
                if ($.trim(customerEmail.val()) == '') {
                    $(customerEmail).next('label').html("Email is required.").addClass('error');
                    return;
                }
                if (!isValidEmail(customerEmail.val())) {
                    $(customerEmail).next('label').html("Invalid email.").addClass("error");
                    return;
                }
                $.each(localDB.customers, function(i, v) {
                    if (v.email == customerEmail) {
                        $(customerEmail).next('label').html("Email is already used.").addClass('error');
                        return;
                    }

                });
                $(customerEmail).next('label').html("");
            });

            /*****************************
             * required validation for #add-customer-password
             ******************************/
            $('#add-customer-password, #edit-customer-password').off('input');
            $('#add-customer-password, #edit-customer-password').on('input', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var customerPassword = $(this);
                if ($.trim(customerPassword.val()) == '') {
                    $(customerPassword).next('label').html("Password is required and Password must be at least 6 characters.").addClass('error');
                    return;
                }
                if ($.trim(customerPassword.val()).length < 6) {
                    $(customerPassword).next('label').html("Password must be at least 6 characters.").addClass('error');
                    return;
                }
                $(customerPassword).next('label').html("");
            });
            /*****************************
             * required validation for #edit-customer-home-number, #edit-customer-office-number, #edit-customer-mobile-number
             ******************************/
            $("#edit-customer-home-number, #edit-customer-office-number, #edit-customer-mobile-number").off("input");
            $("#edit-customer-home-number, #edit-customer-office-number, #edit-customer-mobile-number").on("input", function(e) {
                if ($(this).val() !== '' && !$.isNumeric($(this).val())) {
                    $($(this)).next('label').html("Mobile No must can contain numbers only.").addClass('error');
                    return;
                }
                $($(this)).next('label').html("");
            });

            /*
             * Change Credit Listing on hashchange
             */
            $(window).on("hashchange", function() {
                fillCreditListing(location.hash.slice(1));
            });

            //Return to Logout Button triggers 
            $('#back-to-top-button').off('click');
            $('#back-to-top-button').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#back-to-top-button'));
            $('#back-to-top-button').off("pointerclick");
            $('#back-to-top-button').on("pointerclick", function(e) {
                animateScroll(0, 0);
            });

            /*
             * Attaches event handlers on country dropdowns to change next state dropdowns
             */
            country_elms.on("change", function() {
                var country_id = $("option:selected", this).val();
                populateStateDropdown(country_id, this);
            });

            /***
             * on clicking manage fitting profile button on edit section
             ***/
            $('.manage-fitting-profile-btn').off('click');
            $('.manage-fitting-profile-btn').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('.manage-fitting-profile-btn'));
            $('.manage-fitting-profile-btn').off("pointerclick");
            $('.manage-fitting-profile-btn').on("pointerclick", function(e) {
                var target_page = $(this).data('for');
                var customerID = $(this).data('pre');
                sessionDB.tempCustomerID = customerID;
                sessionDB.save('tempCustomerID');
                $.when(switchPage(target_page)).done(function() {
                    // Remember history (state, title, URL)
                    window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                });
            });

            /***
             * Turns off clicke event for 'view credit history' button
             ***/
            $('#view-credit-history-btn').off('click');
            $('#view-credit-history-btn').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#view-credit-history-btn'));
            $('#view-credit-history-btn').off("pointerclick");
            $('#view-credit-history-btn').on("pointerclick", function(e) {
                showCreditListing();
            });
        } // initEvent

        /***
         * Displays error message on passed label element
         * @param {JQuery Selector} labelElm The element where error message is to be displayed
         * @param {string} errorMessage The error message that is to be displayed
         **/
        function showErrorMessage(labelElm, errorMessage) {
            labelElm.html(errorMessage).addClass('error');
            labelElm.prev("input").focus();
        } // showErrorMessage

        /***
         * Gets the customer id from the email provided
         * @param {string} email The email address of the customer
         * @returns {int} customerId Customer id from the customer email
         ***/
        function getUserIdFromEmail(email) {
            var customerId = false;
            var allCustomers = localDB.customers.sortBy("email", false);
            $(allCustomers).each(function(i, customer) {
                if (email == customer.email) {
                    customerId = customer.id;
                    return false;
                }
            });
            return customerId;
        }

        /***
         * Fill the Credit Listing table with the data, provided the page number
         * @param {Integer} page_no
         ***/
        function fillCreditListing(page_no) {
            $(".pagination").find("a").removeClass("active");
            $(".pagination").find("a[data-for='" + page_no + "']").addClass("active");
            $("#credits-listing tbody").find("tr").remove();
            var startIndex = itemsInPage * (page_no - 1);
            var endIndex = startIndex + itemsInPage - 1;
            var creditsInPage = (credits).filter(function(e, i) {
                return i >= startIndex && i <= endIndex;
            });
            $(creditsInPage).each(function(i, v) {
                var date = new Date(v.createdTime);
                var added = "-";
                var used = "-";
                if (v.amount >= 0) {
                    added = "$" + v.amount;
                } else {
                    used = "$" + (-1) * (v.amount);
                }
                $("#credits-listing tbody").append(
                    $("<tr>").append(
                        $("<td>", {
                            text: date.format("d/m/Y")
                        }),
                        $("<td>", {
                            text: v.description
                        }),
                        $("<td>", {
                            text: added
                        }),
                        $("<td>", {
                            text: used
                        })
                    )
                );
            });
        } //fillCreditListing()

        /***
         * Renders credit listing for current edit activated customer
         ***/
        function showCreditListing() {
            credits = (sessionDB.credits).filter(function(el) {
                return ((el["customerID"] == parseInt(custId)));
            });
            var customerHistroyData = localDB.customers.getByID(custId);
            for (var i = 1; i <= totalPage; i++) {
                $(".pagination").append($("<a />", {
                    "text": i,
                    "href": "#" + i,
                    "data-for": i
                }));
            }
            var pageNo = location.hash.slice(1) == '' ? 1 : location.hash.slice(1);
            fillCreditListing(pageNo);
            var balance = calculateTotalBalance();
            var creditOwnerName = customerHistroyData.firstName + " " + customerHistroyData.lastName;
            $("#credit-owner-name").text(creditOwnerName);
            $("#credit-balance").text("$" + balance);

        } // showCreditListing()

        function calculateTotalBalance() {
            var balance = 0;
            $(credits).each(function(i, v) {
                balance = balance + v.amount;
            });
            return balance;
        } // calculateTotalBalance()

        /***
         * On first load of staff-customer.
         * Checkes user session
         * loads constant header footer display.
         * fetches customer records and stores on LocalDB or SessionDB.
         * reposition the Edit section.
         ***/
        function initUI() {
            staff.checkUserSession('staff-account-settings');
            staff.headerFooterDisplay();
            $("#result-section-customer").attr("data-callbackUrl", "staff-order-history-customer");
            $("#result-section-customer").attr("data-perpagevariable", "customerPerPage");
            $("#result-section-customer").attr("data-currentpagevariable", "customerCurrentPage");
            getCustomer();
            repositionContent();
            loadCountry();
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);

        } //initUI

        function init() {
            $("#result-section").attr("data-perpagevariable", "customerPerPage");
            $("#result-section").attr("data-currentpagevariable", "customerCurrentPage");
            initUI();
            initEvent();
            credits = sessionDB.credits;
            itemsInPage = 5;
            totalPage = Math.ceil((credits.length) / itemsInPage);
        } //init

        return {
            init: init,
            createCustomerList: createCustomerList
        } //return

    }(); //StaffCustomer
}(window.app = window.app || {}, window.app.Module.Staff, jQuery));

window.app.Module.StaffCustomer.init();