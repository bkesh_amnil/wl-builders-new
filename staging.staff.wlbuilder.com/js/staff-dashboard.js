/***
 * Module - app.Module.StaffDashboard
 *
 * This Script incorporates following features:
 *
 * Dependencies :
 *  - jQuery
 *  - db.js
 *
 * Before use:
 *  - ensure ajax-customer-api is init
 *  - ensure localDB & sessionDB are init
 *
 * Init instructions:
 *  - app.Module.StaffDashboard.init();
 *
 * Usage examples:
 * - none
 ***/

(function(app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};
    App.Module.StaffDashboard = function() {
        var
            staffActivitiesList;

        /***
         * Removes all events: click, input, focus, pointerclick
         ***/
        function resetEvent() {

        }

        /***
         * Adds all necessary events: input, focus, pointerclick
         ***/
        function initEvent() {
            /**********************
             * For Dashboard Sub Navigation block
             ***********************/
            $('.menu li a').off('click');
            $('.menu li a').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('.menu li a'));
            $('.menu li a').off("pointerclick");
            $('.menu li a').on("pointerclick", function(e) {
                var element = this;
                var target_page = $(this).data('for');
                $.when(switchPage(target_page)).done(function() {
                    // Remember history (state, title, URL)
                    window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                });
            });

            /**********************
             * For Model Layout DragDrops
             ***********************/

            // when clicked the older or newer activity button
            $('#change-module-layout').off('click');
            $('#change-module-layout').on('click', function(e) {
                e.preventDefault();
            });

            // Bind change layout button
            listenForPointerClick($('#change-layout-button'));
            $('#change-layout-button').off("pointerclick");
            $('#change-layout-button').on("pointerclick", function(e) {
                $('#overlay').addClass("active");
                animateScroll(0, $('#content').offset().top);
                initOverlayModules();
            });

            // Bind event: activity nav
            listenForPointerClick($("#newer-log-button"));
            $("#newer-log-button").off("pointerclick");
            $("#newer-log-button").on("pointerclick", function() {
                var page = $(this).data('page');
                createStaffActivityList(page);
            });

            // Bind event: activity nav
            listenForPointerClick($("#older-log-button"));
            $("#older-log-button").off("pointerclick");
            $("#older-log-button").on("pointerclick", function() {
                var page = $(this).data('page');
                createStaffActivityList(page);
            });
        }

        /***
         * Initializes UI
         ***/
        function initUI() {
            //check for the user session to switch the page to dashboard if logged in.
            resizeOverlay();
            $(window).off('resize.main');
            $(window).on('resize.main', resizeOverlay);
            staff.headerFooterDisplay();
            //initially get all the activities for the DB according to update date.
            getActivities();
            positionModelLayout();

            initPage();
        }

        // To be called once app is ready
        function initPage() {
            // Make sure each module defined in HTML is in module layout
            $('#modules > .module').each(function(i, v) {
                var name = $(this).data('name');
                if (!localDB.staffModuleLayout.contains(name)) {
                    localDB.staffModuleLayout.push(name);
                }
            });
            localDB.save('staffModuleLayout');

            // Pre-load overlay
            repositionContent();

            // Check if overlay is being requested in the URL
            if (window.location.hash == "#overlay") {
                $('#overlay').addClass("active");
                animateScroll(0, $('#content').offset().top);
                initOverlayModules();
            }

            // Ignore clicks on overlay
            $('#overlay').off('click');

            $('#modules > .module').off("click");
            $('#modules > .module').on("click", function(e) {
                e.preventDefault();
            });
            // Bind modules event: switch page
            listenForPointerClick($('#modules > .module'));
            $('#modules > .module').off("pointerclick");
            $('#modules > .module').on("pointerclick", function(e) {
                var target_page = $(this).data('for');
                $.when(switchPage(target_page)).done(function() {
                    // Remember history (state, title, URL)
                    window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                });
            });
        } //initPage()

        /***
         *
         ***/
        function resizeOverlay() {
            var heightContent = $('#content').innerHeight();
            $("#overlay").height(heightContent);
        }

        function positionModelLayout() {
            $("#overlay").removeClass("active");
            if (localDB.layoutPattern != null) {
                var layoutPattern = localDB.layoutPattern;
                var layoutSortList = $('#dashboardMenuBlock > ul li');
                $.each(layoutPattern, function(i, v) {
                    $.each(layoutSortList, function(index, value) {

                        if (value.dataset.sname == v) {
                            $(value).attr('data-sort', i + 1);
                        }
                    });
                });
                sortLayoutList();
            }
        }

        function initOverlayModules() {
            // Arrange overlay modules according to localDB.staffModuleLayout
            $('#overlay-modules').html('');
            var len = localDB.staffModuleLayout.length;
            for (var i = 0; i < len; i++) {
                var name = localDB.staffModuleLayout[i];
                $('#modules > .module').each(function(j, v) {
                    if ($(v).data('name') == name) {
                        $('#overlay-modules').append($(v).clone());
                    }
                });
            }
            // Make last child odd if necessary
            $('#overlay-modules > .module').removeClass('last-odd');
            if ($('#overlay-modules > .module').length % 2 == 1) {
                $('#overlay-modules > .module:last-of-type').addClass('last-odd');
            }

            // Prevent clicks on module from redirecting
            $('#overlay-modules > .module').off('click');
            $('#overlay-modules > .module').on('click', function(e) {
                e.preventDefault();
            });

            // Bind drag
            $('#overlay-modules > .module').off('dragstart');
            $('#overlay-modules > .module').on('dragstart', function(e) {
                e.stopPropagation();

                var name = $(this).data('name');
                var index1 = localDB.staffModuleLayout.indexOf(name);
                $(this).addClass('drag-target');

                $(this).off('dragend');
                $(this).on('dragend', function(e) {
                    $(this).removeClass('drag-target');
                });

                // Define drag event handlers: dragenter, dragleave, dragover, drop
                var dragEnterHandler = function(ev) {
                    var target = this;
                    $(target).addClass('drop-target');
                };
                var dragLeaveHander = function(ev2) {
                    var target = this;
                    $(target).removeClass('drop-target');
                };
                var dragOverHandler = function(ev3) {
                    ev3.preventDefault(); // need to cancel this event to enable firing of drop event
                };
                var dropHandler = function(ev4) {
                    ev4.preventDefault(); // need to do this to prevent anchors from redirecting

                    // Identify target
                    var $target = $(this);
                    $target.removeClass('drop-target');
                    var target_name = $target.data('name');
                    var index2 = localDB.staffModuleLayout.indexOf(target_name);

                    // Swap places
                    var tmp = localDB.staffModuleLayout[index1];
                    localDB.staffModuleLayout[index1] = localDB.staffModuleLayout[index2];
                    localDB.staffModuleLayout[index2] = tmp;

                    initOverlayModules();
                };

                // Bind events
                var modules = document.querySelectorAll('#overlay-modules > .module');
                var len = modules.length;
                for (var i = 0; i < len; i++) {
                    modules[i].removeEventListener('dragenter', dragEnterHandler, false);
                    modules[i].addEventListener('dragenter', dragEnterHandler, false);

                    modules[i].removeEventListener('dragleave', dragLeaveHander, false);
                    modules[i].addEventListener('dragleave', dragLeaveHander, false);

                    modules[i].removeEventListener('dragover', dragOverHandler, false);
                    modules[i].addEventListener('dragover', dragOverHandler, false);

                    modules[i].removeEventListener('drop', dropHandler, false);
                    modules[i].addEventListener('drop', dropHandler, false);
                }
            });


            $("#clear-layout-changes-button").off("click");
            $("#clear-layout-changes-button").on("click", function(e) {
                e.preventDefault();
            });
            listenForPointerClick($('#clear-layout-changes-button'));
            $('#clear-layout-changes-button').off("pointerclick");
            // Bind clear button
            $('#clear-layout-changes-button').on("pointerclick", function(e) {
                localDB.revert('staffModuleLayout');
                // Go back to main dashboard
                $('#overlay').removeClass("active")
                repositionContent();
                animateScroll(0, $('#content').offset().top);
                window.location.hash = '';
            });

            $('#save-layout-button').off("click");
            $('#save-layout-button').on("click", function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#save-layout-button'));
            $('#save-layout-button').off("pointerclick");
            // Bind save button
            $('#save-layout-button').on("pointerclick", function(e) {
                // Save new layout to localDB
                var newModuleLayout = [];
                $('#overlay-modules > .module').each(function(i, v) {
                    newModuleLayout[i] = $(v).data('name');
                });
                localDB.staffModuleLayout = newModuleLayout;
                localDB.save('staffModuleLayout');

                // Go back to main dashboard
                $('#overlay').removeClass("active");
                repositionContent();
                animateScroll(0, $('#content').offset().top);
                window.location.hash = '';
            });
        } //initOverlayModules()

        function sortLayoutList() {
            var checksd = $('#dashboardMenuBlock > ul li').sort(function(a, b) {
                return +a.dataset.sort - +b.dataset.sort;
                //		return a.id > b.id;
            }).appendTo('#dashboardMenuBlock > ul');

        }

        /***
         * get the Activities of the Staffs
         * get all the activities and load to the localDB.staffActivities.
         ***/
        function getActivities() {
            var staffActivityFromTime = 0;
            var staffsFromTime = 0;
            if (localDB.staffActivity != null) {
                var staffActivity = localDB.staffActivity;
                $.each(staffActivity, function(i, v) {
                    if (v.createdTime > staffActivityFromTime) {
                        staffActivityFromTime = v.createdTime;
                    }
                });
            }

            if (localDB.staffs != null) {
                var staffs = localDB.staffs;
                if (staffs) {
                    $.each(staffs, function(i, v) {
                        if (v.lastUpdateTime > staffsFromTime) {
                            staffsFromTime = v.lastUpdateTime;
                        }
                    });
                }
            }

            $.when(staffAPI.getStaffActivities({
                'fromTime': staffActivityFromTime
            }), staffAPI.getUpdatedStaffs({
                'fromTime': staffsFromTime
            })).done(function(data) {
                localDB.staffActivities = localDB.staffActivities.reverse();
                staffActivitiesList = localDB.staffActivities;
                createStaffActivityList(0);
            });
        }

        /***
         * Reposition modules according to module layout
         * Also ensures that activity log and overlay aligns to the modules' total height
         ***/
        function repositionContent() {
            // Re-order modules to the prescribed layout
            var $button = $('#change-layout-button').detach(); // button to be appended last
            $.each(localDB.staffModuleLayout, function(i, v) {
                $('#modules > .module').each(function() {
                    if ($(this).data('name') == v) {
                        $(this).detach().appendTo('#modules');
                    }
                });

                // Make last child odd if necessary
                $('#modules > .module').removeClass('last-odd');
                if ($('#modules > .module').length % 2 == 1) {
                    $('#modules > .module:last-of-type').addClass('last-odd');
                }
            });
            $('#modules').append($button);

            // Ensure height for activity log is aligned to modules height
            var modules_height = $('#modules').innerHeight() - parseInt($('.module').css('margin-bottom')) - $button.innerHeight();
            var activity_header_height = $('#activity-log > h2').innerHeight();
            var activity_height = parseInt($('.activity').innerHeight());
            var activities_per_page = Math.floor((modules_height - activity_header_height) / activity_height);
            $('#activity-log').css('height', modules_height + 'px');
            $('#activity-log').data('activities-per-page', activities_per_page);
            //TEST: console.log(modules_height + ', ' + activity_header_height + ', ' + activity_height + ', ' + activities_per_page);

            // Ensure overlay is aligned to modules height
            $('#save-layout-panel').css('height', modules_height + 'px');
            $('#overlay-modules').css('height', modules_height + 'px');
        } //repositionContent()

        /********************
         * create staff Activity list
         * @param int pageCount (pageCount to view the section of the activities)
         * For pagination newer and older activity log.
         ********************/
        function createStaffActivityList(current_page) {
            // Find the activities to show on this page
           
            var activities_per_page = $('#activity-log').data('activities-per-page');
            
            var start = activities_per_page * current_page;
            var end = start + activities_per_page;
            if (end > localDB.staffActivities.length) {
                end = localDB.staffActivities.length;
            }
            var activities = localDB.staffActivities.slice(start, end);
            
            // Form HTML
            var html = '<h2>Activity log</h2>';
            $.each(activities, function(i, v) {
                if (v.userID == 0) {
                    return;
                }
                var date = timestamp2date(parseInt(v.createdTime)).format('d/m/Y, h:ia');
//                var date = v.createdTime;
                var email = localDB.staffs.getByID(v.userID).email;
                html += '' +
                    '<article class="activity">' +
                    '<a class="user-email" href="mailto:' + email + '">' + email + '</a>' +
                    '<time>' + date + '</time>' +
                    '<div class="action">' + v.action + '</div>' +
                    '</article>';
            });
            $('#activity-log').html(html);

            // Set next pages
            $('#activity-log').data('page', current_page);
            $('#newer-log-button').data('page', (current_page - 1));
            $('#older-log-button').data('page', (current_page + 1));

            // Check if we're already at newest page
            if (current_page == 0) {
                $('#newer-log-button').data('page', 0);
                $('#newer-log-button').attr('disabled', 'disabled');
            } else {
                $('#newer-log-button').removeAttr('disabled');
            }

            // Check if we're already at oldest page
            if (end == localDB.staffActivities.length) {
                $('#older-log-button').data('page', current_page);
                $('#older-log-button').attr('disabled', 'disabled');

                // Check if we already have the very first activity
                var adam = localDB.staffActivities.getByID(1);
                if (adam == null) {
                    //return fetchEarlierActivities(STAFF_ACTIVITY_FETCH_WINDOW_PERIOD);
                }
                //DEBUG: console.log('End of activities');
            } else {
                $('#older-log-button').removeAttr('disabled');
            }

        }

     
   

        /***
         * Initialize Module
         ***/
        function init() {
            $.when(staff.checkUserSession('staff-dashboard')).done(function() {
                resetEvent();
                initEvent();
                initUI();
            });
        }
        return {
            init: init
        }
    }();
}(window.app = window.app || {}, window.app.Module.Staff, jQuery))

window.app.Module.StaffDashboard.init();