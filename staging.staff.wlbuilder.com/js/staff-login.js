(function(app, staffModule, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffLogin = function() {
        function initEvent() {
            //Section for staff login button click (start)
            //return the login form display from the form.
            //unhide the form.
            
            var valid_email = 0;
            var valid_forgot_pass_email = 0;
            var valid_password = 0;

            $('#start-button').off('click');
            $('#start-button').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#start-button'));
            $('#start-button').off("pointerclick");
            $('#start-button').on("pointerclick", function(e) {
                var element = $(this);
                $('#login-form').removeClass('hide').addClass('show');
                $('#login-overlay').addClass("active");
                $("#login-email").focus();
                $('#login-form').find('output').removeClass('error').text('');
                //IF cookie set display email address
                if (localDB.cookie != null) {                    
                    $('#login-email').val(localDB.cookie.email);
                }else{        
                    $('#login-email').val('');
                    $('#login-password').val('');
                    $('.login_button').attr('disabled','disabled');
                    
                }
                element.removeClass('show');
                element.addClass('hide');
            });


            /*******
             * Staff Login in page behaviours
             ********/

            // Email address validation for the login in page. check if is valid
            $('#login-email').off('change , blur');
            $('#login-email').on('change , blur', function(event) {
                event.stopPropagation();
                //event.preventDefault();
                var element = $(this);
                var email = $.trim(element.val());
//                var label = element.prev('label');

                if (email === "") {
//                    var label = element.prev('label');
//                    element.addClass('error');
//                    label.addClass('error');
//                    label.html("*Email is required field");
                    $('.login_button').attr('disabled','disabled');
                    return;
                }

                if (!isValidEmail(email)) {
//                    var label = element.prev('label');
                    element.addClass('error');
//                    label.addClass('error');
//                    label.html("Invalid Email Address");
                    $('.login_button').attr('disabled','disabled');
                    return;
                }

                element.removeClass('error');
//                label.removeClass('error');
//                label.html("Email");


                //#now check for password
                var passwordElement = $('#login-password');
                var passwordvalue = passwordElement.val();
                if (passwordvalue === "") {
                    valid_password = 0;
                }
                
                valid_email = 1;
                
                if(valid_email===1 && valid_password ===1){
                    $('.login_button').removeAttr('disabled');
                }else {
                    $('.login_button').attr('disabled','disabled');
                }

            });


            //Password address validation for the login page check if is not empty
            $('#login-password').off('change , blur, keyup');
            $('#login-password').on('change , blur, keyup', function(event) {
                event.stopPropagation();
               // event.preventDefault();
                var element = $(this);
//                var label = element.prev('label');
                var passwordvalue = $(this).val();

                if (passwordvalue === "") {
//                    element.addClass('error');
//                    label.addClass('error');
//                    label.html("*Password is required.");
                     $('.login_button').attr('disabled','disabled');
                    return;
                }
                if (passwordvalue.length < 8) {
                    element.addClass('error');
//                    label.addClass('error');
//                    label.html("*Password is required.");
                     $('.login_button').attr('disabled','disabled');
                    return;
                }

                element.removeClass('error');
//                label.removeClass('error');
//                label.html("Password");


                //#now check for email 
                var emailElement = $('#login-email');
                var email = $.trim(emailElement.val());
                if (email === "") {
                    valid_email = 0;
                }
                if (!isValidEmail(email)) {
                    valid_email = 0;
                }
                
                valid_password = 1;

                if(valid_email===1 && valid_password ===1){
                    $('.login_button').removeAttr('disabled');
                }else {
                    $('.login_button').attr('disabled','disabled');
                }
            });

            

            //submit form event for the staff-log-in

            $('#login-form').off('submit');
            $('#login-form').on('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();

                //check email validation
                var emailElement = $('#login-email');
                var email = $.trim(emailElement.val());

                if (email === "") {
//                    var label = emailElement.prev('label');
                    emailElement.addClass('error');
//                    label.addClass('error');
//                    label.html("*Email is required field");
                    return;

                }

                if (!isValidEmail(email)) {
//                    var label = emailElement.prev('label');
                    emailElement.addClass('error');
//                    label.addClass('error');
//                    label.html("Invalid Email Address");
                    return;
                }



                var passwordElement = $('#login-password');
                var password = $.trim(passwordElement.val());

                if (password === "") {
//                    var label = passwordElement.prev('label');
                    passwordElement.addClass('error');
//                    label.addClass('error');
//                    label.html("*Password is required");
                    return;
                }
                var keepLoggedIn = 0;
                if ($("#login-persistent").prop("checked")) {
                    keepLoggedIn = 1;
                }
                var formData = {
                    email: email,
                    password: password,
                    persistent: keepLoggedIn
                };
                //return;
                $.when(staffAPI.login(formData)).done(function(data) {
                    //if the user is set in session switch the page to dashboard.
                    if (sessionDB.user.id) {
                        var target_page = "staff-dashboard";
                        $("#login-overlay").removeClass("active");
                        $.when(switchPage(target_page)).done(function(data) {
                            window.history.pushState('staffLogin', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
                        });
                    }
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    $('#login-form').find('output').addClass('error').text(responseTxt.error);
//                    var label = passwordElement.prev('label');
//                    passwordElement.addClass('error');
//                    label.addClass('error');
//                    label.html(responseTxt.error);
//                    $('.login_button').attr('disabled','disabled');
                    return;
                });
            });




            //forgot password section 
            //unhide the forgot-password-form and hide the login form

            $('#forgot-password-link').off('click');
            $('#forgot-password-link').on('click', function(e) {
                e.preventDefault();
                e.stopPropagation();
            });

            listenForPointerClick($('#forgot-password-link'));
            $('#forgot-password-link').off("pointerclick");
            $('#forgot-password-link').on("pointerclick", function(e) {
                $('#login-form').removeClass('show').addClass('hide');
                $('#forgot-password-form').removeClass('hide').addClass('show');
                $('.forgot_pass_button').attr('disabled','disabled');
            });




            //Return to Login Button triggers 
            //unhide the forgot-password-form and hide the login form

            $('#login-link').off('click');
            $('#login-link').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#login-link'));
            $('#login-link').off("pointerclick");
            $('#login-link').on("pointerclick", function(e) {
                $('#login-form').removeClass('hide').addClass('show');
                $('#forgot-password-form').removeClass('show').addClass('hide');
            });

            $('#forgotten-email').off('blur , change');
            $('#forgotten-email').on('blur , change', function(event) {
                event.stopPropagation();
                var element = $(this);
                var email = $.trim(element.val());
//                var label = element.prev('label');

                if (email === "") {
                    element.addClass('error');
//                    label.addClass('error');
//                    label.html("*Email is required field");
                    $('.forgot_pass_button').attr('disabled','disabled');

                    return;
                }

                if (!isValidEmail(email)) {
                    element.addClass('error');
//                    label.addClass('error');
//                    label.html("Invalid Email Address");
//                    var error = "Invalid Email Address";
                    $('.forgot_pass_button').attr('disabled','disabled');
                    return;
                }
                valid_forgot_pass_email = 1;
                
                if(valid_forgot_pass_email===1){
                    $('.forgot_pass_button').removeAttr('disabled');
                }else {
                    $('.forgot_pass_button').attr('disabled','disabled');
                }


                element.removeClass('error');
//                label.removeClass('error');
//                label.html("Email");
            });



            //submit form event for forgotpassword of the staff-log-in
            $('#forgot-password-form').off('submit');
            $('#forgot-password-form').on('submit', function(event) {
                event.preventDefault();
                event.stopPropagation();
                var element = $('#forgotten-email');
                var label = element.prev('label');
                var email = $.trim(element.val());
                if (email === "") {
//                    var label = element.prev('label');
                    element.addClass('error');
//                    label.addClass('error');
//                    label.html("*Email is required field");
                    return;

                }

                if (!isValidEmail(email)) {
//                    var label = element.prev('label');
                    element.addClass('error');
//                    label.addClass('error');
//                    label.html("Invalid Email Address");
//                    var error = "Invalid Email Address";
                    return;
                }


                var formData = ($('#forgot-password-form').serialize());
                $.when(staffAPI.ResetPassword(formData)).done(function() {
                    $('#forgot-password-form input[name="email"]').val("");
                    $('#forgot-password-message').html("Your new password has been sent. Remember to have it changed once your are logged in.");
                    element.removeClass('error');
                    label.removeClass('error');
                    label.html('');
                    return;
                }).fail(function(data) {
                    var responseTxt = $.parseJSON(data.responseText);
                    var responseTxt = $.parseJSON(data.responseText);
                    $('#login-form').find('output').addClass('error').text(responseTxt.error);
//                    $(label).html(responseTxt.error);
                });

            });



            /*****************************
             *Click other than form elements for all the pages.
             ******************************/

            $('#login-overlay').off('click');
            $('#login-overlay').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#login-overlay'));
            $('#login-overlay').off("pointerclick");
            $('#login-overlay').on("pointerclick", function(e, arg) {
                var targetElementId = arg.pointerEvent.target.id;
                if ($(arg.pointerEvent.target).parents("#login-form, #forgot-password-form").length > 0 || targetElementId == "login-form" || targetElementId == "forgot-password-form") {
                    return;
                }
                var element = $(this);
                $(element).removeClass("active");
                $('#forgot-password-form, #login-form').removeClass('show').addClass('hide');
                $("#start-button").removeClass('hide').addClass('show');
            });

            // Bind overlay events: click outside forms to close overlay
            $('.overlay-form').off('click');
            $('.overlay-form').on('click', function(e) {
                e.stopPropagation(); //don't propagate click to overlay
            });
        }

        function init() {
            //check for the user session to switch the page to dashboard if logged in.
            staffModule.checkUserSession('staff-login');

            staffModule.headerFooterDisplay();

            repositionContent();
            $(window).off('resize.main');
            $(window).on('resize.main', repositionContent);
            //initPage();
            //initNavLinks();
            //animateInPage();

            repositionContent();
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);

            initEvent();
        }
        /***
         * Positions any content element
         ***/
        function repositionContent() {
            var headerHeight = $('header').innerHeight();
            var footerHeight = $('footer').innerHeight();

            // Set content dimensions
            var contentMinHeight = Math.max(window.innerHeight, window.minHeight) - headerHeight - footerHeight;

            $('#content').css({
                'min-width': window.minWidth + 'px',
                'min-height': contentMinHeight + 'px'
            });

            // $('#tagline').css({
            //                'min-width': window.minWidth + 'px',
            //                'min-height': contentMinHeight + 'px',
            //                'padding-top': (contentMinHeight / 2 - ($('strong').height() * 3) / 4) + 'px',
            //            });

            $('#overlay').removeClass('show').addClass('hide');
            //$('#login-form , #forgot-password-form').css('right', parseInt((window.innerWidth - 1024) / 2) + 52 + 'px');

            //$('#tagline').css('min-height', $('#content').height());
        }

        return {
            init: init
        };

    }();
}(window.app = window.app || {}, window.app.Module.Staff, jQuery));

window.app.Module.StaffLogin.init();



//
//resizeContent();
//$(window).off('resize.main');
//$(window).on('resize.main', resizeContent);