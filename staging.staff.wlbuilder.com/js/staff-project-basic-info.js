/***
 * This Script incorporates following features:
 * - Add Project
 * - Edit Project
 * - Delete Project
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
(function (app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffProjectBasicInfo = function () {
        var
                perPage,
                currentPage;

        // Image Validation for PNG type 
        function validateFileExtension(fileName) {
            var exp = /^.*\.(png|PNG)$/;
            return exp.test(fileName);
        }

        /***
         * Initialize various UI states
         ***/
        function initUI() {
            $("#result-section").attr("data-perpagevariable", "projectPerPage"); // variable to holde per page pagination
            $("#result-section").attr("data-currentpagevariable", "projectCurrentPage"); // variable to hold current pagination
            $('select').selectric();
        }



        /***
         * Listens the various events
         **/
        function initEvent() {

            /**********************
             * Delete Project Event
             ***********************/

            // Turns off click event delete button
            $("#result-section").off('click', '.delete');
            $("#result-section").on('click', '.delete', function (e) {
                e.preventDefault();
            });

            // Displays delete confirmation row when delete is clicked
            listenForPointerClick($("#result-section"), '.delete');
            $("#result-section").off("pointerclick", '.delete');
            $("#result-section").on("pointerclick", '.delete', function (e) {
                var element = this;
                var projectRow = $(element).closest('tr');
                var projectID = $(element).attr('data-pre');
                $(projectRow).after('<tr><td colspan="5" align="center">Confirm delete?  <span class="confirm-yes" data-pre="' + projectID + '">Yes</span> / <span class="confirm-no">No</span><td></tr>');
                $(projectRow).addClass("hide");
            });

            /**********************
             * Conformation Yes click
             ***********************/

            // Turns off clicke event confirm yes button
            $("#result-section").off('click', '.confirm-yes');
            $("#result-section").on('click', '.confirm-yes', function (e) {
                e.preventDefault();
            });

            // Deletes the clicked project when delete action is confirmed.
            listenForPointerClick($("#result-section"), '.confirm-yes');
            $("#result-section").off("pointerclick", '.confirm-yes');
            $("#result-section").on("pointerclick", '.confirm-yes', function (e) {
                var confirmationRow = $(this).parents("tr");
                var projectID = $(this).attr('data-pre');
                var project = localDB.projects.getByID(projectID);
                // Creates a Form Data object and appends data to it.
                var formData = new FormData(); // form data object
                formData.append('name', project.name);
                formData.append('location', project.location);
                formData.append('longitude', project.longitude);
                formData.append('latitude', project.latitude);
                formData.append('description', project.description);
                formData.append('id', projectID);
                formData.append('displayPriority', 0);
                formData.append('isFeaturedInStaticSection', project.isFeaturedInStaticSection);
                formData.append('isFeaturedInPanoramicSection', project.isFeaturedInPanoramicSection);
                $.when(staffAPI.editProject(formData)).done(function () {
                    createProjectList();
                    repositionContent();
                    $(confirmationRow).prev("tr").removeClass("hide");
                    $(confirmationRow).remove();
                });
            });

            /**********************
             * Conformation No click
             ***********************/

            // Turns off click event confirm no button
            $("#result-section").off('click', '.confirm-no');
            $("#result-section").on('click', '.confirm-no', function (e) {
                e.preventDefault();
            });

            // Removes the confirmation action row when delete action is not cofirmed
            listenForPointerClick($("#result-section"), '.confirm-no');
            $("#result-section").off("pointerclick", '.confirm-no');
            $("#result-section").on("pointerclick", '.confirm-no', function (e) {
                var confirmationRow = $(this).closest('tr');
                $((confirmationRow).prev('tr')).removeClass("hide");
                $(confirmationRow).remove();
            });

            /**********************
             * Edit Project 
             ***********************/

            // when clicked on edit button
            $("#result-section").off('click', '.edit');
            $("#result-section").on('click', '.edit', function (e) {
                e.preventDefault();
            });

            // Renders the edit project form for the clicked project 
            listenForPointerClick($("#result-section"), '.edit');
            $("#result-section").off("pointerclick", '.edit');
            $("#result-section").on("pointerclick", '.edit', function (e) {
                $('.confirm-delete').css('display', 'none');
                var element = $(this);
                // Set the values for the form display 
                renderEditProjectForm($(element).attr('data-pre'));
                $('#edit-project').addClass("active");
                var position = $("#edit-project").position();
                animateScroll(position.left, position.top);
            });

            // Turns of click event for undo button
            $('.undo-btn').off('click');
            $('.undo-btn').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.undo-btn'));
            $('.undo-btn').off("pointerclick");
            $('.undo-btn').on("pointerclick", function (e) {
                var element = $(this);
                var form = $(element).closest('form');
                $('input[type=file]').val('');
                if (form.attr('name') === 'edit-project') {
                    $('form[name="edit-project"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    var projectId = $(form).attr('data-pre');
                    renderEditProjectForm(projectId);
                    $('form[name="edit-project"] label.errorTxt').html('');

                } else {
                    $('form[name="add-project"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    $('form[name="add-project"] label.errorTxt').html('');
                    $('form[name="add-project"] span.contenteditable').text("");
                    $('#add-project-image-preview').attr("src", '');
                }
            });

            //Add section Submit
            $('form[name="add-project"]').off('submit');
            $('form[name="add-project"]').on('submit', function (e) {
                console.log("add form");

                e.preventDefault();

                var swapError = '';
                var checkError = 0;

                $(this).find("label.error").html("");
                /*** Validation starts ***/


                var projectName = $("#add-project-name");
                if ($.trim(projectName.val()) == '') {
                    swapError += '<span>Address is required.</span>';
                    checkError = 1;
                }

                var projectLocation = $("#add-project-location");
                if ($.trim(projectLocation.val()) == '') {
                    swapError += '<span>District is required.</span>';
                    checkError = 1;
                }

                var projectLatitude = $("#add-project-latitude");
                if ($.trim(projectLatitude.val()) == '') {
                    swapError += '<span>Latitude is required.</span>';
                    checkError = 1;
                } else { 
                    if (!$.isNumeric(projectLatitude.val())) {
                        swapError += '<span>Invalid display latitude entered.</span>';
                        checkError = 1;
                    }
                }

                var projectLongitude = $("#add-project-longitude");
                if ($.trim(projectLongitude.val()) == '') {
                    swapError += '<span>Longitude is required.</span>';
                    checkError = 1;
                } else {
                    if (!$.isNumeric(projectLongitude.val())) {
                        swapError += '<span>Invalid display longitude entered.</span>';
                        checkError = 1;
                    }
                }

                var projectDescription = $("#add-project-description");
                if ($.trim(projectDescription.val()) == '') {
                    swapError += '<span>Description is required.</span>';
                    checkError = 1;
                }

                var projectPriority = $("#add-project-priority");
                if ($.trim(projectPriority.val()) == '') {
                    swapError += '<span>Display priority is required.</span>';
                    checkError = 1;
                } else {
                    if (!$.isNumeric(projectPriority.val())) {
                        swapError += '<span>Invalid display priority entered.</span>';
                        checkError = 1;
                    }
                }
                

                var projectImage = $("#add-project-file");

                if ($.trim(projectImage.val()) == '') {
                    swapError += '<span>Project image is required.</span>';
                    checkError = 1;
                } else {
                    if (!validateFileExtension(projectImage.val())) {
                        swapError += '<span>Only PNG image is supported.</span>';
                        checkError = 1;
                    }
                }

                /*** Validation ends ***/
                var formData = new FormData($(this)[0]);
                // formData.append('img', projectImage.get(0).files[0]);
                formData.append('img', projectImage.get(0).files[0]);

                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    //swal({title:"Error",text: '',html: true});
                    return;

                }
                $.when(staffAPI.addProject(formData)).done(function () {
                    createProjectList();
                    repositionContent();
                    $('form[name="add-project"]').find(':input').val('');
                    $('form[name="add-project"] label.errorTxt').html('');
                    $('#add-project-image-preview').attr("src", '');
                }).fail(function (data) {
                    $('.add-error').html(data.responseJSON.error).show().delay(10000).fadeOut();

                });
            });

            //Edit section Submit
            $('form[name="edit-project"]').off('submit');
            $('form[name="edit-project"]').on('submit', function (e) {
//                console.log("edit form");
                e.preventDefault();

                var swapError = '';
                var checkError = 0;

                var projectID = $(this).attr('data-pre');
                e.preventDefault();
                e.stopPropagation();
                $(this).find("label.error").html("");
                /*** Validation starts ***/


                var projectName = $("#edit-project-name");
                if ($.trim(projectName.val()) == '') {
                    swapError += '<span>Address is required.</span>';
                    checkError = 1;
                }

                var projectLocation = $("#edit-project-location");
                if ($.trim(projectLocation.val()) == '') {
                    swapError += '<span>District is required.</span>';
                    checkError = 1;
                }

                var projectLatitude = $("#edit-project-latitude");
                if ($.trim(projectLatitude.val()) == '') {
                    swapError += '<span>Latitude is required.</span>';
                    checkError = 1;
                } else {
                    if (!$.isNumeric(projectLatitude.val())) {
                        swapError += '<span>Invalid display latitude entered.</span>';
                        checkError = 1;
                    }
                }

                var projectLongitude = $("#edit-project-longitude");
                if ($.trim(projectLongitude.val()) == '') {
                    swapError += '<span>Longitude is required.</span>';
                    checkError = 1;
                } else { 
                    if (!$.isNumeric(projectLongitude.val())) {
                        swapError += '<span>Invalid display longitude entered.</span>';
                        checkError = 1;
                    }
                }

                var projectDescription = $("#edit-project-description");
                if ($.trim(projectDescription.val()) == '') {
                    swapError += '<span>Description is required.</span>';
                    checkError = 1;
                }

                var projectPriority = $("#edit-project-priority");
                if ($.trim(projectPriority.val()) == '') {
                    swapError += '<span>Display priority is required.</span>';
                    checkError = 1;
                }else {
                    if (!$.isNumeric(projectPriority.val())) {
                        swapError += '<span>Invalid display priority entered.</span>';
                        checkError = 1;
                    }
                }
                var projectImage = $("#edit-project-file");
                var projectImagePrev = $("#edit-project-image-preview");
                
                if(projectImagePrev.attr('src') == ""){
                    if ($.trim(projectImage.val()) == '') {
                       swapError += '<span>Image is required.</span>';
                       checkError = 1;
                   } else {
                       if ($.trim(projectImage.val()) != '' && !validateFileExtension(projectImage.val())) {
                            swapError += '<span>Only PNG image is supported.</span>';
                            checkError = 1;
                            }  
                        }
                }
                

                var formData = new FormData($(this)[0]);
                formData.append('img', projectImage.get(0).files[0]);
                formData.append('id', projectID);

                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;

                }
                $.when(staffAPI.editProject(formData)).done(function () {
                    createProjectList();
                    repositionContent();
                    $('form[name="edit-project"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    $('#edit-project').removeClass("active");

                }).fail(function (data) {
                    $('.edit-error').html(data.responseJSON.error).show().delay(10000).fadeOut();
                });
            });

            /*************************
             *    Search Projects    *
             *************************/
            $('#search-text').off('click keypress');
            $('#search-text').on('keypress', function (e) {
                //console.log("press");
                e.stopPropagation();
                var code = e.keyCode || e.which;
                // console.log(code);
                if (code == 13) {

                    var searchedText = $.trim($(this).val());
                    //if (searchedText != '') {
                    createProjectList();
                    //}
                }
            });
            $('#search-button').off('click');
            $('#search-button').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#search-button'));
            $('#search-button').off("pointerclick");
            $('#search-button').on("pointerclick", function (e) {
                var searchedText = $.trim($('#search-text').val());
                createProjectList();
            });

            /************************************
             * Breadcum Link to the Page referred
             ***********************************/

            $("#add-project-file, #edit-project-file").off("change");
            $("#add-project-file, #edit-project-file").on("change", function () {
                readURL(this, $(this).parent().prev().find('img'));
                if ($(this).val() == "") {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Project image is required</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    //swal({title:"Error",text: '',html: true});
                    return;
                    //$(this).next().next('output').html("Project image is required").addClas("error");
                }
                $(this).closest('td').find('.deleteInfoImg').css('display', 'inline-block');
                $(this).next().next('output').html("");
            });

            /***
             * Turns off click event for confirmation' button
             ***/

            $('.deleteInfoImg').off('click');
            $('.deleteInfoImg').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.deleteInfoImg'));
            $('.deleteInfoImg').off("pointerclick");
            $('.deleteInfoImg').on("pointerclick", function (e) {
                var element = $(this);

                $('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.confirm-delete').css('display', 'block');

            });


            // Turns off clicke event confirm yes button
            $('.setting-section').off('click', '.confirm-yes');
            $(".setting-section").on('click', '.confirm-yes', function (e) {
                e.preventDefault();
            });

            // Deletes the clicked partner when delete action is confirmed.
            listenForPointerClick($(".setting-section"), '.confirm-yes');
            $('.setting-section').off("pointerclick", '.confirm-yes');
            $('.setting-section').on("pointerclick", '.confirm-yes', function (e) { //alert("test");
                var element = $(this);
                $(element).closest('td').find('input[type="file"]').val('');
                $(element).closest('td').find('img').attr('src', '');
                $(element).closest('td').find('.uploaded-name-holder input').val('');

                $(element).closest('td').find('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.deleteInfoImg').css('display', 'none');
            });

            /**********************
             * Conformation No click
             ***********************/

            // Turns off click event confirm no button
            $('.setting-section').off('click', '.confirm-no');
            $('.setting-section').on('click', '.confirm-no', function (e) {
                e.preventDefault();
            });

            // Removes the confirmation action row when delete action is not cofirmed
            listenForPointerClick($('.setting-section'), '.confirm-no');
            $('.setting-section').off("pointerclick", '.confirm-no');
            $('.setting-section').on("pointerclick", '.confirm-no', function (e) {
                $(this).closest('td').find('.confirm-delete').css('display', 'none');
            });

            /*****************************
             * sorting of button image list on edit section
             ******************************/
            $('form[name="edit-project"] input[name="radiog_dark"]').off('change , blur');
            $('form[name="edit-project"] input[name="radiog_dark"]').on('change , blur', function (e) {
                e.preventDefault();
                e.stopPropagation();
                loadImageSlide("edit");
            });

            /*****************************
             * required validation for #add-project-description
             ******************************/
            

        } //initEvent

        /**
         * Init module
         */
        function init() {
            repositionContent();
            initUI();
            //check for the user session to switch the page to dashboard if logged in.
            staff.checkUserSession('staff-project');
            staff.headerFooterDisplay();
            getProjects();
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);
            initEvent();
        } //init

        /***
         * Positions any content element
         * @param {boolean} animate defaults to true, if set to false the screen is not animated to top
         ***/
        function repositionContent(animate) {
            animate = typeof animate !== 'undefined' ? animate : true;
            var addForm = $("form[name='add-project']");
            addForm.find("label.error").text("");
            addForm.find("input").val("");
            $("#edit-project").removeClass("active");
            if (animate) {
                animateScroll(0, 0);
            }
        } //repositionContent

        /***********************
         * from staff api call getUpdatedprojects set data to localDB
         * creates projects.
         ************************/
        function getProjects() {
            var projectFromTime = 0;
            if (localDB.projects != null) {
                var projects = localDB.projects;
                $.each(projects, function (i, v) {
                    if (v.updatedTime > projectFromTime) {
                        projectFromTime = v.updatedTime;
                    }
                });
            }

            var formData = {
                'fromTime': projectFromTime
            };

            $.when(staffAPI.getUpdatedProjects(formData)).done(function (data) {
                localDB.projects.sortBy("displayPriority", false);
                createProjectList();
            });
        } //getProjects

        /***
         * Creates the list of projects in the display project grid
         ***/
        function createProjectList() {
            perPage = localDB.projectPerPage.value;
            currentPage = localDB.projectCurrentPage.value;
            var projects = (localDB.projects).filter(function (el) {
                //return el['displayPriority'] > 0;
                return 1 == 1;
            });
            var searchedText = $.trim($('#search-text').val());
            if (searchedText != '') {
                var regex = new RegExp(searchedText, "i");
                projects = (projects).filter(function (el) {
                    return regex.test(el['name']);
                });
            }
            var paginateData = paginate(perPage, currentPage, projects.length);
            staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, projects.length);
            var staffProjectHtml = "";
            $.each(projects.slice(paginateData.displayStart, paginateData.displayEnd), function (i, v) {
                var home_page_display = '';
                if (v.isFeaturedInStaticSection == 1 && v.isFeaturedInPanoramicSection == 1) {
                    home_page_display = "Static and Panorama";
                }
                if (v.isFeaturedInStaticSection == 1 && v.isFeaturedInPanoramicSection == 0) {
                    home_page_display = "Static";
                }
                if (v.isFeaturedInStaticSection == 0 && v.isFeaturedInPanoramicSection == 1) {
                    home_page_display = "Panorama";
                }
                if (v.isFeaturedInStaticSection == 0 && v.isFeaturedInPanoramicSection == 0) {
                    home_page_display = "None";
                }
                staffProjectHtml += '<tr data-pre="' + v.id + '"><td class="message">' + v.name + '</td>' + '<td class="message">' + v.location + '</td>' + '<td class="message">' + home_page_display + '</td>' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></button>' + '</td>' + '</tr>';
            });
            $('#result-table tbody').html(staffProjectHtml);
        } //staffProjectHtml

        /***
         *  Renders and displays edit project form for the clicked project
         * @param {int} projectID 
         ***/
        function renderEditProjectForm(projectID) {
            var editForm = $("form[name='edit-project']");
            editForm.find("label.error").text("");
            var projectReference = localDB.projects.getByID(projectID);
            // console.log(projectReference);
            $('form[name="edit-project"]').attr('data-pre', projectID);
            $('form[name="edit-project"] input[name="id"]').val(projectID);
            $('#edit-project-priority').val(projectReference.displayPriority);
            $('#edit-project-name').val(projectReference.name);
            $('#edit-project-location').val(projectReference.location);
            $('#edit-project-longitude').val(projectReference.longitude);
            $('#edit-project-latitude').val(projectReference.latitude);
            $('#edit-project-description').val(projectReference.description);
            /*****************************
             * changed the code on the basis of the new home page display requirement by rupesh 23-sep 2016
             ******************************/
            //old code
//            $('#edit-project-featured-static').val(projectReference.isFeaturedInStaticSection);
//            $('#edit-project-featured-panoromic').val(projectReference.isFeaturedInPanoramicSection);
            //new code
            if (projectReference.isFeaturedInStaticSection == 0 && projectReference.isFeaturedInPanoramicSection == 0) {
                $('#edit-home-page-feature').val(3);
            }
            if (projectReference.isFeaturedInStaticSection == 1 && projectReference.isFeaturedInPanoramicSection == 1) {
                $('#edit-home-page-feature').val(2);
            }
            if (projectReference.isFeaturedInPanoramicSection == 1 && projectReference.isFeaturedInStaticSection == 0) {
                $('#edit-home-page-feature').val(1);
            }
            if (projectReference.isFeaturedInPanoramicSection == 0 && projectReference.isFeaturedInStaticSection == 1) {
                $('#edit-home-page-feature').val(0);
            }

            $('#edit-project-image-preview').attr("src", WWW_URL + "/img/projects/icons/" + projectID + ".png?time=" + $.now());
            $('select').selectric();
            if( $('#edit-project-image-preview').attr("src") != "" ) {
                $('#edit-partner-image-delete').css('display','inline-block');
                $('#edit-partner-image-url').val(projectID + ".png");
            }
//            $('form[name="edit-project"] input[name="layout"][value="' + projectReference.layout + '"]').attr('checked', 'checked').trigger('click');
        } //renderEditprojectForm

        /***
         *  Renders and displays image for the clicked project
         * @param input fieldId 
         ***/
        function readURL(input, fieldID) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(fieldID).closest('td').find('.uploaded-name-holder input').val(input.files[0].name);
                    fieldID
                            .attr('src', e.target.result)
//                        .width(150)
//                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        return {
            init: init,
            createProjectList: createProjectList
        }; //return

    }(); //StaffProject

}(window.app = window.app || {}, window.app.Module.Staff, jQuery));
window.app.Module.StaffProjectBasicInfo.init();
