/***
 * This Script incorporates following features:
 * - Add Project Feature
 * - Edit Project Feature
 * - Delete Project Feature
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
(function (app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffProjectBluePrint = function () {
        var
                perPage,
                currentPage;

        // Image Validation for PNG type 
        function validateFileExtension(fileName) {
            var exp = /^.*\.(png|PNG|jpg|JPG|svg)$/;
            return exp.test(fileName);
        }

        /***
         * Initialize various UI states
         ***/
        function initUI() {
            $("#result-section").attr("data-perpagevariable", "projectPerPage"); // variable to holde per page pagination
            $("#result-section").attr("data-currentpagevariable", "projectCurrentPage"); // variable to hold current pagination
            $('select').selectric();
        }

        /***
         * Listens the various events
         **/
        function initEvent() {

            /**********************
             * Edit Project 
             ***********************/

            // when clicked on edit button
            $("#result-section").off('click', '.edit');
            $("#result-section").on('click', '.edit', function (e) {
                e.preventDefault();
            });

            // Renders the edit project form for the clicked project 
            listenForPointerClick($("#result-section"), '.edit');
            $("#result-section").off("pointerclick", '.edit');
            $("#result-section").on("pointerclick", '.edit', function (e) {
                $('.confirm-delete').css('display', 'none');
                var element = $(this);
                // Set the values for the form display
                $('input[type=file]').val('');
                createBluePrintList($(element).attr('data-pre'));
                $('#manage-blueprints-image').addClass("active");
                var position = $("#manage-blueprints-image").position();
                animateScroll(position.left, position.top);
            });

            // Turns of click event for undo button
            $('.undo-btn').off('click');
            $('.undo-btn').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.undo-btn'));
            $('.undo-btn').off("pointerclick");
            $('.undo-btn').on("pointerclick", function (e) {
                var element = $(this);
                var form = $(element).closest('form');

                if (form.attr('name') === 'edit-project-blue-print') {
                    $('input[type=file]').val('');

                    var projectId = $(form).attr('data-pre');
                    createBluePrintList(projectId);
                    $('form[name="edit-project-blue-print"] label.errorTxt').html('');
                }
            });

            /***
             * Turns off click event for confirmation' button
             ***/
            $('.deleteBlueprintImg').off('click');
            $('.deleteBlueprintImg').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.deleteBlueprintImg'));
            $('.deleteBlueprintImg').off("pointerclick");
            $('.deleteBlueprintImg').on("pointerclick", function (e) {
                var element = $(this);
                $('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.confirm-delete').css('display', 'block');

            });

            /***
             * Turns of click event for 'confirm yes' button
             ***/
            $('.confirm-yes').off('click');
            $('.confirm-yes').on('click', function (e) {
                e.preventDefault();
            });

            /***
             * When 'confirm yes' is clicked,
             ***/
            listenForPointerClick($('.confirm-yes'));
            $('.confirm-yes').off("pointerclick");
            $('.confirm-yes').on("pointerclick", function (e) {
                var element = $(this);
                var imgId = $('#result-table-blueprint').attr('data-pre');
                var level = $(this).closest('td').find('.confirm-delete').attr('data-delete');

//                var formData = {
//                    id: imgId,
//                    level: level
//                };
//                $.when(staffAPI.deleteProjectBluePrint(formData)).done(function () {
                $(element).closest('tr').find('input[type="file"]').val('');
                $(element).closest('tr').find('img').attr('src', '');
                $(element).closest('td').find('.uploaded-name-holder input').val('');
                $(element).closest('tr').find('.hiddenImg').val('');
                $(element).closest('td').find('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.deleteBlueprintImg').css('display', 'none');
                //return false;
                //$(element).css('display','none');
//                });
            });

            /**********************
             * When 'confirm no' is clicked,
             ***********************/

            // when clicked the older or newer activity button
            $('.confirm-no').off('click');
            $('.confirm-no').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('.confirm-no'));
            $('.confirm-no').off("pointerclick");
            $('.confirm-no').on("pointerclick", function (e) {
                $(this).closest('td').find('.confirm-delete').css('display', 'none');

            });

            //Edit section Submit
            $('form[name="edit-project-blue-print"]').off('submit');
            $('form[name="edit-project-blue-print"]').on('submit', function (e) {
//                console.log("edit form");
                e.preventDefault();
                var projectID = $(this).attr('data-pre');
                e.preventDefault();
                e.stopPropagation();

                var swapError = '';
                var checkError = 0;
                /*** Validation starts ***/
                //reset the form data
                var formData = new FormData($(this)[0]);

                var fileCount = 0;
                var fileCountPrev = 0;
                var k = 0;
                var validationPassed = true;
                var bluePrints = new Array("[name='url-low-1']", "[name='url-high-1']", "[name='url-low-2']", "[name='url-high-2']", "[name='url-low-3']", "[name='url-high-3']", "[name='url-low-4']", "[name='url-high-4']");
                var bluePrintsPrev = new Array("[name='url-low-1-prev']", "[name='url-high-1-prev']", "[name='url-low-2-prev']", "[name='url-high-2-prev']", "[name='url-low-3-prev']", "[name='url-high-3-prev']", "[name='url-low-4-prev']", "[name='url-high-4-prev']");

                $(bluePrints).each(function (i, image) {

                    var imageElm = $(image);
                    var imageElmPrev = $(bluePrintsPrev[k]);
                    if (imageElm.val() != "" || imageElmPrev.val() != '') {
                        fileCountPrev++;
                    }
                    k++;
                    if (imageElm.val() == "") { //If no image has been browsed then skip to next iteration
                        return 1;
                    }
                    fileCount++;
                    // Image validation check
                    //$(imageElm).parents('td').find('label:last').html("");
                    if (!validateFileExtension(imageElm.val())) {

                        swal({
                            title: "",
                            type: "warning",
                            text: '<div class="error"><span>Only PNG,JPG,SVG image is supported.</span></div>',
                            confirmButtonColor: '#18589f',
                            html: true
                        });
                        return;
                        
                    }
                }) // each file row

                if (!validationPassed) {
                    return false;
                }
                

                //#bkesh: in need of second loop here to check if the file uploads are in serial order or not as above loop only checks only if the val is empty 
                var i;                
                
                for (i = 0; i < (fileCountPrev); i++) {

                    var imageElm = $(bluePrints[i]);
                    var imageElmPrev = $(bluePrintsPrev[i]);
                    var level = 0;
                    if (i <= 1) {
                        level = 'level1';
                    } else if (i <= 3) {
                        level = 'level2';
                    } else if (i <= 5) {
                        level = 'level3';
                    } else {
                        level = 'level4';
                    }
                    //alert(imageElm.val()+'----'+imageElmPrev.val());
                    if ((imageElmPrev.val() == '' || imageElmPrev.val() == null)) {
                        if ((imageElm.val() == "" || imageElm.val() == null)) {
                            //If no image has been browsed then stop iteration
                            validationPassed = false;
                            swal({
                                title: "",
                                type: "warning",
                                text: '<div class="error"><span>You cannot skip ' + level + ' image.</span></div>',
                                confirmButtonColor: '#18589f',
                                html: true
                            });
                            return;
                        }
                    }
                    if (i == 0 || i == 2 || i == 4 || i == 6) {
                        var imageElem = $(bluePrints[parseInt(i + 1)]);
                        var imageElmPrev = $(bluePrintsPrev[parseInt(i + 1)]);

                        if (imageElem.val() == undefined || (imageElem.val() == '' && imageElmPrev.val() == '')) {

                            swal({
                                title: "",
                                type: "warning",
                                text: '<div class="error"><span>You must have ' + level + ' enlarge image.</span></div>',
                                confirmButtonColor: '#18589f',
                                html: true
                            });

                            return;
                        }
                    }

                }

                formData.append('id', projectID);
                
                $.when(staffAPI.editProjectBluePrint(formData)).done(function () {
                    createProjectList();
                    repositionContent();
                    $('form[name="edit-project-blue-print"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    $('#manage-blueprints-image').removeClass("active");

                }).fail(function (data) {
                    var error = data.responseJSON.error;
                    var errShow = '';
                    $.each(error, function (index, value) {
                        errShow += value + '<br><br>';
                    });
                    $('.edit-error').html(errShow).show().delay(10000).fadeOut();

                });
            });

            /*************************
             *    Search Projects    *
             *************************/
            $('#search-text').off('click keypress');
            $('#search-text').on('keypress', function (e) {
                console.log("press");
                e.stopPropagation();
                var code = e.keyCode || e.which;
                if (code == 13) {

                    var searchedText = $.trim($(this).val());
                    //if (searchedText != '') {
                    createProjectList();
                    //}
                }
            });
            $('#search-button').off('click');
            $('#search-button').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#search-button'));
            $('#search-button').off("pointerclick");
            $('#search-button').on("pointerclick", function (e) {
                var searchedText = $.trim($('#search-text').val());

                createProjectList();
            });

            $('input[type=file]').off("change");
            $('input[type=file]').on("change", function () {
                var id = $(this).attr('id');
                readURL(this, $(this).closest('tr').find('img'));

                if ($(this).val() == "") {
                    $(this).closest('tr').find('.upload-instructions output').html("Project image is required.").addClas("error");
                }
                $('#' + id + '-delete').css('display', 'block');
                $(this).next().next('output').html("");
            });



            /************************************
             * Breadcum Link to the Page referred
             ***********************************/

            /*****************************
             * sorting of button image list on edit section
             ******************************/
            $('form[name="edit-project-blue-print"] input[name="radiog_dark"]').off('change , blur');
            $('form[name="edit-project-blue-print"] input[name="radiog_dark"]').on('change , blur', function (e) {
                e.preventDefault();
                e.stopPropagation();
                loadImageSlide("edit");
            });


        } //initEvent

        /**
         * Init module
         */
        function init() {
            repositionContent();
            initUI();
            //check for the user session to switch the page to dashboard if logged in.
            staff.checkUserSession('staff-project');
            staff.headerFooterDisplay();
            getProjects();
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);
            initEvent();
        } //init

        /***
         * Positions any content element
         * @param {boolean} animate defaults to true, if set to false the screen is not animated to top
         ***/
        function repositionContent(animate) {
            animate = typeof animate !== 'undefined' ? animate : true;
            $("#edit-project-blue-print").removeClass("active");
            if (animate) {
                animateScroll(0, 0);
            }
        } //repositionContent

        /***********************
         * from staff api call getUpdatedprojects set data to localDB
         * creates projects.
         ************************/
        function getProjects() {
            var projectFromTime = 0;
            if (localDB.projects != null) {
                var projects = localDB.projects;
                $.each(projects, function (i, v) {
                    if (v.updatedTime > projectFromTime) {
                        projectFromTime = v.updatedTime;
                    }
                });
            }

            var formData = {
                'fromTime': projectFromTime
            };

            $.when(staffAPI.getUpdatedProjects(formData)).done(function (data) {
                localDB.projects.sortBy("displayPriority", false);
                createProjectList();
                
                //display blueprint list on the first load.
                var firstId = $('body').find('#result-table tbody tr:first').find('.edit').attr('data-pre');
                // Set the values for the form display
                $('input[type=file]').val('');
                createBluePrintList(firstId);
                $('#manage-blueprints-image').addClass("active");
//                var position = $("#manage-blueprints-image").position();
//                animateScroll(position.left, position.top);
            });
        } //getProjects

        /***
         * Creates the list of projects in the display project grid
         ***/
        function createProjectList() {
            perPage = localDB.projectPerPage.value;
            currentPage = localDB.projectCurrentPage.value;
            var projects = (localDB.projects).filter(function (el) {
                //return el['displayPriority'] > 0;
                return 1 == 1;
            });
            var searchedText = $.trim($('#search-text').val());
            if (searchedText != '') {
                var regex = new RegExp(searchedText, "i");
                projects = (projects).filter(function (el) {
                    return regex.test(el['name']);
                });
            }
            var paginateData = paginate(perPage, currentPage, projects.length);
            staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, projects.length);
            var staffProjectHtml = "";
            $.each(projects.slice(paginateData.displayStart, paginateData.displayEnd), function (i, v) {
                var home_page_display = '';
                if (v.isFeaturedInStaticSection == 1 && v.isFeaturedInPanoramicSection == 1) {
                    home_page_display = "Static and Panorama";
                }
                if (v.isFeaturedInStaticSection == 1 && v.isFeaturedInPanoramicSection == 0) {
                    home_page_display = "Static";
                }
                if (v.isFeaturedInStaticSection == 0 && v.isFeaturedInPanoramicSection == 1) {
                    home_page_display = "Panorama";
                }
                if (v.isFeaturedInStaticSection == 0 && v.isFeaturedInPanoramicSection == 0) {
                    home_page_display = "None";
                }

                staffProjectHtml += '<tr data-pre="' + v.id + '"><td class="message">' + v.name + '</td>' + '<td class="message">' + v.location + '</td>' + '<td class="message">' + home_page_display + '</td>' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></td>' + '</tr>';

//                staffProjectHtml += '<tr data-pre="' + v.id + '">' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="message">' + v.name + '</td>' + '<td class="message">' + v.location + '</td>' + '<td class="message">' + featuredInStatic + '</td>' + '<td class="message">' + featuredInPanoramic + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></td>' + '</tr>';
            });
            $('#result-table tbody').html(staffProjectHtml);
        } //staffProjectHtml

        /**
         * Renders Blueprints list display on popup table
         */
        function createBluePrintList(projectID) {
            var editForm = $("form[name='edit-project-blue-print']");
            editForm.find("label.error").text("");
            $('form[name="edit-project-blue-print"]').attr('data-pre', projectID);
            $('form[name="edit-project-blue-print"] input[name="id"]').val(projectID);
            var projectReference = localDB.projects.getByID(projectID);
            $("h2#popup-response-title").text("Upload Blueprint Image for " + projectReference.name);
            $.when(staffAPI.getProjectBluePrints({
                id: projectID
            })).done(function () {
//                var projectBluePrints = localDB.projectBluePrints; //# not using this coz its caching first clicked value and not changing values on other elements click
//                console.log(JSON.parse(localStorage.getItem("projectBluePrints")));
                var projectBluePrints = JSON.parse(localStorage.getItem("projectBluePrints"));
                $('#result-table-blueprint').attr('data-pre', projectBluePrints.id);
                $('.add-project-blueprint-image-preview').attr('src', '');
                
                if ((projectBluePrints.urlHigh1)) {
                    var src = WWW_URL + "/img/projects/blueprints/" + projectBluePrints.urlHigh1;
                    $('.url-high-1-complete').find('img').attr('src', src);
                    $('#url-high-1-url').val(projectBluePrints.urlHigh1);
                    $('[name="url-high-1-prev"]').val(projectBluePrints.urlHigh1);
                    $('#url-high-1-delete').css('display', 'block');
                } else {
                    $('.url-high-1-complete img').attr('src', '');
                    $('#url-high-1-url').val('');
                    // $('.url-high-1-complete').text('No');
                    $('[name="url-high-1-prev"]').val('');
                    $('#url-high-1-delete').css('display', 'none');
                }
                if ((projectBluePrints.urlLow1)) {
                    var src = WWW_URL + "/img/projects/blueprints/" + projectBluePrints.urlLow1;
                    $('.url-low-1-complete').find('img').attr('src', src);
                    $('#url-low-1-url').val(projectBluePrints.urlLow1);
                    $('[name="url-low-1-prev"]').val(projectBluePrints.urlLow1);
                    $('#url-low-1-delete').css('display', 'block');
//                    $('.url-low-1-complete').html('<img src="' + src + '" width="100" height="" >');
                    
                } else {
                    $('.url-low-1-complete img').attr('src', '');
                    $('#url-low-1-delete').css('display', 'none');
                    $('#url-low-1-url').val('');
                    // $('.url-low-1-complete').text('No');
                    $('[name="url-low-1-prev"]').val('');
                }
                if ((projectBluePrints.urlHigh2)) {
                    var src = WWW_URL + "/img/projects/blueprints/" + projectBluePrints.urlHigh2;
                    $('.url-high-2-complete').find('img').attr('src', src);
                    $('#url-high-2-url').val(projectBluePrints.urlHigh2);
                    $('[name="url-high-2-prev"]').val(projectBluePrints.urlHigh2);
                    $('#url-high-2-delete').css('display', 'block');
                } else {
                    $('.url-high-2-complete img').attr('src', '');
                    $('#url-high-2-url').val('');
                    //$('.url-high-2-complete').text('No');
                    $('[name="url-high-2-prev"]').val('');
                    $('#url-high-2-delete').css('display', 'none');
                }
                if ((projectBluePrints.urlLow2)) {
                    var src = WWW_URL + "/img/projects/blueprints/" + projectBluePrints.urlLow2;
                    $('.url-low-2-complete').find('img').attr('src', src);
                    $('#url-low-2-url').val(projectBluePrints.urlLow2);
                    $('[name="url-low-2-prev"]').val(projectBluePrints.urlLow2);
                    $('#url-low-2-delete').css('display', 'block');
                } else {
                    $('.url-low-2-complete img').attr('src', '');
                    $('#url-low-2-url').val('');
                    $('[name="url-low-2-prev"]').val('');
                    $('#url-low-2-delete').css('display', 'none');
                }
                if ((projectBluePrints.urlHigh3)) {
                    var src = WWW_URL + "/img/projects/blueprints/" + projectBluePrints.urlHigh3;
                    $('.url-high-3-complete').find('img').attr('src', src);
                    $('#url-high-3-url').val(projectBluePrints.urlHigh3);
                    $('[name="url-high-3-prev"]').val(projectBluePrints.urlHigh3);
                    $('#url-high-3-delete').css('display', 'block');
                } else {
                    $('.url-high-3-complete img').attr('src', '');
                    $('#url-high-3-url').val('');
                    //$('.url-high-3-complete').text('No');
                    $('[name="url-high-3-prev"]').val('');
                    $('#url-high-3-delete').css('display', 'none');
                }
                if ((projectBluePrints.urlLow3)) {
                    var src = WWW_URL + "/img/projects/blueprints/" + projectBluePrints.urlLow3;
                    $('.url-low-3-complete').find('img').attr('src', src);
                    $('#url-low-3-url').val(projectBluePrints.urlLow3);
                    $('[name="url-low-3-prev"]').val(projectBluePrints.urlLow3);
                    $('#url-low-3-delete').css('display', 'block');
                } else {
                    $('.url-low-3-complete img').attr('src', '');
                    $('#url-low-3-url').val('');
                    // $('.url-low-3-complete').text('No');
                    $('[name="url-low-3-prev"]').val('');
                    $('#url-low-3-delete').css('display', 'none');
                }
                if ((projectBluePrints.urlHigh4)) {
                    var src = WWW_URL + "/img/projects/blueprints/" + projectBluePrints.urlHigh4;
                    $('.url-high-4-complete').find('img').attr('src', src);
                    $('#url-high-4-url').val(projectBluePrints.urlHigh4);
                    $('[name="url-high-4-prev"]').val(projectBluePrints.urlHigh4);
                    $('#url-high-4-delete').css('display', 'block');
                } else {
                    $('.url-high-4-complete img').attr('src', '');
                    $('#url-high-4-url').val('');
                    // $('.url-high-4-complete').text('No');
                    $('[name="url-high-4-prev"]').val('');
                    $('#url-high-4-delete').css('display', 'none');
                }
                if ((projectBluePrints.urlLow4)) {
                    var src = WWW_URL + "/img/projects/blueprints/" + projectBluePrints.urlLow4;
                    $('.url-low-4-complete').find('img').attr('src', src);
                    $('#url-low-4-url').val(projectBluePrints.urlLow4);
                    $('[name="url-low-4-prev"]').val(projectBluePrints.urlLow4);
                    $('#url-low-4-delete').css('display', 'block');
                } else {
                    $('.url-low-4-complete img').attr('src', '');
                    // $('.url-low-4-complete').text('No');
                    $('#url-low-4-url').val('');
                    $('[name="url-low-4-prev"]').val('');
                    $('#url-low-4-delete').css('display', 'none');
                }
//                $('html,body').animate({
//                    scrollTop: $("#manage-blueprints-image").offset().top
//                }, 1);
            });
//            $('html,body').animate({
//                scrollTop: $("#manage-blueprints-image").offset().top
//            }, 1);
        } // createCollarList()

        /***
         *  Renders and displays image for the clicked project
         * @param input fieldId 
         ***/
        function readURL(input, fieldID) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    fieldID.closest('td').find('.uploaded-name-holder input').val(input.files[0].name);
                    fieldID
                            .attr('src', e.target.result)
                    //                        .width(150)
                    //                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }


        return {
            init: init,
            createProjectList: createProjectList
        }; //return

    }(); //StaffProject

}(window.app = window.app || {}, window.app.Module.Staff, jQuery));
window.app.Module.StaffProjectBluePrint.init();
