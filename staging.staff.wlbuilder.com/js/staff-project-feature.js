/***
 * This Script incorporates following features:
 * - Add Project Feature
 * - Edit Project Feature
 * - Delete Project Feature
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
$.when(loadScripts(["js/vendor/datepicker/material.min.js", "js/vendor/datepicker/moment.js", "js/vendor/datepicker/bootstrap-material-datetimepicker.js"])).done(function () {
    (function (app, staff, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};

        app.Module.StaffProjectFeature = function () {
            var
                    perPage,
                    currentPage;

            // Image Validation for PNG type 
            function validateFileExtension(fileName) {
                var exp = /^.*\.(png|PNG)$/;
                return exp.test(fileName);
            }

            /***
             * Initialize various UI states
             ***/
            function initUI() {               
                
                $("#result-section").attr("data-perpagevariable", "projectPerPage"); // variable to holde per page pagination
                $("#result-section").attr("data-currentpagevariable", "projectCurrentPage"); // variable to hold current pagination
                $('select').selectric();
                ///datepicker for the start and end date
                $('#endDate').bootstrapMaterialDatePicker
                        ({time: false, weekStart: 0, format: 'MMMM YYYY '
                        });
                $('#startDate').bootstrapMaterialDatePicker
                        ({
                            time: false, weekStart: 0, format: 'MMMM YYYY ', shortTime: true
                        }).on('change', function (e, date)
                {
                    $('#endDate').bootstrapMaterialDatePicker('setMinDate', date);
                });

            }

            /***
             * Listens the various events
             **/
            function initEvent() {

                /**********************
                 * Delete Project Feature Event
                 ***********************/

                // Turns off click event delete button
                $("#result-section").off('click', '.delete');
                $("#result-section").on('click', '.delete', function (e) {
                    e.preventDefault();
                });

                // Displays delete confirmation row when delete is clicked
                listenForPointerClick($("#result-section"), '.delete');
                $("#result-section").off("pointerclick", '.delete');
                $("#result-section").on("pointerclick", '.delete', function (e) {
                    var element = this;
                    var projectRow = $(element).closest('tr');
                    var projectID = $(element).attr('data-pre');
                    $(projectRow).after('<tr><td colspan="5" align="center">Confirm delete?  <span class="confirm-yes" data-pre="' + projectID + '">Yes</span> / <span class="confirm-no">No</span><td></tr>');
                    $(projectRow).addClass("hide");
                });

                /**********************
                 * Conformation Yes click
                 ***********************/

                // Turns off clicke event confirm yes button
                $("#result-section").off('click', '.confirm-yes');
                $("#result-section").on('click', '.confirm-yes', function (e) {
                    e.preventDefault();
                });

                // Deletes the clicked project when delete action is confirmed.
                listenForPointerClick($("#result-section"), '.confirm-yes');
                $("#result-section").off("pointerclick", '.confirm-yes');
                $("#result-section").on("pointerclick", '.confirm-yes', function (e) {
                    var confirmationRow = $(this).parents("tr");
                    var projectID = $(this).attr('data-pre');
                    var project = localDB.projects.getByID(projectID);
                    // Creates a Form Data object and appends data to it.
                    var formData = new FormData(); // form data object
                    formData.append('name', project.name);
                    formData.append('location', project.location);
                    formData.append('longitude', project.longitude);
                    formData.append('latitude', project.latitude);
                    formData.append('description', project.description);
                    formData.append('id', projectID);
                    formData.append('displayPriority', 0);
                    formData.append('isFeaturedInStaticSection', project.isFeaturedInStaticSection);
                    formData.append('isFeaturedInPanoramicSection', project.isFeaturedInPanoramicSection);
                    $.when(staffAPI.editProject(formData)).done(function () {
                        createProjectList();
                        repositionContent();
                        $(confirmationRow).prev("tr").removeClass("hide");
                        $(confirmationRow).remove();
                    });
                });

                /**********************
                 * Conformation No click
                 ***********************/

                // Turns off click event confirm no button
                $("#result-section").off('click', '.confirm-no');
                $("#result-section").on('click', '.confirm-no', function (e) {
                    e.preventDefault();
                });

                // Removes the confirmation action row when delete action is not cofirmed
                listenForPointerClick($("#result-section"), '.confirm-no');
                $("#result-section").off("pointerclick", '.confirm-no');
                $("#result-section").on("pointerclick", '.confirm-no', function (e) {
                    var confirmationRow = $(this).closest('tr');
                    $((confirmationRow).prev('tr')).removeClass("hide");
                    $(confirmationRow).remove();
                });

                /**********************
                 * Edit Project 
                 ***********************/

                // when clicked on edit button
                $("#result-section").off('click', '.edit');
                $("#result-section").on('click', '.edit', function (e) {
                    e.preventDefault();
                });

                // Renders the edit project form for the clicked project 
                listenForPointerClick($("#result-section"), '.edit');
                $("#result-section").off("pointerclick", '.edit');
                $("#result-section").on("pointerclick", '.edit', function (e) {

                    var element = $(this);
                    // Set the values for the form display 
                    renderEditProjectForm($(element).attr('data-pre'));
                    $('#edit-project').addClass("active");
                    var position = $("#edit-project").position();
                    animateScroll(position.left, position.top);
                });

                // Turns of click event for undo button
                $('.undo-btn').off('click');
                $('.undo-btn').on('click', function (e) {
                    e.preventDefault();
                });

                // Resets the form when undo button is clicked
                listenForPointerClick($('.undo-btn'));
                $('.undo-btn').off("pointerclick");
                $('.undo-btn').on("pointerclick", function (e) {
                    var element = $(this);
                    var form = $(element).closest('form');

                    if (form.attr('name') === 'edit-project') {
                        var projectId = $(form).attr('data-pre');
                        renderEditProjectForm(projectId);
                        $('form[name="edit-project"] label.errorTxt').html('');

                    } else {
                        $('form[name="add-project"]').find(':input').each(function () {
                            $(this).val('');
                        });
                        $('form[name="add-project"] label.errorTxt').html('');
                        $('form[name="add-project"] span.contenteditable').text("");
                    }
                });

                //Edit section Submit
                $('form[name="edit-project"]').off('submit');
                $('form[name="edit-project"]').on('submit', function (e) {
//                console.log("edit form");
                    e.preventDefault();
                    var projectID = $(this).attr('data-pre');
                    e.preventDefault();
                    e.stopPropagation();
                    var swapError = '';
                    var checkError = 0;
                    $(this).find("label.error").html("");
                    /*** Validation starts ***/
                    var projectleaseHold = $("#edit-project-lease-hold");
                    if ($.trim(projectleaseHold.val()) == '') {
                        swapError += '<span>Leasehold is required.</span>';
                        checkError = 1;
                    }else {
                        if (!$.isNumeric(projectleaseHold.val())) {
                            swapError += '<span>Invalid leasehold entered.</span>';
                            checkError = 1;
                        }
                    }
                    var projectBuidUpArea = $("#edit-project-build-up-area");
                    if ($.trim(projectBuidUpArea.val()) == '') {
                        swapError += '<span>Built-up area is required.</span>';
                        checkError = 1;
                    } else {
                        if (!$.isNumeric(projectBuidUpArea.val())) {
                            swapError += '<span>Invalid built-up area entered.</span>';
                            checkError = 1;
                        }
                    }
                    var projectBuidInArea = $("#edit-project-build-in-area");
                    if ($.trim(projectBuidInArea.val()) == '') {
                        swapError += '<span>Built-in area is required.</span>';
                        checkError = 1;
                    } else {
                        if (!$.isNumeric(projectBuidInArea.val())) {
                            swapError += '<span>Invalid built-in area entered.</span>';
                            checkError = 1;
                        }
                    }
                    var projectBedRoom = $("#edit-project-bed-room");
                    if ($.trim(projectBedRoom.val()) == '') {
                        swapError += '<span>Bedrooms is required.</span>';
                        checkError = 1;
                    } else {
                        if (!$.isNumeric(projectBedRoom.val())) {
                            swapError += '<span>Invalid bedrooms entered.</span>';
                            checkError = 1;
                        }
                    }
                    var projectShower = $("#edit-project-shower");
                    if ($.trim(projectShower.val()) == '') {
                        swapError += '<span>Showers is required.</span>';
                        checkError = 1;
                    } else {
                        if (!$.isNumeric(projectShower.val())) {
                            swapError += '<span>Invalid showers entered.</span>';
                            checkError = 1;
                        }
                    }


                    var projectEstimatedCost = $("#edit-project-estimated-cost");
                    if ($.trim(projectEstimatedCost.val()) == '') {
                        swapError += '<span>Estimated cost is required.</span>';
                        checkError = 1;
                    } else {
                        if (!$.isNumeric(projectEstimatedCost.val())) {
                            swapError += '<span>Invalid estimated cost entered.</span>';
                            checkError = 1;
                        }
                    }
                    var projectStartDate = $("#startDate");
                    
                    var projectEndDate = $("#endDate");
                    if ($.trim(projectStartDate.val()) == '' && $.trim(projectEndDate.val()) == '') {
                        swapError += '<span>Timeline is required.</span>';
                        checkError = 1;

                    } else {
                        if ($.trim(projectStartDate.val()) == '') {
                            swapError += '<span>Start date is required.</span>';
                            checkError = 1;
                        } else {

                            if ($.trim(projectEndDate.val()) == '') {
                                swapError += '<span>End date is required.</span>';
                                checkError = 1;
                            }
                        }
                    }

                    var projectDemolitionWork = $("#edit-project-demolition-work");
                    if ($.trim(projectDemolitionWork.val()) == '') {
                        swapError += '<span>Demolition work is required.</span>';
                        checkError = 1;
                    }else {
                        if (!$.isNumeric(projectDemolitionWork.val())) {
                            swapError += '<span>Invalid demolition work entered.</span>';
                            checkError = 1;
                        }
                    }


                    var projectStructureWork = $("#edit-project-structure-work");
                    if ($.trim(projectStructureWork.val()) == '') {
                        swapError += '<span>Structure work is required.</span>';
                        checkError = 1;
                    }else {
                        if (!$.isNumeric(projectStructureWork.val())) {
                            swapError += '<span>Invalid structure work entered.</span>';
                            checkError = 1;
                        }
                    }
                    
                    var projectMEWork = $("#edit-project-me-work");
                    if ($.trim(projectMEWork.val()) == '') {
                        swapError += '<span>M&E work is required.</span>';
                        checkError = 1;
                    }else {
                        if (!$.isNumeric(projectMEWork.val())) {
                            swapError += '<span>Invalid M&E work entered.</span>';
                            checkError = 1;
                        }
                    }

                    var projectExternalWork = $("#edit-project-external-work");
                    if ($.trim(projectExternalWork.val()) == '') {
                        swapError += '<span>External work is required.</span>';
                        checkError = 1;
                    }else {
                        if (!$.isNumeric(projectExternalWork.val())) {
                            swapError += '<span>Invalid external work entered.</span>';
                            checkError = 1;
                        }
                    }
                    
                    var projectArchitecturalWork = $("#edit-project-architectural-work");
                    if ($.trim(projectArchitecturalWork.val()) == '') {
                        swapError += '<span>Architectural work is required.</span>';
                        checkError = 1;
                    }else {
                        if (!$.isNumeric(projectArchitecturalWork.val())) {
                            swapError += '<span>Invalid architectural work entered.</span>';
                            checkError = 1;
                        }
                    }
                    
                    var formData = new FormData($(this)[0]);
                    formData.append('id', projectID);

                    if (checkError == 1) {
                        swal({
                            title: "",
                            type: "warning",
                            text: '<div class="error">' + swapError + '</div>',
                            confirmButtonColor: '#18589f',
                            html: true
                        });
                        return;

                    }
                    $.when(staffAPI.editProjectFeature(formData)).done(function () {
                        createProjectList();
                        repositionContent();
                        $('form[name="edit-project"]').find(':input').each(function () {
                            $(this).val('');
                        });
                        $('#edit-project').removeClass("active");

                    }).fail(function () {

                    });
                });

                /*************************
                 *    Search Projects    *
                 *************************/
                $('#search-text').off('click keypress');
                $('#search-text').on('keypress', function (e) {
                    e.stopPropagation();
                    var code = e.keyCode || e.which;

                    if (code == 13) {

                        var searchedText = $.trim($(this).val());
                        //if (searchedText != '') {
                        createProjectList();
                        //}
                    }
                });
                $('#search-button').off('click');
                $('#search-button').on('click', function (e) {
                    e.preventDefault();
                });

                listenForPointerClick($('#search-button'));
                $('#search-button').off("pointerclick");
                $('#search-button').on("pointerclick", function (e) {
                    var searchedText = $.trim($('#search-text').val());

                    createProjectList();
                });

                /************************************
                 * Breadcum Link to the Page referred
                 ***********************************/


                /*****************************
                 * sorting of button image list on edit section
                 ******************************/
                $('form[name="edit-project"] input[name="radiog_dark"]').off('change , blur');
                $('form[name="edit-project"] input[name="radiog_dark"]').on('change , blur', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    loadImageSlide("edit");
                });



            } //initEvent

            /**
             * Init module
             */
            function init() {
                repositionContent();
                initUI();
                //check for the user session to switch the page to dashboard if logged in.
                staff.checkUserSession('staff-project');
                staff.headerFooterDisplay();
                getProjects();
                $(window).off('resize.page');
                $(window).on('resize.page', repositionContent);
                initEvent();
            } //init

            /***
             * Positions any content element
             * @param {boolean} animate defaults to true, if set to false the screen is not animated to top
             ***/
            function repositionContent(animate) {
                animate = typeof animate !== 'undefined' ? animate : true;
                var addForm = $("form[name='add-project']");
                addForm.find("label.error").text("");
                addForm.find("input").val("");
                $("#edit-project").removeClass("active");
                if (animate) {
                    animateScroll(0, 0);
                }
            } //repositionContent

            /***********************
             * from staff api call getUpdatedprojects set data to localDB
             * creates projects.
             ************************/
            function getProjects() {
                var projectFromTime = 0;
                if (localDB.projects != null) {
                    var projects = localDB.projects;
                    $.each(projects, function (i, v) {
                        if (v.updatedTime > projectFromTime) {
                            projectFromTime = v.updatedTime;
                        }
                    });
                }

                var formData = {
                    'fromTime': projectFromTime
                };

                $.when(staffAPI.getUpdatedProjects(formData)).done(function (data) {
                    localDB.projects.sortBy("displayPriority", false);
                    createProjectList();
                    //render  first project edit 
                    var firstId = $('body').find('#result-table tbody tr:first').find('.edit').attr('data-pre');

                    renderEditProjectForm(firstId);

                    $('#edit-project').addClass("active");
//                    var position = $("#edit-project").position();
//                    animateScroll(position.left, position.top);
                });
            } //getProjects

            /***
             * Creates the list of projects in the display project grid
             ***/
            function createProjectList() {
                perPage = localDB.projectPerPage.value;
                currentPage = localDB.projectCurrentPage.value;
                var projects = (localDB.projects).filter(function (el) {
                    //return el['displayPriority'] > 0;
                    return 1 == 1;
                });
                var searchedText = $.trim($('#search-text').val());
                if (searchedText != '') {
                    var regex = new RegExp(searchedText, "i");
                    projects = (projects).filter(function (el) {
                        return regex.test(el['name']);
                    });
                }
                var paginateData = paginate(perPage, currentPage, projects.length);
                staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, projects.length);
                var staffProjectHtml = "";
                $.each(projects.slice(paginateData.displayStart, paginateData.displayEnd), function (i, v) {
                    var home_page_display = '';
                    if (v.isFeaturedInStaticSection == 1 && v.isFeaturedInPanoramicSection == 1) {
                        home_page_display = "Static and Panorama";
                    }
                    if (v.isFeaturedInStaticSection == 1 && v.isFeaturedInPanoramicSection == 0) {
                        home_page_display = "Static";
                    }
                    if (v.isFeaturedInStaticSection == 0 && v.isFeaturedInPanoramicSection == 1) {
                        home_page_display = "Panorama";
                    }
                    if (v.isFeaturedInStaticSection == 0 && v.isFeaturedInPanoramicSection == 0) {
                        home_page_display = "None";
                    }
                    staffProjectHtml += '<tr data-pre="' + v.id + '"><td class="message">' + v.name + '</td>' + '<td class="message">' + v.location + '</td>' + '<td class="message">' + home_page_display + '</td>' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></td>' + '</tr>';

//                staffProjectHtml += '<tr data-pre="' + v.id + '">' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="message">' + v.name + '</td>' + '<td class="message">' + v.location + '</td>' + '<td class="message">' + featuredInStatic + '</td>' + '<td class="message">' + featuredInPanoramic + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></td>' + '</tr>';
                });
                $('#result-table tbody').html(staffProjectHtml);
            } //staffProjectHtml

            /***
             *  Renders and displays edit project form for the clicked project
             * @param {int} projectID 
             ***/
            function renderEditProjectForm(projectID) {
                var editForm = $("form[name='edit-project']");
                editForm.find("label.error").text("");
                var projectReference = localDB.projects.getByID(projectID);
                $("h2.edit-project-heading").text("Edit Info for " + projectReference.name);
                $('form[name="edit-project"]').attr('data-pre', projectID);
                $('form[name="edit-project"] input[name="id"]').val(projectID);
                $('#edit-project-lease-hold').val(projectReference.leaseHold);
                $('#edit-project-build-up-area').val(projectReference.buildUpArea);
                $('#edit-project-build-in-area').val(projectReference.buildInArea);
                $('#edit-project-bed-room').val(projectReference.bedRoom);
                $('#edit-project-shower').val(projectReference.shower);
                $('#edit-project-estimated-cost').val(projectReference.estimatedCost);
                //changed the code by rupesh in sep 25 as per the new requirement
                var startYear = projectReference.startYear;
                var startMonth = projectReference.startMonth;
                var endMonth = projectReference.endMonth;
                var endYear = projectReference.endYear;
                //console.log(startYear,startMonth,endYear,endMonth);
                if (startYear != null) {
                    var startmonth = getMonth(parseInt(startMonth));
                    var startDate = startmonth+' '+startYear;
                    //change date formate
//                     format('d/m/y, h:ia')

                   // var startdate =new Date(startDate);
                    $('#startDate').val(startDate);
                } else {
                    $('#startDate').val('');
                }
                if (endYear != null) {
                    var endmonth = getMonth(parseInt(endMonth));
                    var endDate = endmonth+' '+endYear;
                    //alert(getMonth(10));
                    //change date formate
                    //var enddate=$.datepicker.formatDate( "M yy", new Date(endDate));               
                    $('#endDate').val(endDate);
                } else {
                    $('#endDate').val('');
                }



//            console.log(endMonth);
                $('#edit-project-end-month').val(endMonth);
                $('#edit-project-demolition-work').val(projectReference.demolitionWork);
                $('#edit-project-structure-work').val(projectReference.structureWork);
                $('#edit-project-me-work').val(projectReference.meWork);
                $('#edit-project-external-work').val(projectReference.externalWork);
                $('#edit-project-architectural-work').val(projectReference.architecturalWork);
                $('select').selectric();
            } //renderEditprojectForm
            
            //get month of the datepicker
            function getMonth(d) { 
                var month = new Array();
                    month[1] = "January";
                    month[2] = "February";
                    month[3] = "March";
                    month[4] = "April";
                    month[5] = "May";
                    month[6] = "June";
                    month[7] = "July";
                    month[8] = "August";
                    month[9] = "September";
                    month[10] = "October";
                    month[11] = "November";
                    month[12] = "December";
                    var n = month[d]; 
                    return n
            }
            return {
                init: init,
                createProjectList: createProjectList
            }; //return
            

        }(); //StaffProject

    }(window.app = window.app || {}, window.app.Module.Staff, jQuery));
    window.app.Module.StaffProjectFeature.init();
});
