/***
 * This Script incorporates following features:
 * - Add Project
 * - Edit Project
 * - Delete Project
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
(function(app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffProject = function() {
        var
            perPage,
            currentPage;

        // Image Validation for PNG type 
        function validateFileExtension(fileName) {
            var exp = /^.*\.(png|PNG)$/;
            return exp.test(fileName);
        }

        /***
         * Initialize various UI states
         ***/
        function initUI() {
            $("#result-section").attr("data-perpagevariable", "projectPerPage"); // variable to holde per page pagination
            $("#result-section").attr("data-currentpagevariable", "projectCurrentPage"); // variable to hold current pagination
            $('select').selectric();
        }

        /***
         * Listens the various events
         **/
        function initEvent() {

            /**********************
             * Delete Project Event
             ***********************/

            // Turns off click event delete button
            $("#result-section").off('click', '.delete');
            $("#result-section").on('click', '.delete', function(e) {
                e.preventDefault();
            });

            // Displays delete confirmation row when delete is clicked
            listenForPointerClick($("#result-section"), '.delete');
            $("#result-section").off("pointerclick", '.delete');
            $("#result-section").on("pointerclick", '.delete', function(e) {
                var element = this;
                var projectRow = $(element).closest('tr');
                var projectID = $(element).attr('data-pre');
                $(projectRow).after('<tr><td colspan="2" align="center">Confirm delete?  <span class="confirm-yes" data-pre="' + projectID + '">Yes</span> / <span class="confirm-no">No</span><td></tr>');
                $(projectID).addClass("hide");
            });

            /**********************
             * Conformation Yes click
             ***********************/

            // Turns off clicke event confirm yes button
            $("#result-section").off('click', '.confirm-yes');
            $("#result-section").on('click', '.confirm-yes', function(e) {
                e.preventDefault();
            });

            // Deletes the clicked project when delete action is confirmed.
            listenForPointerClick($("#result-section"), '.confirm-yes');
            $("#result-section").off("pointerclick", '.confirm-yes');
            $("#result-section").on("pointerclick", '.confirm-yes', function(e) {
                var confirmationRow = $(this).parents("tr");
                var projectID = $(this).attr('data-pre');
                var project = localDB.projects.getByID(projectID);
                // Creates a Form Data object and appends data to it.
                var formData = new FormData(); // form data object
                formData.append('description', project.description);
                formData.append('id', projectID);
                formData.append('displayPriority', 0);
                formData.append('email', project.email);
                formData.append('name', project.name);
                formData.append('website', project.websiteURL);
                $.when(staffAPI.editProject(formData)).done(function() {
                    createProjectList();
                    repositionContent();
                    $(confirmationRow).prev("tr").removeClass("hide");
                    $(confirmationRow).remove();
                });
            });

            /**********************
             * Conformation No click
             ***********************/

            // Turns off click event confirm no button
            $("#result-section").off('click', '.confirm-no');
            $("#result-section").on('click', '.confirm-no', function(e) {
                e.preventDefault();
            });

            // Removes the confirmation action row when delete action is not cofirmed
            listenForPointerClick($("#result-section"), '.confirm-no');
            $("#result-section").off("pointerclick", '.confirm-no');
            $("#result-section").on("pointerclick", '.confirm-no', function(e) {
                var confirmationRow = $(this).closest('tr');
                $((confirmationRow).prev('tr')).removeClass("hide");
                $(confirmationRow).remove();
            });

            /**********************
             * Edit Project 
             ***********************/

            // when clicked on edit button
            $("#result-section").off('click', '.edit');
            $("#result-section").on('click', '.edit', function(e) {
                e.preventDefault();
            });

            // Renders the edit project form for the clicked project 
            listenForPointerClick($("#result-section"), '.edit');
            $("#result-section").off("pointerclick", '.edit');
            $("#result-section").on("pointerclick", '.edit', function(e) {

                var element = $(this);
                // Set the values for the form display 
                renderEditProjectForm($(element).attr('data-pre'));
                $('#edit-project').addClass("active");
                var position = $("#edit-project").position();
                animateScroll(position.left, position.top);
            });

            // Turns of click event for undo button
            $('.undo-btn').off('click');
            $('.undo-btn').on('click', function(e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.undo-btn'));
            $('.undo-btn').off("pointerclick");
            $('.undo-btn').on("pointerclick", function(e) {
                var element = $(this);
                var form = $(element).closest('form');

                if (form.attr('name') === 'edit-project') {
                    var projectId = $(form).attr('data-pre');
                    renderEditProjectForm(projectId);
                    $('form[name="edit-project"] label.errorTxt').html('');

                } else {
                    $('form[name="add-project"]').find(':input').each(function() {
                        $(this).val('');
                    });
                    $('form[name="add-project"] label.errorTxt').html('');
                    $('form[name="add-project"] span.contenteditable').text("");
                }
            });

            //Add section Submit
            $('form[name="add-project"]').off('submit');
            $('form[name="add-project"]').on('submit', function(e) {
                console.log("add form");
                

                e.preventDefault();
                $(this).find("label.error").html("");
                /*** Validation starts ***/
                var projectPriority = $("#add-project-priority");
                if ($.trim(projectPriority.val()) == '') {
                    $(projectPriority).next('label').html("Display priority is required.").addClass('error');
                    projectPriority.focus();
                    return;
                }
                if (!$.isNumeric(projectPriority.val())) {
                    $(projectPriority).next('label').html("Invalid display priority.").addClass('error');
                    projectPriority.focus();
                    return;
                }
                var projectName = $("#add-project-name");
                if ($.trim(projectName.val()) == '') {
                    $(projectName).next('label').html("Name is required.").addClass('error');
                    projectName.focus();
                    return;
                }
                var projectLocation = $("#add-project-location");
                if ($.trim(projectLocation.val()) == '') {
                    $(projectLocation).next('label').html("Location is required.").addClass('error');
                    projectLocation.focus();
                    return;
                }

                var projectLongitude = $("#add-project-longitude");
                if ($.trim(projectLongitude.val()) == '') {
                    $(projectLongitude).next('label').html("Longitude is required.").addClass('error');
                    projectLongitude.focus();
                    return;
                }
                var projectLatitude = $("#add-project-latitude");
                if ($.trim(projectLatitude.val()) == '') {
                    $(projectLatitude).next('label').html("projectLatitude is required.").addClass('error');
                    projectLatitude.focus();
                    return;
                }


                var projectDescription = $("#add-project-description");
                if ($.trim(projectDescription.val()) == '') {
                    $(projectDescription).next('label').html("Description is required.").addClass('error');
                    projectDescription.focus();
                    return;
                }

                var projectImage = $("#add-project-file");
                if ($.trim(projectImage.val()) == '') {
                    $(projectImage).parents('td').find('label').html("project Image is required.").addClass('error');
                    projectImage.focus();
                    return;
                }
                

                /*** Validation ends ***/
                var formData = new FormData($(this)[0]);


                // formData.append('img', projectImage.get(0).files[0]);
                formData.append('img', projectImage.get(0).files[0]);
                $.when(staffAPI.addProject(formData)).done(function() {
                    createProjectList();
                    repositionContent();
                    $('form[name="add-project"]').find(':input').val('');
                    $('form[name="add-project"] label.errorTxt').html('');
                });
            });

            //Edit section Submit
            $('form[name="edit-project"]').off('submit');
            $('form[name="edit-project"]').on('submit', function(e) {
                console.log("edit form");
                e.preventDefault();
                var projectID = $(this).attr('data-pre');
                e.preventDefault();
                e.stopPropagation();
                /*** Validation starts ***/
                var projectPriority = $("#edit-project-priority");
                if ($.trim(projectPriority.val()) == '') {
                    $(projectPriority).next('label').html("Display priority is required.").addClass('error');
                    projectPriority.focus();
                    return;
                }
                if (!$.isNumeric(projectPriority.val())) {
                    $(projectPriority).next('label').html("Invalid display priority.").addClass('error');
                    projectPriority.focus();
                    return;
                }
                var projectName = $("#edit-project-name");

                if ($.trim(projectName.val()) == '') {
                    $(projectName).next('label').html("Name is required .").addClass('error');
                    projectName.focus();
                    return;
                }

                var projectEmail = $("#edit-project-email");
                if ($.trim(projectEmail.val()) == '') {
                    $(projectEmail).next('label').html("Email is required.").addClass('error');
                    projectEmail.focus();
                    return;
                }

                if (!isValidEmail(projectEmail.val())) {
                    $(projectEmail).next('label').html("Invalid email address.").addClass('error');
                    projectEmail.focus();
                    return;
                }

                var projectDescription = $("#edit-project-description");
                if ($.trim(projectDescription.val()) == '') {
                    $(projectDescription).next('label').html("Description is required.").addClass('error');
                    projectDescription.focus();
                    return;
                }
                var projectImage = $("#edit-project-file");

                var formData = new FormData($(this)[0]);
                formData.append('img', projectImage.get(0).files[0]);
                formData.append('id', projectID);
                $.when(staffAPI.editProject(formData)).done(function() {
                    createProjectList();
                    $('form[name="edit-project"]').find(':input').each(function() {
                        $(this).val('');
                    });
                    $('#edit-project').removeClass("active");
                    $('form[name="edit-project"] span.contenteditable').text("");

                }).fail(function() {

                });
            });

            /*************************
             *    Search Projects    *
             *************************/
            $('#search-text').off('click keypress');
            $('#search-text').on('keypress', function(e) {
                console.log("press");
                e.stopPropagation();
                var code = e.keyCode || e.which;
                console.log(code);
                if (code == 13) {

                    var searchedText = $.trim($(this).val());
                    return false;
                    //if (searchedText != '') {
                    createProjectList();
                    //}
                }
            });
            $('#search-button').off('click');
            $('#search-button').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#search-button'));
            $('#search-button').off("pointerclick");
            $('#search-button').on("pointerclick", function(e) {
                var searchedText = $.trim($('#search-text').val());


                createProjectList();
            });

            /************************************
             * Breadcum Link to the Page referred
             ***********************************/

            // Validates the priority value when value is input on add/edit priority
            $('#add-project-priority, #edit-project-priority').off("input");
            $('#add-project-priority, #edit-project-priority').on("input", function() {
                if (!$.isNumeric($(this).val())) {
                    $(this).next('label').html("Invalid display priority.").addClass('error');
                    return;
                }
                $(this).next("label").html("");
            });

            // Validates the email value when value is input on add/edit 

            $('#add-project-email, #edit-project-email').off("input");
            $('#add-project-email, #edit-project-email').on("input", function() {
                var element = $(this);
                var email = $.trim(element.val());
                if (!isValidEmail(email)) {
                    $(this).next('label').html("Invalid email address.").addClass('error');
                    return;
                }

                $(this).next("label").html("");
            });




            // // Validates the description value when value is input on add/edit description box
            // $('#add-project-description, #edit-project-description').off("input");
            // $('#add-project-description, #edit-project-description').on("input", function() {
            //     if ($.trim($(this).val()) == '') {
            //         $(this).next('label').html("Description is required.").addClass('error');
            //         return;
            //     }
            //     $(this).next('label').html("");
            // });

            $("#add-project-file, #edit-project-file").off("change");
            $("#add-project-file, #edit-project-file").on("change", function() {
                if ($(this).val() == "") {
                    $(this).next("label").html("Project image is required").addClas("error");
                }
                $(this).next("label").html("");
            });

            /*****************************
             * sorting of button image list on edit section
             ******************************/
            $('form[name="edit-project"] input[name="radiog_dark"]').off('change , blur');
            $('form[name="edit-project"] input[name="radiog_dark"]').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                loadImageSlide("edit");
            });

            /*****************************
             * required validation for #add-project-description
             ******************************/
            // $('#add-project-description').off('change , blur');
            // $('#add-project-description').on('change , blur', function(e) {
            //     e.preventDefault();
            //     e.stopPropagation();
            //     var projectDescription = $("#add-project-description");
            //     if ($.trim(projectDescription.val()) == '') {
            //         $(projectDescription).next('label').html("Description is required.").addClass('error');
            //         return;
            //     }
            //     $(projectDescription).next('label').html("");
            // });

        } //initEvent

        /**
         * Init module
         */
        function init() {
            repositionContent();
            initUI();
            //check for the user session to switch the page to dashboard if logged in.
            staff.checkUserSession('staff-project');
            staff.headerFooterDisplay();
            getProjects();
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);
            initEvent();
        } //init

        /***
         * Positions any content element
         * @param {boolean} animate defaults to true, if set to false the screen is not animated to top
         ***/
        function repositionContent(animate) {
            animate = typeof animate !== 'undefined' ? animate : true;
            var addForm = $("form[name='add-project']");
            addForm.find("label.error").text("");
            addForm.find("input").val("");
            $("#edit-project").removeClass("active");
            if (animate) {
                animateScroll(0, 0);
            }
        } //repositionContent

        /***********************
         * from staff api call getUpdatedprojects set data to localDB
         * creates projects.
         ************************/
        function getProjects() {
            var projectFromTime = 0;
            if (localDB.projects != null) {
                var projects = localDB.projects;
                $.each(projects, function(i, v) {
                    if (v.updatedTime > projectFromTime) {
                        projectFromTime = v.updatedTime;
                    }
                });
            }

            var formData = {
                'fromTime': projectFromTime
            };

            $.when(staffAPI.getUpdatedProjects(formData)).done(function(data) {
                localDB.projects.sortBy("displayPriority", false);
                createProjectList();
            });
        } //getProjects

        /***
         * Creates the list of projects in the display project grid
         ***/
        function createProjectList() {
            perPage = localDB.projectPerPage.value;
            currentPage = localDB.projectCurrentPage.value;
            var projects = (localDB.projects).filter(function(el) {
                //return el['displayPriority'] > 0;
                return 1 == 1;
            });
            var searchedText = $.trim($('#search-text').val());
            if (searchedText != '') {
                var regex = new RegExp(searchedText, "i");
                projects = (projects).filter(function(el) {
                    return regex.test(el['name']);
                });
            }
            var paginateData = paginate(perPage, currentPage, projects.length);
            staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, projects.length);
            var staffProjectHtml = "";
            $.each(projects.slice(paginateData.displayStart, paginateData.displayEnd), function(i, v) {
                var featuredInStatic = v.isFeaturedInStaticSection == 1?'yes':'No';
                var featuredInPanoramic = v.isFeaturedInPanoramicSection == 1?'yes':'No';
                staffProjectHtml += '<tr data-pre="' + v.id + '">' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="message">' + v.name + '</td>' + '<td class="message">' + v.location + '</td>' + '<td class="message">' + featuredInStatic + '</td>' + '<td class="message">' + featuredInPanoramic + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></button>' + '<button data-pre="' + v.id + '" class="delete"></button></td>' + '</tr>';
            });
            $('#result-table tbody').html(staffProjectHtml);
        } //staffProjectHtml

        /***
         *  Renders and displays edit project form for the clicked project
         * @param {int} projectID 
         ***/
        function renderEditProjectForm(projectID) {
            var editForm = $("form[name='edit-project']");
            editForm.find("label.error").text("");
            var projectReference = localDB.projects.getByID(projectID);

            // console.log(projectReference);
            $('form[name="edit-project"]').attr('data-pre', projectID);
            $('form[name="edit-project"] input[name="id"]').val(projectID);
            $('#edit-project-priority').val(projectReference.displayPriority);
            $('#edit-project-description').val(projectReference.description);
            $('#edit-project-name').val(projectReference.name);
            $('#edit-project-email').val(projectReference.email);
            $('#edit-project-website').val(projectReference.websiteURL);
            $('#edit-project-file-image').attr("src", WWW_URL + "/img/projects/" + projectID + ".png?time=" + $.now());
            $('form[name="edit-project"] input[name="layout"][value="' + projectReference.layout + '"]').attr('checked', 'checked').trigger('click');
        } //renderEditprojectForm

        return {
            init: init,
            createProjectList: createProjectList
        }; //return

    }(); //StaffProject

}(window.app = window.app || {}, window.app.Module.Staff, jQuery));
window.app.Module.StaffProject.init();
