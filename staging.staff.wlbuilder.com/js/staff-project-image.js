/***
 * This Script incorporates following features:
 * - Add Project Image
 * - Edit Project Image
 * - Delete Project Image
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
(function (app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffProjectImage = function () {
        var
                perPage,
                parameter = 0,
                currentPage;

        // Image Validation for PNG type 
        function validateFileExtension(fileName) {
            var exp = /^.*\.(png|PNG|jpg|JPG)$/;
            return exp.test(fileName);
        }

        /***
         * Initialize various UI states
         ***/
        function initUI() {
            $("#result-section").attr("data-perpagevariable", "projectPerPage"); // variable to holde per page pagination
            $("#result-section").attr("data-currentpagevariable", "projectCurrentPage"); // variable to hold current pagination
            $("#result-section-image").attr("data-perpagevariable", "projectPerPage"); // variable to holde per page pagination
            $("#result-section-image").attr("data-currentpagevariable", "projectCurrentPage"); // variable to hold current pagination
            $('select').selectric();
        }


        /***
         * Listens the various events
         **/
        function initEvent() {

            /**********************
             * Edit Project 
             ***********************/

            // when clicked on edit button
            $("#result-section").off('click', '.edit');
            $("#result-section").on('click', '.edit', function (e) {
                e.preventDefault();
            });

            // Renders the edit project form for the clicked project 
            listenForPointerClick($("#result-section"), '.edit');
            $("#result-section").off("pointerclick", '.edit');
            $("#result-section").on("pointerclick", '.edit', function (e) {
                $('.confirm-delete').css('display', 'none');
                var element = $(this);
                // Set the values for the form display 
                var projectID = $(element).attr('data-pre');

                $('#result-section-image').attr('data-parameter', projectID); //added for sortin of the table data
                createImageList(projectID);
                $('#manage-project-image').addClass("active");
                $('#add-project-image').addClass("active");
                $('#edit-project-image').removeClass("active");

                var addForm = $("form[name='add-project-image']");
                addForm.find("label.error").text("");
                addForm.attr('data-pre', projectID);
                $('form[name="add-project-image"] input[name="projectId"]').val(projectID);
                var position = $("#manage-project-image").position();
                animateScroll(position.left, position.top);
            });


            // when clicked on edit button
            $("#result-section-image").off('click', '.edit');
            $("#result-section-image").on('click', '.edit', function (e) {
                e.preventDefault();
            });

            // Renders the edit project form for the clicked project 
            listenForPointerClick($("#result-section-image"), '.edit');
            $("#result-section-image").off("pointerclick", '.edit');
            $("#result-section-image").on("pointerclick", '.edit', function (e) {
                $('.confirm-delete').css('display', 'none');

                var element = $(this);
                // Set the values for the form display 
                var projectId = $("#result-section-image").attr('data-pre');
                //console.log('projectId', projectId, "projectImageId", $(element).attr('data-pre'));
                renderEditProjectImageForm(projectId, $(element).attr('data-pre'));
                $('#edit-project-image').addClass("active");
                var position = $("#edit-project-image").position();
                animateScroll(position.left, position.top);
            });

            //# for edit project image <<

            // Turns of click event for undo button
            $('.undo-btn').off('click');
            $('.undo-btn').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.undo-btn'));
            $('.undo-btn').off("pointerclick");
            $('.undo-btn').on("pointerclick", function (e) {
                var element = $(this);
                var form = $(element).closest('form');

                if (form.attr('name') === 'edit-project-image') {
                    var projectId = $(form).attr('data-pre');
                    var projectImgid = '';
                    if ($(form).attr('data-pro') != '') {
                        projectId = $(form).attr('data-pro');
                        projectImgid = projectId;
                    }

                    createImageList(projectId);
                    if (projectImgid) {
                        renderEditProjectImageForm(projectId, projectImgid);
                    }
                    $('form[name="edit-project-image"] label.errorTxt').html('');
                } else {
                    $('#add-project-image-panoramic').val(1).selectric('refresh');
                    $('form[name="add-project-image"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    $('form[name="add-project-image"] label.errorTxt').html('');
                    $('form[name="add-project-image"] span.contenteditable').text("");
                }
            });

            //Edit section Submit
            $('form[name="edit-project-image"]').off('submit');
            $('form[name="edit-project-image"]').on('submit', function (e) {
//                console.log("edit form");
                e.preventDefault();
                var projectImageID = $(this).attr('data-pre');
                var projectID = $(this).attr('data-pro');
                e.preventDefault();
                e.stopPropagation();
                var swapError = '';
                var checkError = 0;
                /*** Validation starts ***/

                var projectName = $("#edit-project-image-name");
                if ($.trim(projectName.val()) == '') {
                    swapError += '<span>Name is required.</span>';
                    checkError = 1;
                }


                var projectPriority = $("#edit-project-image-priority");
                if ($.trim(projectPriority.val()) == '') {
                    swapError += '<span>Display priority is required.</span>';
                    checkError = 1;

                } else {
                    if (!$.isNumeric(projectPriority.val())) {
                        swapError += '<span>Invalid display priority entered.</span>';
                        checkError = 1;
                    }
                }

                var projectImage = $("#edit-project-image-file");

                var projectImagePrev = $("#edit-project-image-image-preview");

                if (projectImagePrev.attr('src') == "") {
                    if ($.trim(projectImage.val()) == '') {
                        swapError += '<span>Image is required.</span>';
                        checkError = 1;
                    } else {
                        if ($.trim(projectImage.val()) != '' && !validateFileExtension(projectImage.val())) {
                            swapError += '<span>Only PNG and JPG image is supported.</span>';
                            checkError = 1;
                        }
                    }
                }

                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;

                }
                var checkPanoramProject = isProjectPanorama(projectID);
                var checkBoundaryProject = isProjectStatic(projectID);
                var projects = localDB.projects;

                /*** Validation ends ***/
                var formData = new FormData($(this)[0]);
                formData.append('img', projectImage.get(0).files[0]);
                formData.append('id', projectImageID);
                formData.append('projectId', projectID);
                $.when(staffAPI.editProjectImage(formData)).done(function () {
                    if (checkBoundaryProject == 1) {
                        //checks priority,project Image type and Featured of the postdata
                        if ($("#edit-project-image-priority").val() > 0 && $('#edit-project-image-panoramic').val() == 0 && $('#edit-project-image-section-featured').val() == 1) {

                            $.when(staffAPI.getUpdatedBoundaryProjectImages({
                                pid: projectID,
                                id: projectImageID
                            })).done(function () {
                                //update and do nothing nothing

                            });

                            //return false;
                        }
                    }
                    if (checkPanoramProject == 1) {
                        if ($("#edit-project-image-priority").val() > 0 && $('#edit-project-image-panoramic').val() == 1 && $('#edit-project-image-section-featured').val() == 1) {

                            $.each(projects, function (i, v) {
                                if (v.isFeaturedInPanoramicSection == 1) {

                                    var pid = v.id;
                                    $.when(staffAPI.getUpdatedPanoramaProjectImages({
                                        pid: pid,
                                        id: projectImageID
                                    })).done(function () {
                                        //do nothing
                                    });


                                }
                            });

                            //return false;
                        }

                    }
                    createProjectList(projectID);
                    createImageList(projectID);
                    $('form[name="edit-project-image"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    $('#edit-project-image').removeClass("active");
//                    $('form[name="edit-project-blue-print"] span.contenteditable').text("");
                    $('html,body').animate({
                        scrollTop: $("#manage-project-image").offset().top
                    }, 1);

                }).fail(function (data) {
                    $('.edit-error').html(data.responseJSON.error).show().delay(10000).fadeOut();
                });
            });


            //Add section Submit
            $('form[name="add-project-image"]').off('submit');
            $('form[name="add-project-image"]').on('submit', function (e) {
                e.preventDefault();

                $(this).find("label.error").html("");
                var swapError = '';
                var checkError = 0;
                /*** Validation starts ***/

                var projectName = $("#add-project-image-name");
                if ($.trim(projectName.val()) == '') {
                    swapError += '<span>Name is required.</span>';
                    checkError = 1;
                }


                var projectPriority = $("#add-project-image-priority");
                if ($.trim(projectPriority.val()) == '') {
                    swapError += '<span>Display priority is required.</span>';
                    checkError = 1;
                } else {
                    if (!$.isNumeric(projectPriority.val())) {
                        swapError += '<span>Invalid display priority entered.</span>';
                        checkError = 1;
                    }
                }

                var projectImage = $("#add-project-image-file");
                if ($.trim(projectImage.val()) == '') {
                    swapError += '<span>Project image is required.</span>';
                    checkError = 1;

                } else {
                    if (!validateFileExtension(projectImage.val())) {
                        swapError += '<span>Only PNG and JPG image is supported.</span>';
                        checkError = 1;
                    }
                }

                /*** Validation ends ***/
                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });

                    return;
                }


                var pid = $('[name="projectId"]').val();
                var checkPanoramProject = isProjectPanorama(pid);
                var checkBoundaryProject = isProjectStatic(pid);


                var projects = localDB.projects;

                var formData = new FormData($(this)[0]);
                formData.append('img', projectImage.get(0).files[0]);
                $.when(staffAPI.addProjectImage(formData)).done(function (data) {
                    var projectImgId = data.insertId;
                    if (checkBoundaryProject == 1) {
                        //checks priority,project Image type and Featured of the postdata
                        if ($("#add-project-image-priority").val() > 0 && $('#add-project-image-panoramic').val() == 0 && $('#add-project-image-section-featured').val() == 1) {

                            $.when(staffAPI.getUpdatedBoundaryProjectImages({
                                pid: pid,
                                id: projectImgId
                            })).done(function () {
                                //update and do nothing nothing

                            });

                            //return false;
                        }
                    }
                    if (checkPanoramProject == 1) {
                        if ($("#add-project-image-priority").val() > 0 && $('#add-project-image-panoramic').val() == 1 && $('#add-project-image-section-featured').val() == 1) {
                            $.each(projects, function (i, v) {
                                if (v.isFeaturedInPanoramicSection == 1) {

                                    var pid = v.id;
                                    $.when(staffAPI.getUpdatedPanoramaProjectImages({
                                        pid: pid,
                                        id: projectImgId
                                    })).done(function () {
                                        //do nothing
                                    });


                                }
                            });

                            //return false;
                        }

                    }

                    var projectID = $('[name="projectId"]').val();
                    createImageList($('[name="projectId"]').val());
//                    repositionContent();
                    $('form[name="add-project-image"]').find(':input').val('');
                    $('form[name="add-project-image"] input[name="projectId"]').val(projectID);
                    $('form[name="add-project-image"] label.errorTxt').html('');
                    $('#add-project-image-image-preview').attr('src', '');
                    $('html,body').animate({
                        scrollTop: $("#manage-project-image").offset().top
                    }, 1);
                }).fail(function (data) {
                    $('.add-error').html(data.responseJSON.error).show().delay(10000).fadeOut();

                });
            });




            /*************************
             *    Search Projects    *
             *************************/
            $('#search-text').off('click keypress');
            $('#search-text').on('keypress', function (e) {
                console.log("press");
                e.stopPropagation();
                var code = e.keyCode || e.which;
                console.log(code);
                if (code == 13) {

                    var searchedText = $.trim($(this).val());
                    //if (searchedText != '') {
                    createProjectList();
                    //}
                }
            });
            $('#search-button').off('click');
            $('#search-button').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#search-button'));
            $('#search-button').off("pointerclick");
            $('#search-button').on("pointerclick", function (e) {
                var searchedText = $.trim($('#search-text').val());
                createProjectList();
            });

            /************************************
             * Breadcum Link to the Page referred
             ***********************************/

            $("#add-project-image-file,#edit-project-image-file").off("change");
            $("#add-project-image-file,#edit-project-image-file").on("change", function () {
                readURL(this, $(this).parent().prev().find('img'));
                if ($(this).val() == "") {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Project image is required.</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                }
                $(this).closest('td').find('.deleteProjectImage').css('display', 'inline-block');
                $(this).next().next('output').html("");
            });


            /***
             * Turns off click event for confirmation' button
             ***/

            $('.deleteProjectImage').off('click');
            $('.deleteProjectImage').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.deleteProjectImage'));
            $('.deleteProjectImage').off("pointerclick");
            $('.deleteProjectImage').on("pointerclick", function (e) {
                var element = $(this);

                $('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.confirm-delete').css('display', 'block');

            });


            // Turns off clicke event confirm yes button
            $('.setting-section').off('click', '.confirm-yes');
            $(".setting-section").on('click', '.confirm-yes', function (e) {
                e.preventDefault();
            });

            // Deletes the clicked partner when delete action is confirmed.
            listenForPointerClick($(".setting-section"), '.confirm-yes');
            $('.setting-section').off("pointerclick", '.confirm-yes');
            $('.setting-section').on("pointerclick", '.confirm-yes', function (e) { //alert("test");
                var element = $(this);
                $(element).closest('td').find('input[type="file"]').val('');
                $(element).closest('td').find('img').attr('src', '');
                $(element).closest('td').find('.uploaded-name-holder input').val('');

                $(element).closest('td').find('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.deleteProjectImage').css('display', 'none');
            });

            /**********************
             * Conformation No click
             ***********************/

            // Turns off click event confirm no button
            $('.setting-section').off('click', '.confirm-no');
            $('.setting-section').on('click', '.confirm-no', function (e) {
                e.preventDefault();
            });

            // Removes the confirmation action row when delete action is not cofirmed
            listenForPointerClick($('.setting-section'), '.confirm-no');
            $('.setting-section').off("pointerclick", '.confirm-no');
            $('.setting-section').on("pointerclick", '.confirm-no', function (e) {
                $(this).closest('td').find('.confirm-delete').css('display', 'none');
            });



            /*****************************
             * sorting of button image list on edit section
             ******************************/
            $('form[name="edit-project-image"] input[name="radiog_dark"]').off('change , blur');
            $('form[name="edit-project-image"] input[name="radiog_dark"]').on('change , blur', function (e) {
                e.preventDefault();
                e.stopPropagation();
                loadImageSlide("edit");
            });


        } //initEvent

        /**
         * Init module
         */
        function init() {
            repositionContent();
            initUI();
            //check for the user session to switch the page to dashboard if logged in.
            staff.checkUserSession('staff-project');
            staff.headerFooterDisplay();
            getProjects();
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);
            initEvent();
        } //init

        /***
         * Positions any content element
         * @param {boolean} animate defaults to true, if set to false the screen is not animated to top
         ***/
        function repositionContent(animate) {
            animate = typeof animate !== 'undefined' ? animate : true;
            $("#edit-project-image").removeClass("active");
            if (animate) {
                animateScroll(0, 0);
            }
        } //repositionContent

        /***********************
         * from staff api call getUpdatedprojects set data to localDB
         * creates projects.
         ************************/
        function getProjects() {
            var projectFromTime = 0;
            if (localDB.projects != null) {
                var projects = localDB.projects;
                $.each(projects, function (i, v) {
                    if (v.updatedTime > projectFromTime) {
                        projectFromTime = v.updatedTime;
                    }
                });
            }

            var formData = {
                'fromTime': projectFromTime
            };

            $.when(staffAPI.getUpdatedProjects(formData)).done(function (data) {
                localDB.projects.sortBy("displayPriority", false);
                createProjectList();

                //display blueprint list on the first load.
                var projectID = $('body').find('#result-table tbody tr:first').find('.edit').attr('data-pre');
                $('#result-section-image').attr('data-parameter', '');
                // Set the values for the form display 

                $('#result-section-image').attr('data-parameter', projectID);
                // Set the values for the form display

                createImageList(projectID);
                $('#manage-project-image').addClass("active");
                $('#add-project-image').addClass("active");
                $('#edit-project-image').removeClass("active");

                var addForm = $("form[name='add-project-image']");
                addForm.find("label.error").text("");
                addForm.attr('data-pre', projectID);
                $('form[name="add-project-image"] input[name="projectId"]').val(projectID);
//                var position = $("#manage-project-image").position();
//                animateScroll(position.left, position.top);
            });
        } //getProjects

        /***********************
         * checkst whether the project id is featured or not
         * return panaroma features.
         ************************/
        function isProjectPanorama(projectId) {
            var value = 0;
            var projects = localDB.projects;
            $.each(projects, function (i, v) {
                if (v.id == projectId) {
                    value = v.isFeaturedInPanoramicSection;
                }
            });
            return value;

        } //isProjectPanorama

        /***********************
         * checkst whether the project id is featured or not
         * return panaroma features.
         ************************/
        function isProjectStatic(projectId) {
            var value = 0;
            var projects = localDB.projects;
            $.each(projects, function (i, v) {
                if (v.id == projectId) {
                    value = v.isFeaturedInStaticSection;
                }
            });
            return value;

        } //isProjectPanorama


        /***
         * Creates the list of projects in the display project grid
         ***/
        function createProjectList() {
            perPage = localDB.projectPerPage.value;
            currentPage = localDB.projectCurrentPage.value;
            var projects = (localDB.projects).filter(function (el) {
                //return el['displayPriority'] > 0;
                return 1 == 1;
            });
            var searchedText = $.trim($('#search-text').val());
            if (searchedText != '') {
                var regex = new RegExp(searchedText, "i");
                projects = (projects).filter(function (el) {
                    return regex.test(el['name']);
                });
            }
            var paginateData = paginate(perPage, currentPage, projects.length);
            staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, projects.length);
            var staffProjectHtml = "";
            $.each(projects.slice(paginateData.displayStart, paginateData.displayEnd), function (i, v) {
                var home_page_display = '';
                if (v.isFeaturedInStaticSection == 1 && v.isFeaturedInPanoramicSection == 1) {
                    home_page_display = "Static and Panorama";
                }
                if (v.isFeaturedInStaticSection == 1 && v.isFeaturedInPanoramicSection == 0) {
                    home_page_display = "Static";
                }
                if (v.isFeaturedInStaticSection == 0 && v.isFeaturedInPanoramicSection == 1) {
                    home_page_display = "Panorama";
                }
                if (v.isFeaturedInStaticSection == 0 && v.isFeaturedInPanoramicSection == 0) {
                    home_page_display = "None";
                }
                staffProjectHtml += '<tr data-pre="' + v.id + '"><td class="message">' + v.name + '</td>' + '<td class="message">' + v.location + '</td>' + '<td class="message">' + home_page_display + '</td>' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></td>' + '</tr>';
//                staffProjectHtml += '<tr data-pre="' + v.id + '">' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="message">' + v.name + '</td>' + '<td class="message">' + v.location + '</td>' + '<td class="message">' + featuredInStatic + '</td>' + '<td class="message">' + featuredInPanoramic + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></td>' + '</tr>';
            });
            $('#result-table tbody').html(staffProjectHtml);
        } //staffProjectHtml

        /**
         * Renders project image list display on popup table
         */
        function createImageList(projectID) {

            $('#result-section-image').attr('data-pre', projectID);
            $.when(staffAPI.getProjectImages({
                id: projectID
            })).done(function () {
                //alert(projectID);
                getProjectImageHtmlList(projectID);


//                $('html,body').animate({
//                    scrollTop: $("#manage-project-image").offset().top
//                }, 1);
            });
        } // createCollarList()
        function getProjectImageHtmlList(projectID) {
            perPage = localDB.projectPerPage.value;
            currentPage = localDB.projectCurrentPage.value;

            var projectImages = (localDB.projectImages).filter(function (el) {
                return el['projectID'] = projectID;
            });
           
            var searchedText = $.trim($('#search-text').val());
            if (searchedText != '') {
                var regex = new RegExp(searchedText, "i");
                projectImages = (projectImages).filter(function (el) {
                    return regex.test(el['name']);
                });
            }
            //console.log(projectImages);
            var paginateData = paginate(perPage, currentPage, projectImages.length);
            var sectionId = $('#result-section-image');
            staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, projectImages.length,sectionId);
            var tableHtml = "";
            $.each(projectImages.slice(paginateData.displayStart, paginateData.displayEnd), function (i, v) {
                var isFeaturedImage = "";
                var isPanorama = v.isPanorama == 1 ? 'Panorama' : 'Static';
                if (v.isPanorama == 1) {
                    isFeaturedImage = v.isPanoramicSectionFeaturedImage == 1 ? 'Yes' : 'No';

                } else {
                    isFeaturedImage = v.isStaticSectionFeaturedImage == 1 ? 'Yes' : 'No';

                }
                tableHtml += '<tr data-pre="' + v.id + '">' + '<td class="message">' + v.name + '</td>' + '<td class="messages">' + isPanorama + '</td>' + '<td class="message">' + isFeaturedImage + '</td>' + '<td class="message">' + v.displayPriority + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></td>' + '</tr>';
            });
            $('#result-table-image').html(tableHtml);
        }


        /***
         *  Renders and displays edit project image form for the clicked project
         * @param {int} projectID 
         ***/
        function renderEditProjectImageForm(projectID, projectImageID) {
            var editForm = $("form[name='edit-project-image']");
            editForm.find("label.error").text("");
            var projectReferenceName = localDB.projects.getByID(projectID);
            $("h2#popup-response-title").text("Upload Project Image for " + projectReferenceName.name);
            var projectReference = localDB.projectImages.getByID(projectImageID);
            //console.log(projectReference);

            $('form[name="edit-project-image"]').attr('data-pro', projectID);
            $('form[name="edit-project-image"]').attr('data-pre', projectImageID);
            $('#edit-project-image-priority').val(projectReference.displayPriority);
            $('#edit-project-image-name').val(projectReference.name);
            $('#edit-project-image-description').val(projectReference.description);
            $('#edit-project-image-panoramic').val(projectReference.isPanorama);
            //changed the display image type with the requirement by #rupesh
//            var selectpanaroma = '';
//            if(projectReference.isPanoramicSectionFeaturedImage == 1){
//                selectpanaroma = 'selected';
//            }
//            var selectBoundary = '';
//            if(projectReference.isStaticSectionFeaturedImage == 1){
//                selectBoundary = 'selected';
//            }
//            var selectProjectImg = '';
//            if(projectReference.isProjectImage == 1){
//                selectProjectImg = "selected"
//            }
//            
//            $('#panaromicRow').next('.display-Image-type').remove();
//            var html = '';
//                html += '<tr class="display-Image-type">';
//                html += '<td class="label">';
//                html += '<label for="project-image-display-type">Image Display Section</label>';
//                html += '</td>';
//                html += '<td class="input">';
//                html += '<select name="panoramic_display" class="project-image-display-type" id="project-image-display-type">';
//            if(projectReference.isPanorama == 1) {
//                html += '<option value="1" ' + selectProjectImg + '>Project</option><option value="2" ' + selectpanaroma + '>Home Panoramic</option>';
//            }else {
//                html += '<option value="1" ' + selectProjectImg + '>Project</option><option value="3" ' + selectBoundary + '>Beyound Boundary</option>';
//            }
//            html += '</select>';
//            html += '<label></label>';
//            html += '</td>';
//            html += '</tr>';
//
//            $('tr#panaromicRow').after(html);
//            $('#edit-project-image-panoramic-featured').val(projectReference.isPanoramicSectionFeaturedImage); //removed by rupesh
//            $('#edit-project-image-static-featured').val(projectReference.isStaticSectionFeaturedImage); //removed by rupesh
            if (projectReference.isPanorama == 1) {
                $('#edit-project-image-section-featured').val(projectReference.isPanoramicSectionFeaturedImage); //modified on sep27
            } else {
                $('#edit-project-image-section-featured').val(projectReference.isStaticSectionFeaturedImage); //modified on sep27
            }
            $('[name="prevImg"]').val(projectReference.imgUrl);
            $('#edit-project-image-image-preview').attr("src", WWW_URL + "/img/projects/images/" + projectReference.imgUrl + "?time=" + $.now());
            $('select').selectric();
            if ($('#edit-project-image-image-preview').attr("src") != "") {
                $('#edit-project-image-url').val(projectReference.imgUrl);
                $('#edit-project-image-delete').css('display', 'inline-block');
            }
//            $('form[name="edit-project"] input[name="layout"][value="' + projectReference.layout + '"]').attr('checked', 'checked').trigger('click');
        } //renderEditProjectImageForm
        /**
         * Renders project image list display on popup table
         */
        function createImageListGONE(projectID) {
            var editForm = $("form[name='edit-project-image']");
            editForm.find("label.error").text("");
            $('form[name="edit-project-image"]').attr('data-pre', projectID);
            $('form[name="edit-project-image"] input[name="id"]').val(projectID);
            $.when(staffAPI.getProjectImages({
                id: projectID
            })).done(function () {
//                var projectImages = localDB.projectImages; //# not using this coz its caching first clicked value and not changing values on other elements click
//                console.log(JSON.parse(localStorage.getItem("projectImages")));
                var projectImages = JSON.parse(localStorage.getItem("projectImages"));
                var dataLength = (!projectImages) ? 0 : projectImages.length;
//                console.log(projectImages[0].name);
//                alert(length);
                var totalRows = 5;
                if (dataLength > totalRows) {
                    totalRows = dataLength;
                }
                var count;
                var tableHtml = '';
                for (count = 0; count < totalRows; count++) {
//                    console.log(projectImages[count].name);
                    var projectName = (projectImages[count]) ? projectImages[count].name : '';
                    var displayPriority = (projectImages[count]) ? projectImages[count].displayPriority : '';
                    var description = (projectImages[count]) ? projectImages[count].description : '';
                    var projectImageStatus = (projectImages[count]) ? 'Yes' : 'No';
                    var isPanorama = (projectImages[count] && projectImages[count].isPanorama == 1) ? '1' : '0';
                    tableHtml += '<tr>' + '<td><input type="text" name="priority[]" value="' + displayPriority + '"></td>' + '<td><input type="text" name="name[]" value="' + projectName + '"></td>' + '<td><input type="text" name="description[]" value="' + description + '"></td>' + '<td><input type="text" name="paranoma[]" value="' + isPanorama + '"></td>' + '<td class="pop-result-complete">' + projectImageStatus + '</td>' + '<td class="pop-result-action"><span class="file-button"><input type="file[]" name="" ></span><label class="error"></label></td>' + '</tr>';
                }
                $('#result-table-image').html(tableHtml);

//                $('html,body').animate({
//                    scrollTop: $("#manage-project-image").offset().top
//                }, 1);
            });
//            $('html,body').animate({
//                scrollTop: $("#manage-project-image").offset().top
//            }, 1);
        } // createCollarList()

        function readURL(input, fieldID) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    fieldID.closest('td').find('.uploaded-name-holder input').val(input.files[0].name);
                    fieldID
                            .attr('src', e.target.result)
//                        .width(150)
//                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }


        return {
            init: init,
            createProjectList: createProjectList,
            getProjectImageHtmlList: getProjectImageHtmlList
        }; //return

    }(); //StaffProject

}(window.app = window.app || {}, window.app.Module.Staff, jQuery));
window.app.Module.StaffProjectImage.init();
