/***
 * By default, switches to staff fabric care
 ***/
(function() {
    var target_page = 'staff-project-basic-info';
    $.when(switchPage(target_page)).done(function() {
        // Remember history (state, title, URL)
        window.history.pushState('nav', 'Page: ' + target_page, DOMAIN_URL + '/' + target_page);
    });
})()