﻿/***
 * This Script incorporates following features:
 * - Add staff detail includes password
 * - Edit staff detail exckudes password
 * - Delete staff
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
(function(app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffStaff = function() {
        var
            perPage,
            currentPage;

        /***
         * according to LastUpdatedTime, gets staff s table data and stores on localDB
         * than created staff List.
         ***/
        function getStaff() {
            var fromTime = staff.getLastUpdatedTime("staffs", "localDB");

            var formData = {
                'fromTime': fromTime
            };

            $.when(staffAPI.getUpdatedStaffs(formData)).done(function(data) {
                createStaffList();
            }).fail(function() {
                createStaffList();
            });
        } //getStaff

        /***
         * create staff  list to display on table
         *
         * for the searching purpose,
         * filters by inputted text on 'name' column of table.
         ***/
        function createStaffList() {
            perPage = localDB.staffPerPage.value;
            currentPage = localDB.staffCurrentPage.value;

            var staffs = (localDB.staffs).filter(function(el) {
                return el['accessLevel'] > 0;
            });

            var searchedText = $.trim($('#search-text').val());
            if (searchedText != '') {
                var regex = new RegExp(searchedText, "i");
                staffs = (staffs).filter(function(el) {
                    return regex.test(el['email']);
                });
            }


            var paginateData = paginate(perPage, currentPage, staffs.length);
            staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, staffs.length);
            var staffCount = staffs.length;
            var StaffStaffHtml = "";

            $.each(staffs.slice(paginateData.displayStart, paginateData.displayEnd), function(i, v) {
                StaffStaffHtml += '<tr>' + '<td class="access-level">' + v.accessLevel + '</td>' + '<td class="name">' + v.firstName + ' ' + v.lastName + '</td>' + '<td class="email">' + v.email + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></button><button data-pre="' + v.id + '" class="delete"></button></td>' + '</tr>';
            });
            $('#result-table tbody').html(StaffStaffHtml);
        } //createStaffList

        /***
         * Positions any content element.
         * hides 'edit-staff' section
         ***/

        function repositionContent() {
            $("#edit-staff").removeClass("show").addClass('hide');
            $("#overlay").addClass("hide");
        } //repositionContent

        /***
         * selecting particular fabric-weave record by ID
         * used to display fetched record on edit-form
         ***/
        function setStaff(StaffID) {
            var staffReference = localDB.staffs.getByID(StaffID);
            $('form[name="edit-staff"]').attr('data-pre', StaffID);
            $('form[name="edit-staff"] input[name="id"]').val(StaffID);
            $('#edit-staff-access-level').val(staffReference.accessLevel);
            $('#edit-staff-first-name').val(staffReference.firstName);
            $('#edit-staff-last-name').val(staffReference.lastName);
            $('#edit-staff-email').val(staffReference.email);
        } //setStaff

        /***
         * initializing all buttons click events for the action.
         * Adds all necessary events: input, focus, pointerclick
         ***/
        function initEvent() {
            /**********************
             * Delete staff Event
             ***********************/

            // when clicked the older or newer activity button
            $("#result-table").off('click', '.delete');
            $("#result-table").on('click', '.delete', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#result-table"), '.delete');
            $("#result-table").off("pointerclick", '.delete');
            $("#result-table").on("pointerclick", '.delete', function(e) {
                var element = this;
                var tabletd = $(element).closest('tr');
                var trId = $(element).data('pre');
                $(tabletd).after('<tr><td colspan="3" align="center">Confirm delete?  <span class="confirm-yes" data-pre="' + trId + '">Yes</span> / <span class="confirm-no">No</span><td></tr>');
                $(tabletd).addClass("hide");
            });


            /**********************
             * Conformation Yes click
             ***********************/

            // when clicked the older or newer activity button
            $("#result-table").off('click', '.confirm-yes');
            $("#result-table").on('click', '.confirm-yes', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#result-table"), '.confirm-yes');
            $("#result-table").off("pointerclick", '.confirm-yes');
            $("#result-table").on("pointerclick", '.confirm-yes', function(e) {
                var element = this;
                var tabletd = $(element).closest('tr');
                //Remove current and appended div
                var StaffID = $(element).attr('data-pre');
                var disableStaff = localDB.staffs.getByID(StaffID);
                disableStaff.displayPriority = 0;

                $.when(staffAPI.editStaff(disableStaff)).done(function() {
                    $((tabletd).prev('tr')).remove();
                    $(tabletd).remove();
                }).fail(function() {

                });
            });

            /**********************
             * Conformation No click
             ***********************/

            // when clicked the older or newer activity button
            $("#result-table").off('click', '.confirm-no');
            $("#result-table").on('click', '.confirm-no', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#result-table"), '.confirm-no');
            $("#result-table").off("pointerclick", '.confirm-no');
            $("#result-table").on("pointerclick", '.confirm-no', function(e) {
                var element = this;
                var tabletd = $(element).closest('tr');
                $((tabletd).prev('tr')).removeClass("hide");
                $(tabletd).remove();
            });


            /**********************
             * Edit staff Event
             ***********************/

            // when clicked on edit button of each staff data.
            $("#result-table").off('click', '.edit');
            $("#result-table").on('click', '.edit', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($("#result-table"), '.edit');
            $("#result-table").off("pointerclick", '.edit');
            $("#result-table").on("pointerclick", '.edit', function(e) {
                var element = $(this);
                // Set the values for the form display 
                setStaff($(element).data('pre'));
                $('#edit-staff').removeClass('hide').addClass('show');
                var position = $("#edit-staff").position();
                animateScroll(position.left, position.top);
            });

            // Undo Button Section
            $('.undo-btn').off('click');
            $('.undo-btn').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('.undo-btn'));
            $('.undo-btn').off("pointerclick");
            $('.undo-btn').on("pointerclick", function(e) {
                var element = $(this);
                var form = $(element).closest('form');
                if (form.attr('name') === 'edit-staff') {
                    var StaffId = $(form).data('pre');
                    setStaff(StaffId);
                    $('form[name="edit-staff"] label.errorTxt').html('');
                } else {
                    $('form[name="add-staff"]').find(':input').each(function() {
                        $(this).val('');
                    });
                }
            });


            //Add section Submit
            $('form[name="add-staff"]').off('submit');
            $('form[name="add-staff"]').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var accessLevel = $("#add-staff-access-level");
                var accessLevel = parseInt($.trim(accessLevel.val()), 10);
                if (!(isUnsignedInt(accessLevel) && (accessLevel !== 0))) {
                    $(accessLevel).next('label').html("Invalid Access Level").addClass('error');
                    return;
                }

                var staffEmail = $("#add-staff-email");
                if ($.trim(staffEmail.val()) == '') {
                    $(staffEmail).next('label').html("Email is required.").addClass('error');
                    return;
                }

                var staffPassword = $("#add-staff-password");
                var staffConfirmPassword = $("#add-staff-confirm-password");

                if ($.trim(staffPassword.val()) == '') {
                    $(staffPassword).next('label').html("Password is required and Password must be at least 6 characters.").addClass('error');
                    return;
                }
                if ($.trim(staffConfirmPassword.val()) == '') {
                    $(staffConfirmPassword).next('label').html("Confirm Password is required.").addClass('error');
                    return;
                }


                if (!($.trim(staffConfirmPassword.val()) == $.trim(staffPassword.val()))) {
                    $(staffConfirmPassword).next('label').html("password does not matched.").addClass('error');
                    return;
                }

                var staffFirstName = $("#add-staff-first-name");
                if ($.trim(staffFirstName.val()) == '') {
                    $(staffFirstName).next('label').html("First name is required.").addClass('error');
                    return;
                }


                var element = $(this);
                var formData = $('form[name="add-staff"]').serializeFormObject();
                $.when(staffAPI.addStaff(formData)).done(function() {
                    createStaffList();

                    $('form[name="add-staff"]').find(':input').each(function() {
                        $(this).val('');
                    });
                    $('form[name="add-staff"] label.errorTxt').html('');
                }).fail(function() {

                });
            });


            //Edit section Submit
            $('form[name="edit-staff"]').off('submit');
            $('form[name="edit-staff"]').on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var accessLevel = $("#edit-staff-access-level");
                var accessLevel = parseInt($.trim(accessLevel.val()), 10);
                if (!(isUnsignedInt(accessLevel) && (accessLevel !== 0))) {
                    $(accessLevel).next('label').html("Invalid Access Level").addClass('error');
                    return;
                }

                var staffEmail = $("#edit-staff-email");
                if ($.trim(staffEmail.val()) == '') {
                    $(staffEmail).next('label').html("Email is required.").addClass('error');
                    return;
                }


                var staffFirstName = $("#edit-staff-first-name");
                if ($.trim(staffFirstName.val()) == '') {
                    $(staffFirstName).next('label').html("First name is required.").addClass('error');
                    return;
                }


                var element = $(this);
                var formData = $('form[name="edit-staff"]').serializeFormObject();

                $.when(staffAPI.editStaff(formData)).done(function() {
                    createStaffList();
                    $('form[name="edit-staff"]').find(':input').each(function() {
                        $(this).val('');
                    });
                    $('form[name="edit-staff"] label.errorTxt').html('');
                    $('#edit-staff').removeClass('show').addClass('hide');

                }).fail(function() {

                });
            });



            /*************************
             *    Search staff    *
             *************************/
            $('#search-text').off('click keypress');
            $('#search-text').on('keypress', function(e) {
                e.stopPropagation();
                var code = e.keyCode || e.which;
                if (code == 13) {
                    var searchedText = $.trim($(this).val());
                    //if (searchedText != '') {
                    createStafflList();
                    //}
                }
            });

            $('#search-button').off('click');
            $('#search-button').on('click', function(e) {
                e.preventDefault();
            });

            listenForPointerClick($('#search-button'));
            $('#search-button').off("pointerclick");
            $('#search-button').on("pointerclick", function(e) {
                createStaffList();
            });

            /*****************************
             * validation for #edit-staff-access-level
             ******************************/
            $('#edit-staff-access-level').off('change , blur');
            $('#edit-staff-access-level').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var selectedAccessLevel = $("#edit-staff-access-level");
                var accessLevelValue = parseInt($.trim(selectedAccessLevel.val()), 10);
                if (!(isUnsignedInt(accessLevelValue) && (accessLevelValue !== 0))) {
                    $(selectedAccessLevel).next('label').html("Invalid Access Level").addClass('error');
                    return;
                }
                $(selectedAccessLevel).next('label').html("");
            });

            /*****************************
             * required validation for #edit-staff-first-name
             ******************************/
            $('#edit-staff-first-name').off('change , blur');
            $('#edit-staff-first-name').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var StaffFirstName = $("#edit-staff-first-name");
                if ($.trim(StaffFirstName.val()) == '') {
                    $(StaffFirstName).next('label').html("First Name is required.").addClass('error');
                    return;
                }
                $(StaffFirstName).next('label').html("");
            });

            /*****************************
             * required validation for #edit-staff-email
             ******************************/
            $('#edit-staff-email').off('change , blur');
            $('#edit-staff-email').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var staffEmail = $("#edit-staff-email");
                if ($.trim(staffEmail.val()) == '') {
                    $(staffEmail).next('label').html("Email is required.").addClass('error');
                    return;
                }
                if (!isValidEmail(staffEmail.val())) {
                    $(staffEmail).next('label').html("Invalid email");
                    return;
                }

                $.each(localDB.staffs, function(i, v) {
                    if (v.email == staffEmail) {
                        $(staffEmail).next('label').html("Email Reserved").addClass('error');;
                        return;
                    }

                });
                $(staffEmail).next('label').html("");
            });



            /*****************************
             * validation for #add-staff-access-level
             ******************************/
            $('#add-staff-access-level').off('change , blur');
            $('#add-staff-access-level').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var selectedAccessLevel = $("#add-staff-access-level");
                var accessLevelValue = parseInt($.trim(selectedAccessLevel.val()), 10);
                if (!(isUnsignedInt(accessLevelValue) && (accessLevelValue !== 0))) {
                    $(selectedAccessLevel).next('label').html("Invalid Access Level").addClass('error');
                    return;
                }
                $(selectedAccessLevel).next('label').html("");
            });

            /*****************************
             * required validation for #add-staff-first-name
             ******************************/
            $('#add-staff-first-name').off('change , blur');
            $('#add-staff-first-name').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var StaffFirstName = $("#add-staff-first-name");
                if ($.trim(StaffFirstName.val()) == '') {
                    $(StaffFirstName).next('label').html("First Name is required.").addClass('error');
                    return;
                }
                $(StaffFirstName).next('label').html("");
            });

            /*****************************
             * required validation for #add-staff-email
             ******************************/
            $('#add-staff-email').off('change , blur');
            $('#add-staff-email').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var staffEmail = $("#add-staff-email");
                if ($.trim(staffEmail.val()) == '') {
                    $(staffEmail).next('label').html("Email is required.").addClass('error');
                    return;
                }
                if (!isValidEmail(staffEmail.val())) {
                    $(staffEmail).next('label').html("Invalid email");
                    return;
                }

                $.each(localDB.staffs, function(i, v) {
                    if (v.email == staffEmail) {
                        $(staffEmail).next('label').html("Email Reserved").addClass('error');;
                        return;
                    }

                });
                $(staffEmail).next('label').html("");
            });

            /*****************************
             * required validation for #add-staff-password
             ******************************/
            $('#add-staff-password').off('change , blur');
            $('#add-staff-password').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var staffPassword = $("#add-staff-password");
                if ($.trim(staffPassword.val()) == '') {
                    $(staffPassword).next('label').html("Password is required and Password must be at least 6 characters.").addClass('error');
                    return;
                }
                if ($.trim(staffPassword.val()).length < 6) {
                    $(staffPassword).next('label').html("Password must be at least 6 characters.").addClass('error');
                    return;
                }

                $(staffPassword).next('label').html("");
            });
            /*****************************
             * required validation for #add-staff-confirm-password
             ******************************/
            $('#add-staff-confirm-password').off('change , blur');
            $('#add-staff-confirm-password').on('change , blur', function(e) {
                e.preventDefault();
                e.stopPropagation();
                var staffConfirmPassword = $("#add-staff-confirm-password");
                var staffPassword = $("#add-staff-password");
                if ($.trim(staffConfirmPassword.val()) == '') {
                    $(staffConfirmPassword).next('label').html("Confirm Password is required.").addClass('error');
                    return;
                }
                if (!($.trim(staffConfirmPassword.val()) == $.trim(staffPassword.val()))) {
                    $(staffConfirmPassword).next('label').html("Confirm password must match to Password.").addClass('error');
                    return;
                }

                $(staffConfirmPassword).next('label').html("");
            });
        } //initEvent

        /***
         * On first load of staff-staff.
         * Checkes user session
         * loads constant header footer display.
         * fetches Staff records and stores on LocalDB or SessionDB.
         * reposition the Edit section.
         ***/
        function initUI() {
            staff.checkUserSession('staff-account-settings');
            staff.headerFooterDisplay();
            getStaff();

            repositionContent();
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);
        } //initUI

        function init() {
            initEvent();
            initUI();
        } //init

        return {
            init: init,
            createStaffList: createStaffList
        } // return
    }();
}(window.app = window.app || {}, window.app.Module.Staff, jQuery));

window.app.Module.StaffStaff.init();