﻿/***
 * This Script incorporates following features:
 * - Add Testimonial
 * - Edit Testimonial
 * - Delete Testimonial
 *
 * Before use:
 * - ajax-staff-api is init
 * - ensure localDB & sessionDB are init
 ***/
(function (app, staff, $) {
    "use strict";
    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};

    app.Module.StaffTestimonial = function () {
        var
                perPage,
                currentPage;

        // Image Validation for PNG type 
        function validateFileExtension(fileName) {
            var exp = /^.*\.(png|PNG)$/;
            return exp.test(fileName);
        }

        /***
         * according to LastUpdatedTime, gets testimonials table data and stores on localDB
         * than created TestimonialList.
         ***/
        function getTestimonial() {
            var fromTime = staff.getLastUpdatedTime("testimonials", "localDB");

            var formData = {
                'fromTime': fromTime
            };

            $.when(staffAPI.getUpdatedTestimonials(formData)).done(function (data) {
                localDB.testimonials.sortBy("displayPriority", false);
                createTestimonialList();
            }).fail(function () {
                createTestimonialList();
            });
        } // getTestimonial

        /***
         * create testimonial list to display on table
         * for the searching purpose,
         * filters by inputted text on 'name' column of table.
         ***/
        function createTestimonialList() {
            perPage = localDB.testimonialPerPage.value;
            currentPage = localDB.testimonialCurrentPage.value;
            var testimonials = (localDB.testimonials).filter(function (el) {
                return 1 == 1;
            });
            var searchedText = $.trim($('#search-text').val());
            if (searchedText != '') {
                var regex = new RegExp(searchedText, "i");//The i modifier is used to perform case-insensitive matching.

                testimonials = (testimonials).filter(function (el) {
                    return regex.test(el['name']);
                });
            }
            var paginateData = paginate(perPage, currentPage, testimonials.length);
            staff.generatePaginationHTML(currentPage, localDB.maxPageLinks, perPage, localDB.perPageArray, testimonials.length);
            var staffTestimonialHtml = "";
            $.each(testimonials.slice(paginateData.displayStart, paginateData.displayEnd), function (i, v) {

                staffTestimonialHtml += '<tr>' + '<td class="name">' + v.name + '</td>' + '<td class="message">' + v.message + '</td>' + '<td class="priority">' + v.displayPriority + '</td>' + '<td class="action"><button data-pre="' + v.id + '" class="edit"></button></td>' + '</tr>';
            });

            $('#result-table tbody').html(staffTestimonialHtml);
        } // createTestimonialList

        /***
         * Positions any content element.
         * @param {boolean} animate defaults to true, if set to false the screen is not animated to top
         ***/
        function repositionContent(animate) {
            animate = typeof animate !== "undefined" ? animate : true;
            $("#edit-testimonial").removeClass("active");
            var addForm = $("form[name='add-testimonial']");
            addForm.find("label.error").text("");
            addForm.find("input").val("");
            if (animate) {
                animateScroll(0, 0);
            }
        } // repositionContent

        /***
         * selecting particular testimonial record by testimonialID
         * used to display fetched record on edit-form
         * @param {int} testimonialID The testimonial ID
         ***/
        function renderTestimonialForm(testimonialID) {
            var editForm = $("form[name='edit-testimonial']");
            editForm.find("label.error").text("");
            var testimonialReference = localDB.testimonials.getByID(testimonialID);
            $('form[name="edit-testimonial"]').attr('data-pre', testimonialID);
            $('form[name="edit-testimonial"] input[name="id"]').val(testimonialID);
            $('#edit-testimonial-priority').val(testimonialReference.displayPriority);
            $('#edit-testimonial-name').val(testimonialReference.name);
            $('#edit-testimonial-message').val(testimonialReference.message);
            $('#edit-testimonial-image-preview').attr("src", WWW_URL + "/img/testimonials/" + testimonialID + ".png?time=" + testimonialReference.lastUpdateTime);
            if( $('#edit-testimonial-image-preview').attr("src") != "" ) {
                $('#edit-testimonial-image-delete').css('display','inline-block');
                $('#edit-testimonial-image-url').val(testimonialID + ".png");
            }
        } // renderTestimonialForm

        /***
         * initializing all buttons click events for the action.
         * Adds all necessary events: input, focus, pointerclick
         ***/
        function initEvent() {
            /**********************
             * Delete Testimonial Event
             ***********************/

            // Turns of click event for delete button
            $("#result-table").off('click', '.delete');
            $("#result-table").on('click', '.delete', function (e) {
                e.preventDefault();
            });

            // Displays delete confirmation row when delete is clicked
            listenForPointerClick($("#result-table"), '.delete');
            $("#result-table").off("pointerclick", '.delete');
            $("#result-table").on("pointerclick", '.delete', function (e) {
                var element = this;
                var testimonailRow = $(element).closest('tr');
                var announcementID = $(element).data('pre');
                $(testimonailRow).after('<tr><td colspan="4" align="center">Confirm delete?  <span class="confirm-yes" data-pre="' + announcementID + '">Yes</span> / <span class="confirm-no">No</span><td></tr>');
                $(testimonailRow).addClass("hide");
            });

            /**********************
             * Conformation Yes click
             ***********************/

            // Turns off clicke event for confirm-yes button
            $("#result-table").off('click', '.confirm-yes');
            $("#result-table").on('click', '.confirm-yes', function (e) {
                e.preventDefault();
            });

            // Deletes the clicked testimonial when delete action is confirmed.
            listenForPointerClick($("#result-table"), '.confirm-yes');
            $("#result-table").off("pointerclick", '.confirm-yes');
            $("#result-table").on("pointerclick", '.confirm-yes', function (e) {
                var element = this;
                var confirmationRow = $(this).parents("tr");
                //Remove current and appended div
                var testimonialId = $(element).attr('data-pre');

                var disableTestimonial = localDB.testimonials.getByID(testimonialId);
                disableTestimonial.displayPriority = 0;
                $.when(staffAPI.editTestimonial(disableTestimonial)).done(function () {
                    createTestimonialList();
                    repositionContent();
                    $(confirmationRow).prev("tr").removeClass("hide");
                    $(confirmationRow).remove();
                });
            });

            /**********************
             * Conformation No click
             ***********************/

            // Turns off click event confirm no button
            $("#result-table").off('click', '.confirm-no');
            $("#result-table").on('click', '.confirm-no', function (e) {
                e.preventDefault();
            });

            // Removes the confirmation action row when delete action is not cofirmed
            listenForPointerClick($("#result-table"), '.confirm-no');
            $("#result-table").off("pointerclick", '.confirm-no');
            $("#result-table").on("pointerclick", '.confirm-no', function (e) {
                var confirmationRow = $(this).closest('tr');
                $((confirmationRow).prev('tr')).removeClass("hide");
                $(confirmationRow).remove();
            });

            /**********************
             * Edit Testimonial Event
             ***********************/

            // Turns of click event for edit button
            $("#result-table").off('click', '.edit');
            $("#result-table").on('click', '.edit', function (e) {
                e.preventDefault();
            });

            // Renders the edit announcement form for the clicked testimonial
            listenForPointerClick($("#result-table"), '.edit');
            $("#result-table").off("pointerclick", '.edit');
            $("#result-table").on("pointerclick", '.edit', function (e) {
                // Set the values for the form display 
                renderTestimonialForm($(this).data('pre'));
                $("#edit-testimonial").addClass("active");
                var position = $("#edit-testimonial").position();
                animateScroll(position.left, position.top);
            });

            // Turns of click event for undo button
            $('.undo-btn').off('click');
            $('.undo-btn').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.undo-btn'));
            $('.undo-btn').off("pointerclick");
            $('.undo-btn').on("pointerclick", function (e) {
                var form = $(this).closest('form');
                if (form.attr('name') === 'edit-testimonial') {
                    var testimonialId = $(form).data('pre');
                    renderTestimonialForm(testimonialId);
                    form.find("label.error").text("");
                } else if (form.attr('name') === 'add-testimonial') {
                    form.find("textarea").val("");
                    form.find("label.error").text("");
                    form.find("input").val("");
                }
            });

            // Submits the add testimonial form
            $('form[name="add-testimonial"]').off('submit');
            $('form[name="add-testimonial"]').on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var swapError = '';
                var checkError = 0;

                $(this).find('td').removeClass('error');

                var testimonialNameAdd = $("#add-testimonial-name");
                if ($.trim(testimonialNameAdd.val()) == '') {
                    swapError += '<span>Name is required.</span>';
                    checkError = 1;
                }

                var testimonialMessage = $("#add-testimonial-message");
                if ($.trim(testimonialMessage.val()) == '') {
                    swapError += '<span>Message is required.</span>';
                    checkError = 1;
                }

                var selectedPriority = $("#add-testimonial-priority");
                if ($.trim(selectedPriority.val()) == '') {
                    swapError += '<span>Display priority is required.</span>';
                    checkError = 1;
                } else {
                    var priorityValue = parseInt($.trim(selectedPriority.val()), 10);
                    if (!(isUnsignedInt(priorityValue) && (priorityValue !== 0))) {
                        swapError += '<span>Invalid display priority entered.</span>';
                        checkError = 1;
                    }
                }

                var testimonialImage = $("#add-testimonial-file");

                if ($.trim(testimonialImage.val()) == '') {
                    swapError += '<span>Testimonial image is required.</span>';
                    checkError = 1;
                } else {
                    if (!validateFileExtension(testimonialImage.val())) {
                        swapError += '<span>Only PNG image is supported.</span>';
                        checkError = 1;
                    }
                }


                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
                }

//                var formData = $('form[name="add-testimonial"]').serializeFormObject();
                var formData = new FormData($(this)[0]);
                formData.append('img', testimonialImage.get(0).files[0]);
                $.when(staffAPI.addTestimonial(formData)).done(function () {
                    createTestimonialList();
                    repositionContent();
                }).fail(function (data) {
                    $('.add-error').html(data.responseJSON.error).show().delay(10000).fadeOut();

                });
            });

            //Edit section Submit
            $('form[name="edit-testimonial"]').off('submit');
            $('form[name="edit-testimonial"]').on('submit', function (e) {
                e.preventDefault();
                e.stopPropagation();
                var swapError = '';
                var checkError = 0;
                /*** Validation Starts ***/
                $(this).find('td').removeClass('error');

                var testimonialName = $("#edit-testimonial-name");
                if ($.trim(testimonialName.val()) == '') {
                    swapError += '<span>Name is required.</span>';
                    checkError = 1;
                }

                var testimonialMessage = $("#edit-testimonial-message");
                if ($.trim(testimonialMessage.val()) == '') {
                    swapError += '<span>Message is required.</span>';
                    checkError = 1;
                }
                var selectedPriority = $("#edit-testimonial-priority");
                var priorityValue = parseInt($.trim(selectedPriority.val()), 10);
                if (!(isUnsignedInt(priorityValue) && (priorityValue !== 0))) {
                    swapError += '<span>Invalid display priority entered.</span>';
                    checkError = 1;
                }
                
                var testimonialImage = $("#edit-testimonial-file");
                var testimonialImagePrev = $("#edit-testimonial-image-preview");
                
                if(testimonialImagePrev.attr('src') == ""){
                    if ($.trim(testimonialImage.val()) == '') {
                       swapError += '<span>Image is required.</span>';
                       checkError = 1;
                   } else {
                       if ($.trim(testimonialImage.val()) != '' && !validateFileExtension(testimonialImage.val())) {
                            swapError += '<span>Only PNG image is supported.</span>';
                            checkError = 1;
                            }  
                        }
                }
                
                if (checkError == 1) {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error">' + swapError + '</div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    return;
                }
                /*** Validation ends ***/
//                var formData = $('form[name="edit-testimonial"]').serializeFormObject();
                var formData = new FormData($(this)[0]);
                formData.append('img', testimonialImage.get(0).files[0]);
                $.when(staffAPI.editTestimonial(formData)).done(function () {
                    createTestimonialList();
                    $('form[name="edit-testimonial"]').find(':input').each(function () {
                        $(this).val('');
                    });
                    $('form[name="edit-testimonial"] label.errorTxt').html('');
                    repositionContent();
                }).fail(function (data) {
                    $('.edit-error').html(data.responseJSON.error).show().delay(10000).fadeOut();
                });
            });

            /*************************
             *    Search Testimonials    *
             *************************/
            $('#search-text').off('click keypress');
            $('#search-text').on('keypress', function (e) {
                e.stopPropagation();
                var code = e.keyCode || e.which;
                if (code == 13) {
                    var searchedText = $.trim($(this).val());
                    //if (searchedText != '') {
                    createTestimonialList();
                    //}
                }
            });
            $('#search-button').off('click');
            $('#search-button').on('click', function (e) {
                e.preventDefault();
            });

            listenForPointerClick($('#search-button'));
            $('#search-button').off("pointerclick");
            $('#search-button').on("pointerclick", function (e) {
                createTestimonialList();
            });

            /************************************
             * Breadcum Link to the Page referred
             ***********************************/
            
            $("#add-testimonial-file, #edit-testimonial-file").off("change");
            $("#add-testimonial-file, #edit-testimonial-file").on("change", function () {
                readURL(this, $(this).parent().prev().find('img'));
                if ($(this).val() == "") {
                    swal({
                        title: "",
                        type: "warning",
                        text: '<div class="error"><span>Testimonial image is required</span></div>',
                        confirmButtonColor: '#18589f',
                        html: true
                    });
                    //swal({title:"Error",text: '',html: true});
                    return;
                    //$(this).next().next('output').html("Project image is required").addClas("error");
                }
                $(this).closest('td').find('.deleteInfoImg').css('display', 'inline-block');
                $(this).next().next('output').html("");
            });

            /***
             * Turns off click event for confirmation' button
             ***/

            $('.deleteInfoImg').off('click');
            $('.deleteInfoImg').on('click', function (e) {
                e.preventDefault();
            });

            // Resets the form when undo button is clicked
            listenForPointerClick($('.deleteInfoImg'));
            $('.deleteInfoImg').off("pointerclick");
            $('.deleteInfoImg').on("pointerclick", function (e) {
                var element = $(this);

                $('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.confirm-delete').css('display', 'block');

            });


            // Turns off clicke event confirm yes button
            $('.setting-section').off('click', '.confirm-yes');
            $(".setting-section").on('click', '.confirm-yes', function (e) {
                e.preventDefault();
            });

            // Deletes the clicked partner when delete action is confirmed.
            listenForPointerClick($(".setting-section"), '.confirm-yes');
            $('.setting-section').off("pointerclick", '.confirm-yes');
            $('.setting-section').on("pointerclick", '.confirm-yes', function (e) {
                var element = $(this);
                $(element).closest('td').find('input[type="file"]').val('');
                $(element).closest('td').find('img').attr('src', '');
                $(element).closest('td').find('.uploaded-name-holder input').val('');

                $(element).closest('td').find('.confirm-delete').css('display', 'none');
                $(element).closest('td').find('.deleteInfoImg').css('display', 'none');
            });

            /**********************
             * Conformation No click
             ***********************/

            // Turns off click event confirm no button
            $('.setting-section').off('click', '.confirm-no');
            $('.setting-section').on('click', '.confirm-no', function (e) {
                e.preventDefault();
            });

            // Removes the confirmation action row when delete action is not cofirmed
            listenForPointerClick($('.setting-section'), '.confirm-no');
            $('.setting-section').off("pointerclick", '.confirm-no');
            $('.setting-section').on("pointerclick", '.confirm-no', function (e) {
                $(this).closest('td').find('.confirm-delete').css('display', 'none');
            });

        } // initEvent

        /***
         * On first load of staff-testimonial.
         * Checkes user session
         * loads constant header footer display.
         * fetches testimonial records and stores on LocalDB or SessionDB.
         * reposition the Edit section.
         ***/
        function initUI() {
            staff.checkUserSession('staff-account-settings');
            staff.headerFooterDisplay();
            getTestimonial();
            repositionContent();
            $("#result-section").attr("data-perpagevariable", "testimonialPerPage"); // variable to holde per page pagination
            $("#result-section").attr("data-currentpagevariable", "testimonialCurrentPage"); // variable to hold current pagination
            $(window).off('resize.page');
            $(window).on('resize.page', repositionContent);
        } // initUI

        /***
         * Initialize module
         ***/
        function init() {
            initEvent();
            initUI();
        } // init
        
        /***
         *  Renders and displays image for the clicked project
         * @param input fieldId 
         ***/
        function readURL(input, fieldID) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(fieldID).closest('td').find('.uploaded-name-holder input').val(input.files[0].name);
                    fieldID
                            .attr('src', e.target.result)
//                        .width(150)
//                        .height(200);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }

        return {
            init: init,
            createTestimonialList: createTestimonialList
        } // return
    }();
}(window.app = window.app || {}, window.app.Module.Staff, jQuery));
window.app.Module.StaffTestimonial.init();