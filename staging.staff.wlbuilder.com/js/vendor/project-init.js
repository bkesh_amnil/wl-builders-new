

$(document).ready(function() {
    $('#project-slider').slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        fade: true,
        cssEase: 'linear',
        arrows: true,
        prevArrow: $("#prev"),
        nextArrow: $("#next"),
        appendDots: $("#pagination"),
        swipe: false
    });
});

$(function() {
    $('#listing').slimscroll({
        height: '320px',
        width: '280px',
        wheelStep: 80,
        animate: true
    });

    $('#down').click(function() {
        $('#listing').slimScroll({scrollBy: '80px'});
    });

    $('#up').click(function() {
        $('#listing').slimScroll({scrollBy: '-80px'});
    });
});


//paranoma


    var dom_el = document.getElementById('project-panorama');
    var dom_orig = document.getElementById('orig');

    // Settings
    var img_file = 'img/room1.jpg';
    var scaleX = -1;
    var scaleY = 1;
    var scaleZ = 1;


    // Settings
    var MIN_FOV = 10; // how much can user zoom in?
    var MAX_FOV = 120;
    var FOV = 97;
    var WIND_X_SPEED = .1;
    var WIND_Y_SPEED = 0;
    var DRAG_SPEED = 0.1;
    var ZOOM_SENSITIVITY = 0;
    var NEAR_CLIPPING_PLANE = 0.01; // 1
    var FAR_CLIPPING_PLANE = 1000; // 1100
    var ROOM_RADIUS = 0;
    var WIDTH_SEGMENTS = 200; // warning: large numbers cause large

    // Context
    var scene, camera, renderer;
    var isUserInteracting = false;
    var onMouseDownMouseX = 0,
        onMouseDownMouseY = 0;
    var lon = 0,
        onMouseDownLon = 0;
    var lat = 0,
        onMouseDownLat = 0;
    var phi = 0,
        theta = 0;

    init(dom_el, img_file);
    animate();

    function init(dom_el, img_file) {
        // Derive aspect ratio
        var width = dom_el.offsetWidth;
        var height = dom_el.offsetHeight;
        var aspect_ratio = width / height;

        // Create the scene, camera and renderer
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(FOV, aspect_ratio, NEAR_CLIPPING_PLANE, FAR_CLIPPING_PLANE);
        camera.target = new THREE.Vector3(0, 0, 0);
        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(width, height);
        dom_el.innerHTML = '';
        dom_el.appendChild(renderer.domElement);

        // Set Map
        var geometry = new THREE.SphereGeometry(ROOM_RADIUS, WIDTH_SEGMENTS, WIDTH_SEGMENTS / aspect_ratio);
        geometry.scale(scaleX, scaleY, scaleZ);
        var material = new THREE.MeshBasicMaterial({
            map: THREE.ImageUtils.loadTexture(img_file)
        });
        scene.add(new THREE.Mesh(geometry, material));

        // Bindings
        //dom_el.removeEventListener('mousedown', onDocumentMouseDown, false );
        dom_el.addEventListener('mousedown', onDocumentMouseDown, false);
        dom_el.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('mouseup', onDocumentMouseUp, false);
        dom_el.addEventListener('wheel', onDocumentMouseWheel, false);
        //dom_el.addEventListener( 'MozMousePixelScroll', onDocumentMouseWheel, false);
    } // init()

    function onDocumentMouseDown(event) {
        event.preventDefault();

        isUserInteracting = true;

        onPointerDownPointerX = event.clientX;
        onPointerDownPointerY = event.clientY;

        onPointerDownLon = lon;
        onPointerDownLat = lat;
    }

    function onDocumentMouseMove(event) {
        if (isUserInteracting) {
            event.preventDefault();
            lon = (onPointerDownPointerX - event.clientX) * DRAG_SPEED + onPointerDownLon;
            lat = (event.clientY - onPointerDownPointerY) * DRAG_SPEED + onPointerDownLat;
        }
    }

    function onDocumentMouseUp(event) {
        isUserInteracting = false;
    }

    function onDocumentMouseWheel(event) {
        event.preventDefault();
        console.log(getNormalizedWheelSpeed(event) + ' deltaY: ' + event.deltaY + ' wheelDeltaY: ' + event.wheelDeltaY + ' Mode: ' + event.deltaMode);

        /*
        if (event.wheelDeltaY) {
            camera.fov -= event.wheelDeltaY * ZOOM_SENSITIVITY; // Magic number 40 from MDN (https://developer.mozilla.org/en-US/docs/Web/Events/wheel)
        } // WebKit
        else if (event.wheelDelta) {
            camera.fov -= event.wheelDelta / 40 * ZOOM_SENSITIVITY;
        } // Opera / Explorer 9
        else if (event.deltaY) {
            camera.fov -= event.deltaY / 40 * ZOOM_SENSITIVITY;
        }
        else if (event.detail) {
            camera.fov += event.detail * ZOOM_SENSITIVITY;
        } // Firefox
        */

        camera.fov += getNormalizedWheelSpeed(event) * ZOOM_SENSITIVITY;

        // FOV min/max
        if (camera.fov < MIN_FOV) {
            camera.fov = MIN_FOV;
        }
        if (camera.fov > MAX_FOV) {
            camera.fov = MAX_FOV;
        }

        camera.updateProjectionMatrix();
    }

    function getNormalizedWheelSpeed(event) {
        if (event.type === 'wheel') {
            if (event.deltaMode === 1) {
                return event.deltaY * 40;
            } else {
                return event.deltaY;
            }
        } else if (event.type === 'mousewheel') {
            return event.wheelDelta / -40;
        }
    } // getNormalizedWheelSpeed()

    function animate() {
        requestAnimationFrame(animate);
        update();
    }

    function update() {
        if (!isUserInteracting) {
            lat += WIND_Y_SPEED;
            lon += WIND_X_SPEED;
        }

        lat = Math.max(-85, Math.min(85, lat));
        phi = THREE.Math.degToRad(90 - lat);
        theta = THREE.Math.degToRad(lon);

        camera.target.x = 500 * Math.sin(phi) * Math.cos(theta);
        camera.target.y = 500 * Math.cos(phi);
        camera.target.z = 500 * Math.sin(phi) * Math.sin(theta);

        camera.lookAt(camera.target);

        /*
        // distortion
        camera.position.copy( camera.target ).negate();
        */

        renderer.render(scene, camera);
    }