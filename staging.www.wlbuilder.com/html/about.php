<section id="about-banner">
  <div class="background current" id="background-1"></div>
  <div class="background" id="background-2"></div>
  <div class="background" id="background-3"></div>
  <div class="background" id="background-4"></div>
  <div class="background" id="background-5"></div>
  <div id="hero-slider">
        <img src="img/slider-hero1.jpg" />
        <img src="img/slider-hero2.jpg" />
        <img src="img/slider-hero3.jpg" />
        <img src="img/slider-hero4.jpg" />
        <img src="img/slider-hero5.jpg" />
  </div>
  <div id="background-images">
    <img src="img/bg-hero1.png" />
    <img src="img/bg-hero2.png" />
    <img src="img/bg-hero3.png" />
    <img src="img/bg-hero4.png" />
    <img src="img/bg-hero5.png" />
  </div>
</section>
<div id="about-container">
	<section id="commitment">
		<h2> <img src="img/ribbon-commitment.png" alt="The Este Bartin Commitment" /> </h2>
		<h3> <img id="dress-to-impress" src="img/tittle-dress-to-impress.png" alt="Dress To Impress" /> </h3>
		<blockquote id="commitment-quote"> <p><img id="front-quote" src="img/front-quote.png" alt="quote"/> We are driven by a single vision That it is within each individual to want to dress well and impress at first sight.<br>
			At Este Bartin, we strive to provide you with shirts that fit flawlessly – each creatively customised to suit your lifestyle<br>
			and profession. What we want more than anything else is your smile of confidence when you put on Este Bartin shirts.</p></blockquote>
		<h3> <img id="we-believe" src="img/img-webelieve.png" alt="We Believe" /> </h3>
		<p> Your comfort comes first – All our premium fabrics are handpicked.<br>
			Your time is valuable – Measurements are saved online and can be retrieved at anytime.<br>
			Your support should be rewarded – Earn credits to offset purchases when you refer friends to us.</p>
	</section>
	<section id="testimonial">
		<h2> <img id="what-people-say" src="img/ribbon-what-people-say.png" alt="What People Say About Us"> </h2>
		<h3> <img id="thanks-guys" src="img/title-thank-you.png" alt="Thanks guys!"> </h3>
		<button type="button" href="#" id="prev-testimonial" class="nav-btn prevbtn">prev</button>
		<section id="carousel-wrap">
			<div id="testimonial-list"> </div>
		</section>
		<button type="button" href="#" id="next-testimonial" class="nav-btn nextbtn">next</button>
	</section>
</div>
