<section class="inner-hero">
    <img src="img/partner-hero.jpg">
</section>
<section id="profile">
    <div class="container text-center">
        <h2 class="header-title red">Our Capabilities</h2>
        <p class="short-detail">By offering a comprehensive range of in-house construction services for every stage of your housing project from start to finish, WL Builder ensures efficiency and excellence.</p>
        <div id="capabilities">
            <article>
                <div class="icon"></div>
                <h4 class="red">Construction & Structural Works</h4>
                <p>Backed by a BCA General Builder Class 2 license, WL Builder’s experienced in-house team are capable of undertaking piling works to lay the foundation of a project, as well as handling the fabrication, erection and installation of steel and concrete supports. In addition, we are qualified to carry out Alterations & Amendments (A&A) work, reconstruction and demolition work where required.</p>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">Architecture & Engineering</h4>
                <p>Supported by our in-house draftsmen, your project will be accomplished by WL Builder’s large network of veteran architects, and our highly-trained construction crews will build the architectural plans to specification in a safety-conscious worksite. Moreover, our experience in brickworks, plastering work, ceiling and tiling also ensures we maintain our commitment to quality materials and fine workmanship, delivered in a most timely manner.</p>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">Interior Design & Landscaping</h4>
                <p>Our meticulous in-house interior designers and skilled craftsmen provide complete furnishing solutions while partnering experienced carpentry workshop to deliver customised decorations, tile and marble fixing, bathroom and kitchen fixings, lighting and ceiling designs for all your interior design requirements. WL Builder’s landscaping professionals are also capable of providing hardscaping works, installation of plants and flowers, and construction of outdoor features such as gazebos, kitchens, and bars.</p>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">Mechanical & Electrical (M&E) Works</h4>
                <p>WL Builder’s team of seasoned specialists can take on the full range of mechanical and electrical (M&E) works in your project. We possess expertise in electrical works, sanitary and plumbing works, fire protection works, construction of swimming pools, and facilitation for air-conditioning plus mechanical ventilation works. With our professional services, your project can even achieve the BCA Green Mark Certification.</p>
            </article>
        </div>
    </div>
</section> 