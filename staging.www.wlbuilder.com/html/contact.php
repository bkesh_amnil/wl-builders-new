<div id="map"></div>
<section id="contact">
    <div class="container text-center">
        <h2 class="header-title red">Let's Have a Chat</h2>
        <p class="short-detail">Your home is your castle – your sanctuary. It is an embodiment of your unique lifestyle. Take that first step and drop us a message.</p>
        <div id="contact-content">
            <article>
                <div class="icon"></div>
                <h4 class="red">Address Details</h4>
                <span> 52 Telok Blangah Road #04-03</span>
                <span>Telok Blangah House</span>
                <span>Singapore 098829</span>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">Telephone</h4>
                <span> Mobile: +65 8222 6930</span>
                <span>Office: +65 6271 1101</span>
                <span>Fax: +65 6271 3004</span>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">OFFICE HOURS</h4>
                <span>Mon to Sat – 10am to 6pm</span>
                <span>Sun & P.H. – 10am to 2pm</span>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">EMAIL ADDRESS</h4>
                <span><a href="mailto:sam@wlbuilder.com">sam@wlbuilder.com</a></span>
            </article>
        </div>
    </div>
</section>
