<section id="hero" class="text-center">
    <div id="home-hero-img">
    </div>
    <img src="img/logo.svg" alt="WL-Builders" class="main-logo">
    <a href="javascript:void();" class="nav-link button view-project-home" data-for="project" >View Projects</a>
    <a href="#" class="scroll-down"></a>
</section>
<section id="boundaries" class="text-center background parallax" data-diff="100">
    <h2 class="header-title red">Build your dream with WL Builder</h2>
    <p class="short-detail">A trusted Singapore construction company, our professional team is more than capable of skilfully managing every step of the building process. We will work closely with you to realise your project with efficiency and excellence.</p>
    <div id="boundaries-slider-wrap">
        <div id="boundaries-slider">

        </div>
    </div>
    <div class="pagination-scroll"></div>
</section>
<section id="moonbeam" class="text-center">
    <h2 class="header-title white"></h2>
    <p class="short-detail white"></p>
    <a href="javascript:void();" class="nav-link button view-project-home" data-for="project">View Projects</a>    
    <div id="moonbeam-panorama"></div>
</section>
<section id="testimonials" class="text-center">
</section>
