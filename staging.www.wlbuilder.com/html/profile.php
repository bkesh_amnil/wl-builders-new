<section class="inner-hero">
    <img src="img/profile-hero.jpg">
</section>
<section id="profile">
    <div class="container text-center">
        <h2 class="header-title red">Our Profile</h2>
        <p class="short-detail">WL Builder is dedicated to accomplishing your dream. We build not only structures but also relationships, by placing the utmost value on our clients. We focus on your requirements and spare no effort in helping you towards your goals.</p>
        <div id="profile-content">
            <article>
                <div class="icon"></div>
                <h4 class="red">Company Vision</h4>
                <p>We give our best to you through progressive diligence. From the latest building technologies to the newest industry fashions, we continuously update ourselves to serve you better, as building trust and confidence with others is a cornerstone of all we do. We strive to make WL Builder synonymous with expert workmanship, quality material and tasteful design.</p>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">Why Us?</h4>
                <p>Our designers, draftsmen and workers have decades of extensive experience across every stage of construction work. We make a conscious effort to develop in-house capabilities so that we maintain quality control throughout the building journey. Our stringent work processes and consistent commitment to quality ensures the highest standards, while our comprehensive range of services provides time-saving convenience to our clients.</p>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">About Us</h4>
                <p>WL Builder is a licensed and certified builder. We take safety seriously and are fully compliant with BCA regulations. Our perceptive knowledge and understanding enable us to meet your needs by tailoring our services; which includes structural, architectural, mechanical and electrical works, interior design, landscaping and more. </p>
            </article><article>
                <div class="icon"></div>
                <h4 class="red">Our History</h4>
                <p>Mr Liew Sam Wong founded WL Builder in 2013. Through every challenge, Mr Liew persevered with grit, resilience and humility to build a company that is now an industry leader. A veritable industry veteran, Mr Liew possesses more than 30 years of building experience across various domains, such as public works, landed properties, as well as commercial and industrial projects.</p>
            </article>
        </div>
    </div>
</section> 