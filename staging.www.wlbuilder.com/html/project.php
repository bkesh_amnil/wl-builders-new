<section id="project">

    <div id="popup-wrapper">

        <!-- Search Modal -->

        <section id="search-modal" class="modal-wrap" tabindex="-1">

            <button class="modal-close">Close</button>

            <a href="project-map" target="_blank" class="button modal-button btnMap"><img src="img/icons/modal-location-icon.svg" alt="map" />Map</a>

            <div class="text-center white">

                <img class="modal-img" src="">

                <div class="modal-title-wrap">

                    <h3 class="modal-title"></h3>

                </div>

                <p class="address"></p>

                <input type="button" id="up" value="up" />

                <div id="listing">
                </div>

                <input type="button" id="down" value="down" />

            </div>

        </section>



        <!-- Info Modal -->

        <section  id="info-modal" class="modal-wrap" tabindex="-1">        

            <button class="modal-close">Close</button>

            <a href="" id="shearBtn" target="_blank" class="button modal-button"><img src="img/icons/modal-share-icon.svg" alt="share" />Share</a>

            <div class="text-center white">

                <img class="modal-img" src="">

                <div class="modal-title-wrap info-title">

                    <h3 class="modal-title"></h3>

                </div>

                <p class="address"></p>

                <div class="info-item-wrap">

                    <div class="info-item" id="leasehold">

                        <label>Leasehold</label>

                        <span class="unit">0</span>

                        <span>Years</span>

                    </div>

                    <div class="info-item" id="builtUpArea">

                        <label>Built-up Area</label>

                        <span class="unit">0</span>

                        <span>SQF</span>

                    </div>

                    <div class="info-item" id="builtInArea">

                        <label>Built-in Area</label>

                        <span class="unit">0</span>

                        <span>SQF</span>

                    </div>

                    <div class="info-item" id="bedrooms">

                        <label>Bedrooms</label>

                        <span class="unit">0</span>

                        <span>4 Showers</span>

                    </div>

                    <div class="info-item" id="estimatedCost">

                        <label>Estimated Cost</label>

                        <span class="unit">$0</span>

                        <span>Per SQF</span>

                    </div>

                    <div class="info-item" id="timeLine">

                        <label>Timeline</label>

                        <span class="unit">0</span>

                        <span>Months</span>

                    </div>

                </div>

                <div class="modal-title-wrap hidden-mobile">

                    <h3 class="white modal-title">PROJECT TIMELINE</h3>

                </div>

                <div class="project-timeline hidden-mobile">

                    <span class="bar-date">AUG 2016</span><!-- 
    
                    --><div class="timeline-bar">

                        <div class="bar-segment">

                            <span>Demolition (0 weeks)</span>

                        </div><div class="bar-segment">

                            <span>Demolition (0 weeks)</span>

                        </div><div class="bar-segment">

                            <span>Demolition (0 weeks)</span>

                        </div><div class="bar-segment">

                            <span>Demolition (0 weeks)</span>

                        </div><div class="bar-segment">

                            <span>Demolition (0 weeks)</span>

                        </div>

                    </div><!--  
    
                    --><span class="bar-date">JUN 2017</span>

                </div>

            </div>

        </section>



        <!-- Blueprint Modal -->

        <section  id="blueprint-modal" class="modal-wrap" data-id='' tabindex="-1">            

            <button class="modal-close">Close</button>

            <a href="#" class="button modal-button btn-magnify"><img src="img/icons/modal-enlarge-icon.svg" alt="Enlarge" />Enlarge</a>

            <div class="text-center white">



                <div class="blueprint-content">

                    <img class="modal-img" src="">

                    <div class="modal-title-wrap">

                        <h3 class="modal-title"></h3>

                    </div>

                    <p class="address"></p>

                    <div id="blueprint-img">

                        <img class="lowImage"  src="">

                    </div>

                </div>

                <span id="levelBtn">

                    <button class="button button-level active" data-level="level1">Level 1</button>

                    <button class="button button-level"  data-level="level2">Level 2</button>

                    <button class="button button-level"  data-level="level3">Level 3</button>

                    <button class="button button-level"  data-level="level4">Level 4</button>

                </span>

            </div>

        </section>



        <!-- magnifying Modal -->

        <section  id="magnifyImg-modal" class="modal-wrap">  

            <button id="btn-rotate" class="btn-rotate"><img src="img/icons/icon-rotate.png" alt=""/></button>

            <button class="modal-close" id="close-btn-magnify">Close</button>

            <div class="text-center white">



                <embed type="image/svg+xml" id="high-image" src="">

            </div>

        </section>

    </div>



    <section id="project-title">

        <h3 id="level" class="white"></h3>

        <div id="title-options"><a href="javascript:void(0);" id="search"><h3 class="white"> </h3></a><a href="javascript:void(0);" id="info"></a><a href="javascript:void(0);" id="blueprint"></a></div>

    </section>

    <div id="project-slider-wrap">

        <div class="panaroma-loader-wrap">

            <div class="spinner">

                <div class="rect1"></div>

                <div class="rect2"></div>

                <div class="rect3"></div>

                <div class="rect4"></div>

                <div class="rect5"></div>

            </div>

        </div>

        <div id="project-slider">     





        </div>





        <div id="slider-controls" class="text-center">

            <h3 id="level-bottom" class="white"></h3>

            <button id="prev" class="slick-prev "></button>

            <div id="pagination"></div>

            <div class="pagination-count" id="paginationcount"></div>

            <button id="next" class="slick-next "></button>



        </div>

    </div>

    <div class="pagination-scroll"></div>

</section>

