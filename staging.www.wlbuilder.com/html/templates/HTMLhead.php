﻿<!DOCTYPE html>


<head>
    <title><?php echo $this->title;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1; maximum-scale=1.0; minimum-scale=1.0; user-scaleable=no;">
    <meta name="msapplication-square70x70logo" content="/smalltile.png" />
    <meta name="msapplication-square150x150logo" content="/mediumtile.png" />
    <meta name="msapplication-wide310x150logo" content="/widetile.png" />
    <meta name="msapplication-square310x310logo" content="/largetile.png" />
    <meta name="csrf-token" content="<?php echo $_SESSION['CSRF_TOKEN'];?>">
    <!-- Fav-icon -->
    <link rel="apple-touch-icon" sizes="57x57" href="img/fav-icon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/fav-icon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/fav-icon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/fav-icon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/fav-icon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/fav-icon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/fav-icon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/fav-icon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/fav-icon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/fav-icon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/fav-icon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/fav-icon/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!-- Slick Slider -->
    <link rel="stylesheet" type="text/css" href="js/vendor/slick/slick.css">
    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="css/common/reset.css">
    <link rel="stylesheet" type="text/css" href="css/common/main.css">
    <!--<link rel="stylesheet" type="text/css" href="css/home.css">-->
    
    <?php
foreach ($this->meta as $key => $value) {
	echo '	<meta name="'.$key.'" content="'.$value.'">'."\r\n";
}
?>
<?php
foreach ($this->og_meta as $key => $value) {
	echo '	<meta property="og:'.$key.'" content="'.$value.'">'."\r\n";
}
?>
<?php
foreach ($this->css as $css) {
	echo '	<link rel="stylesheet" href="'.SERVER_PATH.'/css/'.$css.'" data-page="'.$this->name.'" />'."\r\n";
}
?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://www.nicasiabank.com/assets/css/style-ie.css">
    <![endif]-->
</head>

<body>