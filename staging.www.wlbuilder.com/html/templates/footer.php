</div>

<!--<footer  class="background parallax" data-diff="100">-->

<footer  class="background" data-diff="100">

    <section class="footer-top">

        <div class="container">

            <img src="img/logo.svg" class="main-logo">

            <div class="find-us">

                <h4 class="white" >Find Us</h4>

                <p>52 Telok Blangah Road Unit #04-03</p>

                <p>Telok Blangah House</p>

                <p> Singapore 098829</p>

            </div>

            <div class="contact-us">

                <h4 class="white" >Contact Us</h4>

                <p><a href="#">sam@wlbuilder.com</a></p>

                <p>Tel +65 8222 6930 </p>

                <p>Mon to Sat - 10am to 6pm</p>

                <p> Sun & P.H. - 10am to 2pm</p>

            </div>

            <div class="social-links">

                <a href="https://www.facebook.com/profile.php?id=100014364493037" target="_blank" class="facebook nav-links"></a>
<!--
                <a href="#" class="pinterest nav-links"></a>-->

                <a href="https://www.instagram.com/wl_builder/" target="_blank" class="instagram nav-links"></a>

            </div>

            <div class="footer-logo hidden-mobile">

                <img src="img/footer-logo-1.png"><img src="img/footer-logo-2.png"><img src="img/footer-logo-3.png"><img src="img/footer-logo-4.png">

            </div>

        </div>

    </section>

    <section class="footer-bottom">

        <div class="container">

            <p>Copyright © 2016 WL Builder Pte Ltd. All rights reserved. <span> Site design by Tech+Art. </span></p><div class="footer-links">

                <!--<a id="termsOfUse" href="#">Terms Of Use</a>|-->
                <a id="privacyPolicy" href="#">Privacy Policy</a>

            </div>

        </div>

    </section>

    <section  id="termsOfUse-modal" class="modal-wrap">  

        <button class="footer-modal-close" id="close-btn-magnify">Close</button>

        <h3>Terms Of Use</h3>

        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>

        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>

        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>

        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>

    </section>

    <section  id="privacyPolicy-modal" class="modal-wrap">  

        <button class="footer-modal-close" id="close-btn-magnify">Close</button>

        <h3>Privacy Policy</h3>

        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>

        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>

        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>

        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>

    </section>

</footer>



<!--<section id="content-progress"></section>                -->





</div>



<!-- include vendor libraries -->

<script type="text/javascript" src="js/vendor/jquery-2.1.3.min.js"></script>



<!-- Scroll Navigation Bar -->

<script type="text/javascript">

    $(document).ready(function () {

        var actWidth = 0;

        var leftPos = 0;

        var url = document.URL

        var currentUrl = url.substr(url.lastIndexOf('/') + 1);

        //alert(currentUrl);

        $('.main-navigation').find('a').removeClass('active');

        if (currentUrl == 'project') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="project"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="project"]').width();

            leftPos = $('.main-navigation').find('a[data-for="project"]').position().left + 28;

        }else

        if (currentUrl == 'capabilities') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="capabilities"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="capabilities"]').width();

            leftPos = $('.main-navigation').find('a[data-for="capabilities"]').position().left + 28;

        }else

        if (currentUrl == 'profile') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="profile"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="profile"]').width();

            leftPos = $('.main-navigation').find('a[data-for="profile"]').position().left + 28;

        }else

        if (currentUrl == 'contact') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="contact"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="contact"]').width();

            leftPos = $('.main-navigation').find('a[data-for="contact"]').position().left + 28;



        }else

        if (currentUrl == 'project-map') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="project"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="project"]').width();

            leftPos = $('.main-navigation').find('a[data-for="project"]').position().left + 28;

        }else

        if (currentUrl == 'home' || currentUrl == '') {

            $('#main-header').removeClass('inner-header');

            $('#main-header img').css('display', 'none');

            $('.main-navigation').find('a[data-for="home"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="home"]').width();

            leftPos = $('.main-navigation').find('a[data-for="home"]').position().left + 28;

            console.log('first', actWidth + '-' + leftPos);

        }else{

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="home"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="home"]').width();

            leftPos = $('.main-navigation').find('a[data-for="home"]').position().left + 28;

            console.log('first', actWidth + '-' + leftPos);

        

        }

        

        actWidth = $(".main-navigation .active").width();

        leftPos = $(".main-navigation .active").position().left + 28;

        //console.log('second', actWidth + '-' + leftPos);

        $(".nav-scroll").css({

            "width": actWidth,

            "left": leftPos

        });

        $(".main-navigation a").on('click', function () {

            actWidth = $(this).width();

            leftPos = $(this).position().left + 28;

            console.log(leftPos);

        });

        $(".view-project-home").on('click', function () {

            

            actWidth = $('.main-navigation').find('a[data-for="project"]').width();

            leftPos = $('.main-navigation').find('a[data-for="project"]').position().left + 28;

            $(".nav-scroll").css({

                "width": actWidth,

                "left": leftPos

                });

        });

        $(".main-navigation a").mouseenter(function () {

            var changeWidth = $(this).width();

            var changePos = $(this).position().left + 28;

            $(".nav-scroll").css({

                "width": changeWidth,

                "left": changePos

            });

        });

        $(".main-navigation a").mouseleave(function () {

            $(".nav-scroll").css({

                "width": actWidth,

                "left": leftPos

            });

        });



    });



//    var resizeId;

//    $(window).resize(function () { alert("test");

//        doneResizing();

//    });

//

//    var doneResizing = function () {

//        //fixed header

//        var headHeight = $("header").height();

//        var winWidth = $(window).width();

//        var scrolled = $(document).scrollTop();

//        if (winWidth >= 1024) {

//            if (scrolled >= 1) { 

//                $('#main-header #title-logo').height('30px');

//                $("header").addClass("fixed-header");

//                $("body").css("padding-top", headHeight);

//            } else {

//                $('#main-header #title-logo').height('40px');

//                $("header").removeClass("fixed-header");

//                $("body").css("padding-top", 100);

//            }

//        } else {

//            $("body").css("padding-top", 0);

//        }

//

//        //nav scroll

//        var actWidth;

//        var leftPos;

//        actWidth = $(".main-navigation .active").width();

//        leftPos = $(".main-navigation .active").position().left + 28;

//        $(".nav-scroll").css({

//            "width": actWidth,

//            "left": leftPos,

//        });

//        $(".main-navigation a").mouseenter(function () {

//            var changeWidth = $(this).width();

//            var changePos = $(this).position().left + 28;

//            $(".nav-scroll").css({

//                "width": changeWidth,

//                "left": changePos

//            });

//        });

//        $(".main-navigation a").mouseleave(function () {

//            $(".nav-scroll").css({

//                "width": actWidth,

//                "left": leftPos

//            });

//        });

//    }

</script>



<!-- Panorama -->

<script type="text/javascript" src="js/vendor/panorama/three.js"></script>

<script type="text/javascript" src="js/vendor/jquery-easings.1.3.min.js"></script>

<script type="text/javascript" src="js/vendor/modernizr-3.3.1.min.js "></script>

<script type="text/javascript" src="js/vendor/preloader.js"></script>

<script type="text/javascript" src="js/vendor/hand-1.3.8.js"></script><!-- unify pointer events -->

<!-- include helpers -->

<script src="js/helpers/pointerclick.js"></script>

<script src="js/helpers/array.js"></script>

<script src="js/helpers/string.js"></script>

<script src="js/helpers/date.js"></script>

<script src="js/helpers/form.js"></script>

<script src="js/helpers/conversion.js"></script>

<script src="js/helpers/amd.js"></script>

<script src="js/vendor/footer.js"></script>





<!-- Include common items / modules -->



<script src="js/common/config.js"></script><!-- Config should always be included first -->

<script src="js/common/db.js"></script>

<script src="js/common/window.js"></script>

<script src="js/common/ajax-public-api.js"></script>

<script src="js/common/ajax-customer-api.js"></script>

<script type="text/javascript" src="js/common/ajaxpageswitcher.js"></script>

<!--<script src="js/login.js"></script>-->

<script src="js/navigation.js"></script>

<!-- Run browser checks then inits main app -->

<script src="js/common/checks.js"></script>

<script src="js/common/init.js"></script>

<script src="js/common/main.js"></script>







<!--**********Blueprint************-->

<script src="js/blueprint.js"></script>





<!-- Fixed Header -->

<script type="text/javascript">

    $(document).scroll(function () {

        var headHeight = $("header").height();

        var winWidth = $(window).width();

        var scrolled = $(document).scrollTop();

        if (winWidth >= 1024) {

            if (scrolled >= 100) {

                $('#main-header #title-logo').height('30px');

                $("header").addClass("fixed-header");

                $("body").css("padding-top", headHeight);

            } else {

                $('#main-header #title-logo').height('40px');

                $("header").removeClass("fixed-header");

                $("body").css("padding-top", 100);

            }

        }

    });

</script>









<?php

foreach ($this->js as $js) {

    if (startsWith($js, 'http://') || startsWith($js, 'https://') || startsWith($js, '//')) {

        echo '<script src="' . $js . '" data-page="' . $this->name . '"></script>' . "\r\n";

    } else {

        echo '<script src="' . SERVER_PATH . '/js/' . $js . '" data-page="' . $this->name . '"></script>' . "\r\n";

    }

}

?>







<!--<script>

  function initMap() {

    var mapDiv = document.getElementById('map');

    var map = new google.maps.Map(mapDiv, {

        center: {lat: 1.267042, lng: 103.823459},

        zoom: 16

    });

            var marker = new google.maps.Marker({

      position: {lat: 1.267042, lng: 103.823459},

      map: map,

    });



  }

</script>

<script async defer

    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBjXHokQ42avPBvmHaEymHBtE7ttqB2Ik0&callback=initMap">

</script>-->



</body></html>