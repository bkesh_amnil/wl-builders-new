<div class="main-container">
<header id="main-header" class=""> <a href="<?= SERVER_PATH ?>/home" class="nav-link home-nav" id="site-logo"><img id="title-logo" src="img/title-logo.svg" style="display: none"></a>
  <div class="nav-scroll hidden-mobile"></div>
  <nav class="text-center main-navigation" id="main-menu">
  <a href="<?= SERVER_PATH ?>/home" class="nav-link home-nav" data-for="home" >Home</a>
  <a href="<?= SERVER_PATH ?>/project" id="project_menu" class="nav-link" data-for="project" >Projects</a>
  <a href="<?= SERVER_PATH ?>/profile" class="nav-link" data-for="profile" >Profile</a>
  <a href="<?= SERVER_PATH ?>/capabilities" class="nav-link" data-for="capabilities" >Capabilities</a>
  <a href="http://blog.wlbuilder.com" class="nav-link" target="_parent" >Blog</a>
  <a href="<?= SERVER_PATH ?>/contact" class="nav-link" data-for="contact">Contact</a></nav>
</header>
<div id="content" data-page="<?php echo $this->name; ?>">
