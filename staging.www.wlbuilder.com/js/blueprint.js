if (!window.app) {
    window.app = {};
}

window.app.blueprint = (function ($) {
    var levelHtml = {};
    var projects;
    var data = new Array();
    var onDocumentReady = function (selector) {
        initEvent();
        initUI();
    },
            initEvent = function () {
                $(document).on('click', '#blueprint-modal .button-level', blueprintLevelClick);

            },
            initUI = function () {

                $("#blueprint").click(function (e) {
                    e.preventDefault();

                    $("#project").removeClass("modal-active");
                    $(".modal-wrap").removeClass("active");

                    $("#project").addClass("modal-active");
                    $("#blueprint-modal").addClass("active");
                    $('#levelBtn').empty();
                    var projectId = $('#blueprint-modal').attr('data-id');
                    if (projectId != 0) {
                        getLevelHtml(projectId);
                    }

                });
                $(".modal-close").click(function () {
                    $("#project").removeClass("modal-active");
                    $(".modal-wrap").removeClass("active");
                });


            },
            getLevelHtml = function (id) {
                projects = ((localDB.WLprojects).filter(function (index) {
                    return index["id"] == id;
                }));
                $.each(projects, function (i, v) {
                    var projectBluePrints = ((localDB.WLprojectBluePrints).filter(function (index) {
                        return index["projectID"] == v.id;
                    }));

//                    console.log($('#levelBtn').html());
                    if (projectBluePrints.length < 1) {
                        var thisBluePrint = '';
                        data = thisBluePrint;
                    } else {
                        var counter = 0;

                        $.each(projectBluePrints, function (ib, vb) {

                            var thisBluePrint = '';
                            var thisBluePrint = {};

                            thisBluePrint['levelProjectId'] = v.id;
                            thisBluePrint['level1'] = [];

                            var urlLow1 = '';
                            if (vb.urlLow1 != null) {
                                counter++;
                                urlLow1 = "img/projects/blueprints/" + vb.urlLow1;
                                var button = '<button class="button button-level"  data-level="level1">Level ' + counter + '</button>';
                                $('#levelBtn').append(button);
                            }
                            thisBluePrint['level1']['blueprint_img'] = urlLow1;
                            thisBluePrint['level1']['title'] = v.name;
                            thisBluePrint['level1']['address'] = v.location;
                            var urlHigh1 = '';
                            if (vb.urlHigh1 != null) {
                                urlHigh1 = "img/projects/blueprints/" + vb.urlHigh1;

                            }
                            thisBluePrint['level1']['high_img'] = urlHigh1;
                            thisBluePrint['level1']['img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();

                            thisBluePrint['level2'] = [];
                            var urlLow2 = '';
                            if (vb.urlLow2 != null) {
                                urlLow2 = "img/projects/blueprints/" + vb.urlLow2;
                                counter++;
                                var button = '<button class="button button-level"  data-level="level2">Level ' + counter + '</button>';
                                $('#levelBtn').append(button);
                            }
                            thisBluePrint['level2']['blueprint_img'] = urlLow2
                            thisBluePrint['level2']['title'] = v.name;
                            thisBluePrint['level2']['address'] = v.location;
                            var urlHigh2 = null;
                            if (vb.urlHigh2 != null) {
                                urlHigh2 = "img/projects/blueprints/" + vb.urlHigh2;
                            }
                            thisBluePrint['level2']['img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();
                            thisBluePrint['level2']['high_img'] = urlHigh2;

                            thisBluePrint['level3'] = [];
                            var urlLow3 = '';
                            if (vb.urlLow3 != null) {
                                urlLow3 = "img/projects/blueprints/" + vb.urlLow3;
                                counter++;
                                var button = '<button class="button button-level"  data-level="level3">Level ' + counter + '</button>';
                                $('#levelBtn').append(button);
                            }
                            thisBluePrint['level3']['blueprint_img'] = urlLow3;
                            thisBluePrint['level3']['title'] = v.name;
                            thisBluePrint['level3']['address'] = v.location;
                            var urlHigh3 = null;
                            if (vb.urlHigh3 != '') {
                                urlHigh3 = "img/projects/blueprints/" + vb.urlHigh3;
                            }
                            thisBluePrint['level3']['img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();
                            thisBluePrint['level3']['high_img'] = urlHigh3;

                            thisBluePrint['level4'] = [];
                            var urlLow4 = '';
                            if (vb.urlLow4 != null) {
                                urlLow4 = "img/projects/blueprints/" + vb.urlLow4;
                                counter++;
                                var button = '<button class="button button-level"  data-level="level4">Level ' + counter + '</button>';
                                $('#levelBtn').append(button);
                            }
                            thisBluePrint['level4']['blueprint_img'] = urlLow4;
                            thisBluePrint['level4']['title'] = v.name;
                            thisBluePrint['level4']['address'] = v.location;
                            var urlHigh4 = '';
                            if (vb.urlHigh4 != null) {
                                urlHigh4 = "img/projects/blueprints/" + vb.urlHigh4;
                            }
                            thisBluePrint['level4']['img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();
                            thisBluePrint['level4']['high_img'] = urlHigh4;
                            thisBluePrint['countLevel'] = counter;

                            $('#levelBtn .button-level:first').addClass('active');
                            data = thisBluePrint;
                        });

                    }

                });



            },
            blueprintLevelClick = function () {
                var $this = $(this);
                if (!$this.hasClass('active')) {
                    var projectId = $('#blueprint-modal').attr('data-id');
                    if (projectId != 0) {
                        var level = $this.attr('data-level');

                        updateBluePrint(level);
                        $('#blueprint-modal .button-level.active').removeClass('active');
                        $($this.addClass('active'));
                    }

                }

            },
            updateBluePrint = function (level) {
                var levelData = data;
                $('#magnifyImg-modal #high-image').attr('src', '');
                $('#magnifyImg-modal #high-image').attr('src', levelData[level].high_img);
                $('#blueprint-modal .modal-img').attr('src', '');
                $('#blueprint-modal .modal-img').attr('src', levelData[level].img);
                $('#blueprint-modal .modal-title').empty();
                $('#blueprint-modal .modal-title').html(levelData[level].title);
                $('#blueprint-modal .address').empty();
                $('#blueprint-modal .address').html(levelData[level].address);
                $('#blueprint-modal #blueprint-img').html('');
                $('#blueprint-modal #blueprint-img').html('<img src="' + levelData[level].blueprint_img + '">');

                //update project title
                $('#project-title #level').empty();
                $('#project-title #level').html("Level 1 :: Swimming Pool");
                $('#project-title #title-options h3').empty();
                $('#project-title #title-options h3').html(levelData[level].title);
            };
    return {
        onDocumentReady: onDocumentReady,
    };

}(jQuery));

jQuery(app.blueprint.onDocumentReady);
