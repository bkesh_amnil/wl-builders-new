/***
 * Module - app.Module.capibilites
 *
 * Provides behaviour for capibilites.php page
 *
 *
 ***/

/*****************************
 *      Execute on load      *
 *****************************/

(function(App, $) {
    "use strict";

    //var App = window.app = window.app || {};

    App.Module = App.Module = App.Module || {};
    App.Module.Capabilities = function() {
        var config = {};

        function resetEvent() {

        } // resetEvent()

        /***
         * Adds all necessary events: input, focus, pointerdown
         ***/
        function initEvent() {
            $(window.app).off("ready");
            $(window.app).on("ready", function() {
                $('#main-header').attr('class', 'inner-header');
                $('#main-header img').css('display', 'block');
//                initCapabilities();
                $(window.app).off("ready");
            });
        } // initEvent()

        /***
         * Initializes UI states
         ***/
        function initUI() {
            if (app.ready) {
                $('#main-header').attr('class', 'inner-header');
                $('#main-header img').css('display', 'block');
//                initCapabilities();
            }

        } // initUI()

        /***
         * Initialize Module
         ***/
        function init($config) {
            //resetEvent();
            initEvent();
            initUI();

        } // init()

        /***
         * Init capabilitiess
         ***/
        function initCapabilities() {
            var capabilityDetails = ((localDB.WLpartners).filter(function(e) {
                return e["displayPriority"] > 0;
            })).sortBy("displayPriority", false);

            $('#partner-content').html('');

            $.each(capabilityDetails, function(i, v) {
                if (v.hasImage == 1) {
                    var img_URL = IMAGES_URL + '/partners/' + v.id + '.png?time=' + $.now();
                } else {
                    var img_URL = "img/partner-logo.png";
                }
                var description = '';
                if (v.description != null || v.description != "") {
                    description = v.description;
                }
                //html contents for the partner page
                var content = '<article><img class="partner-logo" src="' + img_URL + '"  height="200" width="200"> <h4 class="red">' + v.name + '</h4><span>' + description + '</span></article>';
                //append to the page
                $('#partner-content').append(content);
            });

        } // initCapabilities()
        /***
         * Removes all events in this module: click, input, focus, pointerdown
         ***/

        return {
            init: init
        }
    }();
}(window.app = window.app || {}, jQuery));
// Init Capabilities Module
window.app.Module.Capabilities.init();