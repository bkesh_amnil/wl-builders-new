/***
 * This script defines functions to make AJAX calls to server's Public API.
 * - Each function correspond to one function in the API.
 * - Each function returns a jQuery Promise, so functions can be chained.
 *
 * Before use:
 * - PUBLIC_API_URL is defined
 * - ensure localDB & sessionDB are init
 ***/

var publicAPI = {

    /***
     * Gets the current session user
     ***/
    getSessionUser: function() {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getSessionUser',
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.user = data.user;
            sessionDB.save('user');
        });
    },   
    
    /***
     * Gets partners list from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedPartners: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedPartners',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.WLpartners.mergeUniqueByID(data.WLpartners);
            localDB.save('WLpartners');
        });
    },
    
    /***
     * Get a number of testimonials to display on the site. The highest priority ones will be returned.
     * @param: e.g. {count: 2}
     ***/
    getTestimonials: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getTestimonials',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.WLtestimonials.mergeUniqueByID(data.WLtestimonials);
            localDB.save('WLtestimonials');
        });
    },
    
     /***
     * Adds all project to the server
     * @param:
     ***/
    getUpdatedProjects: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedProjects',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.WLprojects.mergeUniqueByID(data.WLprojects);
            localDB.save('WLprojects');
        });
    },
    
    /***
     * Adds all project images to the server
     * @param:
     ***/
    getUpdatedProjectImages: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedProjectImages',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.WLprojectImages.mergeUniqueByID(data.WLprojectImages);
            localDB.save('WLprojectImages');
        });
    },
    
    /***
     * Adds all project blue print to the server
     * @param:
     ***/
    getUpdatedProjectBluePrints: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedProjectBluePrints',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.WLprojectBluePrints.mergeUniqueByID(data.WLprojectBluePrints);
            localDB.save('WLprojectBluePrints');
        });
    },

    /***
     * Gets fabrics from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedFabrics: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedFabrics',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.fabrics.mergeUniqueByID(data.fabrics);
            localDB.save('fabrics');
        });
    },

    /***
     * Gets collars from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedCollars: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedCollars',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.collars.mergeUniqueByID(data.collars);
            localDB.save('collars');
            /*
             * To Remove after all the collar buttons value are obtained from database
             */
            return;
            $(localDB.collars).each(function(i, collar) {
                if (collar.allowsSingleButton) {
                    localDB.collars[i].singleButtonX = 368,
                    localDB.collars[i].singleButtonY = 70,
                    localDB.collars[i].singleButtonR = 0,
                    localDB.collars[i].singleButtonSKEWX = 10,
                    localDB.collars[i].singleButtonSKEWY = 0
                }
                if (collar.allowsDoubleButton) {
                    localDB.collars[i].doubleTopButtonX = 368,
                    localDB.collars[i].doubleTopButtonY = 67,
                    localDB.collars[i].doubleTopButtonR = 0,
                    localDB.collars[i].doubleTopButtonS = 0.5,
                    localDB.collars[i].doubleTopButtonSKEWX = 10,
                    localDB.collars[i].doubleTopButtonSKEWY = 0

                    localDB.collars[i].doubleBottomBottomX = 368,
                    localDB.collars[i].doubleBottomButtonY = 77,
                    localDB.collars[i].doubleBottomButtonR = 0,
                    localDB.collars[i].doubleBottomButtonS = 0.5,
                    localDB.collars[i].doubleBottomButtonSKEWX = 10,
                    localDB.collars[i].doubleBottomButtonSKEWY = 0
                }
                if (collar.hasWingButtons) {
                    localDB.collars[i].wingLeftButtonX = 321,
                    localDB.collars[i].wingLeftButtonY = 94,
                    localDB.collars[i].wingLeftButtonR = 30,
                    localDB.collars[i].wingLeftButtonSKEWX = -13,
                    localDB.collars[i].wingLeftButtonSKEWY = 10

                    localDB.collars[i].wingRightButtonX = 405,
                    localDB.collars[i].wingRightButtonY = 94,
                    localDB.collars[i].wingRightButtonR = -10,
                    localDB.collars[i].wingRightButtonSKEWX = -6,
                    localDB.collars[i].wingRightButtonSKEWY = 10
                }
            });
        });
    },

    /***
     * Get cuffs from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedCuffs: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedCuffs',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.cuffs.mergeUniqueByID(data.cuffs);
            localDB.save('cuffs');
            return;
            $(localDB.cuffs).each(function(i, cuff) {
                if (cuff.allowsSingleButton) {
                    localDB.cuffs[i].singleButtonX = 207,
                    localDB.cuffs[i].singleButtonY = 832,
                    localDB.cuffs[i].singleButtonR = 42,
                    localDB.cuffs[i].singleButtonSKEWX = 0,
                    localDB.cuffs[i].singleButtonSKEWY = -13
                }
                if (cuff.allowsDoubleButton) {
                    localDB.cuffs[i].doubleTopButtonX = 207,
                    localDB.cuffs[i].doubleTopButtonY = 832,
                    localDB.cuffs[i].doubleTopButtonR = 42,
                    localDB.cuffs[i].doubleTopButtonSKEWX = 0,
                    localDB.cuffs[i].doubleTopButtonSKEWY = -13

                    localDB.cuffs[i].doubleBottomBottomX = 235,
                    localDB.cuffs[i].doubleBottomButtonY = 851,
                    localDB.cuffs[i].doubleBottomButtonR = 40,
                    localDB.cuffs[i].doubleBottomButtonSKEWX = 0,
                    localDB.cuffs[i].doubleBottomButtonSKEWY = 0
                }
            });
        });
    },

    /***
     * Get pockets from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedPockets: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedPockets',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.pockets.mergeUniqueByID(data.pockets);
            localDB.save('pockets');
        });
    },

    /***
     * Get plackets from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedPlackets: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedPlackets',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.plackets.mergeUniqueByID(data.plackets);
            localDB.save('plackets');
        });
    },

    /***
     * Get pleats from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedPleats: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedPleats',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.pleats.mergeUniqueByID(data.pleats);
            localDB.save('pleats');
        });
    },

    /***
     * Get buttons from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedButtons: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedButtons',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.buttons.mergeUniqueByID(data.buttons);
            localDB.save('buttons');
        });
    },

    /***
     * Get button holes from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedButtonHoles: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedButtonHoles',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.buttonHoles.mergeUniqueByID(data.buttonHoles);
            localDB.save('buttonHoles');
        });
    },

    /***
     * Get button threads from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedButtonThreads: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedButtonThreads',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.buttonThreads.mergeUniqueByID(data.buttonThreads);
            localDB.save('buttonThreads');
        });
    },

    /***
     * Get fabric colors from server that have
     * been updated since a certain time.
     * Use this function to sync the clientside
     * local DB with the server.
     * @param: fromTime
     ***/
    getUpdatedFabricColors: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedFabricColors',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.fabricColors.mergeUniqueByID(data.fabricColors);
            localDB.save('fabricColors');
        });
    },

    /***
     * Get fabric materials from server that have been updated since a certain time
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedFabricMaterials: function(formData) {
        return $.ajax({
            url: PUBLIC_API_URL + '/getUpdatedFabricMaterials',
            crossDomain: true,
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.fabricMaterials.mergeUniqueByID(data.fabricMaterials);
            localDB.save('fabricMaterials');
        });
    },

    /***
     * Get fabric patterns from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedFabricPatterns: function(formData) {
        return $.ajax({
            url: PUBLIC_API_URL + '/getUpdatedFabricPatterns',
            crossDomain: true,
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.fabricPatterns.mergeUniqueByID(data.fabricPatterns);
            localDB.save('fabricPatterns');
        });
    },

    /***
     * Get fabric weaves from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedFabricWeaves: function(formData) {
        return $.ajax({
            url: PUBLIC_API_URL + '/getUpdatedFabricWeaves',
            crossDomain: true,
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.fabricWeaves.mergeUniqueByID(data.fabricWeaves);
            localDB.save('fabricWeaves');
        });
    },

    /***
     * Get fabric cares from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedFabricCares: function(formData) {
        return $.ajax({
            url: PUBLIC_API_URL + '/getUpdatedFabricCares',
            crossDomain: true,
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.fabricCares.mergeUniqueByID(data.fabricCares);
            localDB.save('fabricCares');
        });
    },
    
    /***
     * Get a particular shirt design from server and stores on sessionDB.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getPartner: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/partner',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.partners.mergeUniqueByID([data.partners]);
            localDB.save('partners');
        });
    },

    /***
     * Get a particular shirt design from server and stores on sessionDB.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getShirtDesign: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getShirtDesign',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.shirtDesigns.mergeUniqueByID([data.shirtDesign]);
            localDB.save('shirtDesigns');
        });
    },

    /***
     * Get latest shirt designs from server.
     * @param: e.g. {count: 10'}
     ***/
    getLatestShirtDesigns: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getLatestShirtDesigns',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.latestShirtDesigns = data.shirtDesigns;
            localDB.save('latestShirtDesigns');
        });
    },

    /***
     * Get shirt designs from server that have been updated since a certain time.
     * @param: e.g. {fromTime: 1234567890123'}
     ***/
    getUpdatedShirtDesigns: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getUpdatedShirtDesigns',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.shirtDesigns.mergeUniqueByID(data.shirtDesigns);
            localDB.save('shirtDesigns');
        });
    },

    /***
     * Get the announcements to be displayed. The highest priority ones will be returned.
     * @param: e.g. {count: 2'}
     ***/
    getAnnouncements: function(formData) {
        return $.ajax({
            url: PUBLIC_API_URL + '/getAnnouncements',
            crossDomain: true,
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.announcements.mergeUniqueByID(data.announcements);
            localDB.save('announcements');
        });
    },

    

    /***
     * Get all discount rules that apply to the site.
     * @param:
     ***/
    getDiscountRules: function(formData) {
        return $.ajax({
            url: PUBLIC_API_URL + '/getDiscountRules',
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.discountRules = data.discountRules;
            sessionDB.save('discountRules');
        });
    },

    /***
     * Adds a new shirt design to the server
     * @param:
     ***/
    addShirtDesign: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: PUBLIC_API_URL + '/addShirtDesign',
            data: formData,
            contentType: false,
            processData: false,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            if (localDB.shirtDesigns == null) {
                localDB.shirtDesigns = [];
            }
            localDB.shirtDesigns.pushUniqueByID(data.shirtDesign);
            localDB.save('shirtDesigns');
        });
    },

    /***
     * Adds a new tailor kit design to the server.
     * To be used during �Add to Cart�
     * Must be logged in.
     ***/
    addTailorKitDesign: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: PUBLIC_API_URL + '/addTailorKitDesign',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.tailorKitDesigns.pushUniqueByID(data.tailorKitDesign);
            sessionDB.save('tailorKitDesigns');
        });
    },

    /***
     * Gets a tailor kit design by its ID.
     ***/
    getTailorKitDesign: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            url: PUBLIC_API_URL + '/getTailorKitDesign',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            sessionDB.tailorKitDesigns.mergeUniqueByID([data.tailorKitDesign]);
            sessionDB.save('tailorKitDesigns');
        });
    },

    /***
     * Sends email to Este Bartin.
     * Valid types: [Make Appointment, Feedback, Order Enquiry]
     ***/
    sendContactMessage: function(formData) {
        return $.ajax({
            xhrFields: {
                'withCredentials': true
            },
            crossDomain: true,
            type: "POST",
            url: PUBLIC_API_URL + '/sendContactMessage',
            data: formData,
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {

        });
    },

    /***
     * Gets countries list from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedCountries: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedCountries',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.countries.mergeUniqueByID(data.countries);
            localDB.save('countries');
        });
    },

    /***
     * Gets states list from server that have been updated since a certain time.
     * @param: e.g. {fromTime: '1234567890'}
     ***/
    getUpdatedStates: function(formData) {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getUpdatedStates',
            dataType: "json",
            data: formData
        }).done(function(data, textStatus, jqXHR) {
            localDB.states.mergeUniqueByID(data.states);
            localDB.save('states');
        });
    },

    /***
     * Gets shipping costs
     ***/
    getDeliveryRates: function() {
        return $.ajax({
            crossDomain: true,
            xhrFields: {
                'withCredentials': true
            },
            url: PUBLIC_API_URL + '/getDeliveryRates',
            dataType: "json"
        }).done(function(data, textStatus, jqXHR) {
            localDB.deliveryRates = data.rates;
            localDB.save("deliveryRates");
        });
    }
}