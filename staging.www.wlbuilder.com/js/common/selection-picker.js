/***
 * Class - app.Controls.SelectionPicks
 *
 * Renders Selection Picks
 *
 * Dependencies
 *  - jQuery
 *
 * Before use
 *  - ensure document is ready
 *
 * Init instructions:
 *  - app.Controls.SelectionPicks.init(config);
 *
 * Usage examples:
 * var materialSelectionPicker = new window.app.Controls.SelectionPicker();
 * var fabricMaterialsConfig = {
 *               el: "fabric-materials",
 *               selectionOptions: fabricMaterials
 *           }
 *            materialSelectionPicker.init(fabricMaterialsConfig);
 * On load, performs:
 * -renders the selection list on the main conatiner
 ***/

(function(app, $) {
    "use strict";

    var App = window.app = window.app || {};
    App.Controls = App.Controls = App.Controls || {};
    App.Controls.SelectionPicker = function() {
        var
            config = {}, // Inital Configuration
            mainContainer = "", // Renders the view on this container
            selectionOptions = []; // Selected elements in an array will be placed here 
        /***
         * Removes all events in this module: click, input, focus, pointerclick
         ***/
        function resetEvent() {
            mainContainer.off('click', 'a');
            mainContainer.on('click', 'a', function(e) {
                e.preventDefault();
            });
            mainContainer.off('pointerclick', 'a');
        }
        /***
         * Adds all necessary events:
         ***/
        function initEvent() {
            // When anchor tag of main container gets clicked this event gets trigger 
            listenForPointerClick(mainContainer, "a");
            mainContainer.on('pointerclick', 'a', function(e) {
                var element = $(this);
                mainContainer.trigger("optionSelected", [element.attr("data-id"), element.attr("data-name")]);
            });
        }
        /***
         * Initializes UI states
         ***/
        function initUI() {
            var liElement = $("<li>");
            var element = $("<a>");
            //element.attr("href", "#");
            element.attr("data-id", "");
            element.attr("data-name", "All");
            element.text("All");
            mainContainer.append(liElement);
            liElement.append(element);
            $.each(selectionOptions, function(i, val) {
                liElement = $("<li>");
                element = $("<a>");
                //element.attr("href", "#");
                element.attr("data-id", val.id);
                element.attr("data-name", val.name);
                element.text(val.name);
                mainContainer.append(liElement);
                liElement.append(element);
            });
        }
        /***
         * Initializes selection
         ***/
        function init($config) {
            config = config || {};
            config = $.extend({}, config, $config);
            config.el = config.el || "";
            mainContainer = $(["#", config.el].join(""));
            selectionOptions = config.selectionOptions;
            resetEvent();
            initEvent();
            initUI();
        };
        return {
            init: init

        }
    };
}(window.app = window.app || {}, jQuery))