/***
 * Module - app.Module.project map image
 *
 * Provides behaviour for project-map.php page
 *
 * Dependencies
 *  - jQuery
 *
 * Before use
 *  - ensure document is ready
 *  - localDB is init
 *
 * Init instructions:
 *  - app.Module.projectMap.init(options);
 *
 *
 ***/

/*****************************
 *      Execute on load      *
 *****************************/

(function (App, $) {
    "use strict";

    //var App = window.app = window.app || {};

    App.Module = App.Module = App.Module || {};
    App.Module.ContactUs = function () {
        var config = {};
      
        /***
         * Adds all necessary events: input, focus, pointerdown
         ***/
        function initEvent() {
            loadGoogleMapScript();
            $('#main-header').attr('class', 'inner-header');
            $('#main-header img').css('display', 'block');
        } // initEvent()

        function loadGoogleMapScript() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBjXHokQ42avPBvmHaEymHBtE7ttqB2Ik0&callback=window.app.Module.ContactUs.init_map';
            document.body.appendChild(script);
        }
        
        /***
         * Initializes UI states
         ***/
        function initUI() {
           // loadGoogleMapScript();
        } // initUI()
        

        function init_map() {
            var mapDiv = document.getElementById('map');
            var map = new google.maps.Map(mapDiv, {
                center: { lat: 1.266356, lng: 103.823496 },
                scrollwheel:  false,
                zoom: 18
            });
            var marker = new google.maps.Marker({
                position: { lat: 1.266356, lng: 103.823496 },
                map: map
            });
             $('#main-header').attr('class', 'inner-header');
            $('#main-header img,#title-logo').css('display', 'inline-block');
        }

        /***
         * Initialize Module
         ***/
        function init() {
            //resetEvent();
            initEvent();
            initUI();
        } // init()

        /***
         * Init partners
         ***/
       
        /***
         * Removes all events in this module: click, input, focus, pointerdown
         ***/

        return {
            init: init,
            init_map: init_map
        };
    }();
}(window.app = window.app || {}, jQuery));
window.app.Module.ContactUs.init();
