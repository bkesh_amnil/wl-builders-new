/*** * Module - app.Module.Home 
 * * * Provides behaviour for home.php page 
 * * * Dependencies 
 * *  - jQuery 
 * *  - app.Controls.Slider 
 * *  - app.Controls.TestimonialSlider 
 * * * Before use 
 * *  - ensure document is ready 
 * *  - localDB is init 
 * * * Init instructions: 
 * *  - app.Module.Home.init(options); 
/***************************** *      Execute on load      * *****************************/
$.when(loadScripts(["js/vendor/slick/slick.min.js", "js/inc/home_includes.js"])).done(function () {
    (function (app, $) {
        "use strict";
        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        app.Module.Home = function () {
            var backgroundImages = [],
                sliderImages = [],
                config = {};
            /***             * Removes all events in this module: click, input, focus, pointerdown             ***/
            function resetEvent() { } // resetEvent()            
            /***             
             * Adds all necessary events: input, focus, pointerdown
             ***/
            function initEvent() {
                $('#main-header').attr('class', '');
                $('#main-header img').css('display', 'none');
                $('.main-navigation a').removeClass('active');
                $('.main-navigation .home-nav').addClass('active');
                $(window.app).off("ready");
                $(window.app).on("ready", function () {
                    initTestimonials();
                    initProjectBoundaryImage();
                    initProjectPanoramaImage();
                    $(".item").off("click", '.view-project-home');
                    $(".item").on("click", '.view-project-home', function (e) {
                        e.preventDefault();
                    });
                    $(".item").on('click', '.view-project-home', function () {
                        var actWidth = '';
                        var leftPos = '';
                        actWidth = $('.main-navigation').find('a[data-for="project"]').width();
                        leftPos = $('.main-navigation').find('a[data-for="project"]').position().left + 28;
                        $(".nav-scroll").css({
                            "width": actWidth,
                            "left": leftPos
                        });
                    });
                    $(window.app).off("ready");
                });
            } // initEvent()
            
            /***             * Initializes UI states             ***/
            function initUI() {
                if (window.app.ready) {
                    //alert("test");//                    
                    $('#main-header').attr('class', '');
                    $('.main-navigation a').removeClass('active');
                    $('.main-navigation .home-nav').addClass('active');
                    $('#main-header img').css('display', 'none');
                    initTestimonials();
                    initProjectBoundaryImage();
                    initProjectPanoramaImage();
                }
            } // initUI()
            
            /***             * Initialize Module             ***/
            function init($config) {
                config = config || {};
                config = $.extend({}, config, $config);
                //                backgroundImages = config.backgroundImages;
                //                sliderImages = config.sliderImages;
                //                resetEvent();                
                initEvent();
                initUI();
            } // init()
            
            /***             * Init testimonials             ***/
            function initTestimonials() {
                var testimonials = ((localDB.WLtestimonials).filter(function (e) {
                    return e["displayPriority"] > 0;
                }));
                $('#testimonials').html('');
                var count = 0;
                $.each(testimonials, function (i, v) {
                    if (count < 3) {
                        if (v.hasImage == 1) {
                            var img_URL = IMAGES_URL + '/testimonials/' + v.id + '.png?time=' + $.now();
                        } else {
                            var img_URL = "img/avatar-2.png";
                        }
                        var content = '<img data-name="just testing" class="avatar" src="' + img_URL + '" /><h2 class="header-title red">' + v.name + '</h2><p class="short-detail red">"' + v.message + '"</p>';
                        
                        $('#testimonials').append(content);
                    }
                    count++;
                });
            } // initTestimonials()
            
            /***             * Init project boundary images             ***/
            function initProjectBoundaryImage() {
                var projects = ((localDB.WLprojects).filter(function (e) {
                    return e["isFeaturedInStaticSection"] > 0 && e["displayPriority"] > 0
                })).sortBy("displayPriority", false);
                $('#boundaries-slider').html('');
                var count = 1;
                $.each(projects, function (i, v) {
                    var projectImages = ((localDB.WLprojectImages).filter(function (e) {
                        return e["isPanorama"] == 0 && e["isStaticSectionFeaturedImage"] == 1 && e["projectID"] == v.id && e["displayPriority"] > 0
                    })).sortBy("lastUpdateTime", false);
                    if (projectImages.length > 0) {
                        if (count <= 5) {
                            var items = '';
                            items += '<div class="item">';
                            items += '<div class="hover-animation">';
                            for (var i = 0; i < 2; i++) {
                                items += '<div class="lines">';
                                for (var j = 0; j < 7; j++) {
                                    items += '<img src="img/line.svg"  alt="svg hover lines">';
                                }
                                items += '</div>';
                            }
                            items += '</div>';
                            items += '<img src="img/projects/images/' + projectImages[0].imgUrl + '?time=' + $.now() + '" alt="' + v.name + '">';
                            items += '<a href="javascript:void();" class="nav-link view-project-home" data-for="project' + '#' + v.name.replace(/\s+/g, "_") + '" ><h2>' + v.name + '</h2></a>';
                            items += '</div>';
                            $('#boundaries-slider').append(items);
                            count++;
                        }
                    }
                });
                if (count <= 2) {
                } else {
                    $('.pagination-scroll').css('display', 'inline-block');
                }
                if (projects.length > 0) {
                    $('#boundaries-slider').slick({
                        dots: true,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        fade: false,
                        cssEase: 'linear',
                        responsive: [{
                            breakpoint: 1024,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        }, {
                            breakpoint: 600,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: false
                            }
                        }, {
                            breakpoint: 480,
                            settings: {
                                slidesToShow: 1,
                                slidesToScroll: 1,
                                arrows: false
                            }
                        } // You can unslick at a given breakpoint now by adding: // settings: "unslick"                            
                          // instead of a settings object                        
                        ]
                    });
                    $('#boundaries-slider').on('swipe', function () {
                        var leftPosPage;
                        var lleftPosPage;
                        var totalLeft;
                        leftPosPage = $(".slick-dots .slick-active").position().left;
                        lleftPosPage = $("#boundaries-slider-wrap").position().left;
                        totalLeft = leftPosPage + lleftPosPage + 15;
                        $(".pagination-scroll").css('left', totalLeft);
                    });
                    var leftPosPage = 0;
                    var lleftPosPage = 0;
                    var totalLeft;
                    if ($(".slick-dots .slick-active").length > 0) {
                        leftPosPage = $(".slick-dots .slick-active").position().left;
                    }
                    lleftPosPage = $("#boundaries-slider-wrap").position().left;
                    totalLeft = leftPosPage + lleftPosPage + 15;
                    $(".pagination-scroll").css('left', totalLeft);
                    $(".slick-dots button").click(function () {
                        var changePosPage = $(this).position().left + lleftPosPage + 15;
                        $(".pagination-scroll").css('left', changePosPage);
                    });
                    $(".slick-prev").click(function () {
                        slidePagination();
                    });
                    $(".slick-next").click(function () {
                        slidePagination();
                    });
                }

                function slidePagination() {
                    var leftPosPage;
                    var lleftPosPage;
                    var totalLeft;
                    leftPosPage = $(".slick-dots .slick-active").position().left;
                    lleftPosPage = $("#boundaries-slider-wrap").position().left;
                    totalLeft = leftPosPage + lleftPosPage + 15;
                    $(".pagination-scroll").css('left', totalLeft);
                }
            } // initProjectBoundaryImage()
            
            /***             * Init project Panaroma images             ***/
            function initProjectPanoramaImage() {
                var onetimeCheck = 0;
                var projects = ((localDB.WLprojects).filter(function (e) {
                    return e["isFeaturedInPanoramicSection"] > 0 && e["displayPriority"] > 0
                })).sortBy("displayPriority", false);
                $.each(projects, function (i, v) {
                    var projectImages = ((localDB.WLprojectImages).filter(function (e) {
                        return e["isPanorama"] == 1 && e["isPanoramicSectionFeaturedImage"] == 1 && e["projectID"] == v.id && e["displayPriority"] > 0
                    })).sortBy("displayPriority", false);
                    if (projectImages.length > 0 && onetimeCheck == 0) {
                        $('#moonbeam .header-title').empty();
                        $('#moonbeam .header-title').html(v.name);
                        $('#moonbeam .short-detail').empty();
                        $('#moonbeam .short-detail').html(v.description);
                        onetimeCheck++;
                    }
                });
            } // initProjectPanoramaImage()
            
            return {
                init: init
            };
        }();
    }(window.app = window.app || {}, jQuery));
    // Init About Module    
    window.app.Module.Home.init();
});