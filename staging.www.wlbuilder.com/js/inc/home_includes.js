$(document).on("click", ".scroll-down", function(e) {
    e.preventDefault();
    var winWidth;
    winWidth = $(window).width();
    if (winWidth > 1024) {
        $('html, body').animate({
            scrollTop: $('#boundaries').offset().top - 120
        }, 700);
        return false;
    } else {
        $('html, body').animate({
            scrollTop: $('#boundaries').offset().top
        }, 700);
        return false;
    }

});

(function() {
    var winWidth;
    winWidth = $(window).width();
    if (winWidth > 1024) {
        /* detect touch */
        if ("ontouchstart" in window) {
            document.documentElement.className = document.documentElement.className + " touch";
        }
        if (!$("html").hasClass("touch")) {
            /* background fix */
            $(".parallax").css("background-attachment", "fixed");
        }

        /* fix vertical when not overflow
         call fullscreenFix() if .fullscreen content changes */
        function fullscreenFix() {
            var h = $('body').height();
            // set .fullscreen height
            $(".content-b").each(function(i) {
                if ($(this).innerHeight() > h) {
                    $(this).closest(".fullscreen").addClass("overflow");
                }
            });
        }
        $(window).resize(fullscreenFix);
        fullscreenFix();

        /* resize background images */
        function backgroundResize() {
            var windowH = $(window).height();
            $(".background").each(function(i) {
                var path = $(this);
                // variables
                var contW = path.width();
                var contH = path.height();
                var imgW = path.attr("data-img-width");
                var imgH = path.attr("data-img-height");
                var ratio = imgW / imgH;
                // overflowing difference
                var diff = parseFloat(path.attr("data-diff"));
                diff = diff ? diff : 0;
                // remaining height to have fullscreen image only on parallax
                var remainingH = 0;
                if (path.hasClass("parallax") && !$("html").hasClass("touch")) {
                    var maxH = contH > windowH ? contH : windowH;
                    remainingH = windowH - contH;
                }
                // set img values depending on cont
                imgH = contH + remainingH + diff;
                imgW = imgH * ratio;
                // fix when too large
                if (contW > imgW) {
                    imgW = contW;
                    imgH = imgW / ratio;
                }
                //
                path.data("resized-imgW", imgW);
                path.data("resized-imgH", imgH);
                path.css("background-size", imgW + "px " + imgH + "px");
            });
        }
        $(window).resize(backgroundResize);
        $(window).focus(backgroundResize);
        backgroundResize();

        /* set parallax background-position */
        function parallaxPosition(e) {
            var heightWindow = $(window).height();
            var topWindow = $(window).scrollTop();
            var bottomWindow = topWindow + heightWindow;
            var currentWindow = (topWindow + bottomWindow) / 2;
            $(".parallax").each(function(i) {
                var path = $(this);
                var height = path.height();
                var top = path.offset().top;
                var bottom = top + height;
                // only when in range
                if (bottomWindow > top && topWindow < bottom) {
                    var imgW = path.data("resized-imgW");
                    var imgH = path.data("resized-imgH");
                    // min when image touch top of window
                    var min = 0;
                    // max when image touch bottom of window
                    var max = -imgH + heightWindow;
                    // overflow changes parallax
                    var overflowH = height < heightWindow ? imgH - height : imgH - heightWindow; // fix height on overflow
                    top = top - overflowH;
                    bottom = bottom + overflowH;
                    // value with linear interpolation
                    var value = min + (max - min) * (currentWindow - top) / (bottom - top);
                    // set background-position
                    var orizontalPosition = path.attr("data-oriz-pos");
                    orizontalPosition = orizontalPosition ? orizontalPosition : "50%";
                    $(this).css("background-position", orizontalPosition + " " + value + "px");
                }
            });
        }
        if (!$("html").hasClass("touch")) {
            $(window).resize(parallaxPosition);
            //$(window).focus(parallaxPosition);
            $(window).scroll(parallaxPosition);
            parallaxPosition();
        }
    }

//Panorama

    var dom_el = document.getElementById('moonbeam-panorama');
    var dom_orig = document.getElementById('orig');

// Settings
    var projects = ((localDB.WLprojects).filter(function(index) {
        return index["isFeaturedInPanoramicSection"] > 0 && index["displayPriority"] > 0
    })).sortBy("displayPriority", false);

    var onetimeCheck = 0;
    var img_file = '';

    $.each(projects, function(i, v) {
        var projectImages = ((localDB.WLprojectImages).filter(function(index) {
            return index["isPanorama"] == 1 && index["isPanoramicSectionFeaturedImage"] == 1 && index["projectID"] == v.id && index["displayPriority"] > 0
        }));

        if (projectImages.length > 0 && onetimeCheck == 0) {
            img_file = 'img/projects/images/' + projectImages[0].imgUrl;
            onetimeCheck++;
        }

    });

//img_file = 'img/panoramas/18-Mount-Sinai-Walk/pool-area.jpg';
    var scaleX = -1;
    var scaleY = 1;
    var scaleZ = 1;


// Settings
    var MIN_FOV = 10; // how much can user zoom in?
    var MAX_FOV = 120;
    var FOV = 97;
    var WIND_X_SPEED = .1;
    var WIND_Y_SPEED = 0;
    var DRAG_SPEED = 0.1;
    var ZOOM_SENSITIVITY = 0.05;
    var NEAR_CLIPPING_PLANE = 0.01; // 1
    var FAR_CLIPPING_PLANE = 1000; // 1100
    var ROOM_RADIUS = 0;
    var WIDTH_SEGMENTS = 200; // warning: large numbers cause large

// Context
    var scene, camera = '', renderer;
    var isUserInteracting = false;
    var onMouseDownMouseX = 0,
            onMouseDownMouseY = 0;
    var lon = 0,
            onMouseDownLon = 0;
    var lat = 0,
            onMouseDownLat = 0;
    var phi = 0,
            theta = 0;

    init(dom_el, img_file);
    animate();

    function init(dom_el, img_file) {
        // Derive aspect ratio
        var width = dom_el.offsetWidth;
        var height = dom_el.offsetHeight;
        var aspect_ratio = width / height;

        // Create the scene, camera and renderer
        scene = new THREE.Scene();
        camera = new THREE.PerspectiveCamera(FOV, aspect_ratio, NEAR_CLIPPING_PLANE, FAR_CLIPPING_PLANE);
        camera.target = new THREE.Vector3(0, 0, 0);
        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(width, height);
        dom_el.innerHTML = '';
        dom_el.appendChild(renderer.domElement);

        // Set Map
        var geometry = new THREE.SphereGeometry(ROOM_RADIUS, WIDTH_SEGMENTS, WIDTH_SEGMENTS / aspect_ratio);
        geometry.scale(scaleX, scaleY, scaleZ);
        var material = new THREE.MeshBasicMaterial({
            map: THREE.ImageUtils.loadTexture(img_file)
        });
        scene.add(new THREE.Mesh(geometry, material));

        // Bindings
        //dom_el.removeEventListener('mousedown', onDocumentMouseDown, false );
        dom_el.addEventListener('mousedown', onDocumentMouseDown, false);
        dom_el.addEventListener('mousemove', onDocumentMouseMove, false);
        document.addEventListener('mouseup', onDocumentMouseUp, false);
        dom_el.addEventListener('wheel', onDocumentMouseWheel, false);
        //dom_el.addEventListener( 'MozMousePixelScroll', onDocumentMouseWheel, false);
    } // init()

    function onDocumentMouseDown(event) {
        event.preventDefault();
        isUserInteracting = true;
        onPointerDownPointerX = event.clientX;
        onPointerDownPointerY = event.clientY;

        onPointerDownLon = lon;
        onPointerDownLat = lat;
    }

    function onDocumentMouseMove(event) {
        if (isUserInteracting) {
            event.preventDefault();
            lon = (onPointerDownPointerX - event.clientX) * DRAG_SPEED + onPointerDownLon;
            lat = (event.clientY - onPointerDownPointerY) * DRAG_SPEED + onPointerDownLat;
        }
    }

    function onDocumentMouseUp(event) {
        isUserInteracting = false;
    }

    function onDocumentMouseWheel(event) {
        event.preventDefault();
        console.log(getNormalizedWheelSpeed(event) + ' deltaY: ' + event.deltaY + ' wheelDeltaY: ' + event.wheelDeltaY + ' Mode: ' + event.deltaMode);

        /*
         if (event.wheelDeltaY) {
         camera.fov -= event.wheelDeltaY * ZOOM_SENSITIVITY; // Magic number 40 from MDN (https://developer.mozilla.org/en-US/docs/Web/Events/wheel)
         } // WebKit
         else if (event.wheelDelta) {
         camera.fov -= event.wheelDelta / 40 * ZOOM_SENSITIVITY;
         } // Opera / Explorer 9
         else if (event.deltaY) {
         camera.fov -= event.deltaY / 40 * ZOOM_SENSITIVITY;
         }
         else if (event.detail) {
         camera.fov += event.detail * ZOOM_SENSITIVITY;
         } // Firefox
         */

        camera.fov += getNormalizedWheelSpeed(event) * ZOOM_SENSITIVITY;

        // FOV min/max
        if (camera.fov < MIN_FOV) {
            camera.fov = MIN_FOV;
        }
        if (camera.fov > MAX_FOV) {
            camera.fov = MAX_FOV;
        }

        camera.updateProjectionMatrix();
    }

    function getNormalizedWheelSpeed(event) {
        if (event.type === 'wheel') {
            if (event.deltaMode === 1) {
                return event.deltaY * 40;
            } else {
                return event.deltaY;
            }
        } else if (event.type === 'mousewheel') {
            return event.wheelDelta / -40;
        }
    } // getNormalizedWheelSpeed()

    function animate() {
        requestAnimationFrame(animate);
        update();
    }

    function update() {
        if (!isUserInteracting) {
            lat += WIND_Y_SPEED;
            lon += WIND_X_SPEED;
        }

        lat = Math.max(-85, Math.min(85, lat));
        phi = THREE.Math.degToRad(90 - lat);
        theta = THREE.Math.degToRad(lon);

        camera.target.x = 500 * Math.sin(phi) * Math.cos(theta);
        camera.target.y = 500 * Math.cos(phi);
        camera.target.z = 500 * Math.sin(phi) * Math.sin(theta);

        camera.lookAt(camera.target);

        /*
         // distortion
         camera.position.copy( camera.target ).negate();
         */

        renderer.render(scene, camera);
    }
}());
