if (!window.app) {

    window.app = {};

}



window.app.projectinclude = (function ($) {

    //alert("projectInclude");return false;

    //# for panorama 
    var isImageNameInHashHandled = false;
    var panorama_img = [];

    var dom_el = document.getElementById('project-panorama');

    var dom_orig = document.getElementById('orig');



    // Settings

    /// var img_file = 'img/panoramas/18-Mount-Sinai-Walk/pool-area.jpg';

    var scaleX = -1;

    var scaleY = 1;

    var scaleZ = 1;



    // Settings

    var MIN_FOV = 0; // how much can user zoom in?

    var MAX_FOV = 120;

    var FOV = 97;

    var WIND_X_SPEED = .07;

    var WIND_Y_SPEED = 0;

    var DRAG_SPEED = 0.1;

    var ZOOM_SENSITIVITY = 0;

    var NEAR_CLIPPING_PLANE = 0.01; // 1

    var FAR_CLIPPING_PLANE = 1000; // 1100

    var ROOM_RADIUS = 0;

    var WIDTH_SEGMENTS = 200; // warning: large numbers cause large



    // Context

    var scene, camera, renderer, mesh, manager;

    var isUserInteracting = false;

    var onMouseDownMouseX = 0,

            onMouseDownMouseY = 0;

    var lon = 0,

            onMouseDownLon = 0;

    var lat = 0,

            onMouseDownLat = 0;

    var phi = 0,

            theta = 0;



    var requestAnimationFrameId;

    var count = 0;

    var data = {};

    var projects;



    var projectImages;



    var projectBluePrints;

    var sliderUpdateCounter = 0;
    var angle = 0; 
    




    /***

     * Init testimonials

     ***/

    function initProject() {

        var no = 1;

        //listing all the projects on the project list 

        $('#listing').empty();

        projects = ((localDB.WLprojects).filter(function (index) {

            return index["displayPriority"] > 0;

        })).sortBy("displayPriority", false);

	console.log('projectinitProject',projects);

        $.each(projects, function (i, v) {

            var project_content = '<a href="javascript:void(0);" class="project" id="' + v.id + '" data-projectId="' + v.id + '"><blockquote><h5>' + v.name + '</h5><p>(' + v.location + ' )</p></blockquote></a>';

            $('#listing').append(project_content);



            no++;

        });

	//alert($('#project_menu').attr('href'));

        //if project sheared on the 
        

        var fbProjectName = window.location.hash.substr(1);

        if (fbProjectName != '') {
            // expect hash in projectname#imagename
            var projectName = "";
            var hash = decodeURIComponent(fbProjectName.replace(/_/g, ' ')).split('#');
            projectName = hash[0];
            
            //get the id of the projectname

            projectData = ((localDB.WLprojects).filter(function (index) {

                return index["name"] == projectName;

            })).sortBy("displayPriority", false);

            if (projectData.length > 0) {

                var pid = projectData[0].id;

                console.log(pid);

                if ($("a[id*='" + pid + "']").html() != undefined && $("a[id*='" + pid + "']").html() != '') {

                    $("a[id*='" + pid + "']").trigger('click', {
                        fakeTrigger : true
                    });

                } else {

                    $('#search-modal .project:first').trigger('click', {
                        fakeTrigger: true
                    });

                }

            }



        } else {

            $('#search-modal .project:first').trigger('click');

        }



    } // initTestimonials()





    var onDocumentReady = function (selector) {



        //slick_index = parent elemnt of canvas and we just navigate to that index with different image i.e for now 

        var projects = ((localDB.WLprojects).filter(function (index) {

            return index["displayPriority"] > 0;

        })).sortBy("displayPriority", false);

        //console.log('projects',projects)

        $.each(projects, function (i, v) {

            count++;

            if (count == 1) {

                projectImages = ((localDB.WLprojectImages).filter(function (index) {

                    return index["isPanorama"] == 1 && index["isStaticSectionFeaturedImage"] == 0 && index["isPanoramicSectionFeaturedImage"] == 0 && index["projectID"] == v.id;

                }));

                console.log("panoromaimg", projectImages);

                $.each(projectImages, function (ii, vi) {

                    var src = 'img/projects/images/' + vi.imgUrl + '.jpg?time=' + $.now();



                    panorama_img.push({src: src});



                });

            }





        });



        initEvent();

        initUI();

    },

            initEvent = function () {

                $('#main-header').attr('class', 'inner-header');

                $('#main-header img').css('display', 'block');

                $(document).on('click', '#search-modal .project', changeProject);

                $(document).off('click', '#slider-controls .panorama-nav');

                $(document).on('beforeChange', '#slider-controls .panorama-nav', navigateToSlider);

                $(document).on('beforeChange', '#slider-controls .staticImg', navigateToSlider);

                



                $(document).on('click', '#next,#prev,#slider-controls .panorama-nav,#slider-controls .staticImg', function () {

                    $('#pagination ul li.slick-active>button').trigger('beforeChange');

                });

                $(document).on('click', '#shearBtn', function (e) {

                    e.preventDefault();
            
                    window.open($(this).attr('href'), 'fbShareWindow', 'height=450, width=550, top=' + ($(window).height() / 2 - 275) + ', left=' + ($(window).width() / 2 - 225) + ', toolbar=0, location=0, menubar=0, directories=0, scrollbars=0');

                    return false;



                });





                slickSlider();

                initProject();

            },

            empty = function (elem) {

                while (elem.lastChild)

                    elem.removeChild(elem.lastChild);

            },

            removeSlide = function (i) {

                if (i !== 0) {

                    $('#project-slider').slick('slickRemove', i);

                    i--;

                    removeSlide(i);

                }

            },

            addSlide = function (html) {

                $('#project-slider').slick('slickAdd', html);

            },

            animateSlideToIndex = function(index) {
                //console.log('pop');
                //return;                
                $("#project-slider").slick('slickGoTo',index);
                $('#pagination ul li.slick-active>button').trigger('beforeChange');
                slidePagination();
            },

            changeSlidePerHashUrlForProjectAndImage = function() {
                if(isImageNameInHashHandled) {                    
                    return;
                }
                isImageNameInHashHandled = true;
                var fbProjectName = window.location.hash.substr(1);

                if (fbProjectName != '') {
                    // expect hash in projectname#imagename
                    var projectName = "";
                    var hash = decodeURIComponent(fbProjectName.replace(/_/g, ' ')).split('#');
                    
                    if(hash.length < 2) {
                        return;
                    }

                    projectName = hash[0];

                    

                    //get the id of the projectname

                    projectData = ((localDB.WLprojects).filter(function (index) {

                        return index["name"] == projectName;

                    })).sortBy("displayPriority", false);

                    if (projectData.length > 0) {

                        var pid = projectData[0].id;
                        var imageName = hash[1];

                        imageName = imageName.toLowerCase();
                        imageData = ((localDB.WLprojectImages).filter(function (index) {

                            return index["name"].toLowerCase() == imageName;

                        })).sortBy("displayPriority", false);

                        if(imageData.length > 0) { 
                            $("#project-slider .item").each(function(index, val) {
                                if($(val).attr("data-img-name").toLowerCase() == imageName) {
                                    animateSlideToIndex(index);
                                }
                            });                           
                            
                        }
                    }
                } else {
                    window.location.hash = localDB.WLprojects.filter(function(el) {
                        return el["id"] == $('.project.active').attr("id");
                    })[0].name;
                }
            },

            initUI = function () {

                $('#main-header').attr('class', 'inner-header');

                $('#main-header img').css('display', 'block');

                $(document).on('click', '#search-modal .project', changeProject);



                $(document).on('beforeChange', '#slider-controls .panorama-nav', navigateToSlider);

                $(document).on('beforeChange', '#slider-controls .staticImg', navigateToSlider);



                $(document).on('click', '#next,#prev,#slider-controls .panorama-nav,#slider-controls .staticImg', function () {

                    $('#pagination ul li.slick-active>button').trigger('beforeChange');

                });



                //slickSlider();

                initProject();



                



                //modals

                $("#search").click(function (e) {

                    e.preventDefault();

                    $("#project").removeClass("modal-active");

                    $(".modal-wrap").removeClass("active");

                    // 

                    $("#project").addClass("modal-active");

                    $("#search-modal").addClass("active").focus();

                    //removeing body scroll

                    if ($("#project").hasClass("modal-active")) {

                        $("body").css("overflow", "hidden");

                    } else {

                         $("body").css({
                            "overflow-y": "auto",
                            "overflow-x": "hidden"
                        });

                    }

                    //align popup verticall if window height is gereater that popup

                    var windowWidth = $(window).width();

                    var windowHeight = $(window).height();

                    if (windowWidth > 1023) {

                        var popupHeight = $(".modal-wrap.active").outerHeight(true);

                        if (windowHeight > popupHeight) {

                            $(".modal-wrap.active").addClass("middle");

                        }

                    }

                });





                $("#info").click(function (e) {

                    e.preventDefault();

                    $("#project").removeClass("modal-active");

                    $(".modal-wrap").removeClass("active");

                    // 

                    $("#project").addClass("modal-active");

                    $("#info-modal").addClass("active").focus();

                    //$('#info-modal .modal-img').attr('src', "");

                    //removeing body scroll

                    if ($("#project").hasClass("modal-active")) {

                        $("body").css("overflow", "hidden");

                    } else {

                         $("body").css({
                            "overflow-y": "auto",
                            "overflow-x": "hidden"
                        });

                    }

                    //align popup verticall if window height is gereater that popup

                    var windowWidth = $(window).width();

                    var windowHeight = $(window).height();

                    if (windowWidth > 1023) {

                        var popupHeight = $(".modal-wrap.active").outerHeight(true);

                        if (windowHeight > popupHeight) {

                            $(".modal-wrap.active").addClass("middle");

                        }

                    }

                });

//                 $('body').on('click',"#blueprint-img",function (e) {

//                     alert();

//                 });

                //magnify image popup

                $('body').on('click',".btn-magnify,#blueprint-img",function (e) {

                    

                    e.preventDefault();

                    angle = 0;


                    $('body').css('overflow', 'hidden');

                    var imgSrc = $('#high-image').attr('src');

                    if (imgSrc == '') {

                        //alert("No Magnifying Image available");

                        return false;

                    }

                    $("#project").removeClass("modal-active");

                    $(".modal-wrap").removeClass("active");

                    // 

                    $("#project").addClass("modal-active");

                    $("#blueprint-modal").addClass("active");

                    $("#magnifyImg-modal").addClass("active").focus();

                    //removeing body scroll

                    if ($("#project").hasClass("modal-active")) {

                        $("body").css("overflow", "hidden");

                    } else {

                         $("body").css({
                            "overflow-y": "auto",
                            "overflow-x": "hidden"
                        });

                    }


//                    $(this).parents("#popup-wrapper").css("overflow", "hidden");//#bkesh:commented


                });





                $("#blueprint").click(function (e) {

                    e.preventDefault();

                    $("#project").removeClass("modal-active");

                    $(".modal-wrap").removeClass("active");

                    // 

                    $("#project").addClass("modal-active");

                    $("#blueprint-modal").addClass("active").focus();

                    //removeing body scroll

                    if ($("#project").hasClass("modal-active")) {

                        $("body").css("overflow", "hidden");

                    } else {

                         $("body").css({
                            "overflow-y": "auto",
                            "overflow-x": "hidden"
                        });

                    }

                    //align popup verticall if window height is gereater that popup

                    var windowWidth = $(window).width();

                    var windowHeight = $(window).height();

                    if (windowWidth > 1023) {

                        var popupHeight = $(".modal-wrap.active").outerHeight(true);

                        if (windowHeight > popupHeight) {

                            $(".modal-wrap.active").addClass("middle");

                        }

                    }

                });

                

                //rotate blue print image

                var img = document.getElementById('high-image');
                angle=0;

                document.getElementById('btn-rotate').onclick = function() {

                    //angle = (angle+90)%360;
					angle = (angle+90);
;
					$('#high-image').css({'transform' : 'rotate('+ angle +'deg)'});
					
					if(angle%360 == 90 || angle%360 == 270) {
						$('#magnifyImg-modal div.white').height($('#high-image').width());
						$('#high-image').css('margin-top', ($('#high-image').width() - $('#high-image').height())/2);
					} else {
						$('#magnifyImg-modal div.white').height($('#high-image').height());
						$('#high-image').css('margin-top', 0);
					}
					
					
                    //img.className = "rotate"+angle;

                };

                //rotate blue print image

               /* var angle = 0, img = document.getElementById('high-image');

                document.getElementById('btn-rotate').onclick = function() {

                    angle = (angle+90)%360;

                    img.className = "rotate"+angle;

                }; */

                

                $(".modal-close").click(function () {

                    $('body').css({
                        'overflow-y': 'auto',
                        'overflow-x': 'hidden'
                    });

                    $("#project").removeClass("modal-active");

                    var id = $(this).attr('id');

                    if (id === "close-btn-magnify") {

                        $("#project").addClass("modal-active");

                        $("#blueprint-modal").addClass("active");

                        $(".btn-rotate").removeClass("active");

                        $("#popup-wrapper").css("overflow", "auto");

                        angle = 0;

                        $('#high-image').css({'transform' : ''});
                        $('#high-image').css('margin-top', 0);


                        img.className = null;

                    } else {



                        $(".modal-wrap").removeClass("active");

                    }

                    //removeing body scroll

                    if ($("#project").hasClass("modal-active")) {

                        $("body").css("overflow", "hidden");

                    } else {

                        $("body").css({
                            "overflow-y": "auto",
                            "overflow-x": "hidden"
                        });

                    } 

                });

                $('body').mouseup(function(e) {

                    var container = $('#search-modal,#info-modal,#blueprint-modal,#magnifyImg-modal');

                    if (!container.is(e.target) && container.has(e.target).length === 0)

                    {

                        $("#project").removeClass("modal-active");

                        $(".modal-wrap").removeClass("active");

                         $("body").css({
                            "overflow-y": "auto",
                            "overflow-x": "hidden"
                        });

                    }

                });

//                $('#search-modal,#info-modal,#blueprint-modal,#magnifyImg-modal').on({

//                    focusout: function () { 

//                        $(this).data('timer', setTimeout(function () {

//                            $("#project").removeClass("modal-active");

//                            $(".modal-wrap").removeClass("active");

//                                

//                            $(".btn-magnify").click(function(){

//                                alert("test");

//                             $('#magnifyImg-modal').focusin();

//                             return false;

//                            });

//                               

//                           

//                            

//                        }.bind(this), 0));

//                    },

//                    focusin: function () {//  console.log("nnnnn");

//                        clearTimeout($(this).data('timer'));

//                    }

//                });

                

                $(function () {

                    $('#listing').slimscroll({

                        height: '320px',

                        width: '280px',

                        wheelStep: 80,

                        animate: true

                    });



                    $('#down').click(function () {

                        $('#listing').slimScroll({

                            scrollBy: '80px'

                        });

                    });



                    $('#up').click(function () {

                        $('#listing').slimScroll({

                            scrollBy: '-80px'

                        });

                    });

                });







            },

            navigateToSlider = function (e) {
                var $this = $(this);

                var _listId = $this.parent().attr('id');

                e.preventDefault();

                var panaromadiv = $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('div.paranoma-img');

                //console.log(_listId, "rupesh-", panaromadiv.length);
                var projectId = $('.project.active').attr("id");
                window.location.hash = localDB.WLprojects.filter(function(el) {
                    return el["id"] == projectId;
                })[0].name + "#" + $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").attr("data-img-name");
                if (panaromadiv.length == 0 || panaromadiv == undefined) {

                    var img_name = $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('img').attr('data-img-name');
        
                    var paginate = $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('img').attr('data-paginate');

                    var description = $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('img').attr('data-desc');



                    $('.pagination-count').empty();

                    $('.pagination-count').html(paginate);



                    $('.slider-controls #paginationcount').empty();

                    $('.slider-controls #paginationcount').html(paginate);



                    $('#project-title #level').empty();

                    $('#project-title #level').html(img_name);

                    console.log("rupesh", img_name);

                    $('#level-bottom').html('');

                    $('#level-bottom').html(img_name);

                    var img = $('.slick-current img');

                    if (img.prop('complete')) {

                      // already loaded

                    } else {

                        console.log('hello i am here');

                        $('.panaroma-loader-wrap').addClass('panaroma-loader-show');

                        img.load(function() {

                            $('.panaroma-loader-wrap').removeClass('panaroma-loader-show');

                        });

                    }

                    

                } else {



                    var img_index = $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('div.paranoma-img').attr('data-img-index');



                    var img_src = panorama_img[parseInt(img_index)].src;

                    mesh.material.map = THREE.ImageUtils.loadTexture(img_src);

                    mesh.material.needsUpdate = true;

                    $('.panaroma-loader-wrap').addClass('panaroma-loader-show');

                    // instantiate a loader

                    var loader =  null;

                    var loader = new THREE.ImageLoader(manager);



                    // load a image resource

                    loader.load(

                            // resource URL

                            img_src,

                            // Function when resource is loaded

                            function ( image ) {

                                    // do something with it



                                    // like drawing a part of it on a canvas

                                    /*var canvas = document.createElement( 'canvas' );

                                    var context = canvas.getContext( '2d' );

                                    context.drawImage( image, 100, 100 );*/

                                    console.log(image);

                            },

                            // Function called when download progresses

                            function ( xhr ) {

                                    console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );

                            },

                            // Function called when download errors

                            function ( xhr ) {

                                    console.log( 'An error happened' );

                            }

                    );

					

                    var slickIndex = $('#project-slider #project-panorama').parent().attr('data-slick-index')

                    //$('#project-slider').slick('slickGoTo', parseInt(slickIndex));

                    var myCanvas;

                    myCanvas = $('canvas').detach();



                    $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('div.paranoma-img').append(myCanvas);

                    var dom_element = $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('div.paranoma-img');

                    console.log(dom_element);

                    dom_element.off('mousedown');

                    dom_element.on('mousedown', onDocumentMouseDown);

                    dom_element.off('mousemove');

                    dom_element.on('mousemove', onDocumentMouseMove);

                    

                    var img_name = $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('div.paranoma-img').attr('data-img-name');

                    var paginate = $("#project-slider .slick-slide[aria-describedby='" + _listId + "']").find('div.paranoma-img').attr('data-paginate');



                    $('.pagination-count').empty();

                    $('.pagination-count').html(paginate);



                    $('.slider-controls #paginationcount').empty();

                    $('.slider-controls #paginationcount').html(paginate);



                    $('.slider-controls #level-bottom').html('');

                    $('.slider-controls #level-bottom').html(img_name);



                    $('#project-title #level').empty();

                    $('#project-title #level').html(img_name);

                }



            },

            slickSlider = function () {

            $('#project-slider').on('init', function(slick) {

                console.log('fired!');

                var img = $('.slick-current img');

                if (img.prop('complete')) {

                    // already loaded

                } else {

                    console.log('hello i am here');

                    $('.panaroma-loader-wrap').addClass('panaroma-loader-show');

                    img.load(function() {

                        $('.panaroma-loader-wrap').removeClass('panaroma-loader-show');

                    });

                }

                $('.pagination-scroll').css('display','inline-block');      

            }).slick({

                dots: true,

                draggable: false,

                infinite: true,

                speed: 300,

                slidesToShow: 1,

                fade: true,

                cssEase: 'linear',

                arrows: true,

                prevArrow: $("#prev"),

                nextArrow: $("#next"),

                appendDots: $("#pagination"),

                customPaging: function(slider, i) {
                    var hasPanaroma = $(slider.$slides[i]).find('.paranoma-img');

                    if (hasPanaroma.length > 0) {

                        var imageIndex = $(slider.$slides[i]).find('.paranoma-img').attr('data-img-index');

                        return '<button type="button" class="panorama-nav" data-role="none" role="button"></button>';

                    } else {

                        return '<button type="button" class="staticImg" data-role="none" role="button"></button>';

                    }

                },

                swipe: true
            });
            setTimeout(function() {
                changeSlidePerHashUrlForProjectAndImage();   
            });
            //

                $(document).on('swipe', '#project-slider', function () {

                    var leftPosPage;

                    var lleftPosPage;

                    var totalLeft;

                    leftPosPage = $(".slick-dots .slick-active").position().left;

                    lleftPosPage = $("#project-slider-wrap").position().left;

                    totalLeft = leftPosPage + lleftPosPage + 15;

                    $(".pagination-scroll").css('left', totalLeft);

                });



                var leftPosPage = 0;

                var lleftPosPage = 0;

                var totalLeft;

                if ($(".slick-dots .slick-active").length > 0) {

                    leftPosPage = $(".slick-dots .slick-active").position().left;

                }

                lleftPosPage = $("#project-slider-wrap").position().left;

                totalLeft = leftPosPage + lleftPosPage + 15;

                $(".pagination-scroll").css('left', totalLeft);



                $(".slick-dots button").click(function () {

                    var changePosPage = $(this).position().left + lleftPosPage + 15;

                    $(".pagination-scroll").css('left', changePosPage);

                });

                $(".slick-prev").click(function () {

                    slidePagination();

                });

                $(".slick-next").click(function () {

                    slidePagination();

                });                

               /* function slidePagination() {

                    var leftPosPage;

                    var lleftPosPage;

                    var totalLeft;

                    leftPosPage = $(".slick-dots .slick-active").position().left;

                    lleftPosPage = $("#project-slider-wrap").position().left;

                    totalLeft = leftPosPage + lleftPosPage + 15;

                    $(".pagination-scroll").css('left', totalLeft);

                }*/



            },

            slidePagination = function() {
                var leftPosPage;

                    var lleftPosPage;

                    var totalLeft;

                    leftPosPage = $(".slick-dots .slick-active").position().left;

                    lleftPosPage = $("#project-slider-wrap").position().left;

                    totalLeft = leftPosPage + lleftPosPage + 15;

                    $(".pagination-scroll").css('left', totalLeft);
                },

            changeProject = function (e, args) {

                e.preventDefault();


                var $this = $(this);



                if (!$this.hasClass('active')) {

                    $('#search-modal .project').each(function (index, elem) {

                        $(elem).removeClass("active");

                    });



                    $this.addClass("active");

                    var projectId = $this.attr('data-projectId');



                    var projectData = getProjectById(projectId);

                    //console.log("projectData",projectData);

                    updateSearchModal(projectData);

                    updateBluePrintModal(projectData.blueprint);



                    updateProjectTitle(projectData);

                    //console.log(projectData);

                    updateSlider(projectData);

                    updateprojectInfo(projectData.projectInfo);

                    

                    //close the box

                    $('body').css('overflow', 'visible');

                    $("#project").removeClass("modal-active");

                    $(".modal-wrap").removeClass("active");

                    //removeing body scroll

                    if ($("#project").hasClass("modal-active")) {

                        $("body").css("overflow", "hidden");

                    } else {

                         $("body").css({
                            "overflow-y": "auto",
                            "overflow-x": "hidden"
                        });

                    } 
                }
                if(args == undefined) {
                    window.location.hash = localDB.WLprojects.filter(function(el) {
                        return el["id"] == $('.project.active').attr("id");
                    })[0].name;
                }        
            },

            updateSearchModal = function (data) {

                $('#search-modal .modal-img').attr('src', data.modal_img);

                $('#search-modal .modal-title').empty();



                $('#search-modal .modal-title').html(data.title);

                $('#search-modal .address').empty();

                $('#search-modal .address').html(data.address);

            },

            updateBluePrintModal = function (data) {

                var levelData = data;



                if (levelData.countLevel < 1 || levelData == '') {

                    $('#levelBtn').empty();

                    $('#blueprint-modal').attr('data-id', '0');

                    var imgSrc = $('#search-modal .modal-img').attr('src');

                    $('#magnifyImg-modal #high-image').attr('src', '');

                    $('#blueprint-modal .modal-img').attr('src', imgSrc);

                    $('#blueprint-modal').attr('data-id', '');

                    $('#blueprint-modal .modal-title').empty();

                    $('#blueprint-modal .modal-title').html("NO data available");

                    $('#blueprint-modal .address').empty();

                    $('#blueprint-modal #blueprint-img').html('');

                    $('#blueprint-modal .button-level.active').removeClass('active');

                    $('#blueprint').css('display', 'none');

                    //$("#blueprint-modal .button-level[data-level='level1']").addClass('disabled');

                } else {

                    $('#magnifyImg-modal #high-image').attr('src', '');

                    $('#magnifyImg-modal #high-image').attr('src', levelData["level1"].high_img);

                    $('#blueprint-modal').attr('data-id', levelData.levelProjectId);

                    $('#blueprint-modal .modal-img').attr('src', levelData["level1"].img);

                    $('#blueprint-modal .modal-title').empty();

                    $('#blueprint-modal .modal-title').html(levelData["level1"].title);

                    $('#blueprint-modal .address').empty();

                    $('#blueprint-modal .address').html(levelData["level1"].address);

                    $('#blueprint-modal #blueprint-img').html('');

                    $('#blueprint-modal #blueprint-img').html('<img class="lowImage" src="' + levelData["level1"].blueprint_img + '">');

                    $('#blueprint-modal .button-level.active').removeClass('active');

                    $("#blueprint-modal .button-level[data-level='level1']").addClass('active');

                    $('#blueprint').css('display', 'inline-block');

                }



                //window.localStorage.setItem("blueprint-level", JSON.stringify(levelData));

            },

            updateProjectTitle = function (data) {

                //console.log(data);

                $('#project-title #level').empty();



                //$('#project-title #level').html(data.slider_images[0].name);



                $('#project-title #title-options>a>h3').empty();

                $('#project-title #title-options>a>h3').html(data.title);

            },

            updateSlider = function (data) {

                //$('#project-slider').slick("unslick");

                //$('#project-slider').empty();

                //$('#project-slider').html(html);

                //slickSlider();



                // var sliderData = data.slider_images;

                //createHtmlForSlider(sliderData, false);



                //destroy previous three.js context



                if (sliderUpdateCounter > 1) {

                    destroyThree();

                    sliderUpdateCounter++;

                }





                $('#project-slider').slick("unslick");

                $('#project-slider').empty();

                var html = htmlForSlider(data.slider_images);



                $('#project-slider').html(html);



                dom_el = document.getElementById('project-panorama');

                if (panorama_img.length > 0) {

                    //console.log(panorama_img[0].src);

                    var img = panorama_img[0].src;

                    init(dom_el, img);

                    animate();

                }

                slickSlider();





            },

            updateprojectInfo = function (data) {



                var img_url = DOMAIN_URL + "/" + data.modal_img;

                $('#info-modal .modal-img').attr('src', '');

                var facebook_appID = '1589886947983338';
                //console.log("alldata",data);
                var facebookShareUrl = DOMAIN_URL + "/share2.php?sharetitle="+data.title+"&sharedescription="+data.address+" Calling all the people to do the projects.&shareimg="+img_url+"&uid=" + data.projectId + "&stamp=" + Math.random();
                var mainUrl = "http://www.facebook.com/sharer.php?u="+ encodeURIComponent(facebookShareUrl);
//mainUrl = facebookShareUrl;

                $('#info-modal #shearBtn').attr('href', mainUrl);

                $('#info-modal .modal-img').attr('src', data.modal_img);

                $('#info-modal .info-title .modal-title').empty();

                $('#info-modal .info-title .modal-title').html(data.title);

                $('#info-modal .address').empty();

                $('#info-modal .address').html(data.address);



                //items-wrap

                var itemsHtml = "";

                var _items = data.items;

                for (var i = 0; i < _items.length; i++) {

                    itemsHtml += '<div class="info-item">';

                    itemsHtml += '<label>' + _items[i].name + '</label>';

                    itemsHtml += '<span class="unit">' + _items[i].value + '</span>';

                    itemsHtml += '<span>' + _items[i].unit + '</span>';

                    itemsHtml += '</div>';

                }



                $('#info-modal .info-item-wrap').empty();

                $('#info-modal .info-item-wrap').html(itemsHtml);



                //timeline



                var timeline = "";

                timeline += '<span class="bar-date">' + data.projectTimeline.start_month + '&nbsp;' + data.projectTimeline.start_date + '</span>';

                timeline += '<div class="timeline-bar">';



                var _works = data.projectTimeline.works;

                for (var i = 0; i < _works.length; i++) {

                    timeline += '<div class="bar-segment">';

                    timeline += '<span>' + _works[i].value + '&nbsp;(' + _works[i].time + ')</span>';

                    timeline += '</div>';

                }

                timeline += '</div>';

                timeline += '<span class="bar-date">' + data.projectTimeline.end_month + '&nbsp;' + data.projectTimeline.end_date + '</span>'

                $('#info-modal .project-timeline').empty();

                $('#info-modal .project-timeline').html(timeline);

                // console.log(data.projectTimeline);



            },

            getProjectById = function (id) {

                /***

                 * Init projects

                 ***/

                projects = ((localDB.WLprojects).filter(function (index) {

                    return index["id"] == id && index['displayPriority']>0;

                }));

                projectImages = ((localDB.WLprojectImages).filter(function (index) {

                    return index['displayPriority']>0 && index["projectID"] == id;

                }));

                projectBluePrints = ((localDB.WLprojectBluePrints).filter(function (index) {

                    return index["projectID"] == id;

                }));



                //add logic to get the project from server by project Id  or ........ later

                data = {};

                $.each(projects, function (i, v) {

                    var projectname = "project" + v.id;

                    var projectId = v.id;



                    data[projectname] = [];



                    data[projectname]['id'] = v.id;

                    data[projectname]['title'] = v.name;

                    data[projectname]['address'] = v.location;

                    data[projectname]['modal_img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();

		    data[projectname]['modal_image'] = "img/projects/icons/" + v.id + ".png";

                    data[projectname]['slider_images'] = new Array();



                    $.each(projectImages, function (ii, vi) {



                        var thisImage = '';



                        var thisImage = [];

                        thisImage['src'] = 'img/projects/images/' + vi.imgUrl + '?time=' + $.now();

                        if (vi.isPanorama == 1) {

                            thisImage['type'] = 'panorama';

                        } else {

                            thisImage['type'] = 'static';

                        }

                        thisImage['description'] = v.description;

                        thisImage['name'] = vi.name;

                        thisImage['displayPriority'] = vi.displayPriority;

                        data[projectname]['slider_images'].push(thisImage);



                    });





                    if (projectBluePrints.length == 0) {

                        data[projectname]['blueprint'] = '';

                    } else {

                        data[projectname]['blueprint'] = new Array();

                        var countBlueprint = 0;

                        $.each(projectBluePrints, function (ib, vb) {

                            var thisBluePrint = '';



                            var thisBluePrint = {};

                            thisBluePrint['levelProjectId'] = v.id;

                            thisBluePrint['level1'] = [];

                            var urlLow1 = '';

                            if (vb.urlLow1 != null) {

                                countBlueprint++;

                                urlLow1 = "img/projects/blueprints/" + vb.urlLow1;

                            }

                            thisBluePrint['level1']['blueprint_img'] = urlLow1;

                            thisBluePrint['level1']['title'] = v.name;

                            thisBluePrint['level1']['address'] = v.location;

                            var urlHigh1 = '';

                            if (vb.urlHigh1 != null) {

                                urlHigh1 = "img/projects/blueprints/" + vb.urlHigh1;

                            }

                            thisBluePrint['level1']['img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();

                            thisBluePrint['level1']['high_img'] = urlHigh1;





                            thisBluePrint['level2'] = [];

                            var urlLow2 = '';

                            if (vb.urlLow2 != null) {

                                countBlueprint++;

                                urlLow2 = "img/projects/blueprints/" + vb.urlLow2;

                            }

                            thisBluePrint['level2']['blueprint_img'] = urlLow2

                            thisBluePrint['level2']['title'] = v.name;

                            thisBluePrint['level2']['address'] = v.location;

                            var urlHigh2 = '';

                            if (vb.urlHigh2 != null) {

                                urlHigh2 = "img/projects/blueprints/" + vb.urlHigh2;

                            }

                            thisBluePrint['level2']['img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();

                            thisBluePrint['level2']['high_img'] = urlHigh2;



                            thisBluePrint['level3'] = [];

                            var urlLow3 = '';

                            if (vb.urlLow3 != null) {

                                countBlueprint++;

                                urlLow3 = "img/projects/blueprints/" + vb.urlLow3;

                            }

                            thisBluePrint['level3']['blueprint_img'] = urlLow3;

                            thisBluePrint['level3']['title'] = v.name;

                            thisBluePrint['level3']['address'] = v.location;

                            var urlHigh3 = '';

                            if (vb.urlHigh3 != null) {

                                urlHigh3 = "img/projects/blueprints/" + vb.urlHigh3;

                            }

                            thisBluePrint['level3']['img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();

                            thisBluePrint['level3']['high_img'] = urlHigh3;



                            thisBluePrint['level4'] = [];

                            var urlLow4 = '';

                            if (vb.urlLow4 != null) {

                                countBlueprint++;

                                urlLow4 = "img/projects/blueprints/" + vb.urlLow4;

                            }

                            thisBluePrint['level4']['blueprint_img'] = urlLow4;

                            thisBluePrint['level4']['title'] = v.name;

                            thisBluePrint['level4']['address'] = v.location;

                            var urlHigh4 = '';

                            if (vb.urlHigh4 != null) {

                                urlHigh4 = "img/projects/blueprints/" + vb.urlHigh4;

                            }

                            thisBluePrint['level4']['img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();

                            thisBluePrint['level4']['high_img'] = urlHigh4;

                            thisBluePrint['countLevel'] = countBlueprint;

                            data[projectname]['blueprint'] = thisBluePrint;

                        });

                    }



                    data[projectname]['projectInfo'] = new Array();



                    var thisProjectInfo = {};

                    thisProjectInfo['projectId'] = v.id;

                    thisProjectInfo['title'] = v.name;

                    thisProjectInfo['description'] = v.description;

                    thisProjectInfo['address'] = v.location;

                    thisProjectInfo['modal_img'] = "img/projects/icons/" + v.id + ".png?time=" + $.now();

                    thisProjectInfo['items'] = new Array();



                    var thisProjectItemInfo = [];

                    var leaseHold = 0;

                    if (v.leaseHold != null) {

                        leaseHold = v.leaseHold;

                    }

                    thisProjectItemInfo['name'] = 'Leasehold';

                    thisProjectItemInfo['value'] = leaseHold;

                    thisProjectItemInfo['unit'] = 'Years';

                    thisProjectInfo['items'].push(thisProjectItemInfo);



                    thisProjectItemInfo = [];

                    var buildUpArea = 0;

                    if (v.buildUpArea != null) {

                        buildUpArea = v.buildUpArea;

                    }

                    thisProjectItemInfo['name'] = 'Built-up Area';

                    thisProjectItemInfo['value'] = buildUpArea;

                    thisProjectItemInfo['unit'] = 'SQF';

                    thisProjectInfo['items'].push(thisProjectItemInfo);



                    thisProjectItemInfo = [];

                    var buildInArea = 0;

                    if (v.buildInArea != null) {

                        buildInArea = v.buildInArea;

                    }



                    thisProjectItemInfo['name'] = 'Built-in Area';

                    thisProjectItemInfo['value'] = buildInArea;

                    thisProjectItemInfo['unit'] = 'SQF';

                    thisProjectInfo['items'].push(thisProjectItemInfo);



                    thisProjectItemInfo = [];

                    var bedRoom = 0;

                    if (v.bedRoom != null) {

                        bedRoom = v.bedRoom;

                    }

                    var shower = 0;

                    if (v.shower != null) {

                        shower = v.shower;

                    }

                    thisProjectItemInfo['name'] = 'Bedrooms';

                    thisProjectItemInfo['value'] = bedRoom;

                    thisProjectItemInfo['unit'] = shower + ' Showers';

                    thisProjectInfo['items'].push(thisProjectItemInfo);



                    thisProjectItemInfo = [];

                    var estimatedCost = 0;

                    if (v.estimatedCost != null) {

                        estimatedCost = parseInt(v.estimatedCost);

                    }

                    thisProjectItemInfo['name'] = 'Estimated Cost';

                    thisProjectItemInfo['value'] = '$ ' + estimatedCost;

                    thisProjectItemInfo['unit'] = 'Per SQF';

                    thisProjectInfo['items'].push(thisProjectItemInfo);



                    var numberOfMonths = '';

                    numberOfMonths = (v.endYear - v.startYear) * 12 + (v.endMonth - v.startMonth) - 1;

                    if (numberOfMonths < 0) {

                        numberOfMonths = 0;

                    }

                    thisProjectItemInfo = [];

                    thisProjectItemInfo['name'] = 'TimeLine';

                    thisProjectItemInfo['value'] = numberOfMonths;

                    thisProjectItemInfo['unit'] = 'Months';

                    thisProjectInfo['items'].push(thisProjectItemInfo);



                    thisProjectInfo['projectTimeline'] = [];

                    var endYear = '';

                    if (v.endYear != null) {

                        endYear = parseInt(v.endYear);

                    }



                    var endMonth = '';

                    if (v.endMonth != null) {

                        endMonth = parseInt(v.endMonth);

                        endMonth = getMonthByNumber(endMonth);

                    }



                    var startYear = '';

                    if (v.startYear != null) {

                        startYear = parseInt(v.startYear);

                    }

                    var startMonth = '';

                    if (v.startMonth != null) {

                        startMonth = parseInt(v.startMonth);

                        startMonth = getMonthByNumber(startMonth);

                    }



                    thisProjectInfo['projectTimeline']['start_date'] = startYear;

                    thisProjectInfo['projectTimeline']['start_month'] = startMonth;

                    thisProjectInfo['projectTimeline']['end_date'] = endYear;

                    thisProjectInfo['projectTimeline']['end_month'] = endMonth;



                    thisProjectInfo['projectTimeline']['works'] = [];

                    var thisProjectWorkInfo = [];



                    var demolitionWork = 0;

                    if (v.demolitionWork != null) {

                        demolitionWork = parseInt(v.demolitionWork);

                    }

                    thisProjectWorkInfo['value'] = 'Demolition';

                    thisProjectWorkInfo['time'] = demolitionWork + ' weeks';

                    thisProjectInfo['projectTimeline']['works'].push(thisProjectWorkInfo);



                    var structureWork = 0;

                    if (v.structureWork != null) {

                        structureWork = parseInt(v.structureWork);

                    }

                    thisProjectWorkInfo = [];

                    thisProjectWorkInfo['time'] = structureWork + ' weeks';

                    thisProjectWorkInfo['value'] = 'Structure Works';

                    thisProjectInfo['projectTimeline']['works'].push(thisProjectWorkInfo);



                    var architecturalWork = 0;

                    if (v.architecturalWork != null) {

                        architecturalWork = parseInt(v.architecturalWork);

                    }

                    thisProjectWorkInfo = [];

                    thisProjectWorkInfo['time'] = architecturalWork + ' weeks';

                    thisProjectWorkInfo['value'] = 'Architectural Works';

                    thisProjectInfo['projectTimeline']['works'].push(thisProjectWorkInfo);



                    var meWork = 0;

                    if (v.meWork != null) {

                        meWork = parseInt(v.meWork);

                    }

                    thisProjectWorkInfo = [];

                    thisProjectWorkInfo['time'] = meWork + ' weeks';

                    thisProjectWorkInfo['value'] = 'M&E Works';

                    thisProjectInfo['projectTimeline']['works'].push(thisProjectWorkInfo);



                    var externalWork = 0;

                    if (v.externalWork != null) {

                        externalWork = parseInt(v.externalWork);

                    }

                    thisProjectWorkInfo = [];

                    thisProjectWorkInfo['time'] = externalWork + ' weeks';

                    thisProjectWorkInfo['value'] = 'External Works';

                    thisProjectInfo['projectTimeline']['works'].push(thisProjectWorkInfo);



                    data[projectname]['projectInfo'] = thisProjectInfo;



                });



                return data['project' + id];

            },

            getMonthByNumber = function (number) {

                var month = new Array();

                month[1] = "Jan";

                month[2] = "Feb";

                month[3] = "Mar";

                month[4] = "Apr";

                month[5] = "May";

                month[6] = "June";

                month[7] = "July";

                month[8] = "Aug";

                month[9] = "Sep";

                month[10] = "Oct";

                month[11] = "Nov";

                month[12] = "Dec";



                return month[number];

            },

            destroyThree = function () {

                cancelAnimationFrame(requestAnimationFrameId); // Stop the animation

                renderer.domElement.addEventListener('dblclick', null, false); //remove listener to render

                renderer.domElement.addEventListener('mousedown', null, false);

                renderer.domElement.addEventListener('mousemove', null, false);

                document.addEventListener('mouseup', null, false);

                scene = null;

                camera = null;

                empty($('#project-panorama'));

                lat = 0;

                lon = 0;

                phi = 0;

                theta = 0;

                onMouseDownLat = 0;

                onMouseDownLon = 0;

                /* init(dom_el, img_file);

                 animate();*/

            },

            htmlForSlider = function (sliderData) {                
                //console.log('project', sliderData);

                panorama_img = [];

                var i = 0;

                var j = 0;

                var slidercount = 1;

                var _fhtml = "";

                var totalslider = sliderData.length;



                $(sliderData).each(function (index, elem) {

                    var name = elem.name;



                    // console.log($('#paginationcount').html());

                    //for first time

                    var pagecounter = slidercount + '&nbsp/&nbsp;' + totalslider;

                    if (j == 0) { //console.log("test",name);

                        $('#project-title #level').html(name);

                        $('#level-bottom').html(name);

                        $('#paginationcount').html(pagecounter);

                        j++;

                    }

                    _fhtml += '<div class="item" data-img-name="' + name + '">';

                    if (elem.type == "static") {

                        _fhtml += '<img src="' + elem.src + '"  data-img-name="' + name + '" data-paginate=' + pagecounter + '>';

                    } else {

                        if (i == 0) {

                            _fhtml += '<div id="project-panorama" class="paranoma-img" data-paginate=' + pagecounter + ' data-img-name="' + name + '" data-img-index="' + i + '"></div>';

                        } else {

                            _fhtml += '<div class="paranoma-img" data-paginate=' + pagecounter + ' data-img-name="' + name + '" data-img-index="' + i + '" ></div>';

                        }

                        panorama_img.push({src: elem.src}); // put 360 image in array

                        i++;



                    }

                    _fhtml += '<div class="pagination-count" >' + pagecounter + '</div>';

                    _fhtml += '<div class="slider-text">';

                    _fhtml += '<div class="container">';

                    _fhtml += '<p class="white text-center">' + elem.description + '</p>';

                    _fhtml += '</div>';

                    _fhtml += '</div>';

                    _fhtml += '</div>';

                    slidercount++;

                });





                return _fhtml;

            },

            init = function (dom_el, img_file) {

                // Derive aspect ratio

                var width = dom_el.offsetWidth;

                var height = dom_el.offsetHeight;

                var aspect_ratio = width / height;



                // Create the scene, camera and renderer

                scene = new THREE.Scene();

                camera = new THREE.PerspectiveCamera(FOV, aspect_ratio, NEAR_CLIPPING_PLANE, FAR_CLIPPING_PLANE);

                camera.target = new THREE.Vector3(0, 0, 0);

                renderer = new THREE.WebGLRenderer();

                renderer.setPixelRatio(window.devicePixelRatio);

                renderer.setSize(width, height);

                dom_el.innerHTML = '';

                dom_el.appendChild(renderer.domElement);



                // Set Map

                var geometry = new THREE.SphereGeometry(ROOM_RADIUS, WIDTH_SEGMENTS, WIDTH_SEGMENTS / aspect_ratio);

                geometry.scale(scaleX, scaleY, scaleZ);

                var material = new THREE.MeshBasicMaterial({

                    map: THREE.ImageUtils.loadTexture(img_file)

                });

                mesh = new THREE.Mesh(geometry, material);

                scene.add(mesh);

                $('.panaroma-loader-wrap').addClass('panaroma-loader-show');

				//document.title = "Loading";

		manager = new THREE.LoadingManager();

                    manager.onProgress = function ( item, loaded, total ) {

                    //	document.title = 'Loading';

                            console.log( item, loaded, total );



                    };



                    manager.onLoad = function() {

                            console.log('done');

                            $('.panaroma-loader-wrap').removeClass('panaroma-loader-show');

                    };





                    // instantiate a loader

                    var loader = null;

                    var loader = new THREE.ImageLoader(manager);



                    // load a image resource

                    loader.load(

                            // resource URL

                            img_file,

                            // Function when resource is loaded

                            function ( image ) {

                                    // do something with it



                                    // like drawing a part of it on a canvas

                                    /*var canvas = document.createElement( 'canvas' );

                                    var context = canvas.getContext( '2d' );

                                    context.drawImage( image, 100, 100 );*/

                                    console.log(image);

                            },

                            // Function called when download progresses

                            function ( xhr ) {

                                    console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );

                            },

                            // Function called when download errors

                            function ( xhr ) {

                                    console.log( 'An error happened' );

                            }

                    );

                //scene1 = undefined;

                // sceneArr.push({ "1": scene1 });



                // Create the scene, camera and renderer

                /* var scene2 = new THREE.Scene();

                 

                 // Set Map

                 var material = new THREE.MeshBasicMaterial({

                 map: THREE.ImageUtils.loadTexture('img/panoramas/18-Mount-Sinai-Walk/pool-area.jpg')

                 });

                 scene2.add(new THREE.Mesh(geometry, material));

                 sceneArr.push({ "2": scene2 });*/









                // Bindings

                //dom_el.removeEventListener('mousedown', onDocumentMouseDown, false );

                dom_el.addEventListener('mousedown', onDocumentMouseDown, false);

                dom_el.addEventListener('mousemove', onDocumentMouseMove, false);

                document.addEventListener('mouseup', onDocumentMouseUp, false);

                //dom_el.addEventListener('wheel', onDocumentMouseWheel, false);

                //dom_el.addEventListener( 'MozMousePixelScroll', onDocumentMouseWheel, false);

            }, // init()



            onDocumentMouseDown = function (event) {

                event.preventDefault();



                isUserInteracting = true;



                onPointerDownPointerX = event.clientX;

                onPointerDownPointerY = event.clientY;



                onPointerDownLon = lon;

                onPointerDownLat = lat;

            },

            onDocumentMouseMove = function (event) {

                if (isUserInteracting) {

                    event.preventDefault();

                    lon = (onPointerDownPointerX - event.clientX) * DRAG_SPEED + onPointerDownLon;

                    lat = (event.clientY - onPointerDownPointerY) * DRAG_SPEED + onPointerDownLat;

                }

            },

            onDocumentMouseUp = function (event) {

                isUserInteracting = false;

            },

            onDocumentMouseWheel = function (event) {



                event.preventDefault();

                console.log(getNormalizedWheelSpeed(event) + ' deltaY: ' + event.deltaY + ' wheelDeltaY: ' + event.wheelDeltaY + ' Mode: ' + event.deltaMode);



                /*

                 if (event.wheelDeltaY) {

                 camera.fov -= event.wheelDeltaY * ZOOM_SENSITIVITY; // Magic number 40 from MDN (https://developer.mozilla.org/en-US/docs/Web/Events/wheel)

                 } // WebKit

                 else if (event.wheelDelta) {

                 camera.fov -= event.wheelDelta / 40 * ZOOM_SENSITIVITY;

                 } // Opera / Explorer 9

                 else if (event.deltaY) {

                 camera.fov -= event.deltaY / 40 * ZOOM_SENSITIVITY;

                 }

                 else if (event.detail) {

                 camera.fov += event.detail * ZOOM_SENSITIVITY;

                 } // Firefox

                 */



                camera.fov += getNormalizedWheelSpeed(event) * ZOOM_SENSITIVITY;



                // FOV min/max

                if (camera.fov < MIN_FOV) {

                    camera.fov = MIN_FOV;

                }

                if (camera.fov > MAX_FOV) {

                    camera.fov = MAX_FOV;

                }



                camera.updateProjectionMatrix();

            },

            getNormalizedWheelSpeed = function (event) {

                if (event.type === 'wheel') {

                    if (event.deltaMode === 1) {

                        return event.deltaY * 40;

                    } else {

                        return event.deltaY;

                    }

                } else if (event.type === 'mousewheel') {

                    return event.wheelDelta / -40;

                }

            }, // getNormalizedWheelSpeed()



            animate = function () {

                requestAnimationFrameId = requestAnimationFrame(animate);

                update();

            },

            update = function () {

                if (!isUserInteracting) {

                    lat += WIND_Y_SPEED;

                    lon += WIND_X_SPEED;

                }



                lat = Math.max(-85, Math.min(85, lat));

                phi = THREE.Math.degToRad(90 - lat);

                theta = THREE.Math.degToRad(lon);



                camera.target.x = 500 * Math.sin(phi) * Math.cos(theta);

                camera.target.y = 500 * Math.cos(phi);

                camera.target.z = 500 * Math.sin(phi) * Math.sin(theta);



                camera.lookAt(camera.target);



                /*

                 // distortion

                 camera.position.copy( camera.target ).negate();

                 */



                renderer.render(scene, camera);

            };

    return {

        onDocumentReady: onDocumentReady,
        slidePagination: slidePagination

    };



}(jQuery));



jQuery(app.projectinclude.onDocumentReady);

