/***
 * Module - app.Module.profile
 *
 * Provides behaviour for profile.php page
 *
 ***/

/*****************************
 *      Execute on load      *
 *****************************/

(function(app, $) {
    "use strict";

    var App = window.app = window.app || {};
    App.Module = App.Module = App.Module || {};
    app.Module.Profile = function() {
        var config = {};

        /***
         * Removes all events in this module: click, input, focus, pointerdown
         ***/
        function resetEvent() {

        } // resetEvent()

        /***
         * Adds all necessary events: input, focus, pointerdown
         ***/
        function initEvent() {
            $(window.app).off("ready");
            $(window.app).on("ready", function() {
                $('#main-header').attr('class', 'inner-header');
                $('#main-header img').css('display', 'block');
                $(window.app).off("ready");
            });
        } // initEvent()

        /***
         * Initializes UI states
         ***/
        function initUI() {
            if (window.app.ready) {
                $('#main-header').attr('class', 'inner-header');
                $('#main-header img').css('display', 'block');
            }
        } // initUI()

        /***
         * Initialize Module
         ***/
        function init($config) {
//                resetEvent();
            initEvent();
            initUI();
        } // init()


        return {
            init: init
        }
    }();
}(window.app = window.app || {}, jQuery));
// Init profie Module
window.app.Module.Profile.init();
