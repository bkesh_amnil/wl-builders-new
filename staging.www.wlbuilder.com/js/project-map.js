/***
 * Module - app.Module.project map image
 *
 * Provides behaviour for project-map.php page
 *
 * Dependencies
 *  - jQuery
 *
 * Before use
 *  - ensure document is ready
 *  - localDB is init
 *
 * Init instructions:
 *  - app.Module.projectMap.init(options);
 *
 *
 ***/

/*****************************
 *      Execute on load      *
 *****************************/

(function (App, $) {
    "use strict";

    //var App = window.app = window.app || {};

    App.Module = App.Module = App.Module || {};
    App.Module.ProjectMap = function () {
        var backgroundImages = [],
                sliderImages = [],
                config = {};
        var mapData = [];


        function resetEvent() {

        } // resetEvent()

        /***
         * Adds all necessary events: input, focus, pointerdown
         ***/
        function initEvent() {
            
            loadGoogleMapScript();
            $('body').on('click', '.info-trigger', function () {
                var that = $(this);
                var id = that.data('marker-id');
                google.maps.event.trigger(markers[id], 'click');
            })
            
        } // initEvent()

        

        function loadGoogleMapScript() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBjXHokQ42avPBvmHaEymHBtE7ttqB2Ik0&callback=window.app.Module.ProjectMap.init_map';
            document.body.appendChild(script);

        }
        
        /***
         * Initializes UI states
         ***/
        function initUI() {
            
            var projects = ((localDB.WLprojects).filter(function (e) {
                return e["displayPriority"] > 0;
            })).sortBy("displayPriority", true);

            $.each(projects, function (i, v) {
                var pdata = {};
                pdata['name'] = v.name;
                pdata['detail'] = v.location;
                pdata['lat'] = v.latitude;
                pdata['long'] = v.longitude;
                mapData.push(pdata);

            });
           // loadGoogleMapScript();

        } // initUI()
        

        function init_map() {
            console.log(mapData);
            var map;
            var infoWindows = [], markers = [], iconType, i;
            infoWindows = [], markers = [];
            var myLatLong = {lat: 1.3521, lng: 103.8198};
            var bounds = new google.maps.LatLngBounds();
            map = new google.maps.Map(document.getElementById('project-map'), {
                center: myLatLong,
                zoom: 15,
                optimized: false
            });
            if (mapData.length > 0) {
                $.each(mapData, function (key, value) {
                    var infowindow = new google.maps.InfoWindow({
                        content: value.detail
                    });
                    infoWindows.push(infowindow);
                    var marker = new google.maps.Marker({
                        position: {lat: parseFloat(value.lat), lng: parseFloat(value.long)},
                        map: map,
                        title: value.name,
                        icon: iconType
                    });
                    marker.addListener('click', function () {
                        closeAllInfoWindows(infowindow);
                        infowindow.open(map, marker);
                        map.setZoom(17);
                    });
                    markers.push(marker);
                    //extend the bounds to include each marker's position
                    bounds.extend(marker.position);
                });
                //now fit the map to the newly inclusive bounds
                map.fitBounds(bounds);
            } else {
                if (markers.length > 0) {
                    for (var i = 0; i < markers.length; i++) {
                        markers[i].setMap(null);
                    }
                }
            }


            // shows the information window  
            //var infowindow = new google.maps.InfoWindow({
            //    content: "31 Keppel Rd"
            //});
//            google.maps.event.addListener(marker, "click", function () {
//                infowindow.open(map, marker);
//            });
            
            //infowindow.open(map, marker);
        } // init_map()
        function closeAllInfoWindows(infoWindows) {
                for (var i = 0; i < infoWindows.length; i++) {
                    infoWindows[i].close();
                }
            }


        /***
         * Initialize Module
         ***/
        function init($config) {
            //resetEvent();
            initEvent();
            initUI();

        } // init()

        /***
         * Init partners
         ***/
        function initPartners() {
            partnersdetails = ((localDB.WLpartners).filter(function (e) {
                return e["displayPriority"] > 0;
            })).sortBy("displayPriority", false);

            $('#partner-content').html('');

            $.each(partnersdetails, function (i, v) {
                if (v.hasImage == 1) {
                    var img_URL = IMAGES_URL + '/partners/' + v.id + '.png?time=' + $.now();
                } else {
                    var img_URL = "img/partner-logo.png";
                }
                //html contents for the partner page
                var content = '<article><img class="partner-logo" src="' + img_URL + '"> <h4 class="red">' + v.name + '</h4><span>' + v.email + '<span>' + v.websiteURL + '</span></article>';
                //append to the page
                $('#partner-content').append(content);
            });

        } // initPartners()
        /***
         * Removes all events in this module: click, input, focus, pointerdown
         ***/

        return {
            init: init,
            init_map: init_map
        }
    }();
}(window.app = window.app || {}, jQuery));
window.app.Module.ProjectMap.init();



