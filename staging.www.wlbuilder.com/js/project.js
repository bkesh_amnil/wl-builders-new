/***
 * Module - app.Module.Project
 *
 * Provides behaviour for Project.php page
 *
 *
 ***/

/*****************************
 *      Execute on load      *
 *****************************/

$.when(loadScripts(["js/vendor/slick/slick.min.js", "js/vendor/jQuery-slimScroll-1.3.8/jquery.slimscroll.js", "js/inc/project_includes.js"])).done(function () {
    (function (app, $) {
        "use strict";

        var App = window.app = window.app || {};
        App.Module = App.Module = App.Module || {};
        app.Module.Project = function () {
            var config = {};
            
            /***
             * Adds all necessary events: input, focus, pointerdown
             ***/
            function initEvent() {
                $(window.app).off("ready");
                $(window.app).on("ready", function () {
                    $('#main-header').attr('class', 'inner-header');
                   $('#main-header img').css('display', 'block');
                    $(window.app).off("ready");
                });
            } // initEvent()

            /***
             * Initializes UI states
             ***/
            function initUI() {
                if (window.app.ready) { 
                    $('#main-header').attr('class', 'inner-header');
                   $('#main-header img').css('display', 'block');
                }
                
            } // initUI()


            /***
             * Initialize Module
             ***/
            function init($config) {
                config = config || {};
                config = $.extend({}, config, $config);
                initEvent();
                initUI();

            } // init()
            
            return {
                init: init
            };
        }();
    }(window.app = window.app || {}, jQuery));

    // Init Project Module
    window.app.Module.Project.init();
})