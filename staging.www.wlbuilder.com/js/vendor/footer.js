//Scroll Navigation Bar
$(document).ready(function () {

        var actWidth = 0;

        var leftPos = 0;

        var url = document.URL

        var currentUrl = url.substr(url.lastIndexOf('/') + 1);
        currentUrl = currentUrl.split("#")[0];

        $('.main-navigation').find('a').removeClass('active');

        if (currentUrl == 'project') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="project"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="project"]').width();

            leftPos = $('.main-navigation').find('a[data-for="project"]').position().left + 28;

        }else

        if (currentUrl == 'capabilities') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="capabilities"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="capabilities"]').width();

            leftPos = $('.main-navigation').find('a[data-for="capabilities"]').position().left + 28;

        }else

        if (currentUrl == 'profile') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="profile"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="profile"]').width();

            leftPos = $('.main-navigation').find('a[data-for="profile"]').position().left + 28;

        }else

        if (currentUrl == 'contact') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="contact"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="contact"]').width();

            leftPos = $('.main-navigation').find('a[data-for="contact"]').position().left + 28;



        }else

        if (currentUrl == 'project-map') {

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="project"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="project"]').width();

            leftPos = $('.main-navigation').find('a[data-for="project"]').position().left + 28;

        }else

        if (currentUrl == 'home' || currentUrl == '') {

            $('#main-header').removeClass('inner-header');

            $('#main-header img').css('display', 'none');

            $('.main-navigation').find('a[data-for="home"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="home"]').width();

            leftPos = $('.main-navigation').find('a[data-for="home"]').position().left + 28;

            console.log('first', actWidth + '-' + leftPos);

        }else{

            $('#main-header').addClass('inner-header');

            $('#main-header img').css('display', 'block');

            $('.main-navigation').find('a[data-for="home"]').addClass('active');

            actWidth = $('.main-navigation').find('a[data-for="home"]').width();

            leftPos = $('.main-navigation').find('a[data-for="home"]').position().left + 28;

            console.log('first', actWidth + '-' + leftPos);

        

        }

        

        actWidth = $(".main-navigation .active").width();

        leftPos = $(".main-navigation .active").position().left + 28;

        //console.log('second', actWidth + '-' + leftPos);

        $(".nav-scroll").css({

            "width": actWidth,

            "left": leftPos

        });

        $(".main-navigation a").on('click', function () {

            actWidth = $(this).width();

            leftPos = $(this).position().left + 28;

            console.log(leftPos);

        });

        $(".view-project-home").on('click', function () {

            

            actWidth = $('.main-navigation').find('a[data-for="project"]').width();

            leftPos = $('.main-navigation').find('a[data-for="project"]').position().left + 28;

            $(".nav-scroll").css({

                "width": actWidth,

                "left": leftPos

                });

        });

        $(".main-navigation a").mouseenter(function () {

            var changeWidth = $(this).width();

            var changePos = $(this).position().left + 28;

            $(".nav-scroll").css({

                "width": changeWidth,

                "left": changePos

            });

        });

        $(".main-navigation a").mouseleave(function () {

            $(".nav-scroll").css({

                "width": actWidth,

                "left": leftPos

            });

        });



    });

$("#termsOfUse").click(function (e) {
    e.preventDefault();    
    $("#termsOfUse-modal").addClass("active");
    $("body").css("overflow", "hidden");
});

$("#privacyPolicy").click(function (e) {
    e.preventDefault();    
    $("#privacyPolicy-modal").addClass("active");
    $("body").css("overflow", "hidden");
});

$(".footer-modal-close").click(function () {
    $("body").css("overflow", "auto");
    if($("#privacyPolicy-modal").hasClass("active")) {
        $("#privacyPolicy-modal").removeClass("active");
    }
    if($("#termsOfUse-modal").hasClass("active")) {
        $("#termsOfUse-modal").removeClass("active");
    }
});