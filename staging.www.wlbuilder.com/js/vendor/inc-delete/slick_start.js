$(document).ready(function () {
    /***
     * Init project featured images
     ***/
    var checkBoudaryImage = 0;
    initProjectBoundaryImage();
    
    function initProjectBoundaryImage() {
        var projects = ((localDB.WLprojects).filter(function (e) {
            return e["isFeaturedInStaticSection"] > 0 && e["displayPriority"] > 0
        })).sortBy("displayPriority", false);

        $('#boundaries-slider').html('');       
        
        $.each(projects, function (i, v) {
            var projectImages = ((localDB.WLprojectImages).filter(function (e) {
                return e["isStaticSectionFeaturedImage"] == 1 && e["projectID"] == v.id && e["displayPriority"] > 0
            })).sortBy("displayPriority", false);
            
            if (projectImages.length > 0) {
                checkBoudaryImage = 1;
                var items = '';
                items += '<div class="item">';
                items += '<div class="hover-animation">';

                for (var i = 0; i < 2; i++) {
                    items += '<div class="lines">';
                    for (var j = 0; j < 7; j++) {
                        items += '<img src="img/line.svg"  alt="svg hover lines">';
                    }
                    items += '</div>';
                }

                items += '</div>';
                items += '<img src="img/projects/images/' + projectImages[0].imgUrl + '?time=' + $.now() + '" alt="' + projectImages[0].name + '">';
                items += '<h2>' + projectImages[0].name + '</h2>';
                items += '</div>';

                $('#boundaries-slider').append(items);

            }

        });

    } // initProjectBoundaryImage()
    
    if (checkBoudaryImage > 0) {

        $('#boundaries-slider').slick({
            dots: true,
            infinite: true,
            speed: 300,
            slidesToShow: 1,
            fade: true,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false
                    }
                }
                // You can unslick at a given breakpoint now by adding:
                // settings: "unslick"
                // instead of a settings object
            ]
        });

        $('#boundaries-slider').on('swipe', function () {
            var leftPosPage;
            var lleftPosPage;
            var totalLeft;
            leftPosPage = $(".slick-dots .slick-active").position().left;
            lleftPosPage = $("#boundaries-slider-wrap").position().left;
            totalLeft = leftPosPage + lleftPosPage + 15;
            $(".pagination-scroll").css('left', totalLeft);
        });

        var leftPosPage;
        var lleftPosPage;
        var totalLeft;
        leftPosPage = $(".slick-dots .slick-active").position().left;
        lleftPosPage = $("#boundaries-slider-wrap").position().left;
        totalLeft = leftPosPage + lleftPosPage + 15;
        $(".pagination-scroll").css('left', totalLeft);
        function slidePagination() {
            var leftPosPage;
            var lleftPosPage;
            var totalLeft;
            leftPosPage = $(".slick-dots .slick-active").position().left;
            lleftPosPage = $("#boundaries-slider-wrap").position().left;
            totalLeft = leftPosPage + lleftPosPage + 15;
            $(".pagination-scroll").css('left', totalLeft);
        }
        $(".slick-dots button").click(function () {
            var changePosPage = $(this).position().left + lleftPosPage + 15;
            $(".pagination-scroll").css('left', changePosPage);
        });
        $(".slick-prev").click(function () {
            slidePagination();
        });
        $(".slick-next").click(function () {
            slidePagination();
        });
    }
});
    